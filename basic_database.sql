-- MySQL dump 10.16  Distrib 10.1.36-MariaDB, for Win32 (AMD64)
--
-- Host: localhost    Database: xe_install_db_inkxe_10
-- ------------------------------------------------------
-- Server version	10.1.36-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `admin_users`
--

DROP TABLE IF EXISTS `admin_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `admin_users` (
  `xe_id` int(11) NOT NULL AUTO_INCREMENT,
  `store_id` varchar(50) DEFAULT NULL,
  `name` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `avatar` varchar(100) DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`xe_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin_users`
--

LOCK TABLES `admin_users` WRITE;
/*!40000 ALTER TABLE `admin_users` DISABLE KEYS */;
/*!40000 ALTER TABLE `admin_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `app_currency`
--

DROP TABLE IF EXISTS `app_currency`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `app_currency` (
  `xe_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(10) NOT NULL,
  `symbol` varchar(2) NOT NULL,
  `code` varchar(5) NOT NULL,
  `is_default` enum('1','0') NOT NULL DEFAULT '0',
  PRIMARY KEY (`xe_id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `app_currency`
--

LOCK TABLES `app_currency` WRITE;
/*!40000 ALTER TABLE `app_currency` DISABLE KEYS */;
INSERT INTO `app_currency` VALUES (1,'Dollar','$','USD','1'),(2,'Rupees','₹','INR','0'),(3,'China Yuan','¥','CNY','0'),(4,'Australia ','$','AUD','0'),(5,'Euro','€','EURO','0');
/*!40000 ALTER TABLE `app_currency` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `app_units`
--

DROP TABLE IF EXISTS `app_units`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `app_units` (
  `xe_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(10) NOT NULL,
  `is_default` enum('0','1') NOT NULL,
  PRIMARY KEY (`xe_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `app_units`
--

LOCK TABLES `app_units` WRITE;
/*!40000 ALTER TABLE `app_units` DISABLE KEYS */;
INSERT INTO `app_units` VALUES (1,'Inch','1'),(2,'Ft','0'),(3,'Cm','0'),(4,'mm','0');
/*!40000 ALTER TABLE `app_units` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `asset_types`
--

DROP TABLE IF EXISTS `asset_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `asset_types` (
  `xe_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `slug` varchar(100) NOT NULL,
  PRIMARY KEY (`xe_id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `asset_types`
--

LOCK TABLES `asset_types` WRITE;
/*!40000 ALTER TABLE `asset_types` DISABLE KEYS */;
INSERT INTO `asset_types` VALUES (1,'Backgrounds','background-patterns'),(2,'Cliparts','cliparts'),(3,'Color Palettes','color-palettes'),(4,'Color Swatches','color-swatch'),(5,'Distresses','distresses'),(6,'Fonts','fonts'),(7,'Graphic Fonts','graphic-fonts'),(8,'Masks','masks'),(9,'Shapes','shapes'),(10,'Word Clouds','word-clouds'),(11,'Templates','templates'),(12,'Products','products');
/*!40000 ALTER TABLE `asset_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `attribute_price_rules`
--

DROP TABLE IF EXISTS `attribute_price_rules`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `attribute_price_rules` (
  `product_id` int(11) DEFAULT NULL,
  `attribute_id` int(11) DEFAULT NULL,
  `attribute_term_id` int(11) DEFAULT NULL,
  `print_profile_id` int(11) DEFAULT NULL,
  `price` decimal(6,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `attribute_price_rules`
--

LOCK TABLES `attribute_price_rules` WRITE;
/*!40000 ALTER TABLE `attribute_price_rules` DISABLE KEYS */;
/*!40000 ALTER TABLE `attribute_price_rules` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `background_category_rel`
--

DROP TABLE IF EXISTS `background_category_rel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `background_category_rel` (
  `background_id` bigint(20) DEFAULT NULL,
  `category_id` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `background_category_rel`
--

LOCK TABLES `background_category_rel` WRITE;
/*!40000 ALTER TABLE `background_category_rel` DISABLE KEYS */;
/*!40000 ALTER TABLE `background_category_rel` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `background_tag_rel`
--

DROP TABLE IF EXISTS `background_tag_rel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `background_tag_rel` (
  `background_id` bigint(20) DEFAULT NULL,
  `tag_id` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `background_tag_rel`
--

LOCK TABLES `background_tag_rel` WRITE;
/*!40000 ALTER TABLE `background_tag_rel` DISABLE KEYS */;
/*!40000 ALTER TABLE `background_tag_rel` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `background_tags`
--

DROP TABLE IF EXISTS `background_tags`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `background_tags` (
  `xe_id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`xe_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `background_tags`
--

LOCK TABLES `background_tags` WRITE;
/*!40000 ALTER TABLE `background_tags` DISABLE KEYS */;
/*!40000 ALTER TABLE `background_tags` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `backgrounds`
--

DROP TABLE IF EXISTS `backgrounds`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `backgrounds` (
  `xe_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `store_id` varchar(50) DEFAULT NULL,
  `value` varchar(50) DEFAULT NULL,
  `price` float(10,2) DEFAULT NULL,
  `type` tinyint(2) DEFAULT NULL COMMENT '1: Pattern, 0: Color',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`xe_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `backgrounds`
--

LOCK TABLES `backgrounds` WRITE;
/*!40000 ALTER TABLE `backgrounds` DISABLE KEYS */;
/*!40000 ALTER TABLE `backgrounds` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `capture_images`
--

DROP TABLE IF EXISTS `capture_images`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `capture_images` (
  `xe_id` int(11) NOT NULL AUTO_INCREMENT,
  `ref_id` int(11) NOT NULL,
  `side_index` int(11) NOT NULL,
  `file_name` varchar(100) NOT NULL,
  PRIMARY KEY (`xe_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `capture_images`
--

LOCK TABLES `capture_images` WRITE;
/*!40000 ALTER TABLE `capture_images` DISABLE KEYS */;
/*!40000 ALTER TABLE `capture_images` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `categories`
--

DROP TABLE IF EXISTS `categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categories` (
  `xe_id` int(11) NOT NULL AUTO_INCREMENT,
  `store_id` varchar(50) DEFAULT NULL,
  `asset_type_id` int(11) NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `sort_order` smallint(4) DEFAULT '0',
  `is_disable` tinyint(1) NOT NULL DEFAULT '0',
  `is_default` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`xe_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categories`
--

LOCK TABLES `categories` WRITE;
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;
INSERT INTO `categories` VALUES (1,'1',3,0,'CMYK',1,0,0,'2020-01-28 10:19:54',NULL),(2,'1',3,0,'RGB',2,0,0,'2020-01-28 10:19:54',NULL),(3,'1',3,0,'Pantone',3,0,0,'2020-01-28 10:19:54',NULL),(4,'1',3,0,'Pattern',4,0,0,'2020-01-28 10:19:54',NULL),(5,'1',3,0,'Embroidery Thread',5,0,0,'2020-01-28 10:19:54',NULL);
/*!40000 ALTER TABLE `categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `clipart_category_rel`
--

DROP TABLE IF EXISTS `clipart_category_rel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `clipart_category_rel` (
  `clipart_id` bigint(20) DEFAULT NULL,
  `category_id` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `clipart_category_rel`
--

LOCK TABLES `clipart_category_rel` WRITE;
/*!40000 ALTER TABLE `clipart_category_rel` DISABLE KEYS */;
/*!40000 ALTER TABLE `clipart_category_rel` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `clipart_tag_rel`
--

DROP TABLE IF EXISTS `clipart_tag_rel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `clipart_tag_rel` (
  `clipart_id` bigint(20) DEFAULT NULL,
  `tag_id` int(10) DEFAULT NULL,
  KEY `FK_clipart_tags_relations_cliparts` (`clipart_id`),
  KEY `FK_clipart_tags_relations_clipart_tags` (`tag_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `clipart_tag_rel`
--

LOCK TABLES `clipart_tag_rel` WRITE;
/*!40000 ALTER TABLE `clipart_tag_rel` DISABLE KEYS */;
/*!40000 ALTER TABLE `clipart_tag_rel` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `clipart_tags`
--

DROP TABLE IF EXISTS `clipart_tags`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `clipart_tags` (
  `xe_id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`xe_id`),
  UNIQUE KEY `index_cliparts_tag_name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `clipart_tags`
--

LOCK TABLES `clipart_tags` WRITE;
/*!40000 ALTER TABLE `clipart_tags` DISABLE KEYS */;
/*!40000 ALTER TABLE `clipart_tags` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cliparts`
--

DROP TABLE IF EXISTS `cliparts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cliparts` (
  `xe_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `price` decimal(10,2) DEFAULT NULL,
  `width` int(5) DEFAULT NULL,
  `height` int(5) DEFAULT NULL,
  `file_name` varchar(30) DEFAULT NULL,
  `color_used` smallint(10) DEFAULT NULL,
  `is_scaling` tinyint(1) DEFAULT '0',
  `store_id` varchar(50) NOT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`xe_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cliparts`
--

LOCK TABLES `cliparts` WRITE;
/*!40000 ALTER TABLE `cliparts` DISABLE KEYS */;
/*!40000 ALTER TABLE `cliparts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `color_palettes`
--

DROP TABLE IF EXISTS `color_palettes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `color_palettes` (
  `xe_id` int(11) NOT NULL AUTO_INCREMENT,
  `store_id` varchar(50) DEFAULT NULL,
  `category_id` int(11) NOT NULL,
  `subcategory_id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `price` float(10,2) NOT NULL,
  `value` varchar(100) DEFAULT NULL,
  `hex_value` varchar(30) DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`xe_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `color_palettes`
--

LOCK TABLES `color_palettes` WRITE;
/*!40000 ALTER TABLE `color_palettes` DISABLE KEYS */;
/*!40000 ALTER TABLE `color_palettes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `color_swatches`
--

DROP TABLE IF EXISTS `color_swatches`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `color_swatches` (
  `xe_id` int(11) NOT NULL AUTO_INCREMENT,
  `attribute_id` bigint(20) NOT NULL,
  `hex_code` varchar(10) DEFAULT NULL,
  `file_name` varchar(50) DEFAULT NULL,
  `color_type` int(10) DEFAULT NULL,
  PRIMARY KEY (`xe_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `color_swatches`
--

LOCK TABLES `color_swatches` WRITE;
/*!40000 ALTER TABLE `color_swatches` DISABLE KEYS */;
/*!40000 ALTER TABLE `color_swatches` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `color_types`
--

DROP TABLE IF EXISTS `color_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `color_types` (
  `xe_id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`xe_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `color_types`
--

LOCK TABLES `color_types` WRITE;
/*!40000 ALTER TABLE `color_types` DISABLE KEYS */;
INSERT INTO `color_types` VALUES (1,'White'),(2,'Dark'),(3,'Light');
/*!40000 ALTER TABLE `color_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `design_states`
--

DROP TABLE IF EXISTS `design_states`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `design_states` (
  `xe_id` int(11) NOT NULL AUTO_INCREMENT,
  `store_id` varchar(50) DEFAULT NULL,
  `product_setting_id` int(11) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `product_variant_id` int(11) DEFAULT NULL,
  `type` enum('cart','pre_deco','social_share','user_slot','template','quote','bgpattern') NOT NULL DEFAULT 'cart',
  `custom_price` float DEFAULT NULL,
  `date_created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`xe_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `design_states`
--

LOCK TABLES `design_states` WRITE;
/*!40000 ALTER TABLE `design_states` DISABLE KEYS */;
/*!40000 ALTER TABLE `design_states` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `distress_category_rel`
--

DROP TABLE IF EXISTS `distress_category_rel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `distress_category_rel` (
  `distress_id` bigint(20) DEFAULT NULL,
  `category_id` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `distress_category_rel`
--

LOCK TABLES `distress_category_rel` WRITE;
/*!40000 ALTER TABLE `distress_category_rel` DISABLE KEYS */;
/*!40000 ALTER TABLE `distress_category_rel` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `distress_tag_rel`
--

DROP TABLE IF EXISTS `distress_tag_rel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `distress_tag_rel` (
  `distress_id` bigint(20) DEFAULT NULL,
  `tag_id` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `distress_tag_rel`
--

LOCK TABLES `distress_tag_rel` WRITE;
/*!40000 ALTER TABLE `distress_tag_rel` DISABLE KEYS */;
/*!40000 ALTER TABLE `distress_tag_rel` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `distress_tags`
--

DROP TABLE IF EXISTS `distress_tags`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `distress_tags` (
  `xe_id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`xe_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `distress_tags`
--

LOCK TABLES `distress_tags` WRITE;
/*!40000 ALTER TABLE `distress_tags` DISABLE KEYS */;
/*!40000 ALTER TABLE `distress_tags` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `distresses`
--

DROP TABLE IF EXISTS `distresses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `distresses` (
  `xe_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `store_id` varchar(50) DEFAULT NULL,
  `file_name` varchar(50) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`xe_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `distresses`
--

LOCK TABLES `distresses` WRITE;
/*!40000 ALTER TABLE `distresses` DISABLE KEYS */;
/*!40000 ALTER TABLE `distresses` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `engraved_surfaces`
--

DROP TABLE IF EXISTS `engraved_surfaces`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `engraved_surfaces` (
  `xe_id` int(11) NOT NULL AUTO_INCREMENT,
  `surface_name` varchar(100) NOT NULL,
  `engraved_type` enum('image','color') DEFAULT NULL,
  `engrave_type_value` varchar(100) DEFAULT NULL COMMENT 'color code or image url',
  `engrave_preview_type` enum('image','color') DEFAULT NULL,
  `engrave_preview_type_value` varchar(50) DEFAULT NULL,
  `shadow_direction` varchar(30) DEFAULT NULL,
  `shadow_size` varchar(20) DEFAULT NULL,
  `shadow_opacity` varchar(20) DEFAULT NULL,
  `shadow_strength` varchar(30) DEFAULT NULL,
  `shadow_blur` varchar(50) DEFAULT NULL,
  `is_user_defined` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`xe_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `engraved_surfaces`
--

LOCK TABLES `engraved_surfaces` WRITE;
/*!40000 ALTER TABLE `engraved_surfaces` DISABLE KEYS */;
INSERT INTO `engraved_surfaces` VALUES (1,'Surface 1',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0),(2,'Surface 2',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0);
/*!40000 ALTER TABLE `engraved_surfaces` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `feature_types`
--

DROP TABLE IF EXISTS `feature_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `feature_types` (
  `xe_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(150) NOT NULL,
  PRIMARY KEY (`xe_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `feature_types`
--

LOCK TABLES `feature_types` WRITE;
/*!40000 ALTER TABLE `feature_types` DISABLE KEYS */;
INSERT INTO `feature_types` VALUES (1,'Graphics'),(2,'Images'),(3,'Text');
/*!40000 ALTER TABLE `feature_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `features`
--

DROP TABLE IF EXISTS `features`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `features` (
  `xe_id` int(11) NOT NULL AUTO_INCREMENT,
  `feature_type_id` int(11) NOT NULL,
  `name` varchar(150) NOT NULL,
  `slug` varchar(100) NOT NULL,
  PRIMARY KEY (`xe_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `features`
--

LOCK TABLES `features` WRITE;
/*!40000 ALTER TABLE `features` DISABLE KEYS */;
INSERT INTO `features` VALUES (1,1,'My Gallery','my-gallery'),(2,1,'Design','design'),(3,1,'Template','template'),(4,2,'QR Code','qr-code'),(5,2,'Background','backgroud'),(6,2,'Shape','shape'),(7,3,'Plain Text','plain-text'),(8,3,'Curve Text','curve-text');
/*!40000 ALTER TABLE `features` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `font_category_rel`
--

DROP TABLE IF EXISTS `font_category_rel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `font_category_rel` (
  `font_id` bigint(20) DEFAULT NULL,
  `category_id` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `font_category_rel`
--

LOCK TABLES `font_category_rel` WRITE;
/*!40000 ALTER TABLE `font_category_rel` DISABLE KEYS */;
/*!40000 ALTER TABLE `font_category_rel` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `font_tag_rel`
--

DROP TABLE IF EXISTS `font_tag_rel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `font_tag_rel` (
  `font_id` bigint(20) DEFAULT NULL,
  `tag_id` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `font_tag_rel`
--

LOCK TABLES `font_tag_rel` WRITE;
/*!40000 ALTER TABLE `font_tag_rel` DISABLE KEYS */;
/*!40000 ALTER TABLE `font_tag_rel` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `font_tags`
--

DROP TABLE IF EXISTS `font_tags`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `font_tags` (
  `xe_id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`xe_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `font_tags`
--

LOCK TABLES `font_tags` WRITE;
/*!40000 ALTER TABLE `font_tags` DISABLE KEYS */;
/*!40000 ALTER TABLE `font_tags` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fonts`
--

DROP TABLE IF EXISTS `fonts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fonts` (
  `xe_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `store_id` varchar(50) DEFAULT NULL,
  `price` decimal(10,2) DEFAULT NULL,
  `font_family` varchar(100) DEFAULT NULL,
  `file_name` varchar(50) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`xe_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fonts`
--

LOCK TABLES `fonts` WRITE;
/*!40000 ALTER TABLE `fonts` DISABLE KEYS */;
/*!40000 ALTER TABLE `fonts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `languages`
--

DROP TABLE IF EXISTS `languages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `languages` (
  `xe_id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `store_id` varchar(50) DEFAULT NULL,
  `file_name` varchar(50) DEFAULT NULL,
  `type` varchar(20) DEFAULT NULL,
  `is_enable` tinyint(1) DEFAULT '0',
  `is_default` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`xe_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `languages`
--

LOCK TABLES `languages` WRITE;
/*!40000 ALTER TABLE `languages` DISABLE KEYS */;
INSERT INTO `languages` VALUES (1,'English','1','lang_english.json','admin',1,1),(2,'English','1','lang_english.json','tool',1,1),(3,'Dutch','1','lang_duch.json','admin',0,0),(4,'Dutch','1','lang_duch.json','tool',0,0);
/*!40000 ALTER TABLE `languages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mask_tag_rel`
--

DROP TABLE IF EXISTS `mask_tag_rel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mask_tag_rel` (
  `mask_id` bigint(20) DEFAULT NULL,
  `tag_id` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mask_tag_rel`
--

LOCK TABLES `mask_tag_rel` WRITE;
/*!40000 ALTER TABLE `mask_tag_rel` DISABLE KEYS */;
/*!40000 ALTER TABLE `mask_tag_rel` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mask_tags`
--

DROP TABLE IF EXISTS `mask_tags`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mask_tags` (
  `xe_id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`xe_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mask_tags`
--

LOCK TABLES `mask_tags` WRITE;
/*!40000 ALTER TABLE `mask_tags` DISABLE KEYS */;
/*!40000 ALTER TABLE `mask_tags` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `masks`
--

DROP TABLE IF EXISTS `masks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `masks` (
  `xe_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `store_id` varchar(50) DEFAULT NULL,
  `mask_name` varchar(50) DEFAULT NULL,
  `file_name` varchar(50) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`xe_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `masks`
--

LOCK TABLES `masks` WRITE;
/*!40000 ALTER TABLE `masks` DISABLE KEYS */;
/*!40000 ALTER TABLE `masks` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `order_log_files`
--

DROP TABLE IF EXISTS `order_log_files`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `order_log_files` (
  `xe_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `order_log_id` bigint(20) DEFAULT NULL,
  `file_name` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`xe_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `order_log_files`
--

LOCK TABLES `order_log_files` WRITE;
/*!40000 ALTER TABLE `order_log_files` DISABLE KEYS */;
/*!40000 ALTER TABLE `order_log_files` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `order_logs`
--

DROP TABLE IF EXISTS `order_logs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `order_logs` (
  `xe_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `order_id` varchar(30) DEFAULT NULL,
  `agent_type` varchar(25) DEFAULT NULL,
  `agent_id` int(11) DEFAULT NULL,
  `store_id` varchar(50) DEFAULT NULL,
  `message` text,
  `log_type` varchar(25) DEFAULT NULL,
  `status` varchar(25) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`xe_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `order_logs`
--

LOCK TABLES `order_logs` WRITE;
/*!40000 ALTER TABLE `order_logs` DISABLE KEYS */;
/*!40000 ALTER TABLE `order_logs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `price_advanced_price_settings`
--

DROP TABLE IF EXISTS `price_advanced_price_settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `price_advanced_price_settings` (
  `xe_id` int(11) NOT NULL AUTO_INCREMENT,
  `advanced_price_type` enum('per_color','per_print_area','per_design_area') DEFAULT NULL,
  `no_of_colors_allowed` int(11) DEFAULT NULL,
  `is_full_color` tinyint(1) DEFAULT '0',
  `area_calculation_type` enum('design_area','bound_area') DEFAULT NULL,
  `min_price` float DEFAULT NULL,
  PRIMARY KEY (`xe_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `price_advanced_price_settings`
--

LOCK TABLES `price_advanced_price_settings` WRITE;
/*!40000 ALTER TABLE `price_advanced_price_settings` DISABLE KEYS */;
/*!40000 ALTER TABLE `price_advanced_price_settings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `price_default_settings`
--

DROP TABLE IF EXISTS `price_default_settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `price_default_settings` (
  `xe_id` int(11) NOT NULL AUTO_INCREMENT,
  `price_module_setting_id` int(11) NOT NULL,
  `price_key` varchar(30) DEFAULT NULL,
  `price_value` float DEFAULT NULL,
  PRIMARY KEY (`xe_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `price_default_settings`
--

LOCK TABLES `price_default_settings` WRITE;
/*!40000 ALTER TABLE `price_default_settings` DISABLE KEYS */;
/*!40000 ALTER TABLE `price_default_settings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `price_module_settings`
--

DROP TABLE IF EXISTS `price_module_settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `price_module_settings` (
  `xe_id` int(11) NOT NULL AUTO_INCREMENT,
  `print_profile_pricing_id` int(11) NOT NULL,
  `price_module_id` int(11) NOT NULL,
  `module_status` tinyint(1) NOT NULL,
  `is_default_price` tinyint(1) DEFAULT '0',
  `is_quote_enabled` tinyint(1) DEFAULT '0',
  `is_advance_price` tinyint(1) DEFAULT '0',
  `advance_price_settings_id` int(11) DEFAULT NULL,
  `is_quantity_tier` tinyint(1) DEFAULT '0',
  `quantity_tier_type` varchar(100) DEFAULT NULL COMMENT '''price'',''volume''',
  PRIMARY KEY (`xe_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `price_module_settings`
--

LOCK TABLES `price_module_settings` WRITE;
/*!40000 ALTER TABLE `price_module_settings` DISABLE KEYS */;
/*!40000 ALTER TABLE `price_module_settings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `price_modules`
--

DROP TABLE IF EXISTS `price_modules`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `price_modules` (
  `xe_id` int(11) NOT NULL AUTO_INCREMENT,
  `slug` varchar(100) NOT NULL,
  `sort_order_index` int(11) NOT NULL,
  PRIMARY KEY (`xe_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `price_modules`
--

LOCK TABLES `price_modules` WRITE;
/*!40000 ALTER TABLE `price_modules` DISABLE KEYS */;
/*!40000 ALTER TABLE `price_modules` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `price_tier_quantity_ranges`
--

DROP TABLE IF EXISTS `price_tier_quantity_ranges`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `price_tier_quantity_ranges` (
  `xe_id` int(11) NOT NULL AUTO_INCREMENT,
  `price_module_setting_id` int(11) NOT NULL,
  `quantity_from` int(11) DEFAULT NULL,
  `quantity_to` int(11) DEFAULT NULL,
  PRIMARY KEY (`xe_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `price_tier_quantity_ranges`
--

LOCK TABLES `price_tier_quantity_ranges` WRITE;
/*!40000 ALTER TABLE `price_tier_quantity_ranges` DISABLE KEYS */;
/*!40000 ALTER TABLE `price_tier_quantity_ranges` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `price_tier_values`
--

DROP TABLE IF EXISTS `price_tier_values`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `price_tier_values` (
  `xe_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `attribute_type` varchar(100) DEFAULT NULL,
  `price_module_setting_id` int(11) NOT NULL,
  `print_area_index` int(11) DEFAULT NULL,
  `color_index` varchar(100) DEFAULT NULL,
  `print_area_id` int(11) DEFAULT NULL COMMENT 'print area sequence index',
  `range_from` int(11) DEFAULT NULL COMMENT 'square inch value / no fo letters',
  `range_to` int(11) DEFAULT NULL COMMENT 'square inch value/ no fo letters',
  `key_name` varchar(100) DEFAULT NULL COMMENT 'For name&number, vdp, sleeve price key id',
  PRIMARY KEY (`xe_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `price_tier_values`
--

LOCK TABLES `price_tier_values` WRITE;
/*!40000 ALTER TABLE `price_tier_values` DISABLE KEYS */;
/*!40000 ALTER TABLE `price_tier_values` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `price_tier_whitebases`
--

DROP TABLE IF EXISTS `price_tier_whitebases`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `price_tier_whitebases` (
  `price_tier_value_id` bigint(20) DEFAULT NULL,
  `tier_range_id` int(11) DEFAULT NULL,
  `white_base_type` char(10) DEFAULT NULL,
  `price` decimal(10,2) DEFAULT '0.00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `price_tier_whitebases`
--

LOCK TABLES `price_tier_whitebases` WRITE;
/*!40000 ALTER TABLE `price_tier_whitebases` DISABLE KEYS */;
/*!40000 ALTER TABLE `price_tier_whitebases` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `print_area_types`
--

DROP TABLE IF EXISTS `print_area_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `print_area_types` (
  `xe_id` int(11) NOT NULL AUTO_INCREMENT,
  `store_id` varchar(50) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `file_name` varchar(255) DEFAULT NULL,
  `is_custom` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`xe_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `print_area_types`
--

LOCK TABLES `print_area_types` WRITE;
/*!40000 ALTER TABLE `print_area_types` DISABLE KEYS */;
INSERT INTO `print_area_types` VALUES (1,'1','Rectangle','rectangle.svg',0),(2,'1','Square','square.svg',0),(3,'1','Circle','circle.svg',0),(4,'1','Triangle','tringle.svg',0),(5,'1','Elipse','ellipse.svg',0),(6,'1','Custom',NULL,1);
/*!40000 ALTER TABLE `print_area_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `print_areas`
--

DROP TABLE IF EXISTS `print_areas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `print_areas` (
  `xe_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `store_id` varchar(50) DEFAULT NULL,
  `name` varchar(50) NOT NULL,
  `print_area_type_id` int(10) NOT NULL,
  `file_name` varchar(150) DEFAULT NULL,
  `width` float(10,2) NOT NULL,
  `height` float(10,2) NOT NULL,
  `is_user_defined` enum('0','1') NOT NULL DEFAULT '0',
  `is_default` enum('0','1') NOT NULL DEFAULT '0',
  `price` float(10,2) DEFAULT NULL,
  PRIMARY KEY (`xe_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `print_areas`
--

LOCK TABLES `print_areas` WRITE;
/*!40000 ALTER TABLE `print_areas` DISABLE KEYS */;
/*!40000 ALTER TABLE `print_areas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `print_profile_allowed_formats`
--

DROP TABLE IF EXISTS `print_profile_allowed_formats`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `print_profile_allowed_formats` (
  `xe_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `type` char(6) NOT NULL DEFAULT 'image',
  `is_disabled` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`xe_id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `print_profile_allowed_formats`
--

LOCK TABLES `print_profile_allowed_formats` WRITE;
/*!40000 ALTER TABLE `print_profile_allowed_formats` DISABLE KEYS */;
INSERT INTO `print_profile_allowed_formats` VALUES (1,'svg','image',0),(2,'jpeg','image',0),(3,'jpg','image',0),(4,'png','image',0),(5,'gif','image',0),(6,'bmp','image',0),(7,'pdf','image',0),(8,'ai','image',0),(9,'psd','image',0),(10,'eps','image',0),(11,'cdr','image',0),(12,'dxf','image',0),(13,'tif','image',0),(14,'pdf','order',0),(15,'png','order',0),(16,'svg','order',0);
/*!40000 ALTER TABLE `print_profile_allowed_formats` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `print_profile_assets_category_rel`
--

DROP TABLE IF EXISTS `print_profile_assets_category_rel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `print_profile_assets_category_rel` (
  `print_profile_id` int(11) NOT NULL,
  `asset_type_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `print_profile_assets_category_rel`
--

LOCK TABLES `print_profile_assets_category_rel` WRITE;
/*!40000 ALTER TABLE `print_profile_assets_category_rel` DISABLE KEYS */;
/*!40000 ALTER TABLE `print_profile_assets_category_rel` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `print_profile_decoration_setting_rel`
--

DROP TABLE IF EXISTS `print_profile_decoration_setting_rel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `print_profile_decoration_setting_rel` (
  `print_profile_id` int(11) NOT NULL,
  `decoration_setting_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `print_profile_decoration_setting_rel`
--

LOCK TABLES `print_profile_decoration_setting_rel` WRITE;
/*!40000 ALTER TABLE `print_profile_decoration_setting_rel` DISABLE KEYS */;
/*!40000 ALTER TABLE `print_profile_decoration_setting_rel` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `print_profile_engrave_settings`
--

DROP TABLE IF EXISTS `print_profile_engrave_settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `print_profile_engrave_settings` (
  `print_profile_id` int(11) NOT NULL,
  `engraved_surface_id` int(11) DEFAULT '0',
  `is_engraved_surface` tinyint(1) DEFAULT '0',
  `is_auto_convert` tinyint(1) DEFAULT '0',
  `auto_convert_type` enum('BW','Grayscale') DEFAULT NULL,
  `is_engrave_image` tinyint(1) DEFAULT '0',
  `is_engrave_preview_image` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `print_profile_engrave_settings`
--

LOCK TABLES `print_profile_engrave_settings` WRITE;
/*!40000 ALTER TABLE `print_profile_engrave_settings` DISABLE KEYS */;
/*!40000 ALTER TABLE `print_profile_engrave_settings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `print_profile_feature_rel`
--

DROP TABLE IF EXISTS `print_profile_feature_rel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `print_profile_feature_rel` (
  `print_profile_id` int(11) NOT NULL,
  `feature_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `print_profile_feature_rel`
--

LOCK TABLES `print_profile_feature_rel` WRITE;
/*!40000 ALTER TABLE `print_profile_feature_rel` DISABLE KEYS */;
/*!40000 ALTER TABLE `print_profile_feature_rel` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `print_profile_product_setting_rel`
--

DROP TABLE IF EXISTS `print_profile_product_setting_rel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `print_profile_product_setting_rel` (
  `print_profile_id` int(11) NOT NULL,
  `product_setting_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `print_profile_product_setting_rel`
--

LOCK TABLES `print_profile_product_setting_rel` WRITE;
/*!40000 ALTER TABLE `print_profile_product_setting_rel` DISABLE KEYS */;
/*!40000 ALTER TABLE `print_profile_product_setting_rel` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `print_profiles`
--

DROP TABLE IF EXISTS `print_profiles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `print_profiles` (
  `xe_id` int(11) NOT NULL AUTO_INCREMENT,
  `store_id` varchar(50) NOT NULL,
  `name` varchar(255) NOT NULL,
  `file_name` varchar(100) DEFAULT NULL,
  `description` text,
  `status` tinyint(1) DEFAULT '0',
  `is_vdp_enabled` tinyint(1) DEFAULT '0',
  `vdp_data` text,
  `is_laser_engrave_enabled` tinyint(1) DEFAULT '0',
  `image_settings` text,
  `color_settings` text,
  `order_settings` text,
  `is_disabled` tinyint(1) DEFAULT '0',
  `is_draft` tinyint(1) DEFAULT '1',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`xe_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `print_profiles`
--

LOCK TABLES `print_profiles` WRITE;
/*!40000 ALTER TABLE `print_profiles` DISABLE KEYS */;
/*!40000 ALTER TABLE `print_profiles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product_decoration_settings`
--

DROP TABLE IF EXISTS `product_decoration_settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product_decoration_settings` (
  `xe_id` int(11) NOT NULL AUTO_INCREMENT,
  `product_setting_id` int(11) NOT NULL,
  `product_side_id` int(11) DEFAULT NULL,
  `name` varchar(30) DEFAULT NULL,
  `dimension` varchar(255) DEFAULT NULL,
  `print_area_id` bigint(20) NOT NULL,
  `sub_print_area_type` enum('normal_size','custom_size','associate_size_variant') NOT NULL DEFAULT 'normal_size',
  `pre_defined_dimensions` varchar(255) DEFAULT NULL,
  `user_defined_dimensions` varchar(255) DEFAULT NULL,
  `custom_min_height` float(5,2) DEFAULT NULL,
  `custom_max_height` float(5,2) DEFAULT NULL,
  `custom_min_width` float(5,2) DEFAULT NULL,
  `custom_max_width` float(5,2) DEFAULT NULL,
  `custom_bound_price` float(5,2) NOT NULL DEFAULT '0.00' COMMENT 'Price per square unit',
  `is_border_enable` tinyint(1) NOT NULL DEFAULT '0',
  `is_sides_allow` tinyint(1) NOT NULL DEFAULT '0',
  `no_of_sides` int(11) NOT NULL DEFAULT '0',
  `is_dimension_enable` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`xe_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product_decoration_settings`
--

LOCK TABLES `product_decoration_settings` WRITE;
/*!40000 ALTER TABLE `product_decoration_settings` DISABLE KEYS */;
/*!40000 ALTER TABLE `product_decoration_settings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product_image_settings_rel`
--

DROP TABLE IF EXISTS `product_image_settings_rel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product_image_settings_rel` (
  `product_setting_id` int(11) NOT NULL,
  `product_image_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product_image_settings_rel`
--

LOCK TABLES `product_image_settings_rel` WRITE;
/*!40000 ALTER TABLE `product_image_settings_rel` DISABLE KEYS */;
/*!40000 ALTER TABLE `product_image_settings_rel` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product_image_sides`
--

DROP TABLE IF EXISTS `product_image_sides`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product_image_sides` (
  `xe_id` int(11) NOT NULL AUTO_INCREMENT,
  `product_image_id` int(11) NOT NULL,
  `side_name` varchar(200) DEFAULT NULL,
  `sort_order` int(11) NOT NULL,
  `file_name` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`xe_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product_image_sides`
--

LOCK TABLES `product_image_sides` WRITE;
/*!40000 ALTER TABLE `product_image_sides` DISABLE KEYS */;
/*!40000 ALTER TABLE `product_image_sides` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product_images`
--

DROP TABLE IF EXISTS `product_images`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product_images` (
  `xe_id` int(11) NOT NULL AUTO_INCREMENT,
  `store_id` varchar(50) DEFAULT NULL,
  `name` varchar(200) NOT NULL,
  `is_disable` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`xe_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product_images`
--

LOCK TABLES `product_images` WRITE;
/*!40000 ALTER TABLE `product_images` DISABLE KEYS */;
/*!40000 ALTER TABLE `product_images` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product_settings`
--

DROP TABLE IF EXISTS `product_settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product_settings` (
  `xe_id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` varchar(30) NOT NULL,
  `is_variable_decoration` tinyint(1) DEFAULT '0',
  `is_ruler` tinyint(1) DEFAULT '0',
  `is_crop_mark` tinyint(1) NOT NULL DEFAULT '0',
  `is_safe_zone` tinyint(1) NOT NULL DEFAULT '0',
  `crop_value` decimal(5,2) NOT NULL,
  `safe_value` decimal(5,2) NOT NULL,
  `is_3d_preview` tinyint(1) NOT NULL DEFAULT '0',
  `3d_object_file` varchar(120) DEFAULT NULL,
  `3d_object` text,
  `scale_unit_id` int(11) NOT NULL,
  PRIMARY KEY (`xe_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product_settings`
--

LOCK TABLES `product_settings` WRITE;
/*!40000 ALTER TABLE `product_settings` DISABLE KEYS */;
/*!40000 ALTER TABLE `product_settings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product_sides`
--

DROP TABLE IF EXISTS `product_sides`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product_sides` (
  `xe_id` int(11) NOT NULL AUTO_INCREMENT,
  `product_setting_id` int(11) NOT NULL,
  `side_name` varchar(50) NOT NULL,
  `side_index` int(11) DEFAULT NULL,
  `product_image_dimension` text NOT NULL,
  `is_visible` tinyint(1) NOT NULL DEFAULT '1',
  `product_image_side_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`xe_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product_sides`
--

LOCK TABLES `product_sides` WRITE;
/*!40000 ALTER TABLE `product_sides` DISABLE KEYS */;
/*!40000 ALTER TABLE `product_sides` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product_size_variant_decoration_settings`
--

DROP TABLE IF EXISTS `product_size_variant_decoration_settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product_size_variant_decoration_settings` (
  `xe_id` int(11) NOT NULL AUTO_INCREMENT,
  `decoration_setting_id` int(11) NOT NULL,
  `print_area_id` bigint(20) DEFAULT NULL,
  `size_variant_id` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`xe_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product_size_variant_decoration_settings`
--

LOCK TABLES `product_size_variant_decoration_settings` WRITE;
/*!40000 ALTER TABLE `product_size_variant_decoration_settings` DISABLE KEYS */;
/*!40000 ALTER TABLE `product_size_variant_decoration_settings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `schema_version_xe`
--

DROP TABLE IF EXISTS `schema_version_xe`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `schema_version_xe` (
  `version` bigint(20) NOT NULL,
  `name` text,
  `md5` text,
  `run_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `schema_version_xe`
--

LOCK TABLES `schema_version_xe` WRITE;
/*!40000 ALTER TABLE `schema_version_xe` DISABLE KEYS */;
INSERT INTO `schema_version_xe` VALUES (0,NULL,NULL,'2020-01-28 04:49:51'),(1,'backgrounds','a8c605d872d62a068ca6469d34e93602','2020-01-27 23:19:52'),(2,'cliparts','622d77ebde3cb7d353149475bbeebf33','2020-01-27 23:19:54'),(3,'color','499082f5aab58b72b187b2291c2db4c0','2020-01-27 23:19:54'),(4,'common','66bbdc77b07a590cb066325182d2bb1d','2020-01-27 23:19:55'),(5,'decoration_area','e7137a4c030d9bf11e7e11caf3168233','2020-01-27 23:19:55'),(6,'distress','fdf338259ee746233fe4947fcd3c8c13','2020-01-27 23:19:56'),(7,'font','38c74387964de94d5a293890b4077d21','2020-01-27 23:19:57'),(8,'mask','094c388305aec99981d62d29118bf007','2020-01-27 23:19:58'),(9,'order','2918216a86cb92eaf7e0ddf6601fb6fc','2020-01-27 23:19:59'),(10,'print_profile','9845be54c346ff536ef95cae89405bad','2020-01-27 23:20:04'),(11,'product_pricing','25066630064e66cefedc967348a61788','2020-01-27 23:20:04'),(12,'product_setting','2857db49faf995a74f67fd46ec9b7afe','2020-01-27 23:20:07'),(13,'settings','e0b0b95bf800248173e4300f26bffa52','2020-01-27 23:20:09'),(14,'shape','118992b328a62a6c6ab40bf75557ad5c','2020-01-27 23:20:10'),(15,'template','8163279f38b91deaf34fdbf50a4f77eb','2020-01-27 23:20:11'),(16,'users','f6b057a4bacba84e9e6dba451745432a','2020-01-27 23:20:13'),(17,'word_cloud','1e352846896aa85ed3ea75217d0c29f1','2020-01-27 23:20:14');
/*!40000 ALTER TABLE `schema_version_xe` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `settings`
--

DROP TABLE IF EXISTS `settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `settings` (
  `xe_id` int(11) NOT NULL AUTO_INCREMENT,
  `setting_key` varchar(100) DEFAULT NULL,
  `setting_value` text,
  `store_id` varchar(50) DEFAULT NULL,
  `type` tinyint(2) DEFAULT '0',
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`xe_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `settings`
--

LOCK TABLES `settings` WRITE;
/*!40000 ALTER TABLE `settings` DISABLE KEYS */;
/*!40000 ALTER TABLE `settings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `shape_category_rel`
--

DROP TABLE IF EXISTS `shape_category_rel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shape_category_rel` (
  `shape_id` bigint(20) DEFAULT NULL,
  `category_id` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shape_category_rel`
--

LOCK TABLES `shape_category_rel` WRITE;
/*!40000 ALTER TABLE `shape_category_rel` DISABLE KEYS */;
/*!40000 ALTER TABLE `shape_category_rel` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `shape_tag_rel`
--

DROP TABLE IF EXISTS `shape_tag_rel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shape_tag_rel` (
  `shape_id` bigint(20) DEFAULT NULL,
  `tag_id` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shape_tag_rel`
--

LOCK TABLES `shape_tag_rel` WRITE;
/*!40000 ALTER TABLE `shape_tag_rel` DISABLE KEYS */;
/*!40000 ALTER TABLE `shape_tag_rel` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `shape_tags`
--

DROP TABLE IF EXISTS `shape_tags`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shape_tags` (
  `xe_id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`xe_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shape_tags`
--

LOCK TABLES `shape_tags` WRITE;
/*!40000 ALTER TABLE `shape_tags` DISABLE KEYS */;
/*!40000 ALTER TABLE `shape_tags` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `shapes`
--

DROP TABLE IF EXISTS `shapes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shapes` (
  `xe_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `store_id` varchar(50) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `file_name` varchar(50) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`xe_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shapes`
--

LOCK TABLES `shapes` WRITE;
/*!40000 ALTER TABLE `shapes` DISABLE KEYS */;
/*!40000 ALTER TABLE `shapes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stores`
--

DROP TABLE IF EXISTS `stores`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stores` (
  `xe_id` int(11) NOT NULL AUTO_INCREMENT,
  `store_name` varchar(70) DEFAULT NULL,
  `store_url` varchar(70) DEFAULT NULL,
  `created_date` datetime DEFAULT CURRENT_TIMESTAMP,
  `status` enum('1','0') DEFAULT '1',
  `settings` text,
  PRIMARY KEY (`xe_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stores`
--

LOCK TABLES `stores` WRITE;
/*!40000 ALTER TABLE `stores` DISABLE KEYS */;
INSERT INTO `stores` VALUES (1,'Woocommerce Instance 001','http://www.woo.com','2020-01-03 11:13:54','1','{\"app_name\":\"Inkxe API 0.1\",\"app_url\":\"http:\\/\\/localhost\\/api\\/v1\\/\",\"date_format\":\"F j, Y\",\"time_format\":\"g:i a\",\"currency_code\":\"\"}'),(2,'Woocommerce Instance 002','http://www.woo.com','2020-01-03 11:13:54','0','{\"app_name\":\"Inkxe API 0.1\",\"app_url\":\"http:\\/\\/localhost\\/api\\/v1\\/\",\"date_format\":\"F j, Y\",\"time_format\":\"g:i a\",\"currency_code\":\"\"}');
/*!40000 ALTER TABLE `stores` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `template_category_rel`
--

DROP TABLE IF EXISTS `template_category_rel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `template_category_rel` (
  `template_id` int(11) DEFAULT NULL,
  `category_id` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `template_category_rel`
--

LOCK TABLES `template_category_rel` WRITE;
/*!40000 ALTER TABLE `template_category_rel` DISABLE KEYS */;
/*!40000 ALTER TABLE `template_category_rel` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `template_print_profile_rel`
--

DROP TABLE IF EXISTS `template_print_profile_rel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `template_print_profile_rel` (
  `template_id` int(11) NOT NULL,
  `print_profile_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `template_print_profile_rel`
--

LOCK TABLES `template_print_profile_rel` WRITE;
/*!40000 ALTER TABLE `template_print_profile_rel` DISABLE KEYS */;
/*!40000 ALTER TABLE `template_print_profile_rel` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `template_tag_rel`
--

DROP TABLE IF EXISTS `template_tag_rel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `template_tag_rel` (
  `template_id` int(11) NOT NULL,
  `tag_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `template_tag_rel`
--

LOCK TABLES `template_tag_rel` WRITE;
/*!40000 ALTER TABLE `template_tag_rel` DISABLE KEYS */;
/*!40000 ALTER TABLE `template_tag_rel` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `template_tags`
--

DROP TABLE IF EXISTS `template_tags`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `template_tags` (
  `xe_id` int(11) NOT NULL AUTO_INCREMENT,
  `store_id` varchar(50) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`xe_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `template_tags`
--

LOCK TABLES `template_tags` WRITE;
/*!40000 ALTER TABLE `template_tags` DISABLE KEYS */;
/*!40000 ALTER TABLE `template_tags` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `templates`
--

DROP TABLE IF EXISTS `templates`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `templates` (
  `xe_id` int(11) NOT NULL AUTO_INCREMENT,
  `ref_id` int(11) NOT NULL,
  `store_id` varchar(50) DEFAULT NULL,
  `name` varchar(100) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `no_of_colors` int(11) DEFAULT NULL,
  `color_hash_codes` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`xe_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `templates`
--

LOCK TABLES `templates` WRITE;
/*!40000 ALTER TABLE `templates` DISABLE KEYS */;
/*!40000 ALTER TABLE `templates` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_privileges`
--

DROP TABLE IF EXISTS `user_privileges`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_privileges` (
  `xe_id` int(11) NOT NULL AUTO_INCREMENT,
  `store_id` varchar(50) DEFAULT NULL,
  `module_name` varchar(100) NOT NULL,
  `status` enum('0','1') NOT NULL DEFAULT '1',
  PRIMARY KEY (`xe_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_privileges`
--

LOCK TABLES `user_privileges` WRITE;
/*!40000 ALTER TABLE `user_privileges` DISABLE KEYS */;
INSERT INTO `user_privileges` VALUES (1,'1','Products','1'),(2,'1','Assets','1'),(3,'1','Print profile','1'),(4,'1','Settings','1'),(5,'1','Orders','1'),(6,'1','Users','1');
/*!40000 ALTER TABLE `user_privileges` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_privileges_rel`
--

DROP TABLE IF EXISTS `user_privileges_rel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_privileges_rel` (
  `user_id` int(11) NOT NULL,
  `privilege_id` int(11) NOT NULL,
  `privilege_type` text COMMENT 'vallue = all/{''view'':1, ''create'':0, ''update'':0, ''delete'':0}'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_privileges_rel`
--

LOCK TABLES `user_privileges_rel` WRITE;
/*!40000 ALTER TABLE `user_privileges_rel` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_privileges_rel` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_role_privileges_rel`
--

DROP TABLE IF EXISTS `user_role_privileges_rel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_role_privileges_rel` (
  `role_id` int(11) NOT NULL,
  `privilege_id` int(11) NOT NULL,
  `privilege_type` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_role_privileges_rel`
--

LOCK TABLES `user_role_privileges_rel` WRITE;
/*!40000 ALTER TABLE `user_role_privileges_rel` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_role_privileges_rel` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_role_rel`
--

DROP TABLE IF EXISTS `user_role_rel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_role_rel` (
  `user_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_role_rel`
--

LOCK TABLES `user_role_rel` WRITE;
/*!40000 ALTER TABLE `user_role_rel` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_role_rel` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_roles`
--

DROP TABLE IF EXISTS `user_roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_roles` (
  `xe_id` int(11) NOT NULL AUTO_INCREMENT,
  `store_id` varchar(50) DEFAULT NULL,
  `role_name` varchar(100) NOT NULL,
  PRIMARY KEY (`xe_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_roles`
--

LOCK TABLES `user_roles` WRITE;
/*!40000 ALTER TABLE `user_roles` DISABLE KEYS */;
INSERT INTO `user_roles` VALUES (1,'1','Super Admin');
/*!40000 ALTER TABLE `user_roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `word_cloud_category_rel`
--

DROP TABLE IF EXISTS `word_cloud_category_rel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `word_cloud_category_rel` (
  `word_cloud_id` bigint(20) DEFAULT NULL,
  `category_id` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `word_cloud_category_rel`
--

LOCK TABLES `word_cloud_category_rel` WRITE;
/*!40000 ALTER TABLE `word_cloud_category_rel` DISABLE KEYS */;
/*!40000 ALTER TABLE `word_cloud_category_rel` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `word_cloud_tag_rel`
--

DROP TABLE IF EXISTS `word_cloud_tag_rel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `word_cloud_tag_rel` (
  `word_cloud_id` bigint(20) DEFAULT NULL,
  `tag_id` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `word_cloud_tag_rel`
--

LOCK TABLES `word_cloud_tag_rel` WRITE;
/*!40000 ALTER TABLE `word_cloud_tag_rel` DISABLE KEYS */;
/*!40000 ALTER TABLE `word_cloud_tag_rel` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `word_cloud_tags`
--

DROP TABLE IF EXISTS `word_cloud_tags`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `word_cloud_tags` (
  `xe_id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`xe_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `word_cloud_tags`
--

LOCK TABLES `word_cloud_tags` WRITE;
/*!40000 ALTER TABLE `word_cloud_tags` DISABLE KEYS */;
/*!40000 ALTER TABLE `word_cloud_tags` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `word_clouds`
--

DROP TABLE IF EXISTS `word_clouds`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `word_clouds` (
  `xe_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `store_id` varchar(50) DEFAULT NULL,
  `file_name` varchar(50) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`xe_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `word_clouds`
--

LOCK TABLES `word_clouds` WRITE;
/*!40000 ALTER TABLE `word_clouds` DISABLE KEYS */;
/*!40000 ALTER TABLE `word_clouds` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-01-28 10:20:17
