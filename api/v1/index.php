<?php
/**
 * Author : Tanmaya Patra/India
 * Inkxe-X Microservice Framework
 * @date 01 nov 2019
 * An open source application development framework for PHP
 *
 * This content is released under the MIT License (MIT)
 *
 * Copyright (c) 2014 - 2018, Inkxe Systems Pvt Ltd
 *
 * @package Inkxe-X Microservice Framework
 * @author  Inkxe-X Dev Team
 * @copyright   Copyright (c) 2019 , InkXE Pvt Ltd
 * @license http://opensource.org/licenses/MIT  MIT License
 * @link    https://inkxe.com
 * @version 1.0.0
 */
    
// Initialize the config file
require __DIR__ . '/config/bootstrap.php';

//echo convertDate('2017-11-10', 'DMY');

//exit;
// Run app
$app->run();
