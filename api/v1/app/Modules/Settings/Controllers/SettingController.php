<?php
/**
 * Manage Color Swatches
 *
 * PHP version 5.6
 *
 * @category  Settings
 * @package   Eloquent
 * @author    Satyabrata <satyabratap@riaxe.com>
 * @copyright 2019-2020 Riaxe Systems
 * @license   http://www.php.net/license/3_0.txt  PHP License 3.0
 * @link      http://inkxe-v10.inkxe.io/xetool/admin
 */
namespace App\Modules\Settings\Controllers;

use App\Components\Controllers\Component as ParentController;
use App\Modules\Settings\Models\Currency;
use App\Modules\Settings\Models\Language;
use App\Modules\Settings\Models\Setting;
use App\Modules\Settings\Models\Unit;

/**
 * Setting Controller
 *
 * @category Class
 * @package  Setting
 * @author   Satyabrata <satyabratap@riaxe.com>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://inkxe-v10.inkxe.io/xetool/admin
 */
class SettingController extends ParentController
{
    /**
     * GET: List of Currency
     *
     * @param $request  Slim's Request object
     * @param $response Slim's Response object
     *
     * @author satyabratap@riaxe.com
     * @date   13 Aug 2019
     * @return All/Single Currency List
     */
    public function getCurrencyValues($request, $response)
    {
        $serverStatusCode = OPERATION_OKAY;
        $jsonResponse = [
            'status' => 0,
            'message' => message('Currency', 'not_found'),
        ];
        $currencyInit = new Currency();
        $getCurrency = $currencyInit->where('xe_id', '>', 0)->orderBy('xe_id', 'DESC')->get();
        if ($currencyInit->count() > 0) {
            $jsonResponse = [
                'status' => 1,
                'data' => $getCurrency,
            ];
        }
        return response($response, ['data' => $jsonResponse, 'status' => $serverStatusCode]);
    }

    /**
     * GET: List of Unit
     *
     * @param $request  Slim's Request object
     * @param $response Slim's Response object
     *
     * @author satyabratap@riaxe.com
     * @date   13 Aug 2019
     * @return All/Single Unit List
     */
    public function getUnitValues($request, $response)
    {
        $serverStatusCode = OPERATION_OKAY;
        $jsonResponse = [
            'status' => 0,
            'message' => message('Unit', 'not_found'),
        ];
        $unitInit = new Unit();
        $getUnits = $unitInit->where('xe_id', '>', 0)->orderBy('xe_id', 'DESC')->get();
        if ($unitInit->count() > 0) {
            $jsonResponse = [
                'status' => 1,
                'data' => $getUnits,
            ];
        }

        return response($response, ['data' => $jsonResponse, 'status' => $serverStatusCode]);
    }

    /**
     * POST: Save Setting
     *
     * @param $request  Slim's Request object
     * @param $response Slim's Response object
     *
     * @author satyabratap@riaxe.com
     * @date   5 Dec 2019
     * @return json response wheather data is saved or any error occured
     */
    public function saveSettings($request, $response)
    {
        $serverStatusCode = OPERATION_OKAY;
        $getStoreDetails = get_store_details($request);
        $settingLastInsertId = 0;
        $msg = '';
        $jsonResponse = [
            'status' => 0,
            'message' => message('Settings', 'error'),
        ];
        $allPostPutVars = $request->getParsedBody();
        if (isset($allPostPutVars['settings']) && $allPostPutVars['settings'] != "") {
            $settingsArray = json_clean_decode($allPostPutVars['settings'], true);

            $settingsType = $settingsArray['setting_type'];
            $settingInit = new Setting();
            $settingInit->where('type', $settingsType)->delete();
            if ($settingsType == 1) {
                foreach ($settingsArray['general'] as $generalKey => $generalValue) {
                    $settingsData = [
                        'setting_key' => $generalKey,
                        'setting_value' => json_encode($generalValue),
                        'type' => $settingsType,
                        'store_id' => $getStoreDetails['store_id'],
                    ];
                    $saveSettingsData = new Setting($settingsData);
                    if ($saveSettingsData->save()) {
                        $settingLastInsertId = $saveSettingsData->xe_id;
                        $msg = 'General Settings';
                    }
                }
            } elseif ($settingsType == 2) {
                foreach ($settingsArray['appearance'] as $appearanceKey => $appearanceValue) {
                    if (isset($appearanceKey) && $appearanceKey == 'custom_css' && $appearanceValue != '') {
                        $cssSettingLocation = path('abs', 'setting') . 'style' . '.css';
                        write_file($cssSettingLocation, $appearanceValue);
                        $appearanceValue = 'style.css';
                    }
                    $settingsData = [
                        'setting_key' => $appearanceKey,
                        'setting_value' => json_encode($appearanceValue),
                        'type' => $settingsType,
                        'store_id' => $getStoreDetails['store_id'],
                    ];
                    $saveSettingsData = new Setting($settingsData);
                    if ($saveSettingsData->save()) {
                        $settingLastInsertId = $saveSettingsData->xe_id;
                        $msg = 'Appearance Settings';
                    }
                }
            } elseif ($settingsType == 3) {
                foreach ($settingsArray['image_setting'] as $imageKey => $imageValue) {
                    $settingsData = [
                        'setting_key' => $imageKey,
                        'setting_value' => json_encode($imageValue),
                        'type' => $settingsType,
                        'store_id' => $getStoreDetails['store_id'],
                    ];
                    $saveSettingsData = new Setting($settingsData);
                    if ($saveSettingsData->save()) {
                        $settingLastInsertId = $saveSettingsData->xe_id;
                        $msg = 'Image Settings';
                    }
                }
            } elseif ($settingsType == 5) {
                foreach ($settingsArray['cart'] as $cartKey => $cartValue) {
                    $settingsData = [
                        'setting_key' => $cartKey,
                        'setting_value' => json_encode($cartValue),
                        'type' => $settingsType,
                        'store_id' => $getStoreDetails['store_id'],
                    ];
                    $saveSettingsData = new Setting($settingsData);
                    if ($saveSettingsData->save()) {
                        $settingLastInsertId = $saveSettingsData->xe_id;
                        $msg = 'Cart Settings';
                    }
                }
            }
            // After save data, write data to json file
            if ($this->writeOnJsonFile($getStoreDetails['store_id']) && $settingLastInsertId > 0 && $msg != '') {
                $jsonResponse = [
                    'status' => 1,
                    'message' => message($msg, 'saved'),
                ];
            }
        }
        return response($response, ['data' => $jsonResponse, 'status' => $serverStatusCode]);
    }

    /**
     * GET: Settings JSOn
     *
     * @param $request  Slim's Request object
     * @param $response Slim's Response object
     *
     * @author satyabratap@riaxe.com
     * @date   13 Dec 2019
     * @return JSON
     */
    public function getSettings($request, $response)
    {
        $serverStatusCode = OPERATION_OKAY;
        $jsonResponse = [
            'status' => 0,
            'message' => message('Settings', 'not_found'),
        ];
        $settingInit = new Setting();
        $getSettings = $settingInit->where('type', '>', 0);
        if ($getSettings->count() > 0) {
            $data = $getSettings->get();
            $typeValue = [];
            foreach ($data as $value) {
                if ($value['type'] == 1) {
                    $generalSettings[$value['setting_key']] = json_clean_decode($value['setting_value'], true);
                } elseif ($value['type'] == 2) {
                    if (pathinfo(str_replace('"', '', $value['setting_value']), PATHINFO_EXTENSION) == 'css') {
                        $cssData = "";
                        if (file_exists(path('read', 'setting')) . str_replace('"', '', $value['setting_value'])) {
                            $myfile = fopen(path('read', 'setting') . str_replace('"', '', $value['setting_value']), "r");
                            $cssData = fgets($myfile);
                            fclose($myfile);
                        }
                        $appearanceSettings[$value['setting_key']] = (!empty($cssData) && $cssData != "") ? $cssData : "";
                    } else {
                        $appearanceSettings[$value['setting_key']] = json_clean_decode($value['setting_value'], true);
                    }
                } elseif ($value['type'] == 3) {
                    $imageSetting[$value['setting_key']] = json_clean_decode($value['setting_value'], true);
                } elseif ($value['type'] == 5) {
                    $cartSetting[$value['setting_key']] = json_clean_decode($value['setting_value'], true);
                }
            }
            $typeValue = [
                'general_settings' => (!empty($generalSettings) && $generalSettings != "") ? $generalSettings : "",
                'appearance_settings' => (!empty($appearanceSettings) && $appearanceSettings != "") ? $appearanceSettings : "",
                'image_setting' => (!empty($imageSetting) && $imageSetting != "") ? $imageSetting : "",
                'cart_setting' => (!empty($cartSetting) && $cartSetting != "") ? $cartSetting : "",
            ];

            $jsonResponse = [
                'status' => 1,
            ];
            $jsonResponse += $typeValue;
        }

        return response($response, ['data' => $jsonResponse, 'status' => $serverStatusCode]);
    }

    /**
     * POST: Save Language
     *
     * @param $request  Slim's Request object
     * @param $response Slim's Response object
     *
     * @author satyabratap@riaxe.com
     * @date   12 Dec 2019
     * @return json response wheather data is saved or any error occured
     */
    public function saveLanguage($request, $response)
    {
        $serverStatusCode = OPERATION_OKAY;
        $allPostPutVars = $request->getParsedBody();
        $jsonResponse = [
            'status' => 0,
            'message' => message('Language', 'error'),
        ];
        // Get Store Specific Details from helper
        $getStoreDetails = get_store_details($request);
        if (isset($allPostPutVars['name']) && $allPostPutVars['name'] != "") {
            $languageInit = new Language();
            $checkLanguage = $languageInit->where(['name' => $allPostPutVars['name'], 'type' => $allPostPutVars['type']])->get();
            if (isset($checkLanguage) && count($checkLanguage) > 0) {
                $jsonResponse = [
                    'status' => 0,
                    'message' => 'This language is present. Please add another.',
                ];
            } else {
                $languageInit = new Language();
                $languageInit->where(['is_default' => 1, 'type' => $allPostPutVars['type']]);
                $defaultLanguage = $languageInit->first();
                $jsonlanguageLocation = path('abs', 'language') . $allPostPutVars['type'] . '/' . 'lang_' . strtolower($allPostPutVars['name']) . '.json';
                write_file($jsonlanguageLocation, file_get_contents($defaultLanguage['file_name']));
                $languageFileName = 'lang_' . strtolower($allPostPutVars['name']) . '.json';
                $allPostPutVars += ['file_name' => $languageFileName];
                $allPostPutVars += ['store_id' => $getStoreDetails['store_id']];
                $languageInit = new Language($allPostPutVars);
                if ($languageInit->save()) {
                    // After save data, write data to json file
                    if ($this->writeOnJsonFile($getStoreDetails['store_id'])) {
                        $jsonResponse = [
                            'status' => 1,
                            'message' => message('Language', 'saved'),
                        ];
                    }
                }
            }
        }
        return response($response, ['data' => $jsonResponse, 'status' => $serverStatusCode]);
    }

    /**
     * PUT: Update a language
     *
     * @param $request  Slim's Request object
     * @param $response Slim's Response object
     * @param $args     Slim's Argument parameters
     *
     * @author satyabratap@riaxe.com
     * @date   13 Aug 2019
     * @return json response wheather data is updated or not
     */
    public function updateLanguage($request, $response, $args)
    {
        $jsonResponse = [
            'status' => 1,
            'message' => message('Language', 'error'),
        ];
        $serverStatusCode = OPERATION_OKAY;
        $allPostPutVars = $this->parsePut();
        $updateData = [];
        if (!empty($args) && $args['id'] > 0) {
            $languageId = $args['id'];
            $jsonlanguageLocation = path('abs', 'language') . $allPostPutVars['type'] . '/' . 'lang_' . strtolower($allPostPutVars['name']) . '.json';
            write_file($jsonlanguageLocation, $allPostPutVars['value']);
            $languageFileName = 'lang_' . strtolower($allPostPutVars['name']) . '.json';
            $updateData += ['file_name' => $languageFileName];
            $languageInit = new Language();
            $languageInit->where('xe_id', '=', $languageId)->update($updateData);
            $jsonResponse = [
                'status' => 1,
                'message' => message('Language', 'updated'),
            ];
        }
        return response($response, ['data' => $jsonResponse, 'status' => $serverStatusCode]);
    }

    /**
     * GET: Settings JSOn
     *
     * @param $request  Slim's Request object
     * @param $response Slim's Response object
     * @param $args     Slim's Response object
     *
     * @author satyabratap@riaxe.com
     * @date   13 Dec 2019
     * @return JSON
     */
    public function getLanguage($request, $response, $args)
    {
        $serverStatusCode = OPERATION_OKAY;
        $jsonResponse = [
            'status' => 0,
            'message' => message('Language', 'not_found'),
        ];
        $type = $request->getQueryParam('type');
        // Get Store Specific Details from helper
        $getStoreDetails = get_store_details($request);
        $languageInit = new Language();
        if (isset($type) && $type != "") {
            $getLanguages = $languageInit->where('type', '=', $type);
        } else {
            $getLanguages = $languageInit->where('xe_id', '>', 0);
        }
        if (isset($args) && count($args) > 0 && $args['id'] != '') {
            $finalData = [];
            $languageInit = new Language();
            $getLanguage = $getLanguages->where(['xe_id' => $args['id'], 'store_id' => $getStoreDetails['store_id']])->first();
            $finalData['data'] = [$getLanguage];
        } else {
            $finalData['data'] = $getLanguages->where(['store_id' => $getStoreDetails['store_id']])->orderBy('xe_id', 'asc')->get();
        }
        if ($languageInit->count() > 0) {
            $jsonResponse = [
                'status' => 1,
                'data' => $finalData['data'],
            ];
        }

        return response($response, ['data' => $jsonResponse, 'status' => $serverStatusCode]);
    }
    /**
     * DELETE: Delete Language
     *
     * @param $request  Slim's Argument parameters
     * @param $response Slim's Response object
     * @param $args     Slim's Argument parameters
     *
     * @author satyabratap@riaxe.com
     * @date   13 Dec 2019
     * @return json response wheather data is deleted or not
     */
    public function deleteLanguage($request, $response, $args)
    {
        $serverStatusCode = OPERATION_OKAY;
        $jsonResponse = [
            'status' => 1,
            'message' => message('Language', 'error'),
        ];
        if (isset($args['id']) && $args['id'] > 0) {
            $languageUpdateId = $args['id'];
            $languageInit = new Language();
            if ($languageInit->where(['xe_id' => $languageUpdateId])->count() > 0) {
                $languageInit = $languageInit->find($languageUpdateId);
                $this->deleteOldFile('languages', 'file', ['xe_id' => $languageUpdateId], path('abs', 'language'));
                if ($languageInit->delete()) {
                    $jsonResponse = [
                        'status' => 1,
                        'message' => message('Language', 'deleted'),
                    ];
                }
            }
        }

        return response($response, ['data' => $jsonResponse, 'status' => $serverStatusCode]);
    }

    /**
     * GET: Default Language
     *
     * @param $request  Slim's Argument parameters
     * @param $response Slim's Response object
     * @param $args     Slim's Argument parameters
     *
     * @author satyabratap@riaxe.com
     * @date   13 Dec 2019
     * @return json message
     */
    public function defaultLanguage($request, $response, $args)
    {
        $serverStatusCode = OPERATION_OKAY;
        $jsonResponse = [];

        if (isset($args['id']) && $args['id'] != '' && $args['id'] > 0) {
            $type = $request->getQueryParam('type');
            $languageInit = new Language();
            $languageInit->where(['type' => $type])->update(['is_default' => 0, 'is_enable' => 0]);
            try {
                $languageInit->where(['xe_id' => $args['id'], 'type' => $type])->update(['is_default' => 1]);
                $jsonResponse = [
                    'status' => 1,
                    'message' => message('Language', 'done'),
                ];
            } catch (\Exception $e) {
                $jsonResponse = [
                    'status' => 0,
                    'message' => message('Language', 'error'),
                    'exception' => show_exception() === true ? $e->getMessage() : '',
                ];
            }
        }
        return response($response, ['data' => $jsonResponse, 'status' => $serverStatusCode]);
    }

    /**
     * GET: Enable/Disable Multi Language
     *
     * @param $request  Slim's Argument parameters
     * @param $response Slim's Response object
     *
     * @author satyabratap@riaxe.com
     * @date   13 Dec 2019
     * @return json message
     */
    public function resetMultiLanguage($request, $response)
    {
        $serverStatusCode = OPERATION_OKAY;
        $jsonResponse = [];
        $type = $request->getQueryParam('type');
        $languageInit = new Language();
        try {
            $languageInit->where(['is_default' => 0, 'type' => $type])->update(['is_enable' => 0]);
            $jsonResponse = [
                'status' => 1,
                'message' => message('Language', 'done'),
            ];
        } catch (\Exception $e) {
            $jsonResponse = [
                'status' => 0,
                'message' => message('Language', 'error'),
                'exception' => show_exception() === true ? $e->getMessage() : '',
            ];
        }
        return response($response, ['data' => $jsonResponse, 'status' => $serverStatusCode]);
    }

    /**
     * GET: Enable Single Language
     *
     * @param $request  Slim's Argument parameters
     * @param $response Slim's Response object
     * @param $args     Slim's Argument parameters
     *
     * @author satyabratap@riaxe.com
     * @date   13 Dec 2019
     * @return json message
     */
    public function enableLanguage($request, $response, $args)
    {
        $serverStatusCode = OPERATION_OKAY;
        $jsonResponse = [];

        if (isset($args['id']) && $args['id'] != '' && $args['id'] > 0) {
            $languageInit = new Language();
            $language = $languageInit->find($args['id']);
            $language->is_enable = !$language->is_enable;

            if ($language->save()) {
                $jsonResponse = [
                    'status' => 1,
                    'message' => message('Language', 'done'),
                ];
            } else {
                $serverStatusCode = EXCEPTION_OCCURED;
                $jsonResponse = [
                    'status' => 0,
                    'message' => message('Language', 'exception'),
                    'exception' => show_exception() === true ? $e->getMessage() : '',
                ];
            }

            return response($response, ['data' => $jsonResponse, 'status' => $serverStatusCode]);
        }
    }

    /**
     * Write onto setting json file
     *
     * @param $storeId Font ID
     *
     * @author debashrib@riaxe.com
     * @date   14 Jan 2020
     * @return boolean
     */
    private function writeOnJsonFile($storeId)
    {
        $settingLocation = path('abs', 'setting') . 'stores/'.$storeId;
        // create directory if not exists
        create_directory($settingLocation);
        $jsonFilePath = $settingLocation.'/settings.json';
        $jsonData = $settingsDataArr = [];
        $response = 0;
        
        $settingInit = new Setting();
        $getSettings = $settingInit->where('type', '>', 0)
            ->where('store_id', '=', $storeId)
            ->select('xe_id', 'setting_key', 'setting_value', 'type');
        if ($getSettings->count() > 0) {
            $settingsData = $getSettings->get();
            if (!empty($settingsData)) {
                foreach ($settingsData as $data) {
                    $settingsDataArr[$data['setting_key']] = json_clean_decode($data['setting_value'], true);
                }
                $jsonData = [
                    'unit' => isset($settingsDataArr['measurement_unit']) ? $settingsDataArr['measurement_unit']['display_lebel'] : '',
                    'currency' => [
                        'value' => isset($settingsDataArr['currency']) ? $settingsDataArr['currency']['currency'] : '',
                        'separator' => isset($settingsDataArr['currency']) ? $settingsDataArr['currency']['separator'] : '',
                        'post_fix' => isset($settingsDataArr['currency']) ? $settingsDataArr['currency']['post_fix'] : ''
                    ],
                    'email' => isset($settingsDataArr['email']) ? $settingsDataArr['email'] : '',
                    'image_setting' => [
                        'facebook_import' => [
                            'app_id' => isset($settingsDataArr['facebook_import']) ? $settingsDataArr['facebook_import']['app_id'] : '',
                            'domain_name' => isset($settingsDataArr['facebook_import']) ? $settingsDataArr['facebook_import']['domain_name'] : '',
                            'url' => isset($settingsDataArr['facebook_import']) ? $settingsDataArr['facebook_import']['url'] : ''
                        ],
                        'dropbox_import' => isset($settingsDataArr['dropbox_import']) ? $settingsDataArr['dropbox_import'] : '',
                        'google_drive_import' => isset($settingsDataArr['google_drive_import']) ? $settingsDataArr['google_drive_import'] : '',
                        'file_uploaded' => isset($settingsDataArr['file_uploaded']) ? $settingsDataArr['file_uploaded'] : '',
                        'terms_condition' => isset($settingsDataArr['terms_condition']) ? $settingsDataArr['terms_condition'] : ''
                    ],
                    'cart' => [
                        'direct_check_out' => isset($settingsDataArr['direct_check_out']) ? $settingsDataArr['direct_check_out'] : '',
                        'cart_terms_condition' => isset($settingsDataArr['cart_terms_condition']) ? $settingsDataArr['cart_terms_condition'] : '',
                        'order_notes' => isset($settingsDataArr['order_notes']) ? $settingsDataArr['order_notes'] : ''
                    ],
                ];
                // get language data
                $languageInit = new Language();
                $getLanguage = $languageInit->where(['type' => 'tool', 'store_id' => $storeId, 'is_enable' => 1])
                    ->select('name', 'code', 'is_default');
                $languageCount = $getLanguage->count();
                if ($languageCount > 0) {
                    $languageData = $getLanguage->get();
                    if (!empty($languageData)) {
                        foreach ($languageData as $key => $data) {
                            if ($data['is_default'] ==  1) {
                                $jsonData['lanuage']['default'] = [
                                    'name' => $data['name'],
                                    'code' => $data['code']
                                ];
                            }
                            unset($data['is_default']);
                            $jsonData['lanuage']['lang_list'][$key] = $data;
                        }
                    }
                    $jsonData['lanuage']['is_multi_lang'] = ($languageCount > 1) ? 1 : 0;
                }
                $jsonData = json_encode($jsonData, JSON_PRETTY_PRINT);
                $response = write_file($jsonFilePath, $jsonData);
            }
        }
        return $response;
    }
}
