<?php
/**
 * Manage Woocommerce Store Colors
 *
 * PHP version 5.6
 *
 * @category  Store_Color
 * @package   Store
 * @author    Satyabrata <satyabratap@riaxe.com>
 * @copyright 2019-2020 Riaxe Systems
 * @license   http://www.php.net/license/3_0.txt  PHP License 3.0
 * @link      http://inkxe-v10.inkxe.io/xetool/admin
 */
namespace SwatchStoreSpace\Controllers;

use App\Modules\Settings\Models\ColorSwatch;
use CommonStoreSpace\Controllers\StoreController;

/**
 * Store Color Controller
 *
 * @category Store_Color
 * @package  Store
 * @author   Satyabrata <satyabratap@riaxe.com>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://inkxe-v10.inkxe.io/xetool/admin
 */
class StoreColorVariantController extends StoreController
{
    /**
     * Instantiate Constructer
     */
    public function __construct()
    {
        parent::__construct();
    }
    /**
     * Get: Get the list of color attributes from the WooCommerce API
     *
     * @param $response Slim's Response object
     *
     * @author satyabratap@riaxe.com
     * @date   5 Dec 2019
     * @return Array of Color terms
     */
    public function getColorVariants($response)
    {
        $serverStatusCode = OPERATION_OKAY;
        $endPoint = 'products/attributes';
        $variantData = [];
        $jsonResponse = [
            'status' => 1,
            'records' => 0,
            'color_id' => NULL,
            'message' => message('Variant Data', 'not_found'),
            'data' => [],
        ];
        try {
            $getProductAttributes = $this->wc->get($endPoint);
            // Get Product Attributes
            foreach ($getProductAttributes as $attributes) {
                if (isset($attributes['name']) && $attributes['name'] == 'color') {
                    $colorId = $attributes['id'];
                    $termEndPoint = 'products/attributes/' . $attributes['id'] . '/terms';
                    $options = [
                        'page' => 1,
                        'per_page' => 100
                    ];
                    $getAttributeTerms = $this->wc->get($termEndPoint, $options);
                    if (isset($getAttributeTerms) && count($getAttributeTerms) > 0) {
                        $variantData = $this->getColorSwatchData($getAttributeTerms);
                    }
                }
            }
            if (is_array($variantData) && count($variantData) > 0) {
                $jsonResponse = [
                    'status' => 1,
                    'records' => count($variantData),
                    'color_id' => $colorId,
                    'data' => $variantData,
                ];
            }
        } catch (\Exception $e) {
            $serverStatusCode = EXCEPTION_OCCURED;
            $jsonResponse = [
                'status' => 0,
                'message' => message('Variant Data', 'insufficient'),
                'exception' => show_exception() === true ? $e->getMessage() : ''
            ];
        }
        return [
            'data' => $jsonResponse,
            'http_status_code' => $serverStatusCode,
        ];
    }

    /**
     * Post: Save Color terms into the store
     *
     * @param $name    Name of the color term
     * @param $colorId Id of the color attribute
     *
     * @author satyabratap@riaxe.com
     * @date   5 Dec 2019
     * @return Array records and server status
     */
    public function saveColor($name, $colorId)
    {
        $serverStatusCode = OPERATION_OKAY;
        $endPoint = 'products/attributes/';
        $getTerm = [];

        if (!empty($colorId) && $colorId > 0 ) {
            try {
                $getStoreTerm = $this->wc->get($endPoint . $colorId . '/terms',  ['search' => $name]);

                if (empty($getStoreTerm)) {
                    $getTerm = $this->wc->post($endPoint . $colorId . '/terms',  ['name' => $name]);
                }

            } catch (\Throwable $th) {
                $getTerm = [];
            }
        }
        return $getTerm;
    }
}
