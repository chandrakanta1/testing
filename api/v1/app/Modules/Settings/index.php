<?php
// Set store autoload 
$vendor->setPsr4("SwatchStoreSpace\\", "app/Modules/Settings/Stores/" . STORE_NAME . "/" . STORE_VERSION . "/");
/**
 * Initilize all Routes
 */
require __DIR__ . '/Routes/routes.php';