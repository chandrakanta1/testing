<?php
/**
 * Manage Customers
 *
 * PHP version 5.6
 *
 * @category  Customer
 * @package   Eloquent
 * @author    Satyabrata <satyabratap@riaxe.com>
 * @copyright 2019-2020 Riaxe Systems
 * @license   http://www.php.net/license/3_0.txt  PHP License 3.0
 * @link      http://inkxe-v10.inkxe.io/xetool/admin
 */

namespace App\Modules\Customers\Controllers;

use CustomerStoreSpace\Controllers\StoreCustomersController;

/**
 * Customers Controller
 *
 * @category Class
 * @package  Customer
 * @author   Satyabrata <satyabratap@riaxe.com>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://inkxe-v10.inkxe.io/xetool/admin
 */
class CustomersController extends StoreCustomersController
{
    /**
     * GET: List of Customers
     *
     * @param $request  Slim's Request object
     * @param $response Slim's Response object
     * @param $args     Slim's Argument parameters
     *
     * @author satyabratap@riaxe.com
     * @date   13 Nov 2019
     * @return All/Single Customer(s) List
     */
    public function allCustomers($request, $response, $args)
    {
        $serverStatusCode = OPERATION_OKAY;
        $jsonResponse = $this->getCustomers($request, $response, $args);
        return response(
            $response, [
            'data' => $jsonResponse['data'], 'status' => $serverStatusCode
            ]
        );
    }

    /**
     * GET: List Orders by customerId
     *
     * @param $request  Slim's Request object
     * @param $response Slim's Response object
     * @param $args     Slim's Argument parameters
     *
     * @author satyabratap@riaxe.com
     * @date   13 Nov 2019
     * @return All/Single Customer(s) List
     */
    public function ordersByCustomers($request, $response, $args)
    {
        $serverStatusCode = OPERATION_OKAY;
        $jsonResponse = $this->getOrdrsByCustomers($request, $response, $args);
        return response(
            $response, [
            'data' => $jsonResponse['data'], 'status' => $serverStatusCode
            ]
        );
    }
}
