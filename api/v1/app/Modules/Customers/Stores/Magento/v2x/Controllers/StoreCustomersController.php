<?php 
    /**
     *
     * This Controller used to save, fetch or delete Magento Customers
     *
     * @category   Customers
     * @package    Magento API
     * @author     Original Author <tapasranjanp@riaxe.com>
     * @author     tapasranjanp@riaxe.com
     * @copyright  2019-2020 Riaxe Systems
     * @license    http://www.php.net/license/3_0.txt  PHP License 3.0
     * @version    Release: @1.0
     */
    namespace CustomerStoreSpace\Controllers;
    
    use ComponentStoreSpace\Controllers\StoreComponent;

    class StoreCustomersController extends StoreComponent
    {
        /**
         * Get list of customer or a Single customer from the Magento API
         *
         * @author     tapasranjanp@riaxe.com
         * @date       18 Dec 2019
         * @parameter  Slim default params
         * @response   Array of list/one customer(s)
         */
        public function getCustomers($request, $response, $args)
        {
            $serverStatusCode = OPERATION_OKAY;
            $getStoreDetails = get_store_details($request);
            try {
                if(isset($args['id']) && $args['id'] > 0) {
                    // Fetching Single Customer details           
                    $filters = array(
                        'store' => $getStoreDetails['store_id'],
                        'customerId' => $args['id']
                    );
                    $result = $this->apiCall('Customer', 'getStoreCustomerDetails', $filters);
                    $result = $result->result;
                    $getCustomers   = json_decode($result, true);
                    $getTotalCustomersCount = 1;
                    $customerOrderDetails = $getCustomers;
                }else{
                    // Fetching all customer by filters
                    $searchstring = (!empty($request->getQueryParam('name')) && $request->getQueryParam('name') != "") ? $request->getQueryParam('name') : '';
                    $page = (!empty($request->getQueryParam('page')) && $request->getQueryParam('page') != "") ? $request->getQueryParam('page') : 0;
                    $limit = (!empty($request->getQueryParam('perpage')) && $request->getQueryParam('perpage') != "") ? $request->getQueryParam('perpage') : 20;
                    $order = (!empty($request->getQueryParam('order')) && $request->getQueryParam('order') != "") ? $request->getQueryParam('order') : 'asc';
                    $orderby = (!empty($request->getQueryParam('orderby')) && $request->getQueryParam('orderby') != "") ? $request->getQueryParam('orderby') : 'id';
                    $filters = array(
                        'store' => $getStoreDetails['store_id'],
                        'searchstring' => $searchstring,
                        'page' => $page,
                        'limit' => $limit,
                        'order' => $order,
                        'orderby' => $orderby
                    );
                    $result = $this->apiCall('Customer', 'getStoreCustomers', $filters);
                    $result = $result->result;
                    $getCustomers = json_decode($result, true);
                    $getTotalCustomersCount = $getCustomers['user_count'];
                    $customerOrderDetails = $getCustomers['customer_list'];
                }
                if(isset($customerOrderDetails) && is_array($customerOrderDetails) && count($customerOrderDetails) > 0) {
                    $response = [
                        'status' => 1,
                        'records' => count($customerOrderDetails),
                        'total_records' => $getTotalCustomersCount,
                        'data' => $customerOrderDetails
                    ];
                } else {
                    $serverStatusCode = OPERATION_OKAY;
                    $response = [
                        'status' => 0,
                        'records' => count($customerOrderDetails),
                        'total_records' => 0,
                        'message' => 'No Customer available',
                        'data' => []
                    ];
                }
            } catch (\Exception $e) {
                $serverStatusCode = EXCEPTION_OCCURED;
                $response = [
                    'status' => 0,
                    'message' => 'Invalid request',
                    'exception' => $e->getMessage()
                ];
            }
            // Reset Total product Count 
            return [
                'data' => $response,
                'httpStatusCode' => $serverStatusCode
            ];
        }
    }
