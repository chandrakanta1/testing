<?php
/**
 * Manage Customer
 *
 * PHP version 5.6
 *
 * @category  Customers
 * @package   Eloquent
 * @author    Satyabrata <satyabratap@riaxe.com>
 * @copyright 2019-2020 Riaxe Systems
 * @license   http://www.php.net/license/3_0.txt  PHP License 3.0
 * @link      http://inkxe-v10.inkxe.io/xetool/admin
 */
namespace CustomerStoreSpace\Controllers;

use CommonStoreSpace\Controllers\StoreController;

/**
 * Customer Controller
 *
 * @category Class
 * @package  Customer
 * @author   Satyabrata <satyabratap@riaxe.com>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://inkxe-v10.inkxe.io/xetool/admin
 */
class StoreCustomersController extends StoreController
{
    /**
     * Instantiate Constructor
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * GET: Get Customer
     *
     * @param $request  Slim's Request object
     * @param $response Slim's Response object
     * @param $args     Slim's Arguments
     *
     * @author satyabratap@riaxe.com
     * @date   7 jan 2019
     * @return json response wheather data is saved or any error occured
     */
    public function getCustomers($request, $response, $args)
    {
        $serverStatusCode = OPERATION_OKAY;
        $totalSpent = 0;
        $endPoint = 'customers';
        $jsonResponse = [
            'status' => 0,
            'message' => 'No Customer available',
            'data' => [],
        ];
        $customerOrderDetails = [];
        if (isset($args['id']) && $args['id'] > 0) {
            $endPoint .= '/' . $args['id'];
            // Fetch Single customers
            $getCustomerData = $this->wc->get($endPoint);
            $orderDetails = $this->_GetOrderData($getCustomerData, 'customer');
            $totalSpent = $orderDetails['total_spent'];
            $prepareOrder = $orderDetails['prepare_order'];
            $totalOrderCount = $orderDetails['orders_count'];
            $lastOrderDetail = (!empty($prepareOrder[0]) && $prepareOrder[0] > 0) ? $prepareOrder[0] : null;
            $lastOrderDetailID = $lastOrderDetail['id'];
            $lastOrder = time_elapsed($lastOrderDetail['created_date']);
            $customerOrderDetails = [
                'id' => $getCustomerData['id'],
                'first_name' => $getCustomerData['first_name'],
                'last_name' => $getCustomerData['last_name'],
                'email' => $getCustomerData['email'],
                'profile_pic' => $getCustomerData['avatar_url'],
                'total_orders' => $totalOrderCount,
                'total_order_amount' => $totalSpent,
                'average_order_amount' => (!empty($prepareOrder) && $prepareOrder > 0) ? $totalSpent / count($prepareOrder) : 0,
                'last_order' => $lastOrder,
                'last_order_id' => $lastOrderDetailID,
                'date_created' => date('Y-m-d h:i:s', strtotime($getCustomerData['date_created'])),
                'billing_address' => $getCustomerData['billing'],
                'shipping_address' => $getCustomerData['shipping'],
                'orders' => (!empty($prepareOrder) && count($prepareOrder) > 0) ? $prepareOrder : [],
            ];
            $jsonResponse = [
                'status' => 1,
                'records' => 1,
                'data' => $customerOrderDetails,
            ];
        } else {
            // Get all requested Query params
            $filters = [
                'search' => $request->getQueryParam('name'),
                'order' => (!empty($request->getQueryParam('order')) && $request->getQueryParam('order') != "") ? $request->getQueryParam('order') : 'asc',
                'orderby' => (!empty($request->getQueryParam('orderby')) && $request->getQueryParam('orderby') != "") ? $request->getQueryParam('orderby') : 'id',
            ];
            $options = [];
            foreach ($filters as $filterKey => $filterValue) {
                if (isset($filterValue) && $filterValue != "") {
                    $options[$filterKey] = $filterValue;
                }
            }
            // Fetch all customers
            $getCustomerData = $this->wc->get($endPoint, $options);
            if (isset($getCustomerData) && count($getCustomerData) > 0) {
                foreach ($getCustomerData as $custKey => $customer) {
                    $orderDetails = $this->_GetOrderData($customer, 'customer');
                    $totalSpent = $orderDetails['total_spent'];
                    $prepareOrder = $orderDetails['prepare_order'];
                    $totalOrderCount = $orderDetails['orders_count'];
                    $lastOrderDetail = (!empty($prepareOrder[0]) && $prepareOrder[0] > 0) ? $prepareOrder[0] : null;
                    $lastOrderDetailID = $lastOrderDetail['id'];
                    $lastOrder = time_elapsed($lastOrderDetail['created_date']);
                    $customerOrderDetails[$custKey] = [
                        'id' => $customer['id'],
                        'first_name' => $customer['first_name'],
                        'last_name' => $customer['last_name'],
                        'email' => $customer['email'],
                        'total_orders' => $totalOrderCount,
                        'last_order_id' => $lastOrderDetailID,
                        'date_created' => date(
                            'Y-m-d h:i:s', strtotime($customer['date_created'])
                        ),
                    ];
                }
                $jsonResponse = [
                    'status' => 1,
                    'records' => 1,
                    'data' => $customerOrderDetails,
                ];
            }
        }
        // Reset Total Customer Count
        return [
            'data' => $jsonResponse,
            'httpStatusCode' => $serverStatusCode,
        ];
    }

    /**
     * GET: Get Orders Of Single Customer
     *
     * @param $request  Slim's Request object
     * @param $response Slim's Response object
     * @param $args     Slim's Response object
     *
     * @author satyabratap@riaxe.com
     * @date   7 jan 2019
     * @return json response wheather data is saved or any error occured
     */
    public function getOrdrsByCustomers($request, $response, $args)
    {
        $serverStatusCode = OPERATION_OKAY;
        $endPoint = 'customers';
        $jsonResponse = [
            'status' => 0,
            'message' => 'No Order available',
            'data' => [],
        ];
        if (isset($args['id']) && $args['id'] != "" && $args['id'] > 0) {
            $endPoint .= '/' . $args['id'];
        }
        $isCustomize = $request->getQueryParam('is_customize');
        try {
            if (!empty($isCustomize) && $isCustomize > 0) {
                $filters = [
                    'search' => $request->getQueryParam('name'),
                    'page' => $request->getQueryParam('page'),
                    'sku' => $request->getQueryParam('sku'),
                    'print_type' => $request->getQueryParam('print_type'),
                    'is_customize' => $request->getQueryParam('is_customize'),
                    'order_by' => $request->getQueryParam('orderby'),
                    'order' => $request->getQueryParam('order'),
                    'to' => $request->getQueryParam('to'),
                    'from' => $request->getQueryParam('from'),
                    'per_page' => $request->getQueryParam('per_page'),
                    'customer_id' => $args['id'],
                ];
                $options = [];
                foreach ($filters as $filterKey => $filterValue) {
                    if (isset($filterValue) && $filterValue != "") {
                        $options += [$filterKey => $filterValue];
                    }
                }
                $orderDetails = $this->plugin->get('orders', $options);
                if (!empty($orderDetails['data'])) {
                    $totalRecords = $orderDetails['records'];
                    $orderDetails = $orderDetails['data'];
                    $jsonResponse = [
                        'status' => 1,
                        'records' => count($orderDetails),
                        'total_records' => $totalRecords,
                        'data' => $orderDetails,
                    ];
                }
            } else {
                $getCustomers = object_to_array($this->wc->get($endPoint));
                if (isset($getCustomers['id']) && $getCustomers['id'] > 0) {
                    $orderDetails = $this->_GetOrderData($getCustomers, 'order');
                    $prepareOrder = $orderDetails['prepare_order'];
                }
                if (isset($prepareOrder) && is_array($prepareOrder) 
                    && count($prepareOrder) > 0
                ) {
                    $jsonResponse = [
                        'status' => 1,
                        'records' => count($prepareOrder),
                        'data' => $prepareOrder,
                    ];
                }
            }
        } catch (\Exception $e) {
            $serverStatusCode = EXCEPTION_OCCURED;
            $jsonResponse = [
                'status' => 0,
                'message' => 'Invalid request',
                'exception' => $e->getMessage(),
            ];
        }
        // Reset Total Customer Count
        return [
            'data' => $jsonResponse,
            'httpStatusCode' => $serverStatusCode,
        ];
    }

    /**
     * GET: Get Order Data Of Single Customer
     *
     * @param $customers Customer Details
     * @param $custFlag  Customer & Order Flag
     *
     * @author satyabratap@riaxe.com
     * @date   7 jan 2019
     * @return json
     */
    private function _GetOrderData($customers, $custFlag)
    {
        $totalSpent = 0;
        $quantity = 0;
        $orderOptions = [];
        $orderOptions['customer'] = $customers['id'];
        $prepareOrder = [];
        $getOrders = object_to_array($this->wc->get('orders', $orderOptions));
        if (isset($getOrders) && count($getOrders) > 0) {
            foreach ($getOrders as $orderValue) {
                if ($custFlag === 'customer') {
                    if (!empty($orderValue['line_items'])) {
                        foreach ($orderValue['line_items'] as $valueItems) {
                            $quantity += $valueItems['quantity'];
                        }
                    }
                    $prepareOrder[] = [
                        'id' => $orderValue['id'],
                        'currency' => (isset($orderValue['currency']) && $orderValue['currency'] != "") ? $orderValue['currency'] : null,
                        'created_date' => (isset($orderValue['date_created']) && $orderValue['date_created'] != "") ? date('Y-m-d h:i:s', strtotime($orderValue['date_created'])) : null,
                        'total_amount' => (isset($orderValue['total']) && $orderValue['total'] > 0) ? $orderValue['total'] : 0.00, // tax will be incl.
                        'quantity' => $quantity,
                    ];
                    $totalSpent = $totalSpent + $orderValue['total'];
                } else {
                    $prepareOrder[] = [
                        'id' => $orderValue['id'],
                        'order_number' => $orderValue['id'],
                        'customer_first_name' => $customers['first_name'],
                        'customer_last_name' => $customers['last_name'],
                        'created_date' => (isset($orderValue['date_created']) && $orderValue['date_created'] != "") ? date('Y-m-d h:i:s', strtotime($orderValue['date_created'])) : null,
                        'total_amount' => (isset($orderValue['total']) && $orderValue['total'] > 0) ? $orderValue['total'] : 0.00,
                        'currency' => (isset($orderValue['currency']) && $orderValue['currency'] != "") ? $orderValue['currency'] : null,
                        'status' => $orderValue['status'],

                    ];
                }
            }
        }
        $orderDetails = [
            'prepare_order' => $prepareOrder,
            'total_spent' => $totalSpent,
            'orders_count' => count($getOrders),
        ];
        return $orderDetails;
    }

}
