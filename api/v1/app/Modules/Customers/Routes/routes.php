<?php
/**
 * This Routes holds all the individual route for the Clipart
 *
 * PHP version 5.6
 *
 * @category  Customer
 * @package   Store
 * @author    Satyabrata <satyabratap@riaxe.com>
 * @copyright 2019-2020 Riaxe Systems
 * @license   http://www.php.net/license/3_0.txt  PHP License 3.0
 * @link      http://inkxe-v10.inkxe.io/xetool/admin
 */

use App\Modules\Customers\Controllers\CustomersController as Customers;

$container = $app->getContainer();

$app->group('/customers', function () use ($app) {
    $app->get('/{id}', Customers::class . ':allCustomers');
    $app->get('/{id}/orders', Customers::class . ':ordersByCustomers');
    $app->get('', Customers::class . ':allCustomers');
});
