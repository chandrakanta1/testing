<?php
/**
 * Initilize all Routes
 */
$vendor->setPsr4("CustomerStoreSpace\\", "app/Modules/Customers/Stores/" . STORE_NAME . "/" . STORE_VERSION . "/");
require __DIR__ . '/Routes/routes.php';
