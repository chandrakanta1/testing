<?php
/**
 * This Model used for Print profile
 *
 * PHP version 5.6
 *
 * @category  Print_Profile
 * @package   Print_Profile
 * @author    Tanmaya Patra <tanmayap@riaxe.com>
 * @copyright 2019-2020 Riaxe Systems
 * @license   http://www.php.net/license/3_0.txt  PHP License 3.0
 * @link      http://inkxe-v10.inkxe.io/xetool/admin
 */

namespace App\Modules\PrintProfiles\Models;

/**
 * Feature Class
 *
 * @category Print_Profile
 * @package  Print_Profile
 * @author   Tanmaya Patra <tanmayap@riaxe.com>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://inkxe-v10.inkxe.io/xetool/admin
 */
class PrintProfile extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'print_profiles';
    protected $primaryKey = 'xe_id';
    protected $fillable = [
        'store_id',
        'name',
        'file_name',
        'description',
        'status',
        'is_vdp_enabled',
        'vdp_data',
        'is_laser_engrave_enabled',
        'image_settings',
        'color_settings',
        'order_settings',
        'is_disabled',
        'is_draft',
    ];
    protected $appends = ['file_name_url', 'thumbnail'];
    public $timestamps = false;

    /**
     * Create a relationship of Print Profile with Pricing
     *
     * @author tanmayap@riaxe.com
     * @date   14 Aug 2019
     * @return relationship object of category
     */
    public function pricing()
    {
        return $this->hasMany('App\Modules\PrintProfiles\Models\Pricing\PrintProfilePricing', 'print_profile_id', 'xe_id');
    }
    /**
     * Create a relationship of Print Profile with Print Profile Feature Relation
     *
     * @author tanmayap@riaxe.com
     * @date   14 Aug 2019
     * @return relationship object of category
     */
    public function features()
    {
        return $this->hasMany('App\Modules\PrintProfiles\Models\PrintProfileFeatureRel', 'print_profile_id', 'xe_id');
    }
    /**
     * Create a relationship of Print Profile with Print Profile Assets Category Relation
     *
     * @author tanmayap@riaxe.com
     * @date   14 Aug 2019
     * @return relationship object of category
     */
    public function assets()
    {
        return $this->hasMany('App\Modules\PrintProfiles\Models\PrintProfileAssetsCategoryRel', 'print_profile_id', 'xe_id');
    }
    /**
     * Create a relationship of Print Profile with Print Profile Engrave Setting
     *
     * @author tanmayap@riaxe.com
     * @date   14 Aug 2019
     * @return relationship object of category
     */
    public function engraves()
    {
        return $this->hasOne('App\Modules\PrintProfiles\Models\PrintProfileEngraveSetting', 'print_profile_id', 'xe_id');
    }
    /**
     * Create a relationship of Print Profile with Print Profile Template Setting
     *
     * @author tanmayap@riaxe.com
     * @date   14 Aug 2019
     * @return relationship object of category
     */
    public function templates()
    {
        return $this->hasMany('App\Modules\Templates\Models\TemplatePrintProfileRel', 'print_profile_id');
    }
    /**
     * Create a relationship of Print Profile with Print Profile Decoration Setting
     *
     * @author tanmayap@riaxe.com
     * @date   14 Aug 2019
     * @return relationship object of category
     */
    public function decorations()
    {
        return $this->hasMany('App\Modules\Products\Models\PrintProfileDecorationSettingRel', 'print_profile_id');
    }
    /**
     * Create a relationship of Print Profile with Print Profile Products Setting
     *
     * @author tanmayap@riaxe.com
     * @date   14 Aug 2019
     * @return relationship object of category
     */
    public function products()
    {
        return $this->hasMany('App\Modules\Products\Models\PrintProfileProductSettingRel', 'print_profile_id');
    }
    /**
     * Get file name data
     *
     * @author tanmayap@riaxe.com
     * @date   14 Aug 2019
     * @return relationship object of category
     */
    public function filename()
    {
        return $this->attributes['file_name'];
    }
    /**
     * Getting full url from file_name by manipulate file_name value
     *
     * @author tanmayap@riaxe.com
     * @date   14 Aug 2019
     * @return relationship object of category
     */
    public function getFileNameUrlAttribute()
    {
        if (isset($this->attributes['file_name']) && $this->attributes['file_name'] != "") {
            return path('read', 'print_profile') . $this->attributes['file_name'];
        } else {
            return "";
        }
    }
    /**
     * Getting full thumb url from file_name by manipulate file_name value
     *
     * @author tanmayap@riaxe.com
     * @date   14 Aug 2019
     * @return relationship object of category
     */
    public function getThumbnailAttribute()
    {
        if (isset($this->attributes['file_name']) && $this->attributes['file_name'] != "") {
            return path('read', 'print_profile') . 'thumb_' . $this->attributes['file_name'];
        } else {
            return "";
        }
    }
}
