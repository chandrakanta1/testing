<?php
/**
 * This Routes holds all the individual route for the Clipart
 *
 * PHP version 5.6
 *
 * @category  Print_Profile
 * @package   Print_Profile
 * @author    Tanmaya Patra <tanmayap@riaxe.com>
 * @copyright 2019-2020 Riaxe Systems
 * @license   http://www.php.net/license/3_0.txt  PHP License 3.0
 * @link      http://inkxe-v10.inkxe.io/xetool/admin
 */
use App\Middlewares\ValidateJWTToken as ValidateJWT;
use App\Modules\PrintProfiles\Controllers\PricingController as Pricing;
use App\Modules\PrintProfiles\Controllers\PrintProfilesController as PrintProfile;

$container = $app->getContainer();

// Print Profile Routes list

$app->group(
    '/print-profiles', function () use ($app) {
        $app->get('', PrintProfile::class . ':getAllPrintProfiles');
        $app->get('/{id}', PrintProfile::class . ':getSinglePrintProfile');
        $app->post('', PrintProfile::class . ':savePrintProfile');
        $app->put('/{id}', PrintProfile::class . ':updatePrintProfile');
        $app->delete('/{id}', PrintProfile::class . ':deletePrintProfile');
        $app->get('/toggle-disable/{id}', PrintProfile::class . ':disablePrintProfile');
        $app->post('/clone', PrintProfile::class . ':clonePrintProfile');
        $app->post('/assign/category', PrintProfile::class . ':assignCategoryToPrintProfile');
    }
)->add(new ValidateJWT($container));

// Print Profile Pricing

$app->group(
    '/print-profile-pricings', function () use ($app) {
        $app->get('', Pricing::class . ':getPricingDetails');
        $app->get('/{id}', Pricing::class . ':getPricingDetails');
        $app->post('', Pricing::class . ':savePricing'); // Save Print profile Pricing
        $app->put('/{id}', Pricing::class . ':updatePricing');
        $app->delete('/{id}', Pricing::class . ':deletePricingDetails');
    }
)->add(new ValidateJWT($container));
