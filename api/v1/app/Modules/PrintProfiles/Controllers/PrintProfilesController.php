<?php
/**
 * Manage Print Profile
 *
 * PHP version 5.6
 *
 * @category  Print_Profile
 * @package   Print_Profile
 * @author    Tanmaya Patra <tanmayap@riaxe.com>
 * @copyright 2019-2020 Riaxe Systems
 * @license   http://www.php.net/license/3_0.txt  PHP License 3.0
 * @link      http://inkxe-v10.inkxe.io/xetool/admin
 */
namespace App\Modules\PrintProfiles\Controllers;

use App\Components\Controllers\Component as ParentController;
use App\Modules\PrintProfiles\Models as PrintProfileModels;
use Illuminate\Database\Capsule\Manager as DB;
use ProductStoreSpace\Controllers\StoreProductsController;


/**
 * Print Profile Controller
 *
 * @category Print_Profile
 * @package  Print_Profile
 * @author   Tanmaya Patra <tanmayap@riaxe.com>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://inkxe-v10.inkxe.io/xetool/admin
 */
class PrintProfilesController extends StoreProductsController
{
    /**
     * dfds adsasdas asd as
     */
    // public function __construct(\Psr\Container\ContainerInterface $container)
    // {
    //     $this->container = $container;
    // }
    /**
     * Assets Slug list
     */
    private $_assetsSlugList = [
        'products',
        'cliparts',
        'background-patterns',
        'colors',
        'fonts',
        'shapes',
        'templates',
        'color-palettes',
    ];
    /**
     * GET: Get list of Print Profiles
     *
     * @param $request  Slim's Request object
     * @param $response Slim's Response object
     *
     * @author tanmayap@riaxe.com
     * @date   5 Oct 2019
     * @return All Print Profile List
     */
    public function getAllPrintProfiles($request, $response)
    {
        $serverStatusCode = OPERATION_OKAY;
        $jsonResponse = [
            'status' => 0,
            'data' => [],
            'message' => message('Print Profile', 'error'),
        ];
        $getStoreDetails = get_store_details($request);
        $printProfileGtInit = new PrintProfileModels\PrintProfile();
        $getPrintProfile = $printProfileGtInit->whereNotNull('name');

        if ($getPrintProfile->count() > 0) {
            // Adding Filters
            $categoryId = $request->getQueryParam('category');
            $assetTypeId = $request->getQueryParam('asset_type');

            // Filter by Category ID and It's Subcategory ID
            if (isset($assetTypeId) && $assetTypeId != "") {
                $seacrhAssetTypes = $this->assetsTypeId($assetTypeId);
                if (!empty($seacrhAssetTypes['asset_type_id'])
                    && $seacrhAssetTypes['asset_type_id'] > 0
                ) {
                    $getPrintProfile->whereHas(
                        'assets', function ($q) use ($seacrhAssetTypes) {
                            return $q->where('asset_type_id', $seacrhAssetTypes['asset_type_id']);
                        }
                    );
                }
            }
            // Filter by Assets type ID
            if (isset($categoryId) && $categoryId != "") {
                $getPrintProfile->whereHas(
                    'assets', function ($q) use ($categoryId) {
                        return $q->where('category_id', $categoryId);
                    }
                );
            }
            // Store conditions
            $getPrintProfile->where($getStoreDetails);

            $printProfileList = $getPrintProfile->select(
                'xe_id as id', 'name',
                'is_disabled', 'file_name'
            )->orderBy('xe_id', 'desc')->get();

            $jsonResponse = [
                'status' => 1,
                'data' => $printProfileList,
            ];
        }

        return response(
            $response, ['data' => $jsonResponse, 'status' => $serverStatusCode]
        );
    }

    /**
     * POST: Clone a Print Profile along with it's secondary Table's Data
     *
     * @param $request  Slim's Request object
     * @param $response Slim's Response object
     *
     * @author tanmayap@riaxe.com
     * @date   5 Oct 2019
     * @return Clone Status
     */
    public function clonePrintProfile($request, $response)
    {
        $jsonResponse = [
            'status' => 0,
            'message' => message('Print Profile Clone', 'error'),
        ];
        $serverStatusCode = OPERATION_OKAY;
        $getPostData = $request->getParsedBody();
        $cloneId = $getPostData['print_profile_id'];
        $cloneNewName = (isset($getPostData['name']) && $getPostData['name'] != "") 
            ? $getPostData['name'] : null;
        $printProfileGet = new PrintProfileModels\PrintProfile();
        $profiles = $printProfileGet->with('features', 'assets', 'engraves')
            ->find($cloneId);
        $cloneProfile = $profiles->replicate();
        if ($cloneProfile->save()) {
            $printProfileInsertId = $cloneProfile->xe_id;
            $features = $cloneProfile->features;
            $assets = $cloneProfile->assets;
            $engraves = $cloneProfile->engraves;
            if (isset($features) && count($features->toArray()) > 0) {
                foreach ($features as $key => $feature) {
                    $featureReplicate = new PrintProfileModels\PrintProfileFeatureRel();
                    $featureReplicate->print_profile_id = $printProfileInsertId;
                    $featureReplicate->feature_id = $feature->feature_id;
                    $featureReplicate->save();
                }
            }
            if (isset($assets) && count($assets->toArray()) > 0) {
                foreach ($assets as $key => $asset) {
                    $assetReplicate = new PrintProfileModels\PrintProfileAssetsCategoryRel();
                    $assetReplicate->print_profile_id = $printProfileInsertId;
                    $assetReplicate->asset_type_id = $asset->asset_type_id;
                    $assetReplicate->category_id = $asset->category_id;
                    $assetReplicate->save();
                }
            }
            if (isset($engraves->print_profile_id) && $engraves->print_profile_id > 0) {
                $engraves->print_profile_id = $printProfileInsertId;
                $engraveReplicate = new PrintProfileModels\PrintProfileEngraveSetting($engraves->toArray());
                $engraveReplicate->save();
            }
            $basicData = [];
            $uploadedFileName = save_file('upload', path('abs', 'print_profile'));
            if (!empty($uploadedFileName)) {
                $basicData += [
                    'file_name' => $uploadedFileName,
                ];
            }
            if (!empty($cloneNewName) && $cloneNewName != "") {
                $basicData += [
                    'name' => $cloneNewName,
                ];
            }
            if (isset($basicData) && count($basicData) > 0) {
                $printProfileUpdate = new PrintProfileModels\PrintProfile();
                $printProfileUpdate->where('xe_id', $printProfileInsertId)
                    ->update($basicData);
            }
            $printProfileGet = new PrintProfileModels\PrintProfile();
            $clonedRecord = $printProfileGet->where('xe_id', $printProfileInsertId)
                ->select('xe_id as id', 'name', 'is_disabled', 'file_name')
                ->first();
            $jsonResponse = [
                'status' => 1,
                'message' => message('Print Profile Clone', 'clone'),
                'data' => $clonedRecord,
            ];
        }

        return response(
            $response, ['data' => $jsonResponse, 'status' => $serverStatusCode]
        );
    }

    /**
     * POST: Save Print Profile records
     *
     * @param $request  Slim's Request object
     * @param $response Slim's Response object
     * @param $args     Slim's Arguments
     *
     * @author tanmayap@riaxe.com
     * @date   5 Oct 2019
     * @return Save status
     */
    public function savePrintProfile($request, $response, $args)
    {
        $jsonResponse = [
            'status' => 0,
            'message' => message('Print Profile', 'error'),
        ];
        $serverStatusCode = OPERATION_OKAY;
        $allPostPutVars = $request->getParsedBody();
        $getStoreDetails = get_store_details($request);
        if (isset($allPostPutVars['data']) && $allPostPutVars['data'] != "") {
            $getAllFormData = json_clean_decode($allPostPutVars['data'], true);
            DB::beginTransaction();
            // Step1: Process Profile Data
            $getResponseProfileData = $this->_ProcessProfileData($request, $response, $args, $getAllFormData['profile'], 'save');
            // Last Insert ID
            $lastInsertedId = $getResponseProfileData['last_inserted_id'];
            if (!empty($lastInsertedId) && $lastInsertedId > 0) {
                // Step2: Process Feature Data
                $this->_ProcessFeatureData(
                    $request, $response,
                    $getAllFormData['features_data'],
                    $lastInsertedId,
                    'save'
                );

                // Step3: Process Assets Data
                $this->_ProcessAssetsData(
                    $request, $response,
                    $getAllFormData['assets_data'],
                    $lastInsertedId,
                    'save'
                );

                // Step4: Process VDP Data
                $this->_ProcessVdpData(
                    $request, $response,
                    $getAllFormData['vdp_data'],
                    $lastInsertedId
                );
                // Step5: Process Laser Engrave data
                $this->_ProcessEngraveData(
                    $request, $response,
                    $getAllFormData['laser_engrave_data'],
                    $lastInsertedId
                );
                // Step6: Process Image Data
                $this->_ProcessImageData(
                    $request, $response,
                    $getAllFormData['image_data'],
                    $lastInsertedId
                );
                // Step7: Process Color Data
                $this->_ProcessColorData(
                    $request, $response,
                    $getAllFormData['color_data'],
                    $lastInsertedId
                );
                // Step8: Process Order Data
                $this->_ProcessOrderData(
                    $request, $response,
                    $getAllFormData['order_data'],
                    $lastInsertedId
                );

                // Set Publish Status
                $printProfilePublish = [
                    'is_draft' => $getAllFormData['is_draft'],
                ];
                try {
                    $draftUpdateProfile = new PrintProfileModels\PrintProfile();
                    $draftUpdateProfile->where('xe_id', $lastInsertedId)
                        ->update($printProfilePublish);
                    // If the json file was created, then commit the last action
                    if ($this->createJsonFile($request, $response, $lastInsertedId)) {
                        DB::commit();
                        $jsonResponse = [
                            'status' => 1,
                            'print_profile_insert_id' => $lastInsertedId,
                            'message' => message('Print Profile', 'saved'),
                        ];
                    } else {
                        // If file not created for tool, then revert the action
                        DB::rollback();
                        $this->deletePrintJsonFile($lastInsertedId, $getStoreDetails['store_id']);
                        $jsonResponse['message'] = message('Print Profile', 'file_not_created');
                    }
                } catch (\Exception $e) {
                    $jsonResponse = [
                        'status' => 0,
                        'message' => message('Print Profile', 'error'),
                        'exception' => show_exception() === true
                        ? $e->getMessage() : '',
                    ];
                }
            }
        }
        return response(
            $response, ['data' => $jsonResponse, 'status' => $serverStatusCode]
        );
    }
    /**
     * Save/Update Profile section of Print Profile
     *
     * @param $request        Slim's Request object
     * @param $response       Slim's Response object
     * @param $args           Slim's Argument parameters
     * @param $getProfileData Profile Data Array
     * @param $type           Flag for save or update
     *
     * @author tanmayap@riaxe.com
     * @date   5 Oct 2019
     * @return All Print Profile List
     */
    private function _ProcessProfileData($request, $response, $args, $getProfileData = [], $type = 'save')
    {
        $getPostPutData = [];
        $profileResponse = [];
        $getStoreDetails = get_store_details($request);

        $fileUploadKey = $getProfileData['image_path'];
        $getUploadedFileName = save_file($fileUploadKey, path('abs', 'print_profile'));
        if (!empty($getUploadedFileName)) {
            $getPostPutData += [
                'file_name' => $getUploadedFileName,
            ];
        }
        // Append Store ID
        $getPostPutData['store_id'] = $getStoreDetails['store_id'];
        // Append name and description
        $getPostPutData += [
            'name' => $getProfileData['name'],
            'description' => $getProfileData['description'],
        ];
        if (!empty($type) && $type == 'save') {
            // Save rcords
            $saveProfile = new PrintProfileModels\PrintProfile($getPostPutData);
            if ($saveProfile->save()) {
                $printProfileInsertId = $saveProfile->xe_id;
                $profileResponse = [
                    'status' => 1,
                    'last_inserted_id' => $saveProfile->xe_id,
                ];
            }
        } else {
            // Update records
            $printProfileUpdateId = isset($args['id']) ? $args['id'] : '';
            $printProfileGet = new PrintProfileModels\PrintProfile();
            $printProfileUpdateInit = $printProfileGet->where('xe_id', $printProfileUpdateId);
            if ($printProfileUpdateInit->count() > 0) {
                try {
                    $printProfileUpdateInit->update($getPostPutData);
                    $printProfileInsertId = $printProfileUpdateId;
                    $profileResponse = [
                        'status' => 1,
                        'last_inserted_id' => $printProfileUpdateId,
                    ];
                } catch (\Exception $e) {
                    $profileResponse = [
                        'status' => 0,
                        'message' => message('Profile', 'exception'),
                        'exception' => show_exception() === true ? $e->getMessage() : '',
                    ];
                }
            } else {
                $profileResponse = [
                    'status' => 0,
                    'last_inserted_id' => 0,
                    'message' => message('Print Profile', 'not_found'),
                ];
            }
        }
        return $profileResponse;
    }
    /**
     * Save/Update Fetaure-and-PrintProfile relational records of Print Profile
     *
     * @param $request         Slim's Request object
     * @param $response        Slim's Response object
     * @param $getFeaturesData Feature Data Array
     * @param $printProfileId  Print Profile Last id
     * @param $type            Flag for save or update
     *
     * @author tanmayap@riaxe.com
     * @date   5 Oct 2019
     * @return All Print Profile List
     */
    private function _ProcessFeatureData($request, $response, $getFeaturesData, $printProfileId, $type = 'save')
    {
        $savePrintProfileFeature = $featureResponse = [];
        if (!empty($getFeaturesData['features']) && count($getFeaturesData['features']) > 0) {
            $getSelectedFeatures = $getFeaturesData['features'];

            foreach ($getSelectedFeatures as $key => $feature) {
                $savePrintProfileFeature[$key] = [
                    'print_profile_id' => $printProfileId,
                    'feature_id' => $feature,
                ];
            }
            // Clean records before process while updating
            if (!empty($type) && $type == 'update') {
                $profileFeatureRelDel = new PrintProfileModels\PrintProfileFeatureRel();
                $profileFeatureRelDel->where(['print_profile_id' => $printProfileId])->delete();
            }
            $profileFeatureRelIns = new PrintProfileModels\PrintProfileFeatureRel();
            if ($profileFeatureRelIns->insert($savePrintProfileFeature)) {
                $featureResponse = [
                    'status' => 1,
                ];
            }

        }
        return $featureResponse;
    }
    /**
     * Save/Update Assets-and-PrintProfile relational records of Print Profile
     *
     * @param $request        Slim's Request object
     * @param $response       Slim's Response object
     * @param $getAssetsData  Assets data array
     * @param $printProfileId Print Profile Last id
     * @param $type           Flag for save or update
     *
     * @author tanmayap@riaxe.com
     * @date   5 Oct 2019
     * @return Assets data save status
     */
    private function _ProcessAssetsData($request, $response, $getAssetsData, $printProfileId, $type = 'save')
    {
        $printProfileAssetsData = $assetsResponse = [];
        if (!empty($getAssetsData) && count($getAssetsData) > 0) {
            foreach ($getAssetsData as $record) {
                foreach ($record['category_id'] as $category) {
                    $printProfileAssetsData[] = [
                        'print_profile_id' => $printProfileId,
                        'asset_type_id' => $record['asset_type_id'],
                        'category_id' => $category,
                    ];
                }

                // Check if Asset type is Product then update Assoc. Products
                if (isset($record['asset_type_id']) && $record['asset_type_id'] === 1) {
                    $productsPool = $this->_GetProductsByCategory($request, $response, $record['category_id']);
                    $this->_UpdateProductPrintProfileRelations($productsPool, $printProfileId);
                }
            }
            // Clean records before process while updating
            if (!empty($type) && $type == 'update') {
                $profAssetCatRelDel = new PrintProfileModels\PrintProfileAssetsCategoryRel();
                $profAssetCatRelDel->where(['print_profile_id' => $printProfileId])
                    ->delete();
            }
            if (isset($printProfileAssetsData) && count($printProfileAssetsData) > 0) {
                $profAssetCatRelIns = new PrintProfileModels\PrintProfileAssetsCategoryRel();
                if ($profAssetCatRelIns->insert($printProfileAssetsData)) {
                    $assetsResponse = [
                        'status' => 1,
                    ];
                }
            }
            return $assetsResponse;
        } else {
            return false;
        }
    }

    /**
     * If categories are selected against a Product, then the corresp. products
     * under the selected categories will be attached automatically to print profiles
     * in Product Settings Table
     *
     * @param $productList    Product List Array
     * @param $printProfileId Print profile ID
     *
     * @author tanmayap@riaxe.com
     * @date   5 Oct 2019
     * @return Boolean
     */
    private function _UpdateProductPrintProfileRelations($productList, $printProfileId)
    {
        if (isset($printProfileId) && $printProfileId > 0
            && isset($productList) && count($productList) > 0
        ) {
            foreach ($productList as $productId) {
                // Set a default array with available parameters
                $productSettings = [
                    'product_id' => $productId,
                    'is_variable_decoration' => 0,
                    'is_ruler' => 0,
                    'is_crop_mark' => 0,
                    'is_safe_zone' => 0,
                    'crop_value' => 0.00,
                    'safe_value' => 0.00,
                    'is_3d_preview' => 0,
                    '3d_object_file' => null,
                    '3d_object' => null,
                    'scale_unit_id' => 1,
                ];
                $getProductSettingsinit = new \App\Modules\Products\Models\ProductSetting();
                $getProdSettGt = $getProductSettingsinit->where(['product_id' => $productId]);

                if ($getProdSettGt->count() == 0) {
                    $updateProductSettings = new \App\Modules\Products\Models\ProductSetting($productSettings);
                    if ($updateProductSettings->save()) {
                        $productSettingsId = $updateProductSettings->xe_id;
                    }
                } else {
                    $getProductSettingData = $getProdSettGt->select('xe_id')->first();
                    $productSettingsId = $getProductSettingData->xe_id;
                }

                $profileProdSettRel = [
                    'print_profile_id' => $printProfileId,
                    'product_setting_id' => $productSettingsId,
                ];
                $ppProdSettRelSvInit = new \App\Modules\Products\Models\PrintProfileProductSettingRel($profileProdSettRel);
                $ppProdSettRelSvInit->save();
            }
            return true;
        }
    }
    /**
     * Save/Update VDP-and-PrintProfile relational records of Print Profile
     *
     * @param $request        Slim's Request object
     * @param $response       Slim's Response object
     * @param $getVdpData     VDP data
     * @param $printProfileId Print Profile ID
     *
     * @author tanmayap@riaxe.com
     * @date   5 Oct 2019
     * @return All Print Profile List
     */
    private function _ProcessVdpData($request, $response, $getVdpData, $printProfileId)
    {
        $vdpResponse['status'] = 0;
        $saveVdpRecords = [
            'is_vdp_enabled' => $getVdpData['is_enabled'],
            'vdp_data' => $getVdpData['vdp'],
        ];
        try {
            $printProfile = new PrintProfileModels\PrintProfile();
            $printProfile->where('xe_id', $printProfileId)
                ->update($saveVdpRecords);
            $vdpResponse['status'] = 1;
        } catch (\Exception $e) {
            $vdpResponse['status'] = 0;
        }

        return $vdpResponse;
    }
    /**
     * Save/Update Engrave-and-PrintProfile relational records of Print Profile
     *
     * @param $request        Slim's Request object
     * @param $response       Slim's Response object
     * @param $getEngraveData Engrave Data
     * @param $printProfileId Print Profile ID
     *
     * @author tanmayap@riaxe.com
     * @date   5 Oct 2019
     * @return All Print Profile List
     */
    private function _ProcessEngraveData($request, $response, $getEngraveData, $printProfileId)
    {
        $engraveSaveData = [];
        $engraveImageStatus = 0;
        $engraveType = 'image';
        $engraveSurfaceInsertId = 0;

        // If any Engrave data exists in table, then first delete that old settings
        $ppEngraveSettDltInit = new PrintProfileModels\PrintProfileEngraveSetting();
        $ppEngraveSettDltInit->where(['print_profile_id' => $printProfileId])->delete();

        // Engrave Preview Image Code engrave_preview_image_path
        $engrPrvImgStatus = 0;
        $engravePreviewType = 'image';
        $engravePreviewTypeValue = "";
        if (!empty($getEngraveData['engrave_preview_image_path']) && $getEngraveData['engrave_preview_image_path'] != "") {
            $getEngravePreviewFileName = save_file($getEngraveData['engrave_preview_image_path'], path('abs', 'print_profile'));
            if (!empty($getEngravePreviewFileName) && $getEngravePreviewFileName != "") {
                $engrPrvImgStatus = 1;
                $engravePreviewType = 'image';
                $engravePreviewTypeValue = $getEngravePreviewFileName;
            }
        } else {
            $engrPrvImgStatus = 0;
            $engravePreviewType = 'color';
            $engravePreviewTypeValue = (isset($getEngraveData['engrave_preview_color_code']) && $getEngraveData['engrave_preview_color_code'] != "") ? $getEngraveData['engrave_preview_color_code'] : null;
        }

        // Engrave Image Code
        if (!empty($getEngraveData['engrave_details']['engrave_image_path'])
            && $getEngraveData['engrave_details']['engrave_image_path'] != ""
        ) {
            $getUploadedFileName = save_file(
                $getEngraveData['engrave_details']['engrave_image_path'],
                path('abs', 'print_profile')
            );
            if (!empty($getUploadedFileName) && $getUploadedFileName != "") {
                $engraveImageStatus = 1;
                $engraveType = 'image';
                $engraveTypeValue = $getUploadedFileName;
            }
        } else {
            $engraveImageStatus = 0;
            $engraveType = 'color';
            $engraveTypeValue = $getEngraveData['engrave_details']['engrave_color_code'];
        }

        /**
         * Check if the given engrave_surface_id is user defined or not. If it's
         * system defined then get it's pk and update the pk else if it's user
         * defined then create or update record and get it's pk
         */
        $isEngrSfcUsrDef = 1;
        $engrSfcUserDefKey = 0;
        $engravedSurface = new PrintProfileModels\EngravedSurface();
        $checkEngraveSurfaceGet = $engravedSurface->where(
            'xe_id', 
            $getEngraveData['engrave_details']['engrave_surface_id']
        );
        if ($checkEngraveSurfaceGet->count() > 0) {
            $checkEngraveSurfaceUserType = $checkEngraveSurfaceGet->select(
                'xe_id', 'is_user_defined'
            )
                ->first();
            if (isset($checkEngraveSurfaceUserType->is_user_defined) 
                && $checkEngraveSurfaceUserType->is_user_defined == 0
            ) {
                $isEngrSfcUsrDef = 0;
                $engrSfcUserDefKey = $checkEngraveSurfaceUserType->xe_id;
            }
        }
        // Update Engrave Settings
        $updateEngraveMaster = [
            'surface_name' => 'Custom',
            'engraved_type' => $engraveType,
            'engrave_type_value' => isset($engraveTypeValue) ? $engraveTypeValue : null,
            'shadow_direction' => isset($getEngraveData['engrave_details']['engrave_shadow']['direction']) ? $getEngraveData['engrave_details']['engrave_shadow']['direction'] : null,
            'shadow_size' => isset($getEngraveData['engrave_details']['engrave_shadow']['size']) ? $getEngraveData['engrave_details']['engrave_shadow']['size'] : null,
            'shadow_opacity' => isset($getEngraveData['engrave_details']['engrave_shadow']['opacity']) ? $getEngraveData['engrave_details']['engrave_shadow']['opacity'] : null,
            'shadow_strength' => isset($getEngraveData['engrave_details']['engrave_shadow']['strength']) ? $getEngraveData['engrave_details']['engrave_shadow']['strength'] : null,
            'shadow_blur' => isset($getEngraveData['engrave_details']['engrave_shadow']['blur']) ? $getEngraveData['engrave_details']['engrave_shadow']['blur'] : null,
            'is_user_defined' => $isEngrSfcUsrDef,
        ];
        // If image exists and Image saved and image file name returned then process
        if (!empty($engravePreviewTypeValue) && $engravePreviewTypeValue != null) {
            $updateEngraveMaster['engrave_preview_type_value'] = $engravePreviewTypeValue;
        }
        if (!empty($engravePreviewType) && $engravePreviewType != null) {
            $updateEngraveMaster['engrave_preview_type'] = $engravePreviewType;
        }
        $ppEngraveSettRecord = [
            'print_profile_id' => $printProfileId,
            //'engraved_surface_id' => $engraveSurfaceInsertId,
            'is_engraved_surface' => $getEngraveData['engrave_details']['is_enabled'],
            'is_auto_convert' => $getEngraveData['auto_convert_color']['is_enabled'],
            'auto_convert_type' => $getEngraveData['auto_convert_color']['auto_convert_id'] == '1' ? 'BW' : 'Grayscale',
            'is_engrave_image' => $engraveImageStatus,
            'is_engrave_preview_image' => isset($engrPrvImgStatus) ? $engrPrvImgStatus : null,
        ];
        $ppEngraveSettGtInit = new PrintProfileModels\PrintProfileEngraveSetting();
        $ppEngraveSettings = $ppEngraveSettGtInit->where(['print_profile_id' => $printProfileId]);
        if ($ppEngraveSettings->count() > 0) {
            // Update Existing Engrave Record
            if (isset($isEngrSfcUsrDef) && $isEngrSfcUsrDef === 1) {
                $printProfileEngraveSettings = $ppEngraveSettings->first();
                $engravedSurfaceId = $printProfileEngraveSettings->engraved_surface_id;
                if ($engravedSurfaceId > 0) {
                    // Update Engrave Settings
                    $engSfcUpdate = new PrintProfileModels\EngravedSurface();
                    $engSfcUpdate->where(
                        'xe_id', $engravedSurfaceId
                    )
                        ->update($updateEngraveMaster);
                }
            } else {
                $engravedSurfaceId = $engrSfcUserDefKey;
            }
            $ppEngraveSettRecord += [
                'engraved_surface_id' => $engravedSurfaceId,
            ];
            $profileEngSettUpdt = new PrintProfileModels\PrintProfileEngraveSetting();
            $profileEngSettUpdt->where(['print_profile_id' => $printProfileId])
                ->update($ppEngraveSettRecord);
        } else {
            // Add New Engrave Settings Record
            if (isset($isEngrSfcUsrDef) && $isEngrSfcUsrDef === 1) {
                $saveEngraveRecord = new PrintProfileModels\EngravedSurface($updateEngraveMaster);
                if ($saveEngraveRecord->save()) {
                    $engraveSurfaceInsertId = $saveEngraveRecord->xe_id;
                }
            } else {
                $engraveSurfaceInsertId = $engrSfcUserDefKey;
            }

            $ppEngraveSettRecord += [
                'engraved_surface_id' => $engraveSurfaceInsertId,
            ];
            $newEngraveSurfaceSettings = new PrintProfileModels\PrintProfileEngraveSetting($ppEngraveSettRecord);
            $newEngraveSurfaceSettings->save();
        }

        $engraveSaveData = [
            'print_profile' => [
                'is_laser_engrave_enabled' => $getEngraveData['is_laser_engrave_enabled'],
            ],
        ];

        // update engrave status
        try {
            $printProfileUpdate = new PrintProfileModels\PrintProfile();
            $printProfileUpdate->where('xe_id', $printProfileId)
                ->update($engraveSaveData['print_profile']);
            return true;
        } catch (\Exception $e) {
            return false;
        }

        return true;
    }

    /**
     * Save/Update Image Settings of Print Profile
     *
     * @param $request        Slim's Request object
     * @param $response       Slim's Response object
     * @param $getImageData   Engrave Data
     * @param $printProfileId Print Profile ID
     *
     * @author tanmayap@riaxe.com
     * @date   5 Oct 2019
     * @return Array
     */
    private function _ProcessImageData($request, $response, $getImageData, $printProfileId)
    {
        $imageResponse['status'] = 0;
        $saveImageData = [
            'image_settings' => json_encode($getImageData),
        ];
        try {
            $printProfile = new PrintProfileModels\PrintProfile();
            $printProfile->where('xe_id', $printProfileId)
                ->update($saveImageData);
            $imageResponse['status'] = 1;
        } catch (\Exception $e) {
            $imageResponse['status'] = 0;
        }

        return $imageResponse;
    }

    /**
     * Save/Update Color Settings of Print Profile
     *
     * @param $request        Slim's Request object
     * @param $response       Slim's Response object
     * @param $getColorData   Engrave Data
     * @param $printProfileId Print Profile ID
     *
     * @author tanmayap@riaxe.com
     * @date   5 Oct 2019
     * @return Array
     */
    private function _ProcessColorData($request, $response, $getColorData, $printProfileId)
    {
        $colorResponse = [];
        $saveColorData = [
            'color_settings' => json_encode($getColorData),
        ];

        $printProfileUpdate = new PrintProfileModels\PrintProfile();
        try {
            $printProfileUpdate->where('xe_id', $printProfileId)
                ->update($saveColorData);
            $colorResponse['status'] = 1;
        } catch (\Exception $e) {
            $colorResponse['status'] = 0;
        }

        return $colorResponse;
    }

    /**
     * Save/Update Order Data
     *
     * @param $request        Slim's Request object
     * @param $response       Slim's Response object
     * @param $getOrderData   Engrave Data
     * @param $printProfileId Print Profile ID
     *
     * @author tanmayap@riaxe.com
     * @date   5 Oct 2019
     * @return Array
     */
    private function _ProcessOrderData($request, $response, $getOrderData, $printProfileId)
    {
        $orderResponse = [];
        $saveOrderData = [
            'order_settings' => json_encode($getOrderData),
        ];

        $printProfileUpdate = new PrintProfileModels\PrintProfile();
        try {
            $printProfileUpdate->where('xe_id', $printProfileId)
                ->update($saveOrderData);
            $orderResponse['status'] = 1;
        } catch (\Exception $e) {
            $orderResponse['status'] = 0;
        }

        return $orderResponse;
    }

    /**
     * PUT: Update Print Profile records
     *
     * @author    tanmayap@riaxe.com
     * @date      8 Nov 2019
     * @parameter Slim default parameters
     * @response  A JSON Response
     */
    public function updatePrintProfile($request, $response, $args)
    {
        $jsonResponse = [
            'status' => 0,
            'message' => message('Print Profile', 'error'),
        ];
        $serverStatusCode = OPERATION_OKAY;

        $allPostPutVars = $this->parsePut();
        if (isset($allPostPutVars['data']) && $allPostPutVars['data'] != "") {

            $getAllFormData = json_decode($allPostPutVars['data'], true);

            // Step1: Process Profile Data
            $getResponseProfileData = $this->_ProcessProfileData(
                $request, $response, $args, $getAllFormData['profile'], 'update'
            );

            

            if (!empty($getResponseProfileData['last_inserted_id']) 
                && is_valid_var($getResponseProfileData['last_inserted_id'], 'int', 'bool')
            ) {
                // Last Insert ID
                $lastInsertedId = $getResponseProfileData['last_inserted_id'];

                // Step2: Process Feature Data
                $this->_ProcessFeatureData(
                    $request, $response,
                    $getAllFormData['features_data'],
                    $lastInsertedId,
                    'update'
                );
                // Step3: Process Assets Data
                $this->_ProcessAssetsData(
                    $request, $response,
                    $getAllFormData['assets_data'],
                    $lastInsertedId,
                    'update'
                );
                // Step4: Process VDP Data
                $this->_ProcessVdpData(
                    $request, $response,
                    $getAllFormData['vdp_data'],
                    $lastInsertedId
                );
                // Step5: Process Laser Engrave data
                $this->_ProcessEngraveData(
                    $request, $response,
                    $getAllFormData['laser_engrave_data'],
                    $lastInsertedId
                );
                // Step6: Process Image Data
                $this->_ProcessImageData(
                    $request, $response,
                    $getAllFormData['image_data'],
                    $lastInsertedId
                );
                // Step7: Process Color Data
                $this->_ProcessColorData(
                    $request, $response,
                    $getAllFormData['color_data'],
                    $lastInsertedId
                );
                // Step8: Process Order Data
                $this->_ProcessOrderData(
                    $request, $response,
                    $getAllFormData['order_data'],
                    $lastInsertedId
                );

                // Set Publish Status
                $printProfilePublish = [
                    'is_draft' => $getAllFormData['is_draft'],
                ];
                try {
                    $printProfileInit = new PrintProfileModels\PrintProfile();
                    $printProfileInit->where('xe_id', $lastInsertedId)->update($printProfilePublish);
                    if ($this->createJsonFile($request, $response, $lastInsertedId)) {
                        $jsonResponse = [
                            'status' => 1,
                            'print_profile_insert_id' => $lastInsertedId,
                            'message' => message('Print Profile', 'updated'),
                        ];
                    }
                } catch (\Exception $e) {
                    $jsonResponse['exception'] = show_exception() === true ? $e->getMessage() : '';
                }
            }
        }

        return response(
            $response, ['data' => $jsonResponse, 'status' => $serverStatusCode]
        );
    }

    /**
     * GET: Get Single Print Profile Record
     *
     * @author    tanmayap@riaxe.com
     * @date      8 Nov 2019
     * @parameter Slim default parameters
     * @response  A JSON Response
     */
    public function getSinglePrintProfile($request, $response, $args)
    {
        $serverStatusCode = OPERATION_OKAY;
        $jsonResponse = [];
        $printProfileRespRecord = [];
        $printProfileId = to_int($args['id']);
        $getStoreDetails = get_store_details($request);

        $printProfileInit = new PrintProfileModels\PrintProfile();
        $getPrintProfileDetails = $printProfileInit->where(
            [
                'xe_id' => $printProfileId,
                'store_id' => $getStoreDetails['store_id'],
            ]
        )
            ->with('features', 'assets', 'engraves')
            ->first();

        // Getting Profile Records feature_data
        $printProfileRespRecord['profile_data'] = [];
        $printProfileRespRecord['image_data'] = [];
        $printProfileRespRecord['color_data'] = [];
        $printProfileRespRecord['order_data'] = [];
        $printProfileRespRecord['vdp_data'] = [
            'is_enabled' => 0,
            'vdp' => '{}',
        ];

        // Check if print profile exist in this ID
        if (!empty($getPrintProfileDetails->xe_id)) {
            $printProfileRespRecord['profile_data'] = [
                'name' => $getPrintProfileDetails->name,
                'description' => $getPrintProfileDetails->description,
                'image_path' => $getPrintProfileDetails->file_name_url,
                'thumbnail' => $getPrintProfileDetails->thumbnail,
            ];

            $printProfileRespRecord['vdp_data'] = [
                'is_enabled' => $getPrintProfileDetails->is_vdp_enabled,
                'vdp' => isset($getPrintProfileDetails->is_vdp_enabled)
                ? $getPrintProfileDetails->is_vdp_enabled : '{}',
            ];

            $printProfileRespRecord['image_data'] = json_clean_decode(
                $getPrintProfileDetails->image_settings, true
            );
            $printProfileRespRecord['color_data'] = json_clean_decode(
                $getPrintProfileDetails->color_settings, true
            );
            $printProfileRespRecord['order_data'] = json_clean_decode(
                $getPrintProfileDetails->order_settings, true
            );
        }

        // Section : Order Data Allowed Format List will be appended to
        // Gather Selected Order's Allowed format ids
        $selectedOrderFormats = [];
        if (!empty($printProfileRespRecord['order_data']['allowed_order_format']) 
            && is_valid_array($printProfileRespRecord['order_data']['allowed_order_format'])
        ) {
            $selectedOrderFormats = $printProfileRespRecord['order_data']['allowed_order_format'];
        }
        $selectedOrderFormatList = $this->_GetAllowedFormats($selectedOrderFormats, 'order');

        $printProfileRespRecord['order_data']['allowed_order_formats'] = $selectedOrderFormatList;
        // Remove the key as this key is utilized
        unset($printProfileRespRecord['order_data']['allowed_order_format']);

        // Section : Image Data
        // Get Allowed Formats and set which formats are selected
        $selectedAllowedFormats = [];
        // Gather Selected Order's Allowed format ids
        if (!empty($printProfileRespRecord['image_data']['allowed_image_format']) 
            && is_valid_array($printProfileRespRecord['image_data']['allowed_image_format'])
        ) {
            $selectedAllowedFormats = $printProfileRespRecord['image_data']['allowed_image_format'];
        }
        $allowedFormatList = $this->_GetAllowedFormats($selectedAllowedFormats, 'image');
        // Replace selected ID array with Readable formatted array
        $printProfileRespRecord['image_data']['allowed_image_format'] = $allowedFormatList;

        // Collect all feature ids for checking for selected property
        $featureRelations = [];
        if (!empty($getPrintProfileDetails->features) 
            && is_valid_array($getPrintProfileDetails->features->toArray())
        ) {
            $featureRelations = $getPrintProfileDetails->features->toArray();
        }

        // Looping thorugh Feature Types
        $printProfileRespRecord['feature_data'] = $this->_GetPrintProfileFeatures($featureRelations);

        // Section : Assets Data
        // Gather Category lists of specified Asset types
        $printProfileRespRecord['assets_data'] = [];
        $assetTypeInit = new PrintProfileModels\AssetType();
        $getAssetsList = $assetTypeInit->whereIn(
            'slug', $this->_assetsSlugList
        );
        
        if ($getAssetsList->count() > 0) {
            $getAssets = $getAssetsList->get();
            foreach ($getAssets as $assetKey => $asset) {
                $printProfileRespRecord['assets_data'][$assetKey] = [
                    'id' => $asset->xe_id,
                    'name' => $asset->name,
                    'slug' => $asset->slug,
                ];
                $getCategoryList = $this->getAssetCategories(
                    $request, $response, $asset->slug
                );
                foreach ($getCategoryList as $categoryKey => $category) {
                    if (!empty($getPrintProfileDetails->assets)) {
                        $relationalAssetsList = $getPrintProfileDetails->assets->toArray();
                        $getSelectedStatus = $this->_CheckIfCategorySelected(
                            $relationalAssetsList, 
                            $asset->xe_id, 
                            $category['id']
                        );
                        $category['is_selected'] = to_int($getSelectedStatus);
                    }
                    $printProfileRespRecord['assets_data'][$assetKey]['categories'][$categoryKey] = $category;
                }
            }
        }

        // Section : Laser Engrave Data
        $isLaserEngraveEnabled = 0;
        if (!empty($getPrintProfileDetails->is_laser_engrave_enabled) 
            && is_valid_var(
                $getPrintProfileDetails->is_laser_engrave_enabled, 'int', 'bool'
            )
        ) {
            $isLaserEngraveEnabled = $getPrintProfileDetails->is_laser_engrave_enabled;
        }
        $engraveDetails = [];
        if (!empty($getPrintProfileDetails->engraves)) {
            $engraveDetails = $getPrintProfileDetails->engraves->toArray();
        }

        $printProfileRespRecord['engrave'] = $this->_GetPrintProfileEngraves(
            $engraveDetails, $isLaserEngraveEnabled
        );

        $printProfileRespRecord['is_draft'] = !empty($getPrintProfileDetails->is_draft) 
            ? is_valid_var($getPrintProfileDetails->is_draft, 'int', 'int') : 0;
        $printProfileRespRecord['is_disabled'] = !empty($getPrintProfileDetails->is_disabled) 
            ? is_valid_var($getPrintProfileDetails->is_disabled, 'int', 'int') : 0;
        $printProfileRespRecord['is_image_magick_enabled'] = to_int(image_magick_status());

        $jsonResponse = $printProfileRespRecord;
        if (isset($args['return_type']) && $args['return_type'] == 'array') {
            return $printProfileRespRecord;
        }

        return response($response, ['data' => $jsonResponse, 'status' => $serverStatusCode]);
    }
    /**
     * Get Products according to the Category, so that these selected products
     * will be updated against Print Profiles
     *
     * @param $categoryIds Slim's Request object
     *
     * @author tanmayap@riaxe.com
     * @date   5 Oct 2019
     * @return Array
     */
    private function _GetProductsByCategory($request, $response, $categoryIds = null)
    {
        $allProductIds = [];
        if (isset($categoryIds) && count($categoryIds) > 0) {
            $getCategoryList = implode(',', $categoryIds);
            $getStoreProducts = $this->getProducts($request, $response, ['categories' => $getCategoryList]);
            // In case of simple Products
            if (!empty($getStoreProducts['data']['status']) && $getStoreProducts['data']['status'] == 1) {
                $allProducts = $getStoreProducts['data']['data'];
                if (!empty($allProducts) && count($allProducts) > 0) {
                    foreach ($allProducts as $key => $product) {
                        $allProductIds[] = $product['id'];
                    }
                }
            }
            // In case of Predecorators
            $getStorePredecos = $this->getProducts($request, $response, ['categories' => $getCategoryList, 'is_customize' => 1]);
            if (!empty($getStorePredecos['data']['status']) && $getStorePredecos['data']['status'] == 1) {
                $allProducts = $getStorePredecos['data']['data'];
                if (!empty($allProducts) && count($allProducts) > 0) {
                    foreach ($allProducts as $key => $product) {
                        $allProductIds[] = $product['id'];
                    }
                }
            }
            $allProductIds = array_unique($allProductIds);
        }

        return $allProductIds;
    }

    /**
     * Enable or Disable particular print profile by ID
     *
     * @param $selectedAssets Array of Selected Assets
     * @param $assetsId       Asset's Id
     * @param $categoryId     Category's Id
     *
     * @author tanmayap@riaxe.com
     * @date   5 Oct 2019
     * @return Boolean
     */
    private function _CheckIfCategorySelected($selectedAssets, $assetsId, $categoryId)
    {
        if (!empty($assetsId) && $assetsId > 0 && !empty($categoryId) && $categoryId > 0) {
            if (!empty($selectedAssets) && is_valid_array($selectedAssets)) {
                foreach ($selectedAssets as $selectedAsset) {
                    if ($selectedAsset['asset_type_id'] == $assetsId 
                        && $selectedAsset['category_id'] == $categoryId
                    ) {
                        return true;
                    }
                }
            }
        }

        return false;
    }
    /**
     * Enable or Disable particular print profile by ID
     *
     * @param $request  Slim's Request object
     * @param $response Slim's Response object
     * @param $args     Slim's Argument parameters
     *
     * @author tanmayap@riaxe.com
     * @date   5 Oct 2019
     * @return Json
     */
    public function disablePrintProfile($request, $response, $args)
    {
        $jsonResponse = [
            'status' => 1,
            'message' => message('Print Profile', 'error'),
        ];
        $serverStatusCode = OPERATION_OKAY;
        $printProfileDisableId = $args['id'];

        if (!empty($printProfileDisableId) && $printProfileDisableId > 0) {
            $printProfileInit = new PrintProfileModels\PrintProfile();
            $getProntProfileData = $printProfileInit->where(['xe_id' => $printProfileDisableId]);
            if ($getProntProfileData->count() > 0) {
                $printProfileInit = new PrintProfileModels\PrintProfile();
                $printAreaInit = $printProfileInit->find($printProfileDisableId);
                $printAreaInit->is_disabled = !$printAreaInit->is_disabled;
                if ($printAreaInit->save()) {
                    $jsonResponse = [
                        'status' => 1,
                        'message' => message('Print Profile', 'done'),
                    ];
                }
            }
        }

        return response(
            $response, ['data' => $jsonResponse, 'status' => $serverStatusCode]
        );
    }
    /**
     * Delete particular print profile by ID
     *
     * @param $request  Slim's Request object
     * @param $response Slim's Response object
     * @param $args     Slim's Argument parameters
     *
     * @author tanmayap@riaxe.com
     * @date   5 Oct 2019
     * @return Json
     */
    public function deletePrintProfile($request, $response, $args)
    {
        $jsonResponse = [
            'status' => 0,
            'message' => message('Print Profile', 'error'),
        ];

        $serverStatusCode = OPERATION_OKAY;
        $success = 0;
        $printProfileDelId = $args['id'];

        DB::beginTransaction();
        if (!empty($printProfileDelId) && $printProfileDelId > 0) {
            $printProfileGtInit = new PrintProfileModels\PrintProfile();
            $printProfileGt = $printProfileGtInit->where(
                ['xe_id' => $printProfileDelId]
            );

            if ($printProfileGt->count() > 0) {
                // Delete Engrave Surface
                $ppEngraveSettGtInit = new PrintProfileModels\PrintProfileEngraveSetting();
                $ppEngraveSettGt = $ppEngraveSettGtInit->where(
                    'print_profile_id', $printProfileDelId
                )->select('engraved_surface_id');
                if ($ppEngraveSettGt->count() > 0) {
                    $engraveSurfaceId = $ppEngraveSettGt->first()->engraved_surface_id;
                    $engraveSfcInit = new PrintProfileModels\EngravedSurface();
                    $engraveSfcGt = $engraveSfcInit->where(
                        ['xe_id' => $engraveSurfaceId, 'is_user_defined' => 1]
                    );
                    if (!empty($engraveSfcGt)) {
                        if ($engraveSfcGt->delete()) {
                            $success++;
                        }
                    }
                }

                $ppFeaturesDel = $printProfileGt->find($printProfileDelId);
                // Delete, Print Profile Feature Relations
                if ($ppFeaturesDel->features()->delete()) {
                    $success++;
                }
                $ppAssetsDel = $printProfileGt->find($printProfileDelId);
                // Delete, Print Profile Assets Relations
                if ($ppAssetsDel->assets()->delete()) {
                    $success++;
                }
                $ppEngraveDel = $printProfileGt->find($printProfileDelId);
                // Delete, Print Profile Engrave Relations
                if ($ppEngraveDel->engraves()->delete()) {
                    $success++;
                }
                $ppTemplateRelDel = $printProfileGt->find($printProfileDelId);
                // Delete, Print Profile Template Relations
                if ($ppTemplateRelDel->templates()->delete()) {
                    $success++;
                }
                $ppDecorationsRelDel = $printProfileGt->find($printProfileDelId);
                // Delete, Print Profile Template Relations
                if ($ppDecorationsRelDel->decorations()->delete()) {
                    $success++;
                }
                $ppProductRelDel = $printProfileGt->find($printProfileDelId);
                // Delete, Print Profile Product Setting Relations
                if ($ppProductRelDel->products()->delete()) {
                    $success++;
                }

                // Delete Print Profile Pricing
                $pricingTrashResponse = $this->deleteProfilePricing($printProfileDelId);
                if (!empty($pricingTrashResponse['status']) && $pricingTrashResponse['status'] == 1) {
                    $success++;
                }
            }
        }
        // Delete Print Profile
        if ($success > 0) {
            if ($printProfileGt->delete()) {
                // Delete Files from directory
                $this->deleteOldFile(
                    'print_profiles',
                    'file_name',
                    ['xe_id' => $printProfileDelId],
                    path('abs', 'print_profile')
                );
                DB::commit();
                $jsonResponse = [
                    'status' => 1,
                    'message' => message('Print Profile', 'deleted'),
                ];
            }
        } elseif ($success == 0) {
            DB::rollback();
            $jsonResponse += [
                'message' => message('Print Profile', 'reverted'),
            ];
        }

        return response(
            $response, ['data' => $jsonResponse, 'status' => $serverStatusCode]
        );
    }
    /**
     * Delete Print Profile and associated records by Print Profile ID
     *
     * @param $printProfileId Print Profile id
     *
     * @author tanmayap@riaxe.com
     * @date   5 Oct 2019
     * @return Array
     */
    public function deleteProfilePricing($printProfileId)
    {
        $jsonResponse = [
            'status' => 0,
            'message' => message('Print Profile Pricing', 'error'),
        ];
        $success = 0;
        DB::beginTransaction();
        if (!empty($printProfileId) && $printProfileId > 0) {
            $printProfDeleteInit = new PrintProfileModels\Pricing\PrintProfilePricing();
            $getPricingDetails = $printProfDeleteInit->where('print_profile_id', $printProfileId);
            if ($getPricingDetails->count() > 0) {
                $getPricingDetails = $getPricingDetails->select('xe_id')->first();
                $printProfilePricingid = $getPricingDetails->xe_id;
                $priceModuleSettInit = new PrintProfileModels\Pricing\PriceModuleSetting();
                $priceModuleSettGet = $priceModuleSettInit->where('print_profile_pricing_id', $printProfilePricingid);
                if ($priceModuleSettGet->count() > 0) {
                    $moduleSettingList = $priceModuleSettGet->get()->pluck('xe_id')->toArray();
                    // Advance Price Setting
                    $priceModuleSettInit = new PrintProfileModels\Pricing\PriceModuleSetting();
                    $modSetAdvPrcSettList = $priceModuleSettInit->where(
                        'print_profile_pricing_id', $printProfilePricingid
                    )->select('advance_price_settings_id')->first();
                    $advPriceSettId = $modSetAdvPrcSettList->advance_price_settings_id;
                    if ($advPriceSettId > 0) {
                        $advPrcSettInit = new PrintProfileModels\Pricing\AdvancePriceSetting();
                        $advPriceSettGtId = $advPrcSettInit->find($advPriceSettId);
                        if ($advPriceSettGtId->delete()) {
                            $success++;
                        }
                    }

                    // Price Tier value  & Tier White Base
                    $tierPrcInit = new PrintProfileModels\Pricing\TierPrice();
                    $tierPriceGt = $tierPrcInit->where(
                        'price_module_setting_id', $moduleSettingList
                    )->get()->pluck('xe_id');
                    if (!empty($tierPriceGt) && count($tierPriceGt) > 0) {
                        $tierWhiteBaseInit = new PrintProfileModels\Pricing\TierWhitebase();
                        $tierWhiteBaseDel = $tierWhiteBaseInit->whereIn(
                            'price_tier_value_id', $tierPriceGt
                        );
                        if ($tierWhiteBaseDel->delete()) {
                            $success++;
                        }
                        $tierPrcDelInit = new PrintProfileModels\Pricing\TierPrice();
                        $tierPrcDelInit->where('price_module_setting_id', $moduleSettingList);
                        if ($tierPrcDelInit->delete()) {
                            $success++;
                        }
                    }

                    // Price Tier Quantity Range
                    $prcTierQtyRangeInit = new PrintProfileModels\Pricing\PriceTierQuantityRange();
                    $prcTierQtyRangeInit->whereIn(
                        'price_module_setting_id', $moduleSettingList
                    );
                    if ($prcTierQtyRangeInit->count() > 0) {
                        if ($prcTierQtyRangeInit->delete()) {
                            $success++;
                        }
                    }

                    // Delete Price Default Setting
                    $priceDefSettInit = new PrintProfileModels\Pricing\PriceDefaultSetting();
                    $priceDefSettGt = $priceDefSettInit->whereIn(
                        'price_module_setting_id', $moduleSettingList
                    );
                    if ($priceDefSettGt->count() > 0) {
                        if ($priceDefSettGt->delete()) {
                            $success++;
                        }
                    }

                    // Delete Price Module Settings
                    $prcModuleSettDelInit = new PrintProfileModels\Pricing\PriceModuleSetting();
                    $prcModuleSettDel = $prcModuleSettDelInit->where(
                        'print_profile_pricing_id', $printProfilePricingid
                    );
                    if ($prcModuleSettDel->count() > 0) {
                        if ($prcModuleSettDel->delete()) {
                            $success++;
                        }
                    }

                }
                // Delete Print Profile
                if ($success > 0) {
                    if ($getPricingDetails->delete()) {
                        DB::commit();
                        $jsonResponse = [
                            'status' => 1,
                            'message' => message('Print Profile Pricing', 'deleted'),
                        ];
                    } else {
                        DB::rollback();
                        $jsonResponse += [
                            'message' => message('Print Profile Pricing', 'reverted'),
                        ];
                    }
                }
            }
        }

        return $jsonResponse;
    }
    /**
     * Post : Assign Print Profiles to Category
     * Used in all Asset's Manage Category Section
     *
     * @param $request  Slim's Request object
     * @param $response Slim's Response object
     *
     * @author tanmayap@riaxe.com
     * @date   5 Oct 2019
     * @return All Print Profile List
     */
    public function assignCategoryToPrintProfile($request, $response)
    {
        $jsonResponse = [
            'status' => 0,
            'message' => message('Print Profile Assign', 'error'),
        ];
        $serverStatusCode = OPERATION_OKAY;
        $allPostPutVars = $request->getParsedBody();

        $getAssetTypeId = $this->assetsTypeId($allPostPutVars['asset_slug']);
        if ((!empty($getAssetTypeId['asset_type_id']) && $getAssetTypeId['asset_type_id'] > 0)
            && (!empty($allPostPutVars['category_id']) && $allPostPutVars['category_id'] > 0)
        ) {
            $printProfileIds = json_clean_decode($allPostPutVars['print_profile_id'], true);
            $saveRelation = [];
            foreach ($printProfileIds as $printProfilekey => $printProfileId) {
                $saveRelation[$printProfilekey] = [
                    'print_profile_id' => $printProfileId,
                    'asset_type_id' => $getAssetTypeId['asset_type_id'],
                    'category_id' => $allPostPutVars['category_id'],
                ];
            }
            // First Delete the Old relationship if any with this combinations
            $ppAssetCatRelGtInit = new PrintProfileModels\PrintProfileAssetsCategoryRel();
            $ppAssetCatRelGtInit->where(
                [
                    'asset_type_id' => $getAssetTypeId['asset_type_id'],
                    'category_id' => $allPostPutVars['category_id'],
                ]
            )->delete();
            // Save all Relation combinations
            $ppAssetCatRelSvInit = new PrintProfileModels\PrintProfileAssetsCategoryRel();
            if ($ppAssetCatRelSvInit->insert($saveRelation)) {
                $jsonResponse = [
                    'status' => 1,
                    'message' => message('Print Profile Assign', 'saved'),
                ];
            }
        }

        return response(
            $response, ['data' => $jsonResponse, 'status' => $serverStatusCode]
        );
    }

    /**
     * Write the Pront Profile details to the Json File and save it inside
     * Settings folder according to Store ID
     *
     * @param $request        Slim's Request object
     * @param $response       Slim's Response object
     * @param $printProfileId Print profile ID
     *
     * @author debashrib@riaxe.com
     * @date   20 Jan 2020
     * @return 1 or 0
     */
    public function createJsonFile($request, $response, $printProfileId)
    {
        $responseStatus = 0;
        $getStoreDetails = get_store_details($request);
        $jsonFileLocation  = path('abs', 'setting');
        $jsonFileLocation .= 'stores/' . $getStoreDetails['store_id'];
        $jsonFileLocation .= '/print_profile';
        // Create directory if not exists
        create_directory($jsonFileLocation);
        $jsonFilePath = $jsonFileLocation . '/' . $printProfileId . '.json';

        // Get print Profile Data as per the Print Profile ID
        $printProfileData = $this->getSinglePrintProfile(
            $request, $response, [
                'id' => $printProfileId,
                'return_type' => 'array',
            ]
        );
        if (!empty($printProfileData)) {
            // Unset extra keys from an array
            unset(
                $printProfileData['assets_data'],
                $printProfileData['is_draft'],
                $printProfileData['is_disabled'],
                $printProfileData['is_image_magick_enabled']
            );
            // Get Pricing Records as per the Print Profile ID
            $printProfilePriceInit = new PricingController();
            $pricingData = $printProfilePriceInit->getPricingDetails(
                $request, $response, [
                    'id' => $printProfileId,
                    'return_type' => 'array',
                ]
            );
            $printProfileData['print_profile_pricing'] = $pricingData;
            $jsonData = json_encode($printProfileData, JSON_PRETTY_PRINT);
            $responseStatus = write_file($jsonFilePath, $jsonData);
        }

        return $responseStatus;
    }
    /**
     * Delete Json file which was generated during file save
     *
     * @param $profileId Print Profile Primary Key
     * @param $storeId   Associated Store's id
     *
     * @author tanmayap@riaxe.com
     * @author debashrib@riaxe.com
     * @date   22 Jan 2020
     * @return array
     */
    protected function deletePrintJsonFile($profileId, $storeId)
    {
        if (!empty($profileId) && $profileId > 0 
            && !empty($storeId) && $storeId > 0
        ) {
            $jsonFileName = path('abs', 'setting');
            $jsonFileName .= 'stores/' . $storeId;
            $jsonFileName .= '/print_profile' . '/' . $profileId . '.json';
            if (file_exists($jsonFileName)) {
                return delete_file($jsonFileName);
            }
        }
        return false;
    }

    /**
     * Get the order/images allowed formats for Print Profile
     *
     * @param $selectedFormats Array of selected format
     * @param $type            order/image
     *
     * @author tanmayap@riaxe.com
     * @author debashrib@riaxe.com
     * @date   22 Jan 2020
     * @return array
     */
    private function _GetAllowedFormats($selectedFormats, $type)
    {
        $allowedFormats = [];
        $selectedMasterList = [];
        if (!empty($selectedFormats) 
            && is_valid_array($selectedFormats)
        ) {
            $profileAllowedFmts = new PrintProfileModels\PrintProfileAllowedFormat();
            $printProfileAllowedFormatsInit = $profileAllowedFmts->where(
                ['type' => $type]
            );
            
            if ($printProfileAllowedFormatsInit->count() > 0) {
                $allowedFormats = $printProfileAllowedFormatsInit->select(
                    'xe_id as id', 'name', 'type', 'is_disabled'
                )
                    ->get();
            }
            
            if (!empty($allowedFormats)) {
                foreach ($allowedFormats as $allFmtKey => $allowedFormat) {
                    $selectedMasterList[$allFmtKey] = [
                        'id' => $allowedFormat['id'],
                        'name' => $allowedFormat['name'],
                        'type' => $allowedFormat['type'],
                        'is_disabled' => $allowedFormat['is_disabled'],
                        'is_selected' => count($selectedFormats) > 0 ? 
                            to_int(
                                in_array($allowedFormat['id'], $selectedFormats)
                            )
                            : 0,
                    ];
                }
            }
        }
        
        return $selectedMasterList;
    }
    /**
     * Get the features for Print Profile
     *
     * @param $selectedFeatures array of selected features
     *
     * @author tanmayap@riaxe.com
     * @author debashrib@riaxe.com
     * @date   22 Jan 2020
     * @return array
     */
    private function _GetPrintProfileFeatures($selectedFeatures)
    {
        $featureType = new PrintProfileModels\FeatureType();
        $getAllFeatureTypeList = $featureType->with('feature')->get();
        $getSelectedFeatures = [];
        // Collect all feature ids for checking for selected property
        // debug($selectedFeatures->toArray(), true);
        if (!empty($selectedFeatures) && is_valid_array($selectedFeatures)) {
            $getSelectedFeatures = array_column($selectedFeatures, 'feature_id');
        }
        // Looping thorugh Feature Types
        $printProfileRespRecord = [];
        foreach ($getAllFeatureTypeList as $featureTypeKey => $featureType) {
            $printProfileRespRecord[$featureTypeKey] = [
                'id' => $featureType->xe_id,
                'name' => $featureType->name,
            ];
            // Looping thorugh Features
            $getFeatureList = [];
            foreach ($featureType->feature as $featureKey => $feature) {
                $getFeatureList[$featureKey] = [
                    'id' => $feature->xe_id,
                    'name' => $feature->name,
                    'is_selected' => in_array($feature->xe_id, $getSelectedFeatures) 
                        ? 1 : 0,
                ];

            }
            $printProfileRespRecord[$featureTypeKey]['features'] = $getFeatureList;
        }

        return $printProfileRespRecord;
    }

    /**
     * Get the Engraves for Print Profile
     *
     * @param $engraves         Array of engraves
     * @param $isEngraveEnabled Laser Engrave status
     *
     * @author tanmayap@riaxe.com
     * @author debashrib@riaxe.com
     * @date   22 Jan 2020
     * @return array
     */
    private function _GetPrintProfileEngraves($engraves, $isEngraveEnabled)
    {
        $printProfileRespRecord = [];
        // Section : Laser Engrave Data
        $engraveSfcInit = new PrintProfileModels\EngravedSurface();
        $engraveSurfaceMasterInit = $engraveSfcInit->where('is_user_defined', 0);
        // If Engrave Data Exists
        if (is_valid_array($engraves)) {
            // get Engrave surface list
            $profileEngraveSettInit = new PrintProfileModels\PrintProfileEngraveSetting();
            $getUserDefEngId = $profileEngraveSettInit->where('print_profile_id', $engraves['print_profile_id'])
                ->select('engraved_surface_id')
                ->first();
            $userDefEngSettId = $getUserDefEngId->engraved_surface_id;
            $engaveSurfaces = $engraveSurfaceMasterInit->orWhere('xe_id', $userDefEngSettId)->get();
            foreach ($engaveSurfaces as $engraveSurfaceKey => $engraveSurface) {
                $printProfileRespRecord['engrave_surface_list'][$engraveSurfaceKey] = [
                    'id' => $engraveSurface->xe_id,
                    'is_selected' => $engraveSurface->xe_id === $engraves['engraved_surface_id'] ? 1 : 0,
                    'surface_name' => $engraveSurface->surface_name,
                    // Shadow parameters
                    'shadow_direction' => $engraveSurface->shadow_direction,
                    'shadow_size' => $engraveSurface->shadow_size,
                    'shadow_opacity' => $engraveSurface->shadow_opacity,
                    'shadow_strength' => $engraveSurface->shadow_strength,
                    'shadow_blur' => $engraveSurface->shadow_blur,
                    'is_user_defined' => $engraveSurface->is_user_defined,
                ];
                $isEngravedTypeImage = 0;
                $engravedImagePath = null;
                $engravedImagePathThumb = null;
                $engraveColorCode = null;
                $isEngravedPreviewTypeImage = 0;
                $engravedPreviewImagePath = null;
                $engravedPreviewImagePathThumb = null;
                $engravePreviewColorCode = null;
                // Skip System Defined Data. Get into Custom Records
                if (isset($engraveSurface['is_user_defined']) && $engraveSurface['is_user_defined'] === 1) {
                    // If User Defined Data then, Fetch - Engrave Image,
                    if (isset($engraveSurface->engraved_type) && $engraveSurface->engraved_type == 'image') {
                        $isEngravedTypeImage = 1;
                        if (!empty($engraveSurface->engrave_type_value) 
                            && $engraveSurface->engrave_type_value != ''
                        ) {
                            $engravedImagePath = path('read', 'print_profile') 
                                . $engraveSurface->engrave_type_value;
                            $engravedImagePathThumb = path('read', 'print_profile') 
                                . 'thumb_' . $engraveSurface->engrave_type_value;
                        }
                    } else {
                        $engraveColorCode = $engraveSurface->engrave_type_value;
                    }
                    // Get Engrave Preview Image Details
                    if (isset($engraveSurface->engrave_preview_type) 
                        && $engraveSurface->engrave_preview_type == 'image'
                    ) {
                        $isEngravedPreviewTypeImage = 1;
                        $engravedPreviewImagePath = path('read', 'print_profile') 
                            . $engraveSurface->engrave_preview_type_value;
                        $engravedPreviewImagePathThumb = path('read', 'print_profile') 
                            . 'thumb_' . $engraveSurface->engrave_preview_type_value;
                    } else {
                        $engravePreviewColorCode = $engraveSurface->engrave_preview_type_value;
                    }
                }
            }
            // Get the list for Auto Convert List
            $printProfileRespRecord['auto_convert_list'] = [
                [
                    'id' => 1,
                    'name' => 'BW',
                    'is_selected' => $engraves['auto_convert_type'] == 'BW' ? 1 : 0,
                ], [
                    'id' => 2,
                    'name' => 'Grayscale',
                    'is_selected' => $engraves['auto_convert_type'] == 'Grayscale' ? 1 : 0,
                ],
            ];
            // Make Engrave Array
            $printProfileRespRecord += [
                'is_laser_engrave_enabled' => $isEngraveEnabled,
                'is_auto_convert_enabled' => $engraves['is_auto_convert'],
                'is_engraved_surface' => $engraves['is_engraved_surface'],
                // Set keys for engrave images
                'is_engrave_image' =>  is_valid_var($isEngravedTypeImage, 'int', 'int'),
                'engrave_image_path' => is_valid_var($engravedImagePath) ? $engravedImagePath : null,
                'engrave_image_path_thumbnail' => is_valid_var($engravedImagePathThumb) ? $engravedImagePathThumb : null,
                'engrave_color_code' => is_valid_var($engraveColorCode) ? $engraveColorCode : null,
                // Set keys for engrave preview images
                'is_engrave_preview_image' => (isset($isEngravedPreviewTypeImage) && $isEngravedPreviewTypeImage != "") 
                    ? $isEngravedPreviewTypeImage : 0,
                'engrave_preview_image_path' => is_valid_var($engravedPreviewImagePath) 
                    ? $engravedPreviewImagePath : null,
                'engrave_preview_image_path_thumbnail' => is_valid_var($engravedPreviewImagePathThumb) 
                    ? $engravedPreviewImagePathThumb : null,
                'engrave_preview_color_code' => is_valid_var($engravePreviewColorCode) 
                    ? $engravePreviewColorCode : null,
            ];
        } else {
            // If Engrave Data not exists then set a Default DataSet. Fetch only
            // System Defined Engrave Surfaces
            $engaveSurfaces = $engraveSurfaceMasterInit->get();
            foreach ($engaveSurfaces as $engraveSurfaceKey => $engraveSurface) {
                $printProfileRespRecord['engrave_surface_list'][$engraveSurfaceKey] = [
                    'id' => $engraveSurface->xe_id,
                    'surface_name' => $engraveSurface->surface_name,
                    'shadow_direction' => $engraveSurface->shadow_direction,
                    'shadow_size' => $engraveSurface->shadow_size,
                    'shadow_opacity' => $engraveSurface->shadow_opacity,
                    'shadow_strength' => $engraveSurface->shadow_strength,
                    'shadow_blur' => $engraveSurface->shadow_blur,
                    'is_user_defined' => $engraveSurface->is_user_defined,
                    'is_selected' => 0,
                ];
            }
            // Send Auto Convert List if No engrave data exists
            $printProfileRespRecord['auto_convert_list'] = [
                [
                    'id' => 1,
                    'name' => 'BW',
                    'is_selected' => 0,
                ], [
                    'id' => 2,
                    'name' => 'Grayscale',
                    'is_selected' => 0,
                ],
            ];
            // Send Engrave other key values as null or 0 if no Engrave data exist
            $printProfileRespRecord += [
                'is_laser_engrave_enabled' => 0,
                'is_auto_convert_enabled' => 0,
                'is_engraved_surface' => 0,
                'is_engrave_image' => 0,
                'engrave_image_path' => "",
                'engrave_image_path_thumbnail' => "",
                'engrave_color_code' => "",
                'is_engrave_preview_image' => 0,
                'engrave_preview_image_path' => "",
                'engrave_preview_image_path_thumbnail' => "",
                'engrave_preview_color_code' => "",
            ];
        }
        return $printProfileRespRecord;
    }
}
