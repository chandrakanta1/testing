<?php
/**
 * Manage Color Palette
 *
 * PHP version 5.6
 *
 * @category  ColorPalettes
 * @package   Assets
 * @author    Debashri Bhakat <debashrib@riaxe.com>
 * @copyright 2019-2020 Riaxe Systems
 * @license   http://www.php.net/license/3_0.txt  PHP License 3.0
 * @link      http://inkxe-v10.inkxe.io/xetool/admin
 */

namespace App\Modules\ColorPalettes\Controllers;

use App\Components\Controllers\Component as ParentController;
use App\Dependencies\Zipper as Zipper;
use App\Modules\ColorPalettes\Models\ColorPalette;
use App\Modules\ColorPalettes\Models\ColorPaletteCategory as Category;
use Intervention\Image\ImageManagerStatic as ImageManager;

/**
 * ColorPalettes Controller
 *
 * @category ColorPalettes
 * @package  Assets
 * @author   Debashri Bhakat <debashrib@riaxe.com>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://inkxe-v10.inkxe.io/xetool/admin
 */

class ColorPaletteController extends ParentController
{
    /**
     * GET: List of ColorPalettes or single ColorPalettes Details
     *
     * @param $request  Slim's Request object
     * @param $response Slim's Response object
     * @param $args     Slim's Argument parameters
     *
     * @author debashrib@riaxe.com
     * @date   05 Dec 2019
     * @return All/Single ColorPalette
     */
    public function getColors($request, $response, $args)
    {
        $serverStatusCode = OPERATION_OKAY;
        $subcatagoryIdArr = [];
        $otherFilterData = [];
        // set the common status for error
        $jsonResponse = [
            'status' => 0,
            'message' => message('Color', 'error'),
        ];
        $result = [];
        $categoryInit = new Category();
        $assetTypeArr = $categoryInit->AssetsTypeId();
        if (!empty($assetTypeArr) && $assetTypeArr['status'] == 1) {
            $assetTypeId = $assetTypeArr['asset_type_id'];

            if (isset($args) && count($args) > 0 && $args['id'] != '') {
                //For single Color data
                $colorPaletteInit = new ColorPalette();
                $processedColors = $colorPaletteInit->join('categories', 'color_palettes.category_id', '=', 'categories.xe_id')
                    ->select('color_palettes.xe_id', 'color_palettes.category_id', 'color_palettes.subcategory_id', 'color_palettes.name', 'color_palettes.price', 'color_palettes.value', 'color_palettes.hex_value', 'categories.name as cat_name')
                    ->where('color_palettes.xe_id', '=', $args['id'])->orderBy('xe_id', 'DESC')->get();
                $totalCounts = $processedColors->count();
                if ($totalCounts > 0) {
                    if ($processedColors[0]['cat_name'] == 'Pattern') {
                        $processedColors[0]['file_name'] = path('read', 'colorpalette') . 'thumb_' . $processedColors[0]['value'];
                    }
                    $jsonResponse = [
                        'status' => 1,
                        'data' => $processedColors,
                    ];
                }
            } else {
                // Collect all Filter columns from url
                $catagoryId = ($request->getQueryParam('cat_id') != '') ? $request->getQueryParam('cat_id') : 0;
                $subcatagoryId = (!empty($request->getQueryParam('subcat_id'))) ? $request->getQueryParam('subcat_id') : [];
                $keyword = ($request->getQueryParam('keyword') != '') ? $request->getQueryParam('keyword') : '';
                $sortBy = $request->getQueryParam('sortby');
                $order = $request->getQueryParam('order');
                $getStoreDetails = get_store_details($request);

                // For multiple Color data
                // Filter by category ID
                if (isset($catagoryId) && $catagoryId != "") {
                    $getCategory = $categoryInit->where(['xe_id' => $catagoryId, 'store_id' => $getStoreDetails['store_id']])
                        ->select('xe_id', 'name')
                        ->get();
                } else {
                    $getCategory = $categoryInit->where(['parent_id' => 0, 'asset_type_id' => $assetTypeId, 'store_id' => $getStoreDetails['store_id']])
                        ->orderBy('sort_order', 'asc')
                        ->select('xe_id', 'name')
                        ->get();
                }
                // Filter by subcatagoryId ID
                if (isset($subcatagoryId) && !empty($subcatagoryId)) {
                    $subcatagoryIdArr = json_clean_decode($subcatagoryId, true);
                }

                // Other filter data
                $otherFilterData = [
                    'keyword' => $keyword,
                    'sortBy' => $sortBy,
                    'order' => $order,
                ];

                if (isset($getStoreDetails['store_id']) && $getStoreDetails['store_id'] != '') {
                    $otherFilterData += [
                        'store_id' => $getStoreDetails['store_id'],
                    ];

                }
                $totalCounts = $getCategory->count();
                if ($totalCounts > 0) {
                    foreach ($getCategory as $catValue) {
                        $result[] = [
                            "cat_id" => $catValue['xe_id'],
                            "cat_name" => $catValue['name'],
                            "subcategory" => $this->getSubcategoryData(((!empty($subcatagoryIdArr) ? $subcatagoryIdArr : $catValue['xe_id'])), $catValue['name'], $otherFilterData),
                        ];
                    }
                    $jsonResponse = [
                        'status' => 1,
                        'data' => $result,
                    ];
                }
            }
        }
        return response(
            $response, ['data' => $jsonResponse, 'status' => $serverStatusCode]
        );
    }

    /**
     * Getting subcategory w.r.t category
     *
     * @param $subcatIds       Subcat id
     * @param $catName         category name
     * @param $otherFilterData data to filter
     *
     * @author debashrib@riaxe.com
     * @date   05 Dec 2019
     * @return array of subcategory
     */
    private function getSubcategoryData($subcatIds, $catName, $otherFilterData)
    {
        $subcategory = [];
        $categoryInit = new Category();
        if (is_array($subcatIds)) {
            $getSubcategories = $categoryInit->select('xe_id', 'name', 'parent_id')->whereIn('xe_id', $subcatIds)
                ->where('is_disable', 0)
                ->orderBy('sort_order', 'asc')
                ->get();
        } else {
            $getSubcategories = $categoryInit->select('xe_id', 'name', 'parent_id')
                ->where(['parent_id' => $subcatIds, 'is_disable' => 0])
                ->orderBy('sort_order', 'asc')
                ->get();
        }

        foreach ($getSubcategories->toArray() as $value) {
            if ($getSubcategories->count() > 0) {
                $subcategory[] = [
                    'subcat_id' => $value['xe_id'],
                    'subcat_name' => $value['name'],
                    'color_data' => $this->getColorData($value['parent_id'], $value['xe_id'], $catName, $otherFilterData),
                ];
            }
        }
        return $subcategory;
    }

    /**
     * Getting calor data w.r.t category and subcategory
     *
     * @param $catId           category id
     * @param $subcatId        Subcat id
     * @param $catName         category name
     * @param $otherFilterData data to filter
     *
     * @author debashrib@riaxe.com
     * @date   05 Dec 2019
     * @return array of color data
     */
    private function getColorData($catId, $subcatId, $catName, $otherFilterData)
    {
        $colordata = [];
        $colorPaletteInit = new ColorPalette();
        $getColor = $colorPaletteInit->select('xe_id', 'name', 'price', 'value', 'hex_value')->where(['category_id' => $catId, 'subcategory_id' => $subcatId]);
        if (isset($otherFilterData) && $otherFilterData['keyword'] != "") {
            $getColor->where(
                function ($query) use ($otherFilterData) {
                    $query->where('name', 'LIKE', '%' . $otherFilterData['keyword'] . '%')
                        ->orwhere('hex_value', 'LIKE', '%' . $otherFilterData['keyword'] . '%');
                }
            );
        }

        if (isset($otherFilterData) && $otherFilterData['store_id'] > 0) {
            $getColor->where('store_id', $otherFilterData['store_id']);
        }

        if (isset($otherFilterData) && $otherFilterData['sortBy'] != "" && $otherFilterData['order'] != "") {
            $getColor->orderBy($otherFilterData['sortBy'], $otherFilterData['order']);
        } else {
            $getColor->orderBy('xe_id', 'DESC');
        }

        $getColorRecords = $getColor->get();
        if (!empty($getColorRecords)) {
            foreach ($getColorRecords as $value) {
                if ($catName == 'Pattern') {
                    $colorValue = path('read', 'colorpalette') . 'thumb_' . $value->value;
                } else {
                    $colorValue = $value->value;
                }
                if ($getColor->count() > 0) {
                    $colordata[] = [
                        'id' => $value->xe_id,
                        'name' => $value->name,
                        'price' => $value->price,
                        'value' => $colorValue,
                        'hex_value' => $value->hex_value,
                    ];
                }
            }
        }
        return $colordata;
    }

    /**
     * POST: Save ColorPalette Data
     *
     * @param $request  Slim's Request object
     * @param $response Slim's Response object
     *
     * @author debashrib@riaxe.com
     * @date   06 Dec 2019
     * @return json response wheather data is saved or any error occured
     */
    public function saveColors($request, $response)
    {
        $serverStatusCode = OPERATION_OKAY;
        $allPostPutVars = $request->getParsedBody();
        $getStoreDetails = get_store_details($request);
        $jsonResponse = [
            'status' => 0,
            'message' => message('Color', 'error'),
        ];
        $fileExtension = '';

        if (isset($allPostPutVars) && count($allPostPutVars) !== "") {
            // If any file exist then upload
            $uploadedFiles = $request->getUploadedFiles();
            // get uploaded file extension
            if ($uploadedFiles) {
                $fileExtension = pathinfo($uploadedFiles['upload']->getClientFilename(), PATHINFO_EXTENSION);
                if (!empty($uploadedFiles['upload']->file) && $uploadedFiles['upload']->file != "") {
                    $fileName = save_file('upload', path('abs', 'colorpalette'));
                }
            }
            $storeId = $allPostPutVars['store_id'] = $getStoreDetails['store_id'];
            $catId = $allPostPutVars['category_id'];
            $subcatId = $allPostPutVars['subcategory_id'];

            if ($fileExtension == 'csv') {
                try {
                    if ($fileName != '') {
                        $rawCsvFilePath = path('abs', 'colorpalette') . $fileName;
                        // function to save data from csv
                        $colorLastInsertId = $this->saveCSVData($storeId, $catId, $subcatId, $rawCsvFilePath);
                    }
                } catch (\Exception $e) {
                    $serverStatusCode = EXCEPTION_OCCURED;
                    $jsonResponse = [
                        'status' => 0,
                        'message' => message('Category', 'exception'),
                        'exception' => show_exception() === true ? $e->getMessage() : '',
                    ];
                }
            } else if ($fileExtension == 'zip') {
                try {
                    if ($fileName != '') {
                        $rawZipFilePath = path('abs', 'colorpalette') . $fileName;
                        // function to save data from zip
                        $colorLastInsertId = $this->saveZipData($storeId, $catId, $subcatId, $rawZipFilePath);
                    }
                } catch (\Exception $e) {
                    $serverStatusCode = EXCEPTION_OCCURED;
                    $jsonResponse = [
                        'status' => 0,
                        'message' => message('ColorPalette', 'exception'),
                        'exception' => show_exception() === true ? $e->getMessage() : '',
                    ];
                }
            } else {
                try {
                    // In the case of Patter (category)
                    if ($fileExtension != '') {
                        if ($fileName != '') {
                            $getUploadedFileName = save_file('upload', path('abs', 'colorpalette'));
                            $allPostPutVars['value'] = $getUploadedFileName;
                        }
                    }
                    $color = new ColorPalette($allPostPutVars);
                    $color->save();
                    $colorLastInsertId = $color->xe_id;
                } catch (\Exception $e) {
                    $serverStatusCode = EXCEPTION_OCCURED;
                    $jsonResponse = [
                        'status' => 0,
                        'message' => message('Color', 'exception'),
                        'exception' => show_exception() === true ? $e->getMessage() : '',
                    ];
                }
            }
        }

        if (isset($colorLastInsertId) && $colorLastInsertId > 0) {
            $jsonResponse = [
                'status' => 1,
                'message' => message('Color', 'saved'),
                'color_id' => $colorLastInsertId,
            ];
        }
        return response(
            $response, ['data' => $jsonResponse, 'status' => $serverStatusCode]
        );
    }

    /**
     * Saving data from csv file
     *
     * @param $storeId  Store_Id
     * @param $catId    Category_Id
     * @param $subcatId Subcategory_Id
     * @param $filePath csv file path
     *
     * @author debashrib@riaxe.com
     * @date   06 Dec 2019
     * @return last inserted id
     */
    private function saveCSVData($storeId, $catId, $subcatId, $filePath)
    {
        $categoryInit = new Category();
        $getCategory = $categoryInit->find($catId);
        $catName = $getCategory->name;
        $file = fopen($filePath, "r");
        $csvData = [];
        $loop = 0;
        $colorLastInsertId = 0;
        while (($column = fgetcsv($file, 10000, ",")) !== false) {
            if ($loop != 0) {
                // Creating a Associative array which contains the Database row for inserting into the DB
                if ($catName == 'CMYK') {
                    $value = $column[3] . ',' . $column[4] . ',' . $column[5] . ',' . $column[6];
                } else if ($catName == 'RGB') {
                    $value = $column[3] . ',' . $column[4] . ',' . $column[5];
                } else if ($catName == 'Pantone' || $catName == 'Embroidery Thread') {
                    $value = $column[3];
                }
                $csvData[$loop] = [
                    'store_id' => $storeId,
                    'category_id' => $catId,
                    'subcategory_id' => $subcatId,
                    'name' => $column[0],
                    'hex_value' => $column[1],
                    'price' => (isset($column[2]) && $column[2] != "") ? $column[2] : 0,
                    'value' => $value,
                ];
                // Save Color Data
                $color = new ColorPalette($csvData[$loop]);
                $color->save();
                $colorLastInsertId = $color->xe_id;
            }
            $loop++;
        }
        fclose($file);
        if (isset($colorLastInsertId) && $colorLastInsertId > 0) {
            if (file_exists($filePath)) {
                try {
                    unlink($filePath);
                } catch (Exception $e) {
                    $e->getMessage();
                }
            }
        }
        return $colorLastInsertId;
    }

    /**
     * Saving data from zip file
     *
     * @param $storeId     Store_Id
     * @param $catId       Category_Id
     * @param $subcatId    Subcategory_Id
     * @param $zipFilePath zip file path
     *
     * @author debashrib@riaxe.com
     * @date   07 Dec 2019
     * @return last inserted id
     */
    private function saveZipData($storeId, $catId, $subcatId, $zipFilePath)
    {
        $categoryInit = new Category();
        $getCategory = $categoryInit->find($catId);
        $catName = $getCategory->name;
        $colorLastInsertId = 0;
        if ($catName == 'Pattern') {
            $imagesPath = path('abs', 'colorpalette');
            $zipExtractedPath = $imagesPath . uniqid('zipextract' . date('Ymdhis') . '-');
            if (!is_dir($zipExtractedPath)) {
                // Check if the Dir exists else create the directory with 777 permission
                mkdir($zipExtractedPath, 0777, true);
            }
            shell_exec('chmod -R 777 ' . $zipExtractedPath);
            $zip = new Zipper();
            $zipStatus = $zip->make($zipFilePath);
            if ($zipStatus) {
                $zip->extractTo($zipExtractedPath);
            }

            $rawCsvFilePathArr = glob($zipExtractedPath . "/*.csv");
            $rawCsvFilePath = $rawCsvFilePathArr[0];

            if ($rawCsvFilePath != '') {
                $file = fopen($rawCsvFilePath, "r");
                $csvData = [];
                $loop = 0;
                while (($column = fgetcsv($file, 10000, ",")) !== false) {
                    if ($loop != 0) {
                        $imagePathArr = glob($zipExtractedPath . "/" . $column[2]);
                        $patterImgPath = $imagePathArr[0];
                        $patternImg = getRandom() . $column[2];
                        $newPatterImgPath = $imagesPath . $patternImg;
                        // copy patter image file from extreacted folder to root folder
                        if (copy($patterImgPath, $newPatterImgPath)) {
                            // creating thumb file
                            $convertToSize = [100];
                            $imageManagerInit = new ImageManager();
                            $img = $imageManagerInit->make($newPatterImgPath);
                            foreach ($convertToSize as $dimension) {
                                $img->resize($dimension, $dimension);
                                $img->save($imagesPath . 'thumb_' . $patternImg);
                            }

                            // Creating a Associative array which contains the Database row for inserting into the DB
                            $csvData[$loop] = [
                                'store_id' => $storeId,
                                'category_id' => $catId,
                                'subcategory_id' => $subcatId,
                                'name' => $column[0],
                                'price' => (isset($column[1]) && $column[1] != "") ? $column[1] : 0,
                                'value' => $patternImg,
                            ];
                            // Save Color Data
                            $color = new ColorPalette($csvData[$loop]);
                            $color->save();
                            $colorLastInsertId = $color->xe_id;
                        }
                    }
                    $loop++;
                }
            }
            $zipper->close();
            fclose($file);
            if (isset($colorLastInsertId) && $colorLastInsertId > 0) {
                // delete zip file
                if (file_exists($zipFilePath)) {
                    unlink($zipFilePath);
                }
                // remove extracted zip folder with file inside it
                if (file_exists($zipExtractedPath)) {
                    array_map('unlink', glob("$zipExtractedPath/*.*"));
                    rmdir($zipExtractedPath);
                }
            }
        }
        return $colorLastInsertId;
    }

    /**
     * PUT: Update a single ColorPalette
     *
     * @param $request  Slim's Request object
     * @param $response Slim's Response object
     * @param $args     Slim's Argument parameters
     *
     * @author debashrib@riaxe.com
     * @date   07 Dec 2019
     * @return json response wheather data is updated or not
     */
    public function updateColor($request, $response, $args)
    {
        $serverStatusCode = OPERATION_OKAY;
        $updateData = $this->parsePut();
        $jsonResponse = [
            'status' => 0,
            'message' => message('Color', 'error'),
        ];
        $getStoreDetails = get_store_details($request);

        if (isset($args['id']) && $args['id'] != "" && $args['id'] > 0) {
            $colorId = $args['id'];
            $colorPaletteInit = new ColorPalette();
            $getOldColor = $colorPaletteInit->where('xe_id', '=', $colorId);
            if ($getOldColor->count() > 0) {
                // Process file uploading
                $getUploadedFileName = save_file('upload', path('abs', 'colorpalette'));
                if ($getUploadedFileName != '') {
                    $updateData += ['value' => $getUploadedFileName];
                    $this->deleteOldFile("color_palettes", "value", ['xe_id' => $colorId], path('abs', 'colorpalette'));
                }
                $updateData += ['store_id' => $getStoreDetails['store_id']];
                // Update record
                try {
                    $colorPaletteInit->where('xe_id', '=', $colorId)->update($updateData);
                    $jsonResponse = [
                        'status' => 1,
                        'message' => message('Color', 'updated'),
                    ];
                } catch (\Exception $e) {
                    $serverStatusCode = EXCEPTION_OCCURED;
                    $jsonResponse = [
                        'status' => 0,
                        'message' => message('Color', 'exception'),
                        'exception' => show_exception() === true ? $e->getMessage() : '',
                    ];
                }
            }
        }
        return response(
            $response, ['data' => $jsonResponse, 'status' => $serverStatusCode]
        );
    }

    /**
     * DELETE: Delete a ColorPalette
     *
     * @param $response Slim's Response object
     * @param $args     Slim's Argument parameters
     *
     * @author debashrib@riaxe.com
     * @date   06 Dec 2019
     * @return json response wheather data is deleted or not
     */
    public function deleteColor($request, $response, $args)
    {
        $serverStatusCode = OPERATION_OKAY;
        $jsonResponse = [
            'status' => 0,
            'message' => message('Color', 'error'),
        ];

        if (isset($args) && count($args) > 0 && $args['id'] != '') {
            $getDeleteIds = $args['id'];
            $getDeleteIdsToArray = json_clean_decode($getDeleteIds, true);
            if (isset($getDeleteIdsToArray) && is_array($getDeleteIdsToArray) && count($getDeleteIdsToArray) > 0) {
                $colorPaletteInit = new ColorPalette();
                if ($colorPaletteInit->whereIn('xe_id', $getDeleteIdsToArray)->count() > 0) {
                    // Fetch Color details
                    $getColorDetails = $colorPaletteInit->whereIn('xe_id', $getDeleteIdsToArray)->select('xe_id')->get();
                    try {
                        foreach ($getColorDetails as $colorFile) {
                            if (isset($colorFile['xe_id']) && $colorFile['xe_id'] != "") {
                                $this->deleteOldFile("color_palettes", "value", ['xe_id' => $colorFile['xe_id']], path('abs', 'colorpalette'));
                            }
                        }
                        $colorPaletteInit->whereIn('xe_id', $getDeleteIdsToArray)->delete();
                        $jsonResponse = [
                            'status' => 1,
                            'message' => message('Color', 'deleted'),
                        ];
                    } catch (\Exception $e) {
                        $serverStatusCode = EXCEPTION_OCCURED;
                        $jsonResponse = [
                            'status' => 0,
                            'message' => message('Color', 'exception'),
                            'exception' => show_exception() === true ? $e->getMessage() : '',
                        ];
                    }
                }
            }
        }
        return response(
            $response, ['data' => $jsonResponse, 'status' => $serverStatusCode]
        );
    }

    /**
     * DELETE: Delete a ColorPalette Category
     *
     * @param $request  Slim's Request object
     * @param $response Slim's Response object
     * @param $args     Slim's Argument parameters
     *
     * @author debashrib@riaxe.com
     * @date   04 Dec 2019
     * @return json response wheather data is deleted or not
     */
    public function deleteColorCategory($request, $response, $args)
    {
        $serverStatusCode = OPERATION_OKAY;
        $jsonResponse = [
            'status' => 0,
            'message' => message('Subcategory', 'error'),
        ];
        $key = $request->getQueryParam('force_delete') > 0 ? $request->getQueryParam('force_delete') : 0;
        if (!empty($args) && $args['id'] > 0) {
            $deleteId = $args['id'];
            $categoryInit = new Category();
            $category = $categoryInit->find($deleteId);

            if (isset($category->xe_id) && $category->xe_id != "" && $category->xe_id > 0) {
                try {
                    $colorPaletteInit = new ColorPalette();
                    $color = $colorPaletteInit->where('subcategory_id', $deleteId)->select('xe_id')->get();
                    if ($color->count() == 0 || $key == 1) {
                        $colorIds = [];
                        $category->delete();
                        foreach ($color->toArray() as $value) {
                            $temp = '';
                            if ($value['xe_id'] != '') {
                                $temp = $value['xe_id'];
                            }
                            $this->deleteOldFile("color_palettes", "value", ['xe_id' => $value['xe_id']], path('abs', 'colorpalette'));
                            if ($temp !== '') {
                                array_push($colorIds, $temp);
                            }
                        }
                        $colorPaletteInit->whereIn('xe_id', $colorIds)->delete();
                        $jsonResponse = [
                            'status' => 1,
                            'message' => message('Subcategory', 'deleted'),
                        ];
                    } else {
                        $jsonResponse = [
                            'status' => 0,
                            'message' => 'The category name alrady exists, please choose a different one',
                        ];
                    }
                } catch (\Exception $e) {
                    $serverStatusCode = EXCEPTION_OCCURED;
                    $jsonResponse = [
                        'status' => 0,
                        'message' => message('Subcategory', 'exception'),
                        'exception' => showException() === true ? $e->getMessage() : '',
                    ];
                }
            }
        }
        return response(
            $response, ['data' => $jsonResponse, 'status' => $serverStatusCode]
        );
    }

}
