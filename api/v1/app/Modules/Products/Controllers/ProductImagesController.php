<?php
/**
 * Manage Product Image Sides
 *
 * PHP version 5.6
 *
 * @category  Product_Image
 * @package   Product
 * @author    Tanmaya Patra <tanmayap@riaxe.com>
 * @copyright 2019-2020 Riaxe Systems
 * @license   http://www.php.net/license/3_0.txt  PHP License 3.0
 * @link      http://inkxe-v10.inkxe.io/xetool/admin
 */
namespace App\Modules\Products\Controllers;

use App\Modules\Products\Models\ProductImage;
use App\Modules\Products\Models\ProductImageSides;
use App\Modules\Products\Models\ProductSide;
use ProductStoreSpace\Controllers\StoreProductsController;

/**
 * Product Image Controller
 *
 * @category Product_Image
 * @package  Product
 * @author   Tanmaya Patra <tanmayap@riaxe.com>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://inkxe-v10.inkxe.io/xetool/admin
 */
class ProductImagesController extends StoreProductsController
{
    /**
     * GET:Getting List of All Product Images
     *
     * @param $request  Slim's Request object
     * @param $response Slim's Response object
     * @param $args     Slim's Argument parameters
     *
     * @author tanmayap@riaxe.com
     * @date   5 Oct 2019
     * @return A JSON Response
     */
    public function getProductImages($request, $response, $args)
    {
        $serverStatusCode = OPERATION_OKAY;
        $storeId = get_store_details($request);
        $jsonResponse = [
            'status' => 1,
            'data' => [],
            'message' => message('Product Images', 'not_found'),
        ];

        $prodImgInit = new ProductImage();
        $productImageInfo = $prodImgInit->with('sides');
        if (isset($args['product_image_id']) && $args['product_image_id'] != "" && $args['product_image_id'] > 0) {
            $productImageInfo->where('xe_id', $args['product_image_id']);
        }
        if ($productImageInfo->count() > 0) {
            if (isset($storeId) && count($storeId) > 0) {
                $productImageInfo->where($storeId);
            }
            $productImageList = $productImageInfo->orderBy('xe_id', 'desc')->get();
            $jsonResponse = [
                'status' => 1,
                'data' => $productImageList,
            ];
        }

        return response($response, ['data' => $jsonResponse, 'status' => $serverStatusCode]);
    }

    /**
     * Post: Save Product Images
     *
     * @param $request  Slim's Request object
     * @param $response Slim's Response object
     *
     * @author tanmayap@riaxe.com
     * @date   5 Oct 2019
     * @return A JSON Response
     */
    public function saveProductImages($request, $response)
    {
        $serverStatusCode = OPERATION_OKAY;
        // Get Store Specific Details from helper
        $store = get_store_details($request);
        $jsonResponse = [];
        $allPostPutVars = $request->getParsedBody();
        $productSidesJson = $allPostPutVars['product_sides'];
        $productSidesArray = json_decode($productSidesJson, true);

        // Save Product Image Details
        if (isset($productSidesArray['name']) && $productSidesArray['name'] != "") {
            $productImageData = [
                'name' => $productSidesArray['name'],
            ];
            $productImageData['store_id'] = $store['store_id'];
            $saveProductImage = new ProductImage($productImageData);
            $saveProductImage->save();
            $productImageInsertId = $saveProductImage->xe_id;

            // Save Product Image Sides
            if (isset($productSidesArray['sides']) && count($productSidesArray['sides']) > 0) {
                foreach ($productSidesArray['sides'] as $sideData) {
                    // Start saving each sides
                    $imageUploadIndex = $sideData['image_upload_data'];

                    // If image resource was given then upload the image into
                    // the specified folder
                    $uploadedFiles = $request->getUploadedFiles();
                    if (!empty($uploadedFiles[$imageUploadIndex]->file)
                        && $uploadedFiles[$imageUploadIndex]->file != ""
                    ) {
                        $getUploadedFileName = save_file($imageUploadIndex, path('abs', 'product'), false);
                    }

                    // Setup data for Saving/updating
                    $productImageSides = [
                        'product_image_id' => $productImageInsertId,
                        'side_name' => !empty($sideData['side_name']) ? $sideData['side_name'] : null,
                        'sort_order' => $sideData['sort_order'],
                    ];

                    // If File was choosen from frontend then only save/update
                    // the image or skip the image saving
                    if (isset($getUploadedFileName) && $getUploadedFileName != "") {
                        $productImageSides['file_name'] = $getUploadedFileName;
                    }

                    // Insert Product Image Sides
                    $saveProductImageSide = new ProductImageSides($productImageSides);
                    if ($saveProductImageSide->save()) {
                        $jsonResponse = [
                            'status' => 1,
                            'product_image_id' => $productImageInsertId,
                            'message' => message('Product Image', 'saved'),
                        ];
                    } else {
                        $jsonResponse = [
                            'status' => 0,
                            'message' => message('Product Image', 'error'),
                        ];
                    }

                }
            }
        } else {
            $jsonResponse = [
                'status' => 0,
                'message' => message('Product Image', 'insufficient'),
            ];
        }

        return response($response, ['data' => $jsonResponse, 'status' => $serverStatusCode]);
    }

    /**
     * Put: Update Product Images
     *
     * @param $request  Slim's Request object
     * @param $response Slim's Response object
     * @param $args     Slim's Argument parameters
     *
     * @author tanmayap@riaxe.com
     * @date   5 Oct 2019
     * @return A JSON Response
     */
    public function updateProductImages($request, $response, $args)
    {
        $jsonResponse = [];
        $serverStatusCode = OPERATION_OKAY;
        $storeId = get_store_details($request);
        $allPostPutVars = $this->parsePut();
        $productSidesJson = isset($allPostPutVars['product_sides']) ? $allPostPutVars['product_sides'] : '{}';
        $productSidesArray = json_decode($productSidesJson, true);
        $productImageId = $args['product_image_id'];
        // Save Product Image Details
        $prodImgInit = new ProductImage();
        $productImageInit = $prodImgInit->where('xe_id', $productImageId);
        if (isset($productSidesArray['name']) && $productSidesArray['name'] != "" && $productImageInit->count() > 0) {
            // Update Product Image Details
            $updateProdImage = [
                'name' => $productSidesArray['name'],
            ];
            $updateProdImage += $storeId;
            $productImageInit->update($updateProdImage);
            $productImageInsertId = $productImageId;

            // Save Product Image Sides
            if (isset($productSidesArray['sides']) && count($productSidesArray['sides']) > 0) {
                foreach ($productSidesArray['sides'] as $sideData) {
                    $getUploadedFileName = '';
                    // Start analysing each side
                    if (!empty($sideData['image_upload_data'])
                        && $sideData['image_upload_data'] != ""
                    ) {
                        // Case #1: If New File uploading requested
                        $requestedFileKey = $sideData['image_upload_data'];
                        $getUploadedFileName = save_file($requestedFileKey, path('abs', 'product'));

                        // Case #1 : If New file added, then again 2 cases will
                        // arrise. 1. Save new record and 2. Update existing
                        $productImageSides = [
                            'product_image_id' => $productImageInsertId,
                            'side_name' => $sideData['side_name'],
                            'sort_order' => $sideData['sort_order'],
                        ];
                        if (!empty($getUploadedFileName) && $getUploadedFileName != "") {
                            $productImageSides['file_name'] = $getUploadedFileName;
                        }

                        $prodImgSideInit = new ProductImageSides();
                        $checkIdDataExist = $prodImgSideInit->where('xe_id', $sideData['xe_id']);
                        if ($checkIdDataExist->count() > 0) {
                            // Update Record
                            $checkIdDataExist->update($productImageSides);
                        } else {
                            // Save New
                            $saveSide = new ProductImageSides($productImageSides);
                            $saveSide->save();
                        }
                    } else if (isset($sideData['is_trash']) && $sideData['is_trash'] === 1) {
                        // Case #2: Image Side will be deleted from the Product
                        $prodImgSideInit = new ProductImageSides();
                        $trashSide = $prodImgSideInit->where(
                            [
                                'xe_id' => $sideData['xe_id'],
                                'product_image_id' => $productImageInsertId
                            ]
                        );
                        if ($trashSide->delete()) {
                            $prodSideInit = new ProductSide();
                            $trashProdSide = $prodSideInit->where(
                                [
                                    'product_image_side_id' => $sideData['xe_id']
                                ]
                            );
                            $trashProdSide->delete();
                        }
                    } else {
                        // Case #3: Existing Image Side will be Updated
                        $productImageSides = [
                            'side_name' => $sideData['side_name'],
                            'sort_order' => $sideData['sort_order'],
                        ];
                        $prodImgSideInit = new ProductImageSides();
                        $updateSide = $prodImgSideInit->where(['xe_id' => $sideData['xe_id'], 'product_image_id' => $productImageInsertId]);
                        $updateSide->update($productImageSides);
                    }
                }
            }
            $jsonResponse = [
                'status' => 1,
                'message' => message('Product Image Side', 'updated'),
            ];
        } else {
            $jsonResponse = [
                'status' => 0,
                'message' => message('Product Image Side', 'insufficient'),
            ];
        }

        return response($response, ['data' => $jsonResponse, 'status' => $serverStatusCode]);
    }

    /**
     * Delete: Delete Product Image(s)
     *
     * @param $request  Slim's Request object
     * @param $response Slim's Response object
     * @param $args     Slim's Argument parameters
     *
     * @author tanmayap@riaxe.com
     * @date   5 Oct 2019
     * @return A JSON Response
     */
    public function productImageDelete($request, $response, $args)
    {
        $serverStatusCode = OPERATION_OKAY;
        // If user wants to delete Product Image(s)
        if (isset($args) && count($args) > 0 && $args['ids'] != '') {
            $getProductImagesId = json_decode($args['ids'], true);
            $prodImgInit = new ProductImage();
            $getSelectedItems = $prodImgInit->whereIn('xe_id', $getProductImagesId);

            if ($getSelectedItems->count() > 0) {
                // Get Existing Data for further processing
                $getExistingData = $getSelectedItems->with('sides')->get();

                if ($getSelectedItems->delete()) {
                    // Get existing Images and Delte those from directory
                    foreach ($getExistingData as $selectedValue) {
                        if (isset($selectedValue['sides']) && count($selectedValue['sides']) > 0) {
                            foreach ($selectedValue['sides'] as $singleSide) {
                                $rawFileLocation = PRODUCT_FOLDER . $singleSide['file_name'];
                                // Delete file from the directory
                                if (file_exists($rawFileLocation)) {
                                    permit_directory($rawFileLocation);
                                    unlink($rawFileLocation);
                                }
                            }
                        }
                    }
                    $jsonResponse = [
                        'status' => 1,
                        'message' => 'Product Image data deleted successfully',
                    ];
                } else {
                    $jsonResponse = [
                        'status' => 0,
                        'message' => 'Product Image data could not deleted',
                    ];
                }
            } else {
                $serverStatusCode = MISSING_PARAMETER;
                $jsonResponse = [
                    'status' => 0,
                    'message' => 'Product Image not found. Try again later',
                ];
            }
        } else {
            $serverStatusCode = MISSING_PARAMETER;
            $jsonResponse = [
                'status' => 0,
                'message' => 'Invalid data provided. Please provide valid data.',
            ];
        }

        return response($response, ['data' => $jsonResponse, 'status' => $serverStatusCode]);
    }

    /**
     * Get: Enable/Disable Product Image
     *
     * @param $request  Slim's Request object
     * @param $response Slim's Response object
     * @param $args     Slim's Argument parameters
     *
     * @author tanmayap@riaxe.com
     * @date   5 Oct 2019
     * @return A JSON Response
     */
    public function disableProductImage($request, $response, $args)
    {
        $serverStatusCode = OPERATION_OKAY;
        // If user wants to delete Product Image(s)
        if (isset($args) && count($args) > 0 && $args['id'] != '') {
            $getProductImageId = $args['id'];
            $prodImgInit = new ProductImage();
            $getSelectedItems = $prodImgInit->find($getProductImageId);
            if (isset($getSelectedItems) && count(object_to_array($getSelectedItems)) > 0) {
                $getSelectedItems->is_disable = !$getSelectedItems->is_disable;
                if ($getSelectedItems->save()) {
                    $jsonResponse = [
                        'status' => 1,
                        'message' => message('Product Image', 'updated'),
                    ];
                } else {
                    $jsonResponse = [
                        'status' => 0,
                        'message' => message('Product Image', 'error'),
                    ];
                }
            } else {
                $jsonResponse = [
                    'status' => 1,
                    'data' => [],
                    'message' => message('Product Image', 'not_found'),
                ];
            }
        } else {
            $jsonResponse = [
                'status' => 0,
                'message' => 'Invalid request. please try again later',
            ];
        }

        return response($response, ['data' => $jsonResponse, 'status' => $serverStatusCode]);
    }
}
