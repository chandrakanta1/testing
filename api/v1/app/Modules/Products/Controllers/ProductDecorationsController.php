<?php
/**
 * Manage Product Decorations - Single and Multiple Variation
 *
 * PHP version 7.2
 *
 * @category  Product_Decorations
 * @package   Decoration_Settings
 * @author    Tanmaya Patra <tanmayap@riaxe.com>
 * @copyright 2019-2020 Riaxe Systems
 * @license   http://www.php.net/license/3_0.txt  PHP License 3.0
 * @link      http://inkxe-v10.inkxe.io/xetool/admin
 */
namespace App\Modules\Products\Controllers;

use App\Modules\Products\Models\PrintProfileDecorationSettingRel;
use App\Modules\Products\Models\PrintProfileProductSettingRel;
use App\Modules\Products\Models\ProductDecorationSetting;
use App\Modules\Products\Models\ProductImageSettingsRel;
use App\Modules\Products\Models\ProductImageSides;
use App\Modules\Products\Models\ProductSetting;
use App\Modules\Products\Models\ProductSide;
use App\Modules\Products\Models\ProductSizeVariantDecorationSetting;
use GuzzleHttp\Client;
// use Illuminate\Database\Capsule\Manager as DB;
use ProductStoreSpace\Controllers\StoreProductsController;
use App\Modules\DecorationAreas\Models\PrintArea;

/**
 * Product Decoration Controller
 *
 * @category Product_Decoration
 * @package  Product
 * @author   Tanmaya Patra <tanmayap@riaxe.com>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://inkxe-v10.inkxe.io/xetool/admin
 */
class ProductDecorationsController extends StoreProductsController
{
    private $updateRecord = false;
    /**
     * Delete: Delete Decoration Settings
     *
     * @param $request  Slim's Request object
     * @param $response Slim's Response object
     * @param $args     Slim's Argument parameters
     *
     * @author tanmayap@riaxe.com
     * @date   5 Oct 2019
     * @return All Print Profile List
     */
    public function trashDecoration($request, $response, $args)
    {
        $serverStatusCode = OPERATION_OKAY;
        $jsonResponse = [];
        $productSettingId = $args['product_id'];
        $prodSettInt = new ProductSetting();
        $initProductSetting = $prodSettInt->where(['product_id' => $productSettingId]);
        if ($initProductSetting->delete()) {
            $jsonResponse = [
                'response' => [
                    'status' => 1,
                    'message' => message('Decoration Setting', 'deleted'),
                ],
            ];
        } else {
            $jsonResponse = [
                'response' => [
                    'status' => 0,
                    'message' => message('Decoration Setting', 'error'),
                ],
            ];
        }
        return response($response, ['data' => $jsonResponse['response'], 'status' => $serverStatusCode]);
    }
    
    /**
     * Post: Save Product Decoration Settings
     *
     * @param $request  Slim's Request object
     * @param $response Slim's Response object
     *
     * @author tanmayap@riaxe.com
     * @date   5 Oct 2019
     * @return Json
     */
    public function saveProductDecorations($request, $response)
    {
        $serverStatusCode = OPERATION_OKAY;
        $jsonResponse = [];
        $allPostPutVars = $request->getParsedBody();
        $getProdDecoInfo = !empty($allPostPutVars['decorations']) ? json_decode($allPostPutVars['decorations'], true) : null;
        $replaceExisting = !empty($allPostPutVars['replace']) ? $allPostPutVars['replace'] : 0;

        $isVariableDecoration = !empty($getProdDecoInfo['is_variable_decoration']) ? $getProdDecoInfo['is_variable_decoration'] : 0;
        if (!empty($isVariableDecoration) && $isVariableDecoration === 1) {
            // Process for variable decoration area
            $variableDecoResp = $this->saveVariableProductDecoration($request, $response, $getProdDecoInfo, $replaceExisting, false);
            return response($response, ['data' => $variableDecoResp['response'], 'status' => $variableDecoResp['server_status_code']]);
        }
        $prodSettInit = new ProductSetting();
        // Clear old records if replace is set to 1
        $checkRecord = $prodSettInit->where(
            ['product_id' => $getProdDecoInfo['product_id']]
        );
        if ($checkRecord->count() > 0) {
            if (!empty($replaceExisting) && (int) $replaceExisting === 1) {
                $checkRecord->delete();
            } else {
                $jsonResponse = [
                    'status' => 0,
                    'message' => message('Variable Product Decoration Setting', 'exist'),
                ];
                return response($response, ['data' => $jsonResponse, 'status' => OPERATION_OKAY]);
            }
        }

        // If any file exist then upload

        $uploadedFiles = $request->getUploadedFiles();
        if (!empty($uploadedFiles['3d_object_file']->file) && $uploadedFiles['3d_object_file']->file != "") {
            $objectFileName = save_file('3d_object_file', path('abs', '3d_object'));
        }

        // Processing for Table: product_settings
        if (isset($getProdDecoInfo['product_id']) && $getProdDecoInfo['product_id'] > 0
        ) {
            $productSettData = [
                'product_id' => $getProdDecoInfo['product_id'],
                'is_crop_mark' => $getProdDecoInfo['is_crop_mark'],
                'is_ruler' => !empty($getProdDecoInfo['is_ruler']) ? $getProdDecoInfo['is_ruler'] : 0,
                'is_safe_zone' => $getProdDecoInfo['is_safe_zone'],
                'crop_value' => $getProdDecoInfo['crop_value'],
                'safe_value' => $getProdDecoInfo['safe_value'],
                'is_3d_preview' => $getProdDecoInfo['is_3d_preview'],
                '3d_object_file' => isset($objectFileName) ? $objectFileName : null,
                '3d_object' => !empty($getProdDecoInfo['3d_object']) ? $getProdDecoInfo['3d_object'] : null,
                'scale_unit_id' => $getProdDecoInfo['scale_unit_id'],
            ];
            $productSetting = new ProductSetting($productSettData);
            $productSetting->save();
            $prodSettInsId = $productSetting->xe_id;
        }

        // Processing for Table: product_image_settings_rel
        if (isset($prodSettInsId) && $prodSettInsId > 0
            && isset($getProdDecoInfo['product_image_id']) && $getProdDecoInfo['product_image_id'] > 0
        ) {
            $productImageSettings = new ProductImageSettingsRel(
                [
                    'product_setting_id' => $prodSettInsId,
                    'product_image_id' => $getProdDecoInfo['product_image_id'],
                ]
            );
            $productImageSettings->save();
        }

        // Processing for Table: product_sides, product_decoration_settings
        if (isset($prodSettInsId) && $prodSettInsId > 0
            && isset($getProdDecoInfo['sides']) && count($getProdDecoInfo['sides']) > 0
        ) {
            $imageSides = $getProdDecoInfo['sides'];
            foreach ($imageSides as $productSideData) {
                $productSide = new ProductSide(
                    [
                        'product_setting_id' => $prodSettInsId,
                        'side_name' => $productSideData['name'],
                        'product_image_dimension' => $productSideData['image_dimension'],
                        'is_visible' => $productSideData['is_visible'],
                        'product_image_side_id' => $productSideData['product_image_side_id'],
                    ]
                );
                $productSide->save();
                $productSideInsertId = $productSide->xe_id;
                $prodDecoSettRecord = [];
                foreach ($productSideData['product_decoration'] as $pdsKey => $productDecoSetting) {
                    $prodDecoSettRecord[$pdsKey] = $productDecoSetting;
                    $prodDecoSettRecord[$pdsKey]['product_side_id'] = $productSideInsertId;
                    $prodDecoSettRecord[$pdsKey]['dimension'] = isset($productDecoSetting['dimension']) ? json_encode($productDecoSetting['dimension'], true) : "{}";
                    $prodDecoSettRecord[$pdsKey]['product_setting_id'] = $prodSettInsId;

                    $prodDecoSettInit = new ProductDecorationSetting($prodDecoSettRecord[$pdsKey]);
                    $prodDecoSettInit->save();
                    $prodDecoSettInsId = $prodDecoSettInit->xe_id;

                    if (isset($prodDecoSettInsId) && $prodDecoSettInsId > 0) {
                        // Processing for Table: print_profile_decoration_setting_rel
                        if (isset($productDecoSetting['print_profile_ids']) && count($productDecoSetting['print_profile_ids']) > 0) {
                            $printProfiles = $productDecoSetting['print_profile_ids'];
                            $ppDecoSettRelData = [];
                            foreach ($printProfiles as $ppKey => $printProfile) {
                                $ppDecoSettRelData[$ppKey]['print_profile_id'] = $printProfile;
                                $ppDecoSettRelData[$ppKey]['decoration_setting_id'] = $prodDecoSettInsId;
                            }
                            $ppDecoSettInit = new PrintProfileDecorationSettingRel();
                            $ppDecoSettInit->insert($ppDecoSettRelData);
                        }

                        /**
                         * Processing for the table : product_size_variant_decoration_settings
                         * - Saving the data of Size Variants
                         */
                        if (isset($productDecoSetting['size_variants']) && count($productDecoSetting['size_variants']) > 0) {
                            $sizeVariants = $productDecoSetting['size_variants'];
                            $sizeVariantDecoSett = [];
                            foreach ($sizeVariants as $svKey => $sizeVariant) {
                                $sizeVariantDecoSett[$svKey]['size_variant_id'] = $sizeVariant['size_variant_id'];
                                $sizeVariantDecoSett[$svKey]['print_area_id'] = $sizeVariant['print_area_id'];
                                $sizeVariantDecoSett[$svKey]['decoration_setting_id'] = $prodDecoSettInsId;
                            }
                            $sizeVariantDecoSettInit = new ProductSizeVariantDecorationSetting();
                            $sizeVariantDecoSettInit->insert($sizeVariantDecoSett);
                        }
                    }
                }
            }
        }

        /**
         * Processing for Table: print_profile_product_setting_rel
         * - For saving the outer Print profile Ids
         */
        if (isset($prodSettInsId) && $prodSettInsId > 0
            && isset($getProdDecoInfo['print_profile_ids']) && count($getProdDecoInfo['print_profile_ids']) > 0
        ) {
            $printProfiles = $getProdDecoInfo['print_profile_ids'];
            $ppProdSettRelData = [];
            foreach ($printProfiles as $ppKey => $printProfile) {
                $ppProdSettRelData[$ppKey]['print_profile_id'] = $printProfile;
                $ppProdSettRelData[$ppKey]['product_setting_id'] = $prodSettInsId;
            }
            $ppProdSettRelSvInit = new PrintProfileProductSettingRel();
            $ppProdSettRelSvInit->insert($ppProdSettRelData);
        }

        if (isset($prodSettInsId) && $prodSettInsId > 0) {
            $jsonResponse = [
                'status' => 1,
                'product_settings_insert_id' => $prodSettInsId,
                'message' => message('Product Decoration Setting', 'saved'),
            ];
        } else {
            $jsonResponse = [
                'status' => 0,
                'message' => message('Product Decoration Setting', 'error'),
            ];
        }
        return response($response, ['data' => $jsonResponse, 'status' => $serverStatusCode]);
    }

    /**
     * Put: Update Product Decoration Settings
     *
     * @param $request  Slim's Request object
     * @param $response Slim's Response object
     * @param $args     Slim's Argument parameters
     *
     * @author tanmayap@riaxe.com
     * @date   5 Oct 2019
     * @return Json
     */
    public function updateProductDecorations($request, $response, $args)
    {
        $serverStatusCode = OPERATION_OKAY;
        $jsonResponse = [];
        $allPostPutVars = $this->parsePut();
        $productUpdateId = $args['product_id'];
        $getProductDecorationData = json_decode($allPostPutVars['decorations'], true);
        $productDecorationDataSet = [];

        $isVariableDecoration = $getProductDecorationData['is_variable_decoration'];
        if (!empty($isVariableDecoration) && $isVariableDecoration === 1) {
            // Process for variable decoration area
            $getVariableProductDecoration = $this->saveVariableProductDecoration($request, $response, $getProductDecorationData, 0, true);
            return response($response, ['data' => $getVariableProductDecoration['response'], 'status' => $getVariableProductDecoration['server_status_code']]);
        }
        $updateObjectFile = save_file('3d_object_file', path('abs', '3d_object'));
        $productDecorationDataSet += [
            '3d_object_file' => isset($updateObjectFile) ? $updateObjectFile : null,
        ];

        // Processing for Table: product_settings
        if (isset($productUpdateId) && $productUpdateId > 0
        ) {
            $productDecorationDataSet += [
                'is_variable_decoration' => $isVariableDecoration,
                'is_crop_mark' => $getProductDecorationData['is_crop_mark'],
                'is_safe_zone' => $getProductDecorationData['is_safe_zone'],
                'crop_value' => $getProductDecorationData['crop_value'],
                'safe_value' => $getProductDecorationData['safe_value'],
                'is_3d_preview' => $getProductDecorationData['is_3d_preview'],
                '3d_object' => isset($getProductDecorationData['3d_object']) ? $getProductDecorationData['3d_object'] : "{}",
                'scale_unit_id' => $getProductDecorationData['scale_unit_id'],
            ];

            $prodSettGtInit = new ProductSetting();
            $productSettingsUpdateInit = $prodSettGtInit->where(['product_id' => $productUpdateId]);
            $productSettingsUpdateInit->update($productDecorationDataSet);

            $productSettingsGet = $productSettingsUpdateInit->orderBy('xe_id', 'desc')->first();
            $productSettingInsertId = $productSettingsGet->xe_id;
        }

        // Processing for Table: product_image_settings_rel
        if (isset($productSettingInsertId) && $productSettingInsertId > 0
            && isset($getProductDecorationData['product_image_id']) && $getProductDecorationData['product_image_id'] > 0
        ) {
            $prodImgSettRelGtInit = new ProductImageSettingsRel();
            $productImageSettingInit = $prodImgSettRelGtInit->where(['product_setting_id' => $productSettingInsertId]);
            if ($productImageSettingInit->count() > 0) {
                // Update record
                $productImageSettingInit->update(['product_image_id' => $getProductDecorationData['product_image_id']]);
                $productImageSettingsInsertId = '';
            } else {
                // Save record
                $productImageSettings = new ProductImageSettingsRel(
                    [ //product_image_settings_rel PrintProfileProductSetting
                        'product_setting_id' => $productSettingInsertId,
                        'product_image_id' => $getProductDecorationData['product_image_id'],
                    ]
                );
                $productImageSettings->save();
                $productImageSettingsInsertId = $productImageSettings->xe_id;
            }
        } else if (isset($getProductDecorationData['product_image_id']) && $getProductDecorationData['product_image_id'] == 0) {
            // If product_setting_id sent as 0 then delete the relation from product_image_setting_rel
            $prodImgSettRelGtInit = new ProductImageSettingsRel();
            $productImageSettingInit = $prodImgSettRelGtInit->where(['product_setting_id' => $productSettingInsertId]);
            $productImageSettingInit->delete();
        }

        // Processing for Table: product_sides, product_decoration_settings
        if (isset($productSettingInsertId) && $productSettingInsertId > 0
            && isset($getProductDecorationData['sides']) && count($getProductDecorationData['sides']) > 0
        ) {
            // Clearing the old records
            ProductSide::where(
                ['product_setting_id' => $productSettingInsertId]
            )->delete();

            $imageSides = $getProductDecorationData['sides'];
            foreach ($imageSides as $productSideData) {
                $productSide = new ProductSide(
                    [
                        'product_setting_id' => $productSettingInsertId,
                        'side_name' => $productSideData['name'],
                        'product_image_dimension' => $productSideData['image_dimension'],
                        'is_visible' => $productSideData['is_visible'],
                        'product_image_side_id' => $productSideData['product_image_side_id']
                    ]
                );
                $productSide->save();
                $productSideInsertId = $productSide->xe_id;
                $prodDecoSettData = [];
                foreach ($productSideData['product_decoration'] as $pdsKey => $productDecoSetting) {
                    $prodDecoSettData[$pdsKey] = $productDecoSetting;
                    $prodDecoSettData[$pdsKey]['product_side_id'] = $productSideInsertId;
                    $prodDecoSettData[$pdsKey]['dimension'] = isset($productDecoSetting['dimension']) ? json_encode($productDecoSetting['dimension'], true) : "{}";
                    $prodDecoSettData[$pdsKey]['product_setting_id'] = $productSettingInsertId;

                    $productDecorationSettingInit = new ProductDecorationSetting($prodDecoSettData[$pdsKey]);
                    $productDecorationSettingInit->save();
                    $prodDecoSettInsId = $productDecorationSettingInit->xe_id;

                    // Processing for Table: print_profile_decoration_setting_rel
                    if (isset($prodDecoSettInsId) && $prodDecoSettInsId > 0
                        && isset($productDecoSetting['print_profile_ids']) && count($productDecoSetting['print_profile_ids']) > 0
                    ) {
                        $printProfiles = $productDecoSetting['print_profile_ids'];
                        $ppDecoSettRelData = [];
                        foreach ($printProfiles as $ppKey => $printProfile) {
                            $ppDecoSettRelData[$ppKey]['print_profile_id'] = $printProfile;
                            $ppDecoSettRelData[$ppKey]['decoration_setting_id'] = $prodDecoSettInsId;
                        }
                        // debug($ppDecoSettRelData, true);
                        $ppDecoSettRelInit = new PrintProfileDecorationSettingRel();
                        $ppDecoSettRelInit->insert($ppDecoSettRelData);
                    }

                    /**
                     * NEW
                     * Processing for the table : product_size_variant_decoration_settings
                     * - Saving the data of Size Variants
                     */
                    if (isset($productDecoSetting['size_variants']) && count($productDecoSetting['size_variants']) > 0) {
                        // Clean up the old records
                        ProductSizeVariantDecorationSetting::where('decoration_setting_id', $productDecorationSettingInsertId)->delete();

                        $sizeVariants = $productDecoSetting['size_variants'];
                        $prodSizeVarDecoSett = [];
                        foreach ($sizeVariants as $svKey => $sizeVariant) {
                            $prodSizeVarDecoSett[$svKey]['size_variant_id'] = $sizeVariant['size_variant_id'];
                            $prodSizeVarDecoSett[$svKey]['print_area_id'] = $sizeVariant['print_area_id'];
                            $prodSizeVarDecoSett[$svKey]['decoration_setting_id'] = $productDecorationSettingInsertId;
                        }
                        $prodSzVarDecoSettini = new ProductSizeVariantDecorationSetting();
                        $prodSzVarDecoSettini->insert($prodSizeVarDecoSett);
                    }

                }
            }
        }

        // Processing for Table: print_profile_product_setting_rel
        if (isset($productSettingInsertId) && $productSettingInsertId > 0
            && isset($getProductDecorationData['print_profile_ids']) && count($getProductDecorationData['print_profile_ids']) > 0
        ) {
            // Clearing the old records
            PrintProfileProductSettingRel::where(
                ['product_setting_id' => $productSettingInsertId]
            )->delete();

            $printProfiles = $getProductDecorationData['print_profile_ids'];
            $ppProdSettRelData = [];
            foreach ($printProfiles as $ppKey => $printProfile) {
                $ppProdSettRelData[$ppKey]['print_profile_id'] = $printProfile;
                $ppProdSettRelData[$ppKey]['product_setting_id'] = $productSettingInsertId;
            }
            $ppProdSettRelDataInit = new PrintProfileProductSettingRel();
            $ppProdSettRelDataInit->insert($ppProdSettRelData);
        }

        if (isset($productSettingInsertId) && $productSettingInsertId > 0) {
            $jsonResponse = [
                'status' => 1,
                'product_settings_insert_id' => $productSettingInsertId,
                'message' => message('Product Decoration Setting', 'updated'),
            ];
        } else {
            $jsonResponse = [
                'status' => 0,
                'message' => message('Product Decoration Setting', 'error'),
            ];
        }

        return response($response, ['data' => $jsonResponse, 'status' => $serverStatusCode]);

    }

    /**
     * GET: Update and Save Variable Product Decoration Module
     * (ProductSetting ProductSide ProductDecorationSetting)
     *
     * @param $request         Slim's Request parameters
     * @param $request         Slim's Response parameters
     * @param $prodVarDecoData Product Variable Decoration Data
     * @param $doReplace       Flag to whearher replace/not
     * @param $updateRecord    Slim's Argument parameters
     *
     * @author tanmayap@riaxe.com
     * @date   5 Oct 2019
     * @return Json
     */
    private function saveVariableProductDecoration($request, $response, $prodVarDecoData = [], $doReplace, $updateRecord)
    {
        $serverStatusCode = OPERATION_OKAY;
        $prodSettingData = [
            'product_id' => $prodVarDecoData['product_id'],
            'is_variable_decoration' => $prodVarDecoData['is_variable_decoration'],
            'is_crop_mark' => $prodVarDecoData['is_crop_mark'],
            'is_ruler' => $prodVarDecoData['is_ruler'],
            'is_safe_zone' => $prodVarDecoData['is_safe_zone'],
            'crop_value' => $prodVarDecoData['crop_value'],
            'safe_value' => $prodVarDecoData['safe_value'],
            'is_3d_preview' => $prodVarDecoData['is_3d_preview'],
            '3d_object_file' => "",
            '3d_object' => $prodVarDecoData['3d_object'],
            'scale_unit_id' => $prodVarDecoData['scale_unit_id'],
        ];

        // Clear old records if replace is set to 1
        $prodSettInit = new ProductSetting();
        $checkProductSettingRecord = $prodSettInit->where(['product_id' => $prodVarDecoData['product_id']]);
        if ($checkProductSettingRecord->count() > 0 && $updateRecord == false) {
            if (!empty($doReplace) && (int) $doReplace === 1) {
                $checkProductSettingRecord->delete();
            } else {
                $jsonResponse = [
                    'status' => 0,
                    'message' => message('Variable Product Decoration Setting', 'exist'),
                ];
                // Stop and return the error message from here
                return [
                    'response' => $jsonResponse,
                    'server_status_code' => $serverStatusCode,
                ];
            }
        }
        // Processing Product Settings
        // If any file exist then upload
        $uploadedFiles = $request->getUploadedFiles();
        if (!empty($prodVarDecoData['3d_object_file_upload'])
            && (!empty($uploadedFiles[$prodVarDecoData['3d_object_file_upload']]->file) && $uploadedFiles[$prodVarDecoData['3d_object_file_upload']]->file != "")
        ) {

            $objectFileName = save_file($prodVarDecoData['3d_object_file_upload'], path('abs', '3d_object'));
            $prodSettingData['3d_object_file'] = $objectFileName;
        }

        $productSettingInsertId = 0;
        if (!empty($updateRecord) && $updateRecord === true) {
            unset($prodSettingData['product_id']);
            $prodSettInit = new ProductSetting();
            $getCheckProductSettingRecord = $prodSettInit->where(['product_id' => $prodVarDecoData['product_id']])->select('xe_id')->first();

            $checkProductSettingRecord->update($prodSettingData);
            if (!empty($getCheckProductSettingRecord->xe_id) && $getCheckProductSettingRecord->xe_id > 0) {
                $productSettingInsertId = $getCheckProductSettingRecord->xe_id;
                // Delete old records from Print_Profile_Product_Setting_Rel
                PrintProfileProductSettingRel::where(['product_setting_id' => $productSettingInsertId])->delete();
                // Delete old records from Product_Image_Settings_Rel
                ProductImageSettingsRel::where(['product_setting_id' => $productSettingInsertId])->delete();
            }
        } else {
            $productSetting = new ProductSetting($prodSettingData);
            $productSetting->save();
            $productSettingInsertId = $productSetting->xe_id;
        }

        if (!empty($productSettingInsertId) && $productSettingInsertId > 0) {
            // For Variable Decoration Ares, delete Sides if Any sides are available
            $prodSideGtInit = new ProductSide();
            $initProductSides = $prodSideGtInit->where(['product_setting_id' => $productSettingInsertId]);
            if ($initProductSides->count() > 0) {
                $initProductSides->delete();
            }

            if (isset($prodVarDecoData['print_profile_ids']) && count($prodVarDecoData['print_profile_ids']) > 0) {
                $printProfiles = $prodVarDecoData['print_profile_ids'];
                $ppProdSettRelData = [];
                foreach ($printProfiles as $ppKey => $printProfile) {
                    $ppProdSettRelData[$ppKey]['print_profile_id'] = $printProfile;
                    $ppProdSettRelData[$ppKey]['product_setting_id'] = $productSettingInsertId;
                }
                $ppProdSettRelSvInit = new PrintProfileProductSettingRel();
                $ppProdSettRelSvInit->insert($ppProdSettRelData);
            }

            // Processing Product Image Settings Rel
            if (isset($prodVarDecoData['product_image_id']) && $prodVarDecoData['product_image_id'] > 0) {
                $productImageSettings = new ProductImageSettingsRel(
                    [
                        'product_setting_id' => $productSettingInsertId,
                        'product_image_id' => $prodVarDecoData['product_image_id'],
                    ]
                );
                $productImageSettings->save();
            }

            // Processing Product Decoration Settings Record
            $productDecorationData = $prodVarDecoData['product_decoration'];
            // Initialize Nw/new Product Deco Sett Data Array
            $prodDecoSettDataNw = [];
            // Choose dimension according to the flag
            if (!empty($productDecorationData['is_pre_defined']) && $productDecorationData['is_pre_defined'] === 1) {
                $prodDecoSettDataNw['pre_defined_dimensions'] = json_encode($productDecorationData['pre_defined_dimensions']);
                $prodDecoSettDataNw['user_defined_dimensions'] = null;
            } else {
                $prodDecoSettDataNw['user_defined_dimensions'] = json_encode($productDecorationData['user_defined_dimensions']);
                $prodDecoSettDataNw['pre_defined_dimensions'] = null;
            }
            // Setup decoration settings array
            $prodDecoSettDataNw += [
                'product_setting_id' => $productSettingInsertId,
                'dimension' => isset($productDecorationData['dimension']) ? json_encode($productDecorationData['dimension'], true) : "{}",
                'is_border_enable' => $productDecorationData['is_border_enable'],
                'is_sides_allow' => $productDecorationData['is_sides_allow'],
            ];
            if (!empty($productDecorationData['print_area_id']) && $productDecorationData['print_area_id'] > 0) {
                $prodDecoSettDataNw += ['print_area_id' => $productDecorationData['print_area_id']];
            }

            if (!empty($updateRecord) && $updateRecord === true) {
                unset($prodDecoSettDataNw['product_setting_id']);
                $prodDecoSettInit = new ProductDecorationSetting();
                $prodDecoSettUpdate = $prodDecoSettInit->where(['product_setting_id' => $productSettingInsertId]);
                $prodDecoSettUpdate->update($prodDecoSettDataNw);
                $prodDecoSettUpdGet = $prodDecoSettUpdate->select('xe_id')->first();
                $prodDecoSettInsId = $prodDecoSettUpdGet->xe_id;

                // Delete old records from "print_profile_decoration_setting_rel
                $ppDecoSettRelGtInit = new PrintProfileDecorationSettingRel();
                $ppDecoSettRelGtInit->where(['decoration_setting_id' => $prodDecoSettInsId])->delete();
            } else {
                $prodDecoSettSaveInit = new ProductDecorationSetting($prodDecoSettDataNw);
                $prodDecoSettSaveInit->save();
                $prodDecoSettInsId = $prodDecoSettSaveInit->xe_id;
            }

            // Processing Print Profile Decoration Setting Rel
            if (isset($prodDecoSettInsId) && $prodDecoSettInsId > 0) {
                // Processing for Table: print_profile_decoration_setting_rel
                if (isset($productDecorationData['print_profile_ids']) && count($productDecorationData['print_profile_ids']) > 0) {
                    $printProfiles = $productDecorationData['print_profile_ids'];
                    $ppDecoSettRelData = [];
                    foreach ($printProfiles as $ppKey => $printProfile) {
                        $ppDecoSettRelData[$ppKey]['print_profile_id'] = $printProfile;
                        $ppDecoSettRelData[$ppKey]['decoration_setting_id'] = $prodDecoSettInsId;
                    }
                    $ppDecoSettRelSvInit = new PrintProfileDecorationSettingRel();
                    $ppDecoSettRelSvInit->insert($ppDecoSettRelData);
                }
            }

            $jsonResponse = [
                'status' => 1,
                'product_settings_insert_id' => $productSettingInsertId,
                'message' => message('Variable Product Decoration Setting', (!empty($updateRecord) && $updateRecord === true) ? 'updated' : 'saved'),
            ];
        } else {
            $jsonResponse = [
                'status' => 0,
                'data' => [],
                'message' => message('Variable Product Decoration Setting', 'error'),
            ];
        }

        return [
            'response' => $jsonResponse,
            'server_status_code' => $serverStatusCode,
        ];
    }

    /**
     * Get: Fetch all Product Decoration Settings
     *
     * @param $request  Slim's Request object
     * @param $response Slim's Response object
     * @param $args     Slim's Argument parameters
     *
     * @author tanmayap@riaxe.com
     * @date   5 Oct 2019
     * @return Json
     */
    public function getProductDecorations($request, $response, $args)
    {
        $serverStatusCode = OPERATION_OKAY;
        $jsonResponse = [];
        $productDecorationSettingData = [];
        $productSideImages = [];

        /**
         * Relational Tables by their function names from Model size_variant_id
         */
        $getSettingsAssociatedRecords = ProductSetting::with(
            'sides', 'sides.product_decoration_setting', 'sides.product_decoration_setting.product_size_variant_decoration_settings',
            'sides.product_decoration_setting.print_profile_decoration_settings',
            'sides.product_decoration_setting.print_profile_decoration_settings.print_profile',
            'print_profiles', 'print_profiles.profile' // Print Profile Product Setting Relations Alias name
        )->where('product_id', $args['product_id']);

        // Check if any record(s) exist
        if ($getSettingsAssociatedRecords->count() > 0) {
            $getFinalArray = $getSettingsAssociatedRecords->orderBy('xe_id', 'desc')->first();

            /**
             * (#001) Get Product Image Sides from Respective Stores If the
             * store has no product images then, create a blank array with
             * exception error
             */
			$productId = $getFinalArray->product_id;
            $getStoreProduct = $this->getProducts($request, $response, ['id' => $productId]);
            if (!empty($getStoreProduct['data']['status']) && $getStoreProduct['data']['status'] == 1) {
                try {
                    $getProductDetails = $getStoreProduct['data'];
                    $productSideImages = (!empty($getProductDetails['data']['images']) && $getProductDetails['data']['images'] != "") ? $getProductDetails['data']['images'] : $getProductDetails['data']['images'];
                    $productSizes = (!empty($getProductDetails['data']['attributes']) && $getProductDetails['data']['attributes'] != "") ? $getProductDetails['data']['attributes'] : $getProductDetails['data']['attributes'];
                } catch (\Exception $e) {
                    // If no Image found or if there is no product with the given product ID
                    $productSideImages = [
                        'id' => 0,
                        'message' => 'Sorry! It seems that, Store has no relevant product images for this Product',
                        'exception' => show_exception() === true ? $e->getMessage() : '',
                    ];
                }
            }
            // Ends
            // Append Product Name to the final array from the Store API
            if (isset($getProductDetails['data']['name']) && $getProductDetails['data']['name'] != "") {
                $getFinalArray['product_name'] = $getProductDetails['data']['name'];
            }

            /**
             * if the DB has it's own image product ID, send product_image_id, or send 0
             */
            $checkForProductImage = ProductImageSettingsRel::where('product_setting_id', $getFinalArray['xe_id'])->first();
            $hasProdImgId = (isset($checkForProductImage['product_image_id']) && $checkForProductImage['product_image_id'] > 0) ? $checkForProductImage['product_image_id'] : 0;

            // Build Product Settings Array
            $productDecorationSettingData = [
                'id' => $getFinalArray['xe_id'],
                'product_id' => $getFinalArray['product_id'],
                'is_variable_decoration' => $getFinalArray['is_variable_decoration'],
                'product_name' => $getFinalArray['product_name'],
                'is_ruler' => $getFinalArray['is_ruler'],
                'is_crop_mark' => $getFinalArray['is_crop_mark'],
                'is_safe_zone' => $getFinalArray['is_safe_zone'],
                'crop_value' => (float) $getFinalArray['crop_value'],
                'safe_value' => (float) $getFinalArray['safe_value'],
                'is_3d_preview' => $getFinalArray['is_3d_preview'],
                '3d_object_file' => $getFinalArray['3d_object_file'],
                '3d_object' => $getFinalArray['3d_object'],
                'scale_unit_id' => $getFinalArray['scale_unit_id'],
                'is_product_image' => $hasProdImgId > 0 ? 1 : 0,
                'product_image_id' => $hasProdImgId,
            ];
            if (isset($productSizes) && count($productSizes) > 0) {
                $productDecorationSettingData += $productSizes;
            }
            $productDecorationSettingData += ['store_images' => $productSideImages];

            $getProductImageSideInit = ProductImageSides::where('product_image_id', $checkForProductImage['product_image_id'])->get();
            $productDecorationSides = [];
            // Check if requested array is for Decoration or for variable Decoration
            if (isset($getFinalArray['sides']) && count($getFinalArray['sides']) > 0) {
                foreach ($getFinalArray['sides'] as $sideKey => $side) {
                    // Build Product Side Array
                    $productDecorationSides['id'] = $side['xe_id'];
                    $productDecorationSides['name'] = $side['side_name'];
                    $productDecorationSides['index'] = $side['side_index'];
                    $productDecorationSides['dimension'] = $side['dimension'];
                    $productDecorationSides['is_visible'] = $side['is_visible'];

                    // Get side image accordoing to the side index of the side array
                    // Check if product_image_id exist in ProductImageSettingsRel
                    $prodImgSettRelGtInit = new ProductImageSettingsRel();
                    $doExistProdImgSetts = $prodImgSettRelGtInit->where('product_image_id', $checkForProductImage['product_image_id']);
                    if ($doExistProdImgSetts->count() === 0) {
                        // Get Product Image Sides from Respective Stores
                        /**
                         * In the section #001, we got all images from Store
                         * end. There may be multiple images. Each image belongs
                         * to one side Programatically, we get each side by the
                         * foreach loop key's index
                         */
                        if (!empty($productSideImages[$sideKey])) {
                            $productDecorationSides['image'] = $productSideImages[$sideKey];
                        }
                    } else {
                        // Get Product Image Sides from DB
                        if (!empty($getProductImageSideInit[$sideKey])) {
                            $getProductImageSideData = $getProductImageSideInit[$sideKey];
                            if (!empty($getProductImageSideData)) {
                                $productDecorationSides['image'] = [
                                    'id' => $getProductImageSideData->xe_id,
                                    'src' => $getProductImageSideData->file_name,
                                    'thumbnail' => $getProductImageSideData->thumbnail,
                                ];
                            }
                        }
                    }
                    // End

                    // Loop through, Product Decoration Settings
                    $productDecorationSides['decoration_settings'] = [];
                    if (isset($side['product_decoration_setting']) && count($side['product_decoration_setting']) > 0) {
                        foreach ($side['product_decoration_setting'] as $decorationSettingLoop => $decorationSetting) {
                            $decorationSizeVariantList = [];
                            foreach ($decorationSetting['product_size_variant_decoration_settings'] as $sizevariantKey => $sizeVariant) {
                                $decorationSizeVariantList[$sizevariantKey] = [
                                    'print_area_id' => $sizeVariant['print_area_id'],
                                    'size_variant_id' => (int) $sizeVariant['size_variant_id'],
                                ];
                            }
                            $productDecorationSides['decoration_settings'][$decorationSettingLoop] = [
                                'name' => $decorationSetting['name'],
                                'dimension' => $decorationSetting['dimension'],
                                'sub_print_area_type' => $decorationSetting['sub_print_area_type'],
                                'min_height' => $decorationSetting['min_height'],
                                'max_height' => $decorationSetting['max_height'],
                                'min_width' => $decorationSetting['min_width'],
                                'max_width' => $decorationSetting['max_width'],
                                'is_border_enable' => $decorationSetting['is_border_enable'],
                                'is_sides_allow' => $decorationSetting['is_sides_allow'],
                                'print_area_id' => $decorationSetting['print_area_id'],
                                'decoration_size_variants' => (isset($decorationSizeVariantList) && count($decorationSizeVariantList) > 0) ? $decorationSizeVariantList : [],
                            ];
                            // Loop through, Print Profile Decoration Setting
                            if (isset($decorationSetting['print_profile_decoration_settings']) && count($decorationSetting['print_profile_decoration_settings']) > 0) {
                                foreach ($decorationSetting['print_profile_decoration_settings'] as $ppDecoSetting) {
                                    if ((isset($ppDecoSetting['print_profile'][0]['xe_id']) && $ppDecoSetting['print_profile'][0]['xe_id'] > 0)
                                        && (isset($ppDecoSetting['print_profile'][0]['name']) && $ppDecoSetting['print_profile'][0]['name'] != "")
                                    ) {
                                        $productDecorationSides['decoration_settings'][$decorationSettingLoop]['print_profiles'][] = [
                                            'id' => $ppDecoSetting['print_profile'][0]['xe_id'],
                                            'name' => $ppDecoSetting['print_profile'][0]['name'],
                                        ];
                                    }
                                }
                            }
                        }
                    }
                    $productDecorationSettingData['sides'][$sideKey] = $productDecorationSides;
                } // End of foreach print_area_id
            } else {
                $varProdDecoData = [];
                // Get variable decoration settings data
                $productSettingId = $getFinalArray->xe_id;
                $getProductDecorationSetting = ProductDecorationSetting::where(['product_setting_id' => $productSettingId])->first();

                $varProdDecoData['product_decoration']['dimension'] = json_clean_decode($getProductDecorationSetting['dimension'], true);
                $varProdDecoData['product_decoration']['bound_price'] = $getProductDecorationSetting['custom_bound_price'];
                $varProdDecoData['product_decoration']['is_border_enable'] = $getProductDecorationSetting['is_border_enable'];
                $varProdDecoData['product_decoration']['is_sides_allow'] = $getProductDecorationSetting['is_sides_allow'];
                $varProdDecoData['product_decoration']['no_of_sides'] = $getProductDecorationSetting['no_of_sides'];
                $varProdDecoData['product_decoration']['print_area_id'] = $getProductDecorationSetting['print_area_id'];

                if (!empty($getProductDecorationSetting['pre_defined_dimensions']) && $getProductDecorationSetting['pre_defined_dimensions'] != "") {
                    $varProdDecoData['product_decoration']['is_pre_defined'] = !empty($getProductDecorationSetting['pre_defined_dimensions']) ? 1 : 0;
                    $varProdDecoData['product_decoration']['pre_defined_dimensions'] = json_decode($getProductDecorationSetting['pre_defined_dimensions'], true);
                } else if (!empty($getProductDecorationSetting['user_defined_dimensions']) && $getProductDecorationSetting['user_defined_dimensions'] != "") {
                    $varProdDecoData['product_decoration']['is_pre_defined'] = !empty($getProductDecorationSetting['user_defined_dimensions']) ? 0 : 1;
                    $varProdDecoData['product_decoration']['user_defined_dimensions'] = json_decode($getProductDecorationSetting['user_defined_dimensions'], true);
                }

                $productDecorationSettingData += $varProdDecoData;
            }

            // Processing Print Profiles
            $productDecorationSettingData['print_profiles'] = [];
            if (isset($getFinalArray['print_profiles']) && count($getFinalArray['print_profiles']) > 0) {
                foreach ($getFinalArray['print_profiles'] as $printProfile) {
                    if (!empty($printProfile['profile']['xe_id']) && $printProfile['profile']['xe_id'] > 0 && !empty($printProfile['profile']['name']) && $printProfile['profile']['name'] != null) {
                        $productDecorationSettingData['print_profiles'][] = [
                            'id' => $printProfile['profile']['xe_id'],
                            'name' => $printProfile['profile']['name'],
                        ];
                    }
                }
            }

            $jsonResponse = [
                'status' => 1,
                'data' => $productDecorationSettingData,
            ];
        } else {
            $jsonResponse = [
                'status' => 1,
                'data' => [],
                'message' => message('Product Decoration Setting', 'not_found'),
            ];
        }

        return response($response, ['data' => $jsonResponse, 'status' => $serverStatusCode]);
    }

    /**
     * Get: Fetch Product Decoration Settings
     *
     * @param $request  Slim's Request object
     * @param $response Slim's Response object
     * @param $args     Slim's Argument parameters
     *
     * @author tapasranjanp@riaxe.com
     * @date   17 Jan 2020
     * @return Json
     */
    public function getProductDecorationsClient($request, $response, $args)
    {
        $serverStatusCode = OPERATION_OKAY;
        $jsonResponse = [];
        $productDecorationSettingData = [];
        $productSideImages = [];

        /**
         * Relational Tables by their function names from Model size_variant_id
         */
        $getSettingsAssociatedRecords = ProductSetting::with(
            'sides', 'sides.product_decoration_setting', 'sides.product_decoration_setting.product_size_variant_decoration_settings',
            'sides.product_decoration_setting.print_profile_decoration_settings',
            'sides.product_decoration_setting.print_profile_decoration_settings.print_profile',
            'print_profiles', 'print_profiles.profile' // Print Profile Product Setting Relations Alias name
        )->where('product_id', $args['product_id']);

        // Check if any record(s) exist
        if ($getSettingsAssociatedRecords->count() > 0) {
            $getFinalArray = $getSettingsAssociatedRecords->orderBy('xe_id', 'desc')->first();

            /**
             * (#001) Get Product Image Sides from Respective Stores If the
             * store has no product images then, create a blank array with
             * exception error
             */
            $productApiUri = BASE_URL . 'products/' . $request->getQueryParam('variant_id');
            $client = new Client();
            try {
                $productAPIresponse = $client->request('GET', $productApiUri);
                // $clientResponseCode = $productAPIresponse->getStatusCode();
                $responseBody = $productAPIresponse->getBody();
                $getProductDetailsJson = $responseBody->getContents();
                $getProductDetails = json_decode($getProductDetailsJson, true);
                $productSideImages = (!empty($getProductDetails['data']['images']) && $getProductDetails['data']['images'] != "") ? $getProductDetails['data']['images'] : $getProductDetails['data']['images'];
                $productSizes = (!empty($getProductDetails['data']['attributes']) && $getProductDetails['data']['attributes'] != "") ? $getProductDetails['data']['attributes'] : $getProductDetails['data']['attributes'];
            } catch (\Exception $e) {
                // If no Image found or if there is no product with the given product ID
                $productSideImages = [
                    'id' => 0,
                    'message' => 'Sorry! It seems that, Store has no relevant product images for this Product',
                    'exception' => show_exception() === true ? $e->getMessage() : '',
                ];
            }
            // Ends
            // Append Product Name to the final array from the Store API
            if (isset($getProductDetails['data']['name']) && $getProductDetails['data']['name'] != "") {
                $getFinalArray['product_name'] = $getProductDetails['data']['name'];
            }

            /**
             * if the DB has it's own image product ID, send product_image_id, or send 0
             */
            $checkForProductImage = ProductImageSettingsRel::where('product_setting_id', $getFinalArray['xe_id'])->first();
            $hasProdImgId = (isset($checkForProductImage['product_image_id']) && $checkForProductImage['product_image_id'] > 0) ? $checkForProductImage['product_image_id'] : 0;

            // Build Product Settings Array
            $productDecorationSettingData = [
                'id' => $getFinalArray['xe_id'],
                'product_id' => $getFinalArray['product_id'],
                'is_variable_decoration' => $getFinalArray['is_variable_decoration'],
                'product_name' => $getFinalArray['product_name'],
                'is_ruler' => $getFinalArray['is_ruler'],
                'is_crop_mark' => $getFinalArray['is_crop_mark'],
                'is_safe_zone' => $getFinalArray['is_safe_zone'],
                'crop_value' => (float) $getFinalArray['crop_value'],
                'safe_value' => (float) $getFinalArray['safe_value'],
                'is_3d_preview' => $getFinalArray['is_3d_preview'],
                '3d_object_file' => $getFinalArray['3d_object_file'],
                '3d_object' => $getFinalArray['3d_object'],
                'scale_unit_id' => $getFinalArray['scale_unit_id'],
                'is_product_image' => $hasProdImgId > 0 ? 1 : 0,
                'product_image_id' => $hasProdImgId,
            ];
            if (isset($productSizes) && count($productSizes) > 0) {
                $productDecorationSettingData += $productSizes;
            }

            $getProductImageSideInit = ProductImageSides::where('product_image_id', $checkForProductImage['product_image_id'])->get();
            $productDecorationSides = [];
            // Check if requested array is for Decoration or for variable Decoration
            if (isset($getFinalArray['sides']) && count($getFinalArray['sides']) > 0) {
                foreach ($getFinalArray['sides'] as $sideKey => $side) {
                    // Build Product Side Array
                    $productDecorationSides['name'] = $side['side_name'];
                    $productDecorationSides['is_visible'] = $side['is_visible'];

                    // Get side image accordoing to the side index of the side array
                    // Check if product_image_id exist in ProductImageSettingsRel
                    $prodImgSettRelGtInit = new ProductImageSettingsRel();
                    $doExistProdImgSetts = $prodImgSettRelGtInit->where('product_image_id', $checkForProductImage['product_image_id']);
                    if ($doExistProdImgSetts->count() === 0) {
                        // Get Product Image Sides from Respective Stores
                        /**
                         * In the section #001, we got all images from Store
                         * end. There may be multiple images. Each image belongs
                         * to one side Programatically, we get each side by the
                         * foreach loop key's index
                         */
                        if (!empty($productSideImages[$sideKey])) {
                            $productDecorationSides['image'] = $productSideImages[$sideKey];
                        }
                    } else {
                        // Get Product Image Sides from DB
                        if (!empty($getProductImageSideInit[$sideKey])) {
                            $getProductImageSideData = $getProductImageSideInit[$sideKey];
                            if (!empty($getProductImageSideData)) {
                                $productDecorationSides['image'] = [
                                    'id' => $getProductImageSideData->xe_id,
                                    'src' => $getProductImageSideData->file_name,
                                    'thumbnail' => $getProductImageSideData->thumbnail,
                                ];
                            }
                        }
                    }
                    // End

                    // Loop through, Product Decoration Settings
                    $productDecorationSides['decoration_settings'] = [];
                    if (isset($side['product_decoration_setting']) && count($side['product_decoration_setting']) > 0) {
                        foreach ($side['product_decoration_setting'] as $decorationSettingLoop => $decorationSetting) {
                            $decorationSizeVariantList = [];
                            foreach ($decorationSetting['product_size_variant_decoration_settings'] as $sizevariantKey => $sizeVariant) {
                                $decorationSizeVariantList[$sizevariantKey] = [
                                    'print_area_id' => $sizeVariant['print_area_id'],
                                    'size_variant_id' => (int) $sizeVariant['size_variant_id'],
                                ];
                            }
                            // Get print area and print area type
                            $getPrintAreaDetails = PrintArea::where('xe_id', $decorationSetting['print_area_id'])
                            ->with('print_area_type')
                            ->get();

                            $printAreaDetails = $getPrintAreaDetails->toArray();
                            if (!empty($printAreaDetails)) {
                                $path = "";
                                $typeName = "";
                                if (!empty($printAreaDetails[0]['print_area_type'])) {
                                    $typeName = $printAreaDetails[0]['print_area_type']['name'];
                                }
                                $path = $printAreaDetails[0]['file_name'];
                                $printArea = array('id' => $printAreaDetails[0]['xe_id'],'name' => $printAreaDetails[0]['name'],'type' => $typeName,'height' => $printAreaDetails[0]['height'],'width' => $printAreaDetails[0]['width'],'path' => $path);
                            } else {
                                $printArea = array();
                            }
                            // End
                            $productDecorationSides['decoration_settings'][$decorationSettingLoop] = [
                                'name' => $decorationSetting['name'],
                                'dimension' => $decorationSetting['dimension'],
                                'sub_print_area_type' => $decorationSetting['sub_print_area_type'],
                                'min_height' => $decorationSetting['min_height'],
                                'max_height' => $decorationSetting['max_height'],
                                'min_width' => $decorationSetting['min_width'],
                                'max_width' => $decorationSetting['max_width'],
                                'is_border_enable' => $decorationSetting['is_border_enable'],
                                'is_sides_allow' => $decorationSetting['is_sides_allow'],
                                'print_area' => $printArea,
                                'decoration_size_variants' => (isset($decorationSizeVariantList) && count($decorationSizeVariantList) > 0) ? $decorationSizeVariantList : [],
                            ];
                            // Loop through, Print Profile Decoration Setting
                            if (isset($decorationSetting['print_profile_decoration_settings']) && count($decorationSetting['print_profile_decoration_settings']) > 0) {
                                foreach ($decorationSetting['print_profile_decoration_settings'] as $ppDecoSetting) {
                                    if ((isset($ppDecoSetting['print_profile'][0]['xe_id']) && $ppDecoSetting['print_profile'][0]['xe_id'] > 0)
                                        && (isset($ppDecoSetting['print_profile'][0]['name']) && $ppDecoSetting['print_profile'][0]['name'] != "")
                                    ) {
                                        $productDecorationSides['decoration_settings'][$decorationSettingLoop]['print_profiles'][] = [
                                            'id' => $ppDecoSetting['print_profile'][0]['xe_id'],
                                            'name' => $ppDecoSetting['print_profile'][0]['name'],
                                        ];
                                    }
                                }
                            }
                        }
                    }
                    $productDecorationSettingData['sides'][$sideKey] = $productDecorationSides;
                } // End of foreach print_area_id
            } else {
                $varProdDecoData = [];
                // Get variable decoration settings data
                $productSettingId = $getFinalArray->xe_id;
                $getProductDecorationSetting = ProductDecorationSetting::where(['product_setting_id' => $productSettingId])->first();
                // Get print area and print area type
                $getPrintAreaDetails = PrintArea::where('xe_id', $getProductDecorationSetting['print_area_id'])
                ->with('print_area_type')
                ->get();

                $printAreaDetails = $getPrintAreaDetails->toArray();
                if (!empty($printAreaDetails)) {
                    $path = "";
                    $typeName = "";
                    if (!empty($printAreaDetails[0]['print_area_type'])) {
                        $typeName = $printAreaDetails[0]['print_area_type']['name'];
                    }
                    $path = $printAreaDetails[0]['file_name'];
                    $printArea = array('id' => $printAreaDetails[0]['xe_id'],'name' => $printAreaDetails[0]['name'],'type' => $typeName,'height' => $printAreaDetails[0]['height'],'width' => $printAreaDetails[0]['width'],'path' => $path);
                } else {
                    $printArea = array();
                }
                // End

                $varProdDecoData['product_decoration']['dimension'] = json_clean_decode($getProductDecorationSetting['dimension'], true);
                $varProdDecoData['product_decoration']['bound_price'] = $getProductDecorationSetting['custom_bound_price'];
                $varProdDecoData['product_decoration']['is_border_enable'] = $getProductDecorationSetting['is_border_enable'];
                $varProdDecoData['product_decoration']['is_sides_allow'] = $getProductDecorationSetting['is_sides_allow'];
                $varProdDecoData['product_decoration']['no_of_sides'] = $getProductDecorationSetting['no_of_sides'];
                $varProdDecoData['product_decoration']['print_area'] = $printArea;

                if (!empty($getProductDecorationSetting['pre_defined_dimensions']) && $getProductDecorationSetting['pre_defined_dimensions'] != "") {
                    $varProdDecoData['product_decoration']['is_pre_defined'] = !empty($getProductDecorationSetting['pre_defined_dimensions']) ? 1 : 0;
                    $varProdDecoData['product_decoration']['pre_defined_dimensions'] = json_decode($getProductDecorationSetting['pre_defined_dimensions'], true);
                } else if (!empty($getProductDecorationSetting['user_defined_dimensions']) && $getProductDecorationSetting['user_defined_dimensions'] != "") {
                    $varProdDecoData['product_decoration']['is_pre_defined'] = !empty($getProductDecorationSetting['user_defined_dimensions']) ? 0 : 1;
                    $varProdDecoData['product_decoration']['user_defined_dimensions'] = json_decode($getProductDecorationSetting['user_defined_dimensions'], true);
                }

                $productDecorationSettingData += $varProdDecoData;
            }

            // Processing Print Profiles
            $productDecorationSettingData['print_profiles'] = [];
            if (isset($getFinalArray['print_profiles']) && count($getFinalArray['print_profiles']) > 0) {
                foreach ($getFinalArray['print_profiles'] as $printProfile) {
                    if (!empty($printProfile['profile']['xe_id']) && $printProfile['profile']['xe_id'] > 0 && !empty($printProfile['profile']['name']) && $printProfile['profile']['name'] != null) {
                        $productDecorationSettingData['print_profiles'][] = [
                            'id' => $printProfile['profile']['xe_id'],
                            'name' => $printProfile['profile']['name'],
                        ];
                    }
                }
            }

            $jsonResponse = [
                'status' => 1,
                'data' => $productDecorationSettingData,
            ];
        } else {
            $jsonResponse = [
                'status' => 1,
                'data' => [],
                'message' => message('Product Decoration Setting', 'not_found'),
            ];
        }

        return response($response, ['data' => $jsonResponse, 'status' => $serverStatusCode]);
    }
}