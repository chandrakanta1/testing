<?php
/**
 * Manage Products from Woocommerce Store
 *
 * PHP version 5.6
 *
 * @category  Products
 * @package   Store
 * @author    Tanmaya Patra <tanmayap@riaxe.com>
 * @copyright 2019-2020 Riaxe Systems
 * @license   http://www.php.net/license/3_0.txt  PHP License 3.0
 * @link      http://inkxe-v10.inkxe.io/xetool/admin
 */
namespace App\Modules\Products\Controllers;

use App\Modules\Products\Models\AppUnit;
use App\Modules\Products\Models\AttributePriceRule;
use App\Modules\Products\Models\ProductSetting;
use App\Modules\Settings\Models\ColorSwatch;
use ProductStoreSpace\Controllers\StoreProductsController;

/**
 * Products Controller
 *
 * @category Class
 * @package  Print_Profile
 * @author   Tanmaya Patra <tanmayap@riaxe.com>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://inkxe-v10.inkxe.io/xetool/admin
 */
class ProductsController extends StoreProductsController
{
    /*
    |--------------------------------------------------------------------------
    | Get details of Products and their Attributes
    |--------------------------------------------------------------------------
     */

    /**
     * GET: Getting List of All product or Single product information
     *
     * @param $request  Slim's Request object
     * @param $response Slim's Response object
     * @param $args     Slim's Argument parameters
     *
     * @author tanmayap@riaxe.com
     * @date   5 Oct 2019
     * @return A JSON Response
     */
    public function getProductList($request, $response, $args)
    {

        $serverStatusCode = OPERATION_OKAY;
        $jsonResponse = $this->getProducts($request, $response, $args);

        return response(
            $response, [
                'data' => $jsonResponse['data'], 'status' => $serverStatusCode,
            ]
        );

    }
    /**
     * GET: Getting List of All Category/Subcategory or Single
     * Category/Subcategory information
     *
     * @param $request  Slim's Request object
     * @param $response Slim's Response object
     * @param $args     Slim's Argument parameters
     *
     * @author tanmayap@riaxe.com
     * @date   5 Oct 2019
     * @return A JSON Response
     */
    public function totalCategories($request, $response, $args)
    {

        $serverStatusCode = OPERATION_OKAY;
        $jsonResponse = $this->getCategories($request, $response, $args);

        return response(
            $response, [
                'data' => $jsonResponse['data'], 'status' => $serverStatusCode
                ]
        );
    }

    /**
     * GET: Get list of Measurement Units
     *
     * @param $request  Slim's Request object
     * @param $response Slim's Response object
     * @param $args     Slim's Argument parameters
     *
     * @author tanmayap@riaxe.com
     * @date   5 Oct 2019
     * @return A JSON Response
     */
    public function getMeasurementUnits($request, $response, $args)
    {
        $serverStatusCode = OPERATION_OKAY;
        $appUnitInit = new AppUnit();
        $initAppUnit = $appUnitInit->whereNotNull('name');
        $jsonResponse = [
            'status' => 0,
            'message' => message('Measurement Unit', 'error'),
        ];

        if ($initAppUnit->count() > 0) {
            $jsonResponse = [
                'status' => 1,
                'data' => $initAppUnit->orderBy('xe_id', 'desc')->get(),
            ];
        }
        return response(
            $response, ['data' => $jsonResponse, 'status' => $serverStatusCode]
        );
    }

    /*
    |--------------------------------------------------------------------------
    | Manage Product Predecorator and their Variants
    |--------------------------------------------------------------------------
     */

    /**
     * Post: Save Predecorator Data at Store end
     *
     * @param $request  Slim's Request object
     * @param $response Slim's Response object
     *
     * @author tanmayap@riaxe.com
     * @date   5 Oct 2019
     * @return Save response Array
     */
    public function savePredecorator($request, $response)
    {
        $serverStatusCode = OPERATION_OKAY;
        $allPostPutVars = $request->getParsedBody();
        $storeId = get_store_details($request);
        // Call Internal Store
        $savePredecoratedResponse = $this->saveProduct($request, $response);
        return response(
            $response, [
                'data' => $savePredecoratedResponse['data'],
                'status' => $savePredecoratedResponse['server_status']
                ]
        );
    }
    /**
     * Post: Create Product Variations
     *
     * @param $request  Slim's Request object
     * @param $response Slim's Response object
     *
     * @author tanmayap@riaxe.com
     * @date   5 Oct 2019
     * @return Save response Array
     */
    public function createVariations($request, $response)
    {
        $variationResponse = $this->createProductVariations($request, $response);
        return response(
            $response, [
                'data' => $variationResponse['data'],
                'status' => $variationResponse['server_status']
                ]
        );
    }

    /**
     * Post: Validate Sku and Name
     *
     * @param $request  Slim's Request object
     * @param $response Slim's Response object
     *
     * @author tanmayap@riaxe.com
     * @date   5 Oct 2019
     * @return Validate response Array
     */
    public function validateParams($request, $response)
    {
        $validationResponse = $this->validateStoreSkuName($request, $response);
        return response(
            $response, [
                'data' => $validationResponse['data'],
                'status' => $validationResponse['server_status']
                ]
        );
    }
    /**
     * Get: Get Attribute List
     *
     * @param $request  Slim's Request object
     * @param $response Slim's Response object
     *
     * @author tanmayap@riaxe.com
     * @date   5 Oct 2019
     * @return A JSON Response
     */
    public function getStoreAttributes($request, $response)
    {
        $attributeResponse = $this->storeAttributeList($request, $response);
        return response(
            $response, [
                'data' => $attributeResponse['data'],
                'status' => $attributeResponse['server_status']
                ]
        );
    }
    /*
    |--------------------------------------------------------------------------
    | Manage Product Attribute Pricing
    |--------------------------------------------------------------------------
     */

    /**
     * GET: Get Product Attribute Pricing by Product Id
     *
     * @param $request  Slim's Request object
     * @param $response Slim's Response object
     * @param $args     Slim's Argument parameters
     *
     * @author satyabratap@riaxe.com
     * @date   5 Oct 2019
     * @return Json
     */
    public function getProductAttrPrc($request, $response, $args)
    {
        $serverStatusCode = OPERATION_OKAY;
        $productData = $this->storeProductAttrPrc($request, $response, $args);
        if ($productData['status'] == 1) {
            $productAttributes = [];
            $printProfiles = $productData['data']['print_profile'];
            $productDetails = $productData['data']['product_details'];
            foreach ($productDetails as $attrKey => $attributes) {
                foreach ($attributes['options'] as $termKey => $termValue) {
                    $getPriceData = AttributePriceRule::where(['product_id' => intval($args['id']), 'attribute_id' => $attributes['id'], 'attribute_term_id' => $termValue['id']])->select('print_profile_id', 'price');
                    $priceData = [];
                    if (isset($getPriceData) && $getPriceData->count() > 0) {
                        foreach ($getPriceData->get() as $priceKey => $priceValue) {
                            $priceData[$priceKey] = [
                                'print_profile_id' => $priceValue->print_profile_id,
                                'price' => $priceValue->price,
                            ];
                        }
                    }
                    $attrDetails = [];
                    $attrDetails = [
                        'id' => $termValue['id'],
                        'name' => $termValue['name'],
                    ];
                    if (isset($attributes['name']) && $attributes['name'] == 'color') {
                        $getTermLocalData = ColorSwatch::where('attribute_id', $termValue['id'])->first();
                        $attrDetails['hex_code'] = $attrDetails['file_name'] = "";
                        if (!empty($getTermLocalData['hex_code']) && $getTermLocalData['hex_code'] != "") {
                            $attrDetails['hex_code'] = $getTermLocalData['hex_code'];
                        }
                        if (!empty($getTermLocalData['file_name']) && $getTermLocalData['file_name'] != "") {
                            $attrDetails['file_name'] = $getTermLocalData['file_name'];
                        }
                    }
                    $attrDetails['price_data'] = $priceData;
                    $attrTerms[$termKey] = $attrDetails;
                }
                $productAttributes[$attrKey] = [
                    'id' => $attributes['id'],
                    'name' => $attributes['name'],
                    'attribute_term' => $attrTerms,
                ];
            }
            $jsonResponse = [
                'status' => 1,
                'data' => [
                    'print_profile' => $printProfiles,
                    'attributes' => $productAttributes,
                ]
            ];
        }

        return response(
            $response, ['data' => $jsonResponse, 'status' => $serverStatusCode]
        );
    }

    /**
     * Save Product attribute prices
     *
     * @param $request  Slim's Request object
     * @param $response Slim's Response object
     * @param $args     Slim's Argument parameters
     *
     * @author satyabratap@riaxe.com
     * @date   13 Dec 2019
     * @return boolean
     */
    public function saveProdAttrPrc($request, $response, $args)
    {
        $serverStatusCode = OPERATION_OKAY;
        $allPostPutVars = $request->getParsedBody();
        $jsonResponse = [
            'status' => 0,
            'message' => message('Attribute Pricing', 'error'),
        ];

        if (isset($allPostPutVars['price_data']) && !empty($allPostPutVars['price_data'])) {
            $attributePriceJson = $allPostPutVars['price_data'];
            $attributePriceArray = json_decode($attributePriceJson, true);
            if (isset($attributePriceArray['product_id']) && $attributePriceArray['product_id'] > 0) {
                $productId = $attributePriceArray['product_id'];
                AttributePriceRule::where('product_id', $productId)->delete();
                if (isset($attributePriceArray['attributes']) 
                    && count($attributePriceArray['attributes']) == 0
                ) {
                    $jsonResponse = [
                        'status' => 1,
                        'message' => message('Attribute Pricing', 'updated'),
                    ];
                } else {
                    $success = 0;
                    foreach ($attributePriceArray['attributes'] as $attributeKey => $attributeData) {
                        $attributeId = $attributeData['attribute_id'];
                        if (isset($attributeData['attribute_term']) && $attributeData['attribute_term'] != "") {
                            foreach ($attributeData['attribute_term'] as $attributeTermKey => $attributeTermData) {
                                $attributeTermId = $attributeTermData['attribute_term_id'];
                                if (isset($attributeTermData['price_data']) && $attributeTermData['price_data'] != "") {
                                    foreach ($attributeTermData['price_data'] as $priceKey => $priceData) {
                                        $printProfileId = $priceData['print_profile_id'];
                                        $price = $priceData['price'];
                                        $attributePrices = [
                                            'product_id' => $productId,
                                            'attribute_id' => $attributeId,
                                            'attribute_term_id' => $attributeTermId,
                                            'print_profile_id' => $printProfileId,
                                            'price' => $price,
                                        ];
                                        $saveAttributePrice = new AttributePriceRule($attributePrices);
                                        if ($saveAttributePrice->save()) {
                                            $success++;
                                        }
                                    }
                                }
                            }
                        }
                    }
                    if (!empty($success) && $success > 0) {
                        $jsonResponse = [
                            'status' => 1,
                            'message' => $success . ' Prices saved successfully',
                        ];
                    }
                }
            }
        }
        return response(
            $response, ['data' => $jsonResponse, 'status' => $serverStatusCode]
        );
    }

    /**
     * Getting List of All color of a single product
     *
     * @param $request  Slim's Request object
     * @param $response Slim's Response object
     * @param $args     Slim's Argument parameters
     *
     * @author satyabratap@riaxe.com
     * @date   19 Sept 2019
     * @return Array list of colors
     */
    public function colorsByProductId($request, $response, $args)
    {
        $serverStatusCode = OPERATION_OKAY;
        $jsonResponse = $this->colorsByProduct($request, $response, $args);

        return response(
            $response, [
                'data' => $jsonResponse['data'], 'status' => $serverStatusCode
                ]
        );
    }

}
