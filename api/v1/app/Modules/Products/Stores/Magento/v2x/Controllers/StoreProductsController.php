<?php 
    /**
     *
     * This Controller used to save, fetch or delete Magento Products on various endpoints
     *
     * @category   Products
     * @package    Magento API
     * @author     Original Author <tapasranjanp@riaxe.com>
     * @author     tapasranjanp@riaxe.com
     * @copyright  2019-2020 Riaxe Systems
     * @license    http://www.php.net/license/3_0.txt  PHP License 3.0
     * @version    Release: @1.0
     */
    namespace ProductStoreSpace\Controllers;

    use App\Modules\Products\Models\ProductSetting;
    use ComponentStoreSpace\Controllers\StoreComponent;

    class StoreProductsController extends StoreComponent
    {
        /**
         * Get list of product or a Single product from the Magento API
         *
         * @author     tapasranjanp@riaxe.com
         * @date       18 Dec 2019
         * @parameter  Slim default params
         * @response   Array of list/one product(s)
         */
        public function getProducts($request, $response, $args)
        {
            $serverStatusCode = OPERATION_OKAY;
            $products = [];
            $getStoreDetails = get_store_details($request);
            if(isset($args['id']) && $args['id'] != "" && $args['id'] > 0) {
                // For fetching Single Product
                $productId = $args['id'];
                $productInfo = array(
                    'productId' =>$productId,
                    'store' => $getStoreDetails['store_id']
                );
                $productData = array();
                $result = $this->apiCall('Product', 'getProductById', $productInfo);
                $data  = json_decode($result->result,true);
                if(isset($data) && is_array($data) && count($data) > 0) {
                    $printProfiles = array();
                    $isDecorationExists = 0;
                    $getSettings = ProductSetting::where('product_id', $productId);
                    if($getSettings->count() > 0) {
                        $isDecorationExists = 1;
                        $productSetting = $getSettings->with(
                            'print_profiles', 'print_profiles.profile'
                        )->first();
                        foreach ($productSetting['print_profiles'] as $profileKey => $profile) {
                            $printProfiles[$profileKey] = [
                                'id' => $profile['profile']['xe_id'],
                                'name' => $profile['profile']['name'],
                            ];
                        }
                    }
                    $data['data']['printProfiles'] = $printProfiles;
                    $data['data']['is_decoration_exists'] = $isDecorationExists;
                    $jsonResponse = [
                        'status' => 1,
                        'records' => sizeof($data),
                        'total_records' => sizeof($data),
                        'data' => $data['data']
                    ];   
                }else{
                    $jsonResponse = [
                        'status' => 0,
                        'records' => 0,
                        'total_records' => 0,
                        'data' => 'No products available'
                    ];
                }
            } else {
                // For fetching All Product by filteration
                $getTotalProductsCount = 0;
                $filterArray = array('type' => array('eq' => 'configurable'));
                $searchstring = $request->getQueryParam('name') ? $request->getQueryParam('name') : '';
                $categoryid = $request->getQueryParam('category') ? $request->getQueryParam('category') : 2;
                $page = $request->getQueryParam('page') ? $request->getQueryParam('page') : 0;
                $perpage = $request->getQueryParam('perpage') ? $request->getQueryParam('perpage') : 20;
                $sku = $request->getQueryParam('sku') ? $request->getQueryParam('sku') : '';
                $order = $request->getQueryParam('order') ? $request->getQueryParam('order') : 'desc';
                $orderby  = $request->getQueryParam('orderby') ? $request->getQueryParam('orderby') : 'created_at';
                $filters = array(
                    'filters' => $filterArray,
                    'categoryid' => $categoryid,
                    'searchstring' => $searchstring,
                    'store' => $getStoreDetails['store_id'],
                    'range' => array('start' => $page, 'range' => $perpage),
                    'offset' => $page,
                    'limit' => $perpage,
                    'sku' => $sku,
                    'order' => $order,
                    'orderby' => $orderby
                );
               try {
                    $result = $this->apiCall('Product', 'getProducts', $filters);
                    $products = json_decode($result->result, true);
                    $getTotalProductsCount = $products['records'];
                    if(is_array($products) && count($products) > 0) {
                        $productArray = [];
                        $productArray = $products['data'];
                        foreach ($productArray as $key => $value) {
                            $productId = $value['id'];
                            $getSettings = ProductSetting::where('product_id', $productId);
                            $isDecorationExists = 0;
                            $printProfiles = [];
                            if($getSettings->count() > 0) {
                                $isDecorationExists = 1;
                                $productSetting = $getSettings->with('print_profiles', 'print_profiles.profile')->first();
                                foreach ($productSetting['print_profiles'] as $profileKey => $profile) {
                                    $printProfiles[$profileKey] = [
                                        'id' => $profile['profile']['xe_id'],
                                        'name' => $profile['profile']['name'],
                                    ];
                                }
                            }
                            $productArray[$key]['print_profile'] = $printProfiles;
                            $productArray[$key]['is_decoration_exists'] = $isDecorationExists;
                        }
                        $jsonResponse = [
                            'status' => 1,
                            'records' => count($products['data']),
                            'total_records' => $getTotalProductsCount,
                            'data' => $productArray
                        ];

                    } else {
                        $serverStatusCode = OPERATION_OKAY;
                        $jsonResponse = [
                            'status' => 0,
                            'records' => count($products),
                            'total_records' => $getTotalProductsCount,
                            'message' => 'No products available',
                            'data' => []
                        ];
                    }
                } catch (\Exception $e) {
                    $serverStatusCode = EXCEPTION_OCCURED;
                    $jsonResponse = [
                        'status' => 0,
                        'message' => 'Invalid request',
                        'exception' => $e->getMessage()
                    ];
                }
            }
            // Reset Total product Count 
            $_SESSION['productsCount'] = 0;
            return [
                'data' => $jsonResponse,
                'httpStatusCode' => $serverStatusCode
            ];
        }
        /**
         * Get list of category/subcategory or a Single category/subcategory from the Magento API
         *
         * @author     tapasranjanp@riaxe.com
         * @date       18 Dec 2019
         * @parameter  Slim default params
         * @response   Array of list/one category/subcategory(s)
         */
        public function getCategories($request, $response, $args)
        {
            $serverStatusCode = OPERATION_OKAY;
            $getStoreDetails = get_store_details($request);
            try {
                // Get all category/subcategory(s) with filterration
                $name = $request->getQueryParam('name') ? $request->getQueryParam('name') : '';
                $order = $request->getQueryParam('order') ? $request->getQueryParam('order') : 'desc';
                $orderby  = $request->getQueryParam('orderby') ? $request->getQueryParam('orderby') : 'created_at';
                $filters = [
                    'order' => $order,
                    'orderby' => $orderby,
                    'name' => $name,
                    'store' => $getStoreDetails['store_id']
                ];
                $result = $this->apiCall('Product', 'getCategories', $filters);
                $result = $result->result;
                $categoryDetails   = json_decode($result, true);

                if(isset($categoryDetails) && is_array($categoryDetails) && count($categoryDetails) > 0) {
                    $response = [
                        'status' => 1,
                        'data' => $categoryDetails
                    ];
                }else{
                    $response = [
                        'status' => 0,
                        'data' => 'No category/subcategory(s) available'
                    ];
                }
            } catch (\Exception $e) {
                $serverStatusCode = EXCEPTION_OCCURED;
                $response = [
                    'status' => 0,
                    'message' => 'Invalid request',
                    'exception' => $e->getMessage()
                ];
            }
            return [
                'data' => $response,
                'httpStatusCode' => $serverStatusCode
            ];
        }
        /**
         * Get list of Color Variants from the Magento API as per the product
         *
         * @param $request  Slim's Request object
         * @param $response Slim's Response object
         * @param $args     Slim's Arguments
         *
         * @author tapasranjanp@riaxe.com
         * @date   21 Jan 2020
         * @return Json
         */
        public function colorsByProduct($request, $response, $args)
        {
            $serverStatusCode = OPERATION_OKAY;
            $getStoreDetails = get_store_details($request);
            $variantData = [];
            $jsonResponse = [
                'status' => 1,
                'records' => 0,
                'message' => 'No Color available',
                'data' => [],
            ];
            $filters = [
                'product_id' => $args['id'],
                'store' => $getStoreDetails['store_id'],
                'attribute' => 'xe_color'
            ];
            try {
                $result = $this->apiCall('Product', 'getColorVariantsByProduct', $filters);
                $result = $result->result;
                $singleProductDetails   = json_decode($result, true);
                if (!empty($singleProductDetails)) {
                    $variantData = $this->getColorData($singleProductDetails);
                    $jsonResponse = [
                        'status' => 1,
                        'records' => count($variantData),
                        'data' => $variantData,
                    ];
                }
            } catch (\Exception $e) {
                $serverStatusCode = EXCEPTION_OCCURED;
                $jsonResponse = [
                    'status' => 0,
                    'message' => message('Distress', 'error'), 'exception' => show_exception() === true ? $e->getMessage() : '',
                ];
            }
            return [
                'data' => $jsonResponse,
                'httpStatusCode' => $serverStatusCode,
            ];
        }
        /**
         * GET: Product Attribute Pricing  Details by Product Id
         *
         * @param $request  Slim's Request object
         * @param $response Slim's Response object
         * @param $args     Slim's Arguments
         *
         * @author tapasranjanp@riaxe.com
         * @date   21 Jan 2020
         * @return All store attributes
         */
        public function storeProductAttrPrc($request, $response, $args)
        {
            $serverStatusCode = OPERATION_OKAY;
            $getStoreDetails = get_store_details($request);
            $jsonResponse = [
                'status' => 0,
                'message' => message('Product Pricing', 'not_found'),
                'data' => [],
            ];
            try {
                $productId = intval($args['id']);
                $getProducts = [];
                $filters = [
                    'product_id' => $productId,
                    'store' => $getStoreDetails['store_id']
                ];
                $result = $this->apiCall('Product', 'getAllVariantsByProduct', $filters);
                $result = $result->result;
                $singleProductDetails   = json_decode($result, true);
                $attrTerms = [];
                $productAttributes = [];
                if (!empty($singleProductDetails)) {
                    $productAttributes = $this->attributeDetails($singleProductDetails, $productId);
                }
                // Print Profile Details of the Product.
                $printProfiles = [];
                $printProfileDetails = $this->getAssocPrintProfiles($productId);
                if (!empty($printProfileDetails['print_profiles'])) {
                    $printProfiles = $printProfileDetails['print_profiles'];
                }
                $jsonResponse = [
                    'status' => 1,
                    'data' => [
                        'print_profile' => $printProfiles,
                        'attributes' => $productAttributes
                    ]
                ];
            } catch (\Exception $e) {
                $serverStatusCode = OPERATION_OKAY;
                $jsonResponse = [
                    'status' => 0,
                    'message' => message('Distress', 'error'), 'exception' => show_exception() === true ? $e->getMessage() : '',
                ];
            }
            return [
                'data' => $jsonResponse,
                'status_code' => $serverStatusCode,
            ];
        }
    }