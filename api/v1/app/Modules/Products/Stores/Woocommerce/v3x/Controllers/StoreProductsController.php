<?php
/**
 * Manage Woocommerce Store Products
 *
 * PHP version 5.6
 *
 * @category  Store_Product
 * @package   Store
 * @author    Tanmaya Patra <tanmayap@riaxe.com>
 * @copyright 2019-2020 Riaxe Systems
 * @license   http://www.php.net/license/3_0.txt  PHP License 3.0
 * @link      http://inkxe-v10.inkxe.io/xetool/admin
 */
namespace ProductStoreSpace\Controllers;

use CommonStoreSpace\Controllers\StoreController;

/**
 * Store product Controller
 *
 * @category Store_Product
 * @package  Store
 * @author   Tanmaya Patra <tanmayap@riaxe.com>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://inkxe-v10.inkxe.io/xetool/admin
 */
class StoreProductsController extends StoreController
{
    /**
     * Instantiate Constructer
     */
    public function __construct()
    {
        parent::__construct();
    }
    /**
     * Get: Get the list of product or a Single product from the WooCommerce API
     *
     * @param $request  Slim's Request object
     * @param $response Slim's Response object
     * @param $args     Slim's Argument parameters
     *
     * @author tanmayap@riaxe.com
     * @date   5 Oct 2019
     * @return Array of list/one product(s)
     */
    public function getProducts($request, $response, $args)
    {
        $serverStatusCode = OPERATION_OKAY;
        $products = [];
        $jsonResponse = [
            'status' => 1,
            'records' => 0,
            'total_records' => 0,
            'data' => [],
        ];
        $allowedAttributeNames = ['Size', 'Color'];
        if (isset($args['id']) && $args['id'] > 0) {
            // Fetch Single product Details
            $singleProductDetails = object_to_array($this->wc->get('products/' . $args['id']));
            $productList = [];
            // Collecting Images into the Product Array
            $productImages = [];
            if (isset($singleProductDetails['images']) && count($singleProductDetails['images']) > 0) {
                foreach ($singleProductDetails['images'] as $prodImgKey => $prodImg) {
                    $productImages[] = [
                        'id' => $prodImg['id'],
                        'src' => $prodImg['src'],
                        'thumbnail' => $this->getVariableImageSizes($prodImg['src'], '50'),
                    ];
                }
            }

            $wcAttributes = [];
            if (isset($singleProductDetails['attributes']) && count($singleProductDetails['attributes']) > 0) {
                foreach ($singleProductDetails['attributes'] as $attrKey => $attributes) {
                    if (strtolower($attributes['name']) === 'color') {
                        $colorData = [];
                        $colorDetails = $this->plugin->get('options', ['product_id' => $args['id'], 'attribute' => 'pa_color']);
                        if (!empty($colorDetails)) {
                            $colorData = $this->getColorSwatchData($colorDetails);
                        }
                        $dynamicKeyAttribute = strtolower($attributes['name']);
                        $wcAttributes[$dynamicKeyAttribute] = $attributes['options'];
                        $wcAttributes[$dynamicKeyAttribute] = $colorData;
                    } else {
                        $dynamicKeyAttribute = strtolower($attributes['name']);
                        $wcAttributes[$dynamicKeyAttribute] = $attributes['options'];
                        foreach ($attributes['options'] as $storeSizeKey => $storeSize) {
                            $wcAttributes[$dynamicKeyAttribute][$storeSizeKey] = [
                                'id' => $storeSizeKey,
                                'name' => $storeSize,
                            ];
                        }
                    }
                }
            }

            $price = 0;
            if (isset($singleProductDetails['sale_price']) && $singleProductDetails['sale_price'] != "") {
                $price = $singleProductDetails['sale_price'];
            } else if (isset($singleProductDetails['price']) && $singleProductDetails['price'] != "") {
                $price = $singleProductDetails['price'];
            }
            // Get Print Profile Details
            $getAssociatedPrintProfileData = $this->getAssocPrintProfiles($singleProductDetails['id']);
            $sanitizedProduct = [
                'id' => $singleProductDetails['id'],
                'name' => $singleProductDetails['name'],
                'sku' => $singleProductDetails['sku'],
                'type' => $singleProductDetails['type'],
                'description' => preg_replace("/\r|\n/", "", substr(strip_tags($singleProductDetails['description']), 0, 110)),
                'price' => $price,
                'images' => $productImages,
                'is_decoration_exists' => $getAssociatedPrintProfileData['is_decoration_exists'],
                'print_profile' => $getAssociatedPrintProfileData['print_profiles'],
            ];
            $sanitizedProduct += [
                'categories' => $singleProductDetails['categories'],
                'attributes' => $wcAttributes,
            ];
            $jsonResponse = [
                'status' => 1,
                'records' => 1,
                'data' => $sanitizedProduct,
            ];
        } else {
            // Get all requested Query params
            $filters = [
                'search' => $request->getQueryParam('name'),
                'category' => $request->getQueryParam('category'),
                'range' => $request->getQueryParam('per_page'),
                'page' => $request->getQueryParam('page'),
                'order' => (!empty($request->getQueryParam('order')) && $request->getQueryParam('order') != "") ? $request->getQueryParam('order') : 'desc',
                'order_by' => (!empty($request->getQueryParam('orderby')) && $request->getQueryParam('orderby') != "") ? $request->getQueryParam('orderby') : 'post_date',
                'is_customize' => (!empty($request->getQueryParam('is_customize')) && $request->getQueryParam('is_customize') > 0) ? 1 : 0,
            ];
            // If any outer methods sends category(ies) then this code will run
            if (!empty($args['categories']) && $args['categories'] != "") {
                $filters['category'] = trim($args['categories']);
            }
            if (!empty($args['is_customize']) && $args['is_customize'] != "") {
                $filters['is_customize'] = trim($args['is_customize']);
            }
            $options = [];
            foreach ($filters as $filterKey => $filterValue) {
                if (isset($filterValue) && $filterValue != "") {
                    $options += [$filterKey => $filterValue];
                }
            }
            /**
             * Fetch All Products
             */
            // Calling to Custom API for getting Product List
            $getProducts = object_to_array($this->plugin->get('products', $options));
            $productList = [];
            if (isset($getProducts) && count($getProducts['data']) > 0) {
                foreach ($getProducts['data'] as $eachProductKey => $eachProduct) {
                    $getAssociatedPrintProfileData = $this->getAssocPrintProfiles($eachProduct['id']);
                    $productList[$eachProductKey] = [
                        'id' => $eachProduct['id'],
                        'name' => $eachProduct['name'],
                        'type' => $eachProduct['type'],
                        'sku' => $eachProduct['sku'],
                        'price' => $eachProduct['price'],
                        'image' => isset($eachProduct['image']) ? $eachProduct['image'] : null,
                        'is_decoration_exists' => $getAssociatedPrintProfileData['is_decoration_exists'],
                        'print_profile' => $getAssociatedPrintProfileData['print_profiles'],
                    ];
                }
                $jsonResponse = [
                    'status' => 1,
                    'records' => count($productList),
                    'total_records' => $getProducts['records'],
                    'data' => $productList,

                ];
            }
        }
        // Reset Total product Count
        return [
            'data' => $jsonResponse,
            'httpStatusCode' => $serverStatusCode,
        ];
    }

    /**
     * GET: Get list of category/subcategory or a Single category/subcategory
     * from the WooCommerce API
     *
     * @param $request  Slim's Request object
     * @param $response Slim's Response object
     * @param $args     Slim's Argument parameters
     *
     * @author tanmayap@riaxe.com
     * @date   5 Oct 2019
     * @return Array of list/one product(s)
     */
    public function getCategories($request, $response, $args)
    {
        $serverStatusCode = OPERATION_OKAY;
        $categories = [];
        $jsonResponse = [];
        $endPoint = 'products/categories';

        // Get all requested Query params
        $name = $request->getQueryParam('name');

        // Set default option parameters
        $options = [
            'page' => 1,
            'per_page' => 100,
            'order' => 'desc',
            'orderby' => 'id',
        ];
        /**
         * Filter process starts
         */
        if (isset($args['id']) && $args['id'] != "" && $args['id'] > 0) {
            $endPoint .= '/' . $args['id'];
        }
        if (isset($name) && $name != "") {
            $options += ['search' => $name];
        }
        // End of the filter
        try {
            $getCategories = $this->wc->get($endPoint, $options);
            $getCategories = object_to_array($getCategories);
            /**
             * For single category listing, we get a 1D category array.
             * But the for-loop works for Multi-Dimentional Array. So to push the single category array into the for-loop
             * I converted the 1D array to Multi dimentional array, so that foreach loop will be intact
             */
            if (!isset($getCategories[0])) {
                $getCategories = [$getCategories];
            }
            foreach ($getCategories as $key => $category) {
                $categories[$key] = [
                    'id' => $category['id'],
                    'name' => $category['name'],
                    'slug' => $category['slug'],
                    'parent_id' => $category['parent'],
                ];
            }
            if (isset($categories) && is_array($categories) && count($categories) > 0) {
                $jsonResponse = [
                    'status' => 1,
                    'data' => $categories,
                ];
            }
        } catch (\Exception $e) {
            $serverStatusCode = EXCEPTION_OCCURED;
            $jsonResponse = [
                'status' => 0,
                'message' => 'Invalid request',
                'exception' => show_exception() === true ? $e->getMessage() : '',
            ];
        }
        return [
            'data' => $jsonResponse,
            'httpStatusCode' => $serverStatusCode,
        ];
    }

    /**
     * Generate thumb images from store product images by using store end image urls
     *
     * @param $imagePath  Image Path
     * @param $resolution Image Resolution
     *
     * @author    tanmayap@riaxe.com
     * @date      24 sep 2019
     * @parameter Slim default params
     * @return    Array of list/one product(s)
     */
    public function getVariableImageSizes($imagePath, $resolution)
    {
        // Only available 100, 150, 300, 450 and 768 resolution image sizes
        $imageResolution = 300;
        if (isset($resolution) && ($resolution == 100 || $resolution == 150 || $resolution == 300 || $resolution == 450 || $resolution == 768)) {
            $imageResolution = $resolution;
        }
        $explodeImage = explode('/', $imagePath);
        $getImageFromUrl = end($explodeImage);
        $fileExtension = pathinfo($getImageFromUrl, PATHINFO_EXTENSION);
        $fileName = pathinfo($getImageFromUrl, PATHINFO_FILENAME);
        $updatedImageName = $fileName . '-' . $imageResolution . 'x' . $imageResolution . '.' . $fileExtension;
        $updatedImagePath = str_replace($getImageFromUrl, $updatedImageName, $imagePath);
        return $updatedImagePath;
    }

    /**
     * GET: Product Attribute Pricing  Details by Product Id
     *
     * @param $request  Slim's Request object
     * @param $response Slim's Response object
     * @param $args     Slim's Arguments
     *
     * @author satyabratap@riaxe.com
     * @date   5 Oct 2019
     * @return All store attributes
     */
    public function storeProductAttrPrc($request, $response, $args)
    {
        $jsonResponse = [
            'status' => 0,
            'message' => message('Product Pricing', 'not_found'),
            'data' => [],
        ];
        try {
            $productId = intval($args['id']);
            $getProducts = [];
            $filters = [
                'product_id' => $productId,
            ];
            $options = [];
            foreach ($filters as $filterKey => $filterValue) {
                if (isset($filterValue) && $filterValue != "") {
                    $options += [$filterKey => $filterValue];
                }
            }
            $singleProductDetails = $this->plugin->get('product/attributes', $options);
            // Print Profile Details of the Product.
            $printProfiles = [];
            $printProfileDetails = $this->getAssocPrintProfiles($productId);
            if (!empty($printProfileDetails['print_profiles'])) {
                $printProfiles = $printProfileDetails['print_profiles'];
            }
            $jsonResponse = [
                'status' => 1,
                'data' => [
                    'print_profile' => $printProfiles,
                    'product_details' => $singleProductDetails,
                ],
            ];
        } catch (\Exception $e) {
            $serverStatusCode = OPERATION_OKAY;
            $jsonResponse = [
                'status' => 0,
                'message' => message('Price', 'error'),
                'exception' => show_exception() === true ? $e->getMessage() : '',
            ];
        }
        return  $jsonResponse;
    }

    // Predecorator

    /**
     * Find Combinations for the existing Attributes like Woocommerce style
     *
     * @param $arrays Multidimentional Arrays
     * @param $i      Key
     *
     * @author tanmayap@riaxe.com
     * @date   5 Oct 2019
     * @return Comination Arrays
     */
    public function combinations($arrays, $i = 0)
    {
        if (!isset($arrays[$i])) {
            return array();
        }
        if ($i == count($arrays) - 1) {
            return $arrays[$i];
        }
        // get combinations from subsequent arrays
        $tmp = $this->combinations($arrays, $i + 1);
        $result = array();
        // concat each array from tmp with each element from $arrays[$i]
        foreach ($arrays[$i] as $v) {
            foreach ($tmp as $t) {
                $result[] = is_array($t) ?
                array_merge(array($v), $t) :
                array($v, $t);
            }
        }
        return $result;
    }

    /**
     * Post: Save predecorated products into the store
     *
     * @param $request  Slim's Request object
     * @param $response Slim's Response object
     *
     * @author tanmayap@riaxe.com
     * @date   5 Oct 2019
     * @return Array records and server status
     */
    public function saveProduct($request, $response)
    {
        $serverStatusCode = OPERATION_OKAY;
        $jsonResponse = [
            'status' => 0,
            'message' => message('Predecorated Product', 'error'),
        ];
        $createVariation = true;
        $getPostData = (isset($saveType) && $saveType == 'update') ? $this->parsePut() : $request->getParsedBody();

        if (isset($getPostData['data']) && $getPostData['data'] != "") {
            $predecorData = json_clean_decode($getPostData['data'], true);

            $productSaveEndPoint = 'products';
            $mode = 'saved';
            if (isset($predecorData['product_id']) && $predecorData['product_id'] > 0) {
                $productSaveEndPoint = 'products/' . $predecorData['product_id'];
                $mode = 'updated';
            }

            $productType = 'simple';
            if (isset($predecorData['type']) && $predecorData['type'] != "") {
                $productType = $predecorData['type'];
            }

            // Setup a array of Basic Product attributes
            $productSaveData = [
                'name' => $predecorData['name'],
                'sku' => $predecorData['sku'],
                'type' => strtolower($productType), //'variable',
                'description' => (isset($predecorData['description']) && $predecorData['description'] != "") ? $predecorData['description'] : null,
                'short_description' => (isset($predecorData['short_description']) && $predecorData['short_description'] != "") ? $predecorData['short_description'] : null,
                'manage_stock' => true,
                'stock_quantity' => $predecorData['quantity'],
                'in_stock' => true,
                'visible' => true,
                'catalog_visibility' => "visible",
            ];

            // Append category IDs product_image_filesproduct_image_files
            // product_image_files product_image_files
            $categories = [];
            if (isset($predecorData['categories']) && count($predecorData['categories']) > 0) {
                foreach ($predecorData['categories'] as $category) {
                    $categories['categories'][] = [
                        'id' => $category['category_id'],
                    ];
                }
                $productSaveData += $categories;
            }
            // End
            // Append Image Urls
            $productImages = [];
            $convertImageToSize = 500; // w*h pixel
            // If Images url are sent via json array
            // Check for is_array() because if they send images_url as string then it will not satisfy.
            // So if I check is_array() then if they send array or string, both cond. will be stisfied
            if (isset($predecorData['product_image_url']) && is_array($predecorData['product_image_url']) && count($predecorData['product_image_url']) > 0) {

                $fileSavePath = path('abs', 'temp');
                $fileFetchPath = path('read', 'temp');

                foreach ($predecorData['product_image_url'] as $imageUrl) {
                    $randomName = getRandom();
                    $tempFileName = 'products_' . $randomName;
                    $fileExtension = pathinfo($imageUrl, PATHINFO_EXTENSION);
                    $filenameToProcess = $tempFileName . '.' . $fileExtension;

                    download_file($imageUrl, $fileSavePath, $filenameToProcess);
                    $fileUrlToProcess = $fileFetchPath . $filenameToProcess;
                    $img = \Intervention\Image\ImageManagerStatic::make($fileUrlToProcess);
                    $img->resize(
                        $convertImageToSize, null, function ($constraint) {
                            $constraint->aspectRatio();
                        }
                    );
                    $img->save($fileSavePath . 'resize_' . $filenameToProcess);
                    $productImages['images'][] = [
                        'src' => $fileFetchPath . 'resize_' . $filenameToProcess,
                    ];
                }

                // foreach ($predecorData['product_image_url'] as $imageUrl) {
                //     $productImages['images'][] = [
                //         'src' => $imageUrl,
                //     ];
                // }
            } else {
                // If Images are sent from front-end
                $uploadedFileNameList = mutiple_upload('product_image_files', path('abs', 'predecorator'));
                foreach ($uploadedFileNameList as $uploadedImageKey => $uploadedImage) {
                    $productImages['images'][] = [
                        'src' => path('read', 'predecorator') . $uploadedImage,
                    ];
                }
            }
            $productSaveData += $productImages;
            // End

            // Append Ref_ID to the Custom meta data. This data will be saved into post_meta table in wooCommerce
            $metaData = [
                'meta_data' => [
                    [
                        'key' => 'refid',
                        'value' => $predecorData['ref_id'],
                    ],
                ],
            ];
            $productSaveData += $metaData;

            if ($productType == 'variable') {
                // Append Attributes by Looping through each Attribute
                $productAttributes = $getAttributeCombinations = [];
                foreach ($predecorData['attributes'] as $prodAttributekey => $prodAttribute) {
                    $getAttrTermsList = [];
                    // Append Attribute Term slugs
                    if (isset($prodAttribute['attribute_options']) && is_array($prodAttribute['attribute_options']) && count($prodAttribute['attribute_options']) > 0) {
                        foreach ($prodAttribute['attribute_options'] as $attrTermkey => $attrTerm) {
                            // Get Product Assoc. Terms from API
                            $getAtteributeTermDetails = $this->wc->get('products/attributes/' . $prodAttribute['attribute_id'] . '/terms' . '/' . $attrTerm);
                            $getAttrTermsList[] = $getAtteributeTermDetails['slug'];
                            $getAttributeCombinations[$prodAttributekey][] = $prodAttributekey . '___' . $getAtteributeTermDetails['slug'];
                        }
                    }
                    $productAttributes['attributes'][] = [
                        'id' => $prodAttribute['attribute_id'],
                        'variation' => true,
                        'visible' => true,
                        'options' => $getAttrTermsList,
                    ];

                }
                $productSaveData += $productAttributes;
            } else {
                $productSaveData += [
                    'regular_price' => strval($predecorData['price']),
                ];
            }
            // Process the Data to the Product's Post API
            try {
                $getProducts = $this->wc->post($productSaveEndPoint, $productSaveData);
                // Call Another API for Create Variations
                // Your code goes here if required
                $jsonResponse = [
                    'status' => 1,
                    'product_id' => $getProducts['id'],
                    'message' => message('Predecorated Product', $mode),
                ];

                // Create Predeco if the option is set to true
                if ($createVariation === true && $productType == 'variable') {
                    $variationCreateData = [
                        'product_id' => $getProducts['id'],
                        'price' => $predecorData['price'],
                        // Auto change SKU
                        'sku' => $predecorData['sku'] . time(),
                        'attributes' => $predecorData['attributes'],
                    ];
                    $this->createProductVariations($request, $response, $variationCreateData);
                }
                // End of Variation creation

            } catch (\Exception $e) {
                $jsonResponse['exception'] = show_exception() === true ? $e->getMessage() : message('Predeco Product', 'exception');
            }
        }

        return [
            'data' => $jsonResponse,
            'server_status' => $serverStatusCode,
        ];
    }

    /**
     * Post: Create product variations and save to corsp. Product
     *
     * @param $request       Slim's Request object
     * @param $response      Slim's Response object
     * @param $variationData Variation Data
     *
     * @author tanmayap@riaxe.com
     * @date   5 Oct 2019
     * @return Array records and server status
     */
    public function createProductVariations($request, $response, $variationData = [])
    {
        $serverStatusCode = OPERATION_OKAY;
        $jsonResponse = [
            'status' => 0,
            'message' => message('Product Variation', 'error'),
        ];
        $getPostData = (isset($saveType) && $saveType == 'update') ? $this->parsePut() : $request->getParsedBody();
        $predecorData = json_clean_decode($getPostData['data'], true);
        if (!empty($variationData) && count($variationData) > 0) {
            $predecorData = $variationData;
        }

        try {
            // Get products details from product Store by Product ID
            $getproductData = $this->wc->get('products/' . $predecorData['product_id']);
            $variationsForProductId = $getproductData['id'];
            $variationsForProductImage = $getproductData['images'][0]['id'];
            /**
             * Start process for creating Variation Array List
             */
            $attributes = $predecorData['attributes'];
            // Modify the get Attribute array as per our requirements
            $attributesList = [];
            for ($loop = 0; $loop < count($attributes); $loop++) {
                $attributeId = $attributes[$loop]['attribute_id'];
                $options = [];
                foreach ($attributes[$loop]['attribute_options'] as $attributeOptionKey => $attributeOptionVal) {
                    $attributesList[$loop][] = $attributeId . '___' . $attributeOptionVal;
                }
            }
            $getCombinations = $this->combinations($attributesList);
            // Below Comments can be used in future
            $variantsAvailable = [
                'regular_price' => strval($predecorData['price']),
                //'stock_status' => isset($predecorData['in_stock']) ? $predecorData['in_stock'] : 'instock',
                //'stock_quantity' => 4,
                'sku' => (isset($predecorData['sku']) && $predecorData['sku'] != "") ? $predecorData['sku'] : strtoupper($product['slug'] . time()),
                // 'image' => [
                //     'id' => $getproductData->images[0]->id,
                // ],
            ];
            // Append Image if Exists
            if (isset($variationsForProductImage) && $variationsForProductImage != "") {
                $variantsAvailable += [
                    'image' => [
                        'id' => $variationsForProductImage,
                    ],
                ];
            }
            $variantsPerCombination = [];
            foreach ($getCombinations as $combinationKey => $eachComination) {
                foreach ($eachComination as $eachCombinationOptionKey => $eachCombinationOption) {
                    $splitEachCombinationOption = explode('___', $eachCombinationOption);
                    // Get Attribute Option Slug from Store API
                    $getAttributeOptionDetails = $this->wc->get('products/attributes/' . $splitEachCombinationOption[0] . '/terms' . '/' . $splitEachCombinationOption[1]);
                    if (isset($getAttributeOptionDetails['slug']) && $getAttributeOptionDetails['slug'] != "") {
                        $variantsPerCombination[] = [
                            'id' => $splitEachCombinationOption[0],
                            'option' => $getAttributeOptionDetails['slug'],
                        ];
                    }
                }
            }

            $variantsAvailable['attributes'] = $variantsPerCombination;

            try {
                $variationResponse = $this->wc->post('products/' . $variationsForProductId . '/variations', $variantsAvailable);
                if (isset($variationResponse) && isset($variationResponse['id']) && $variationResponse['id'] > 0) {
                    $jsonResponse = [
                        'status' => 1,
                        'product_id' => $variationsForProductId,
                        'variation_id' => $variationResponse['id'],
                        'message' => message('Product Variantions', 'saved'),
                    ];
                }
            } catch (\Exception $e) {
                $jsonResponse['exception'] = show_exception() === true ? $e->getMessage() : message('Product Variation', 'exception');
            }
        } catch (\Exception $e) {
            $jsonResponse['exception'] = show_exception() === true ? $e->getMessage() : message('Product Fetch', 'exception');

        }
        return [
            'data' => $jsonResponse,
            'server_status' => $serverStatusCode,
        ];
    }

    /**
     * Post: Validate SKU or Name at Store end
     *
     * @param $request  Slim's Request object
     * @param $response Slim's Response object
     *
     * @author tanmayap@riaxe.com
     * @date   5 Oct 2019
     * @return Validate response Array
     */
    public function validateStoreSkuName($request, $response)
    {
        $serverStatusCode = OPERATION_OKAY;
        $rootEndpoint = 'products';
        $jsonResponse = [
            'status' => 1,
            'message' => 'You can use this combination for new product',
        ];
        $allPostPutVars = (isset($saveType) && $saveType == 'update') ? $this->parsePut() : $request->getParsedBody();
        $filters = [];
        if (isset($allPostPutVars['name']) && $allPostPutVars['name'] != "") {
            $filters += [
                'search' => isset($allPostPutVars['name']) ? $allPostPutVars['name'] : null,
            ];
        }
        if (isset($allPostPutVars['sku']) && $allPostPutVars['sku'] != "") {
            $filters += [
                'sku' => isset($allPostPutVars['sku']) ? $allPostPutVars['sku'] : null,
            ];
        }
        $getProducts = $this->wc->get($rootEndpoint, $filters);
        if (isset($getProducts[0]['id']) && $getProducts[0]['id'] > 0) {
            $jsonResponse = [
                'status' => 0,
                'message' => 'Combination already in use. Please refer to product_id to know more',
                'product_id' => $getProducts[0]['id'],
            ];
        }
        return [
            'data' => $jsonResponse,
            'server_status' => $serverStatusCode,
        ];
    }

    /**
     * Get: Get all Attributes List from Store-end
     *
     * @param $request  Slim's Request object
     * @param $response Slim's Response object
     *
     * @author tanmayap@riaxe.com
     * @date   5 Oct 2019
     * @return Array list of Attributes
     */
    public function storeAttributeList($request, $response)
    {
        $serverStatusCode = OPERATION_OKAY;
        $endPoint = 'products/attributes';
        $jsonResponse = [
            'status' => 0,
            'data' => message('Prodcut Attributes', 'not_found'),
        ];
        $attributeList = [];
        $getAllAttributes = $this->wc->get($endPoint);
        if (isset($getAllAttributes) && count($getAllAttributes) > 0) {
            $loop = 0;
            foreach ($getAllAttributes as $attrkey => $attribute) {
                if ($attribute['type'] === 'select' || $attribute['type'] === 'checkbox') {
                    $getAssocTerms = $this->wc->get('products/attributes/' . $attribute['id'] . '/terms');
                    $termList = [];
                    foreach ($getAssocTerms as $term) {
                        $termList[] = [
                            'id' => $term['id'],
                            'name' => $term['name'],
                            'slug' => $term['slug'],
                        ];
                    }
                    $attributeList[$loop] = [
                        'id' => $attribute['id'],
                        'name' => $attribute['name'],
                        'slug' => $attribute['slug'],
                        'type' => $attribute['type'],
                        'terms' => $termList,
                    ];
                    $loop++;
                }
            }
            $jsonResponse = [
                'status' => 1,
                'data' => $attributeList,
            ];
        }
        return [
            'data' => $jsonResponse,
            'server_status' => $serverStatusCode,
        ];
    }

    /**
     * Get list of Color Variants from the WooCommerce API as per the product
     *
     * @param $request  Slim's Request object
     * @param $response Slim's Response object
     * @param $args     Slim's Arguments
     *
     * @author tanmayap@riaxe.com
     * @date   5 Oct 2019
     * @return Json
     */
    public function colorsByProduct($request, $response, $args)
    {
        $serverStatusCode = OPERATION_OKAY;
        $filters = [
            'product_id' => $args['id'],
            'attribute' => 'pa_color',
        ];
        $jsonResponse = [
            'status' => 1,
            'records' => 0,
            'message' => 'No Color available',
            'data' => [],
        ];
        $options = [];
        $variantData = [];
        foreach ($filters as $filterKey => $filterValue) {
            if (isset($filterValue) && $filterValue != "") {
                $options += [$filterKey => $filterValue];
            }
        }
        try {
            $singleProductDetails = $this->plugin->get('options', $options);
            if (!empty($singleProductDetails)) {
                $variantData = $this->getColorSwatchData($singleProductDetails);
                $jsonResponse = [
                    'status' => 1,
                    'records' => count($variantData),
                    'data' => $variantData,
                ];
            }
        } catch (\Exception $e) {
            $serverStatusCode = EXCEPTION_OCCURED;
            $jsonResponse = [
                'status' => 0,
                'message' => message('Products', 'error'),
                'exception' => show_exception() === true ? $e->getMessage() : '',
            ];
        }
        return [
            'data' => $jsonResponse,
            'httpStatusCode' => $serverStatusCode,
        ];
    }
}
