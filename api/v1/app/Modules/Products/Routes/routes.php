<?php
/**
 * Routes
 *
 * PHP version 5.6
 *
 * @category  Routes
 * @package   SLIM_Routes
 * @author    Tanmaya Patra <tanmayap@riaxe.com>
 * @copyright 2019-2020 Riaxe Systems
 * @license   http://www.php.net/license/3_0.txt  PHP License 3.0
 * @link      http://inkxe-v10.inkxe.io/xetool/admin ProductsController
 */
use App\Middlewares\ValidateJWTToken as ValidateJWT;
use App\Modules\Products\Controllers\ProductAttrPrcRulesController as ProdAttrPrc;
use App\Modules\Products\Controllers\ProductDecorationsController as ProductDecoration;
use App\Modules\Products\Controllers\ProductImagesController as ProductImages;
use App\Modules\Products\Controllers\ProductsController as Products; 

$container = $app->getContainer();

/**
 * Measurement Units Routes list
 */
$app->group(
    '/attribute-prices', function () use ($app) {
        $app->get('/{id}', Products::class . ':getProductAttrPrc');
        $app->post('', Products::class . ':saveProdAttrPrc');
    }
)->add(new ValidateJWT($container));

$app->post('/images/save', Products::class . ':saveImages'); //->add(new ValidateJWT($container));
/**
 * Measurement Units Routes list
 */
$app->group(
    '/products/measurement-units', function () use ($app) {
        // Get all Measurement Units
        $app->get('', Products::class . ':getMeasurementUnits');
    }
)->add(new ValidateJWT($container));

$app->group(
    '/products', function () use ($app) {
        // Category Routes
        $app->get('/list-of-categories', Products::class . ':totalCategories');
        $app->get('/categories', Products::class . ':totalCategories');
        $app->get('/categories/{id}', Products::class . ':totalCategories');
        // Products Routes
        $app->get('/{id}', Products::class . ':getProductList');
        $app->get('', Products::class . ':getProductList');
    }
)->add(new ValidateJWT($container));

// Separate Category list route for Print profile Assets section

/**
 * Product Images and Sides Routes List
 */
$app->group(
    '/image-sides', function () use ($app) {
        $app->get('', ProductImages::class . ':getProductImages');
        $app->get('/{product_image_id}', ProductImages::class . ':getProductImages');
        $app->post('', ProductImages::class . ':saveProductImages');
        $app->put('/{product_image_id}', ProductImages::class . ':updateProductImages');
        $app->delete('/{ids}', ProductImages::class . ':productImageDelete');
        // Enable/Disable Product Images
        $app->get('/disable-toggle/{id}', ProductImages::class . ':disableProductImage');
    }
)->add(new ValidateJWT($container));

/**
 * Product Decoration Settings Data Save
 */
$app->group(
    '/decorations', function () use ($app) {
        $app->post('', ProductDecoration::class . ':saveProductDecorations');
        $app->get('/{product_id}', ProductDecoration::class . ':getProductDecorations');
        $app->put('/{product_id}', ProductDecoration::class . ':updateProductDecorations');
        $app->delete('/{product_id}', ProductDecoration::class . ':trashDecoration');
    }
)->add(new ValidateJWT($container));

// Predecorator Route
$app->group(
    '/predecorators', function () use ($app) {
        // Save new Record
        $app->post('', Products::class . ':savePredecorator'); 
        // Update existing Record
        $app->put('', Products::class . ':updateProduct'); 
        // Save new Records
        $app->get('/attributes', Products::class . ':getStoreAttributes'); 
        // Validate SKU or Name or Other Parameters
        $app->post('/validate', Products::class . ':validateParams'); 
        // Validate SKU or Name or Other Parameters
        $app->post('/variations', Products::class . ':createVariations');
        // Get Single and Multiple Predeco Data with Filters and paginations
        $app->get('', Products::class . ':getPredecorators');
        $app->get('/{id}', Products::class . ':getPredecorators');
    }
)->add(new ValidateJWT($container));

// Color Variants
$app->group(
    '/color-variants', function () use ($app) {
        $app->get('/{id}', Products::class . ':colorsByProductId');
    }
);

/**
 * Product Decoration Settings Data For Designer Tool
 */
$app->group(
    '/product-details', function () use ($app) {
        $app->get('/{product_id}', ProductDecoration::class . ':getProductDecorationsClient');
    }
)->add(new ValidateJWT($container));
