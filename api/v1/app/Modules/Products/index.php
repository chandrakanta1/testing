<?php
/**
 * Initilize all Routes
 */
// Set store autoload 
$vendor->setPsr4("ProductStoreSpace\\", "app/Modules/Products/Stores/" . STORE_NAME . "/" . STORE_VERSION . "/");
require __DIR__ . '/Routes/routes.php';