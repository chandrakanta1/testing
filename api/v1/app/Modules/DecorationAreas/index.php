<?php
/**
 * Index File for Print Area
 *
 * PHP version 5.6
 *
 * @category  Print_Area
 * @package   Print_Area
 * @author    Tanmaya Patra <tanmayap@riaxe.com>
 * @copyright 2019-2020 Riaxe Systems
 * @license   http://www.php.net/license/3_0.txt  PHP License 3.0
 * @link      http://inkxe-v10.inkxe.io/xetool/admin
 */
require __DIR__ . '/Routes/routes.php';
