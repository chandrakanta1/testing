<?php
/**
 * Manage Shapes
 *
 * PHP version 5.6
 *
 * @category  Shape
 * @package   Eloquent
 * @author    Satyabrata <satyabratap@riaxe.com>
 * @copyright 2019-2020 Riaxe Systems
 * @license   http://www.php.net/license/3_0.txt  PHP License 3.0
 * @link      http://inkxe-v10.inkxe.io/xetool/admin
 */

namespace App\Modules\Shapes\Controllers;

use App\Components\Controllers\Component as ParentController;
use App\Modules\Shapes\Models\Shape;
use App\Modules\Shapes\Models\ShapeTagRelation;
use App\Modules\Shapes\Models\ShapeCategoryRelation;
use App\Modules\Shapes\Models\ShapeCategory;
use App\Modules\Shapes\Models\ShapeTag as Tag;

/**
 * Shape Controller
 *
 * @category Class
 * @package  Shape
 * @author   Satyabrata <satyabratap@riaxe.com>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://inkxe-v10.inkxe.io/xetool/admin
 */
class ShapeController extends ParentController
{

    /**
     * POST: Save Shape
     *
     * @param $request  Slim's Request object
     * @param $response Slim's Response object
     *
     * @author satyabratap@riaxe.com
     * @date   4th Nov 2019
     * @return json response wheather data is saved or any error occured
     */
    public function saveShapes($request, $response)
    {
        $serverStatusCode = OPERATION_OKAY;
        $success = 0;      
        $saveShapeList = [];
        $allFileNames = [];
        $jsonResponse = [
            'status' => 0,
            'message' => message('Shapes', 'error'),
        ];
        // Get Store Specific Details from helper
        $getStoreDetails = get_store_details($request);
        $allPostPutVars = $request->getParsedBody();
        // Save file if request contain files
        $uploadedFiles = $request->getUploadedFiles();
        $uploadingFilesNo = count($uploadedFiles['upload']);
        if (!empty($uploadingFilesNo) && $uploadingFilesNo > 0) {
            $allFileNames = mutiple_upload('upload', path('abs', 'shape'), false);
        }
        foreach ($allFileNames as $eachFile) {
            $lastInsertId = 0;
            $saveShapeList = [];
            if (!empty($eachFile) && $eachFile != "") {
                $saveShapeList = [
                    'store_id' => $getStoreDetails['store_id'],
                    'name' => $allPostPutVars['name'],
                    'file_name' => $eachFile
                ];

                $saveEachShape = new Shape($saveShapeList);
                if ($saveEachShape->save()) {
                    $lastInsertId = $saveEachShape->xe_id;
                    /**
                     * Save category and subcategory data
                     * Category id format: [4,78,3]
                     */
                    if (isset($allPostPutVars['categories']) 
                        && $allPostPutVars['categories'] != ""
                    ) {
                        $categoryIds = $allPostPutVars['categories'];
                        $this->_saveShapeCategories(
                            $lastInsertId, $categoryIds
                        );
                    }
                    /**
                     * Save tags
                     * Tag Names format : tag1,tag2,tag3
                     */
                    if (isset($allPostPutVars['tags']) && $allPostPutVars['tags'] != "") {
                        $tags = $allPostPutVars['tags'];
                        $this->_saveShapeTags($lastInsertId, $tags);
                    }
                    $success++;
                }
            }
        }
        if (!empty($success) && $success > 0) {
            $jsonResponse = [
                'status' => 1,
                'message' => $success . ' out of ' . $uploadingFilesNo . ' Shape(s) uploaded successfully'
            ];
        }
       
        return response($response, ['data' => $jsonResponse, 'status' => $serverStatusCode]);
    }

    /**
     * Save Categories/Sub-categories and Shape-Category Relations
     *
     * @param $shapeId     Shape ID
     * @param $categoryIds (in  an array with comma separated)
     *
     * @author satyabratap@riaxe.com
     * @date   4th Nov 2019
     * @return boolean
     */
    private function _saveShapeCategories($shapeId, $categoryIds)
    {
        $getAllCategoryArr = json_clean_decode($categoryIds, true);
        // SYNC Categories to the Shape_Category Relationship Table
        $shapeInit = new Shape();
        $findShape = $shapeInit->find($shapeId);
        if ($findShape->categories()->sync($getAllCategoryArr)) {
            return true;
        }
        return false;
    }

    /**
     * Save Tags and Shape-Tag Relations
     *
     * @param $shapeId      Shape ID
     * @param $multipletags (in comma separated)
     *
     * @author satyabratap@riaxe.com
     * @date   4th Nov 2019
     * @return boolean
     */
    private function _saveShapeTags($shapeId, $multipletags)
    {
        // Save Shape and tags relation
        if (!empty($multipletags)) {
            $updatedTagIds = [];
            $tagsStringToArray = explode(',', $multipletags);
            foreach ($tagsStringToArray as $tag) {
                // Save each individual Tag to table
                $tagInit = new Tag();
                if ($tagInit->where(['name' => trim($tag)])->count() === 0) {
                    $saveTag = new Tag(['name' => trim($tag)]);
                    $saveTag->save();
                    $lastInsertId = $saveTag->xe_id;
                } else {
                    $getTagDetails = $tagInit->where(
                        ['name' => trim($tag)]
                    )->select('xe_id')->first();
                    $lastInsertId = $getTagDetails['xe_id'];
                }
                $updatedTagIds[] = $lastInsertId;
            }

             // Start SYNC Tags into Shape_Tag Relationship Table
             $shapeInit = new Shape();
             $findShape = $shapeInit->find($shapeId);
            if ($findShape->tags()->sync($updatedTagIds)) {
                return true;
            }
        } else {
            // If user requests blank/no tags
            $tagRelInit = new ShapeTagRelation();
            $shapeTags = $tagRelInit->where('shape_id', $shapeId);
            if ($shapeTags->delete()) {
                return true;
            }
        }
        return false;
    }

    /**
     * GET: List of Shape
     *
     * @param $request  Slim's Request object
     * @param $response Slim's Response object
     * @param $args     Slim's Argument parameters
     *
     * @author satyabratap@riaxe.com
     * @date   4th Nov 2019
     * @return All/Single Shape List
     */
    public function getShapes($request, $response, $args)
    {

        $serverStatusCode = OPERATION_OKAY;
        $shapeData = [];
        $jsonResponse = [
            'status' => 0,
            'data' => [],
            'message' => message('Shape', 'not_found'),
        ];
        // Get Store Specific Details from helper
        $getStoreDetails = get_store_details($request);
        $offset = 0;
        $shapeInit = new Shape();
        $getShapes = $shapeInit->where('xe_id', '>', 0);

        if (!empty($args) && $args['id'] > 0) {
            $shapeId = $args['id'];
            //For single Shape data
            $shapeData = $getShapes->where('xe_id', '=', $shapeId)->first();
            $categoryIdList = $tagNameList = [];
            // Get Category Ids
            $categoryInit = new ShapeCategory();
            $getCategories = $this->getCategoriesById('Shapes', 'ShapeCategoryRelation', 'shape_id', $shapeId);
            $getTags = $this->getTagsById('Shapes', 'ShapeTagRelation', 'shape_id', $shapeId);
            $shapeData['categories'] = $getCategories;
            $shapeData['tags'] = $getTags;
            // Unset category_name Key in case of single record fetch
            $shapeData = json_clean_decode($shapeData, true);
            unset($shapeData['category_names']);
            $jsonResponse = [
                'status' => 1,
                'data' => [
                    $shapeData
                ]
            ];
        } else {
            //All Filter columns from url
            $page = $request->getQueryParam('page');
            $perpage = $request->getQueryParam('perpage');
            $categoryId = $request->getQueryParam('category');
            $sortBy = !empty($request->getQueryParam('sortby')) && $request->getQueryParam('sortby') != "" ? $request->getQueryParam('sortby') : 'xe_id';
            $order = !empty($request->getQueryParam('order')) && $request->getQueryParam('order') != "" ? $request->getQueryParam('order') : 'desc';
            $name = $request->getQueryParam('name');
            // For multiple Shape data
            $getShapes->select('xe_id', 'name', 'file_name');
            $getShapes->where('store_id', '=', $getStoreDetails['store_id']);
            if (isset($name) && $name != "") {
                // Search name inside Shape
                $getShapes->where('name', 'LIKE', '%' . $name . '%')
                // Search name inside Category and Tags
                    ->orWhereHas(
                        'shapeTags.tag', function ($q) use ($name) {
                            return $q->where('name', 'LIKE', '%' . $name . '%');
                        }
                    )->orWhereHas(
                        'shapeCategory.category', function ($q) use ($name) {
                            return $q->where('name', 'LIKE', '%' . $name . '%');
                        }
                    );
            }
            // Filter by Category ID
            if (isset($categoryId) && $categoryId != "") {
                $searchCategories = json_clean_decode($categoryId, true);
                $getShapes->whereHas(
                    'shapeCategory', function ($q) use ($searchCategories) {
                        return $q->whereIn('category_id', $searchCategories);
                    }
                );
            }
            // Total records including all filters
            $getTotalPerFilters = $getShapes->count();
            // Pagination Data
            if (isset($page) && $page != "") {
                $totalItem = empty($perpage) ? PAGINATION_MAX_ROW : $perpage;
                $offset = $totalItem * ($page - 1);
                $getShapes->skip($offset)->take($totalItem);
            }
            // Sorting All records by column name and sord order parameter
            if (isset($sortBy) && $sortBy != "" && isset($order) && $order != "") {
                $getShapes->orderBy($sortBy, $order);
            }
            $shapeData = $getShapes->get();
            $jsonResponse = [
                'status' => 1,
                'records' => count($shapeData),
                'total_records' => $getTotalPerFilters,
                'data' => $shapeData
            ];
        }

        return response($response, ['data' => $jsonResponse, 'status' => $serverStatusCode]);
    }
    
    /**
     * PUT: Update a Single Shape
     *
     * @param $request  Slim's Request object
     * @param $response Slim's Response object
     * @param $args     Slim's Argument parameters
     *
     * @author satyabratap@riaxe.com
     * @date   4th Nov 2019
     * @return json response wheather data is updated or not
     */
    public function updateShape($request, $response, $args)
    {
        $serverStatusCode = OPERATION_OKAY;
        $jsonResponse = [
            'status' => 0,
            'message' => message('Shape', 'not_found'),
        ];
        $allPostPutVars = $updateData = $this->parsePut();
        // Get Store Specific Details from helper
        $getStoreDetails = get_store_details($request);
        if (isset($args['id']) && $args['id'] > 0) {
            $shapeId = $args['id'];
            $shapeInit = new Shape();
            $getOldShape = $shapeInit->where('xe_id', '=', $shapeId);
            if ($getOldShape->count() > 0) {
                unset($updateData['id'], $updateData['tags'], $updateData['categories'], $updateData['upload'], $updateData['shapeId']);

                // Delete old file if exist
                $this->deleteOldFile("shapes", "file_name", ['xe_id' => $shapeId], path('abs', 'shape'));
                $getUploadedFileName = save_file('upload', path('abs', 'shape'));
                if (!empty($getUploadedFileName)) {
                    $updateData += ['file_name' => $getUploadedFileName];
                }
                $updateData += ['store_id' => $getStoreDetails['store_id']];
                // Update record
                try {
                    $shapeInit = new Shape();
                    $shapeInit->where('xe_id', '=', $shapeId)->update($updateData);
                    /**
                     * Save category and subcategory data
                     * Category id format: [4,78,3]
                     */
                    if (isset($allPostPutVars['categories']) && $allPostPutVars['categories'] != "") {
                        $categoryIds = $allPostPutVars['categories'];
                        $this->_saveShapeCategories($shapeId, $categoryIds);
                    }
                    /**
                     * Save tags
                     * Tag Names format : tag1,tag2,tag3
                     */
                    $tags = (isset($allPostPutVars['tags']) && $allPostPutVars['tags'] != "") ? $allPostPutVars['tags'] : "";
                    $this->_saveShapeTags($shapeId, $tags);
                    $jsonResponse = [
                        'status' => 1,
                        'message' => message('Shape', 'updated'),
                    ];
                } catch (\Exception $e) {
                    $serverStatusCode = EXCEPTION_OCCURED;
                    $jsonResponse = [
                        'status' => 0,
                        'message' => message('Shapes', 'exception'),
                        'exception' => show_exception() === true ? $e->getMessage() : ''
                    ];
                }
            }
        }
        return response($response, ['data' => $jsonResponse, 'status' => $serverStatusCode]);
    }

    /**
     * DELETE: Delete single/multiple Shape
     *
     * @param $request  Slim's Argument parameters
     * @param $response Slim's Response object
     * @param $args     Slim's Argument parameters
     *
     * @author satyabratap@riaxe.com
     * @date   4th Nov 2019
     * @return json response wheather data is deleted or not
     */
    public function deleteShape($request, $response, $args)
    {
        $serverStatusCode = OPERATION_OKAY;
        $jsonResponse = [
            'status' => 0,
            'message' => message('Shape', 'not_found'),
        ];
        if (isset($args) && $args['id'] != '') {
            $getDeleteIds = $args['id'];
            $getDeleteIdsToArray = json_clean_decode($getDeleteIds, true);
            $totalCount = count($getDeleteIdsToArray);
            if (is_array($getDeleteIdsToArray) && count($getDeleteIdsToArray) > 0) {
                $shapeInit = new Shape();
                if ($shapeInit->whereIn('xe_id', $getDeleteIdsToArray)->count() > 0) {
                    try {
                        $success = 0;
                        foreach ($getDeleteIdsToArray as $shapeId) {
                            $this->deleteOldFile("shapes", "file_name", ['xe_id' => $shapeId], path('abs', 'shape'));
                            $shapeInit->where('xe_id', $shapeId)->delete();
                            $success++;
                        }
                        $jsonResponse = [
                            'status' => 1,
                            'message' => $success . ' out of ' . $totalCount . ' Shape(s) deleted successfully'
                        ];
                    } catch (\Exception $e) {
                        $serverStatusCode = EXCEPTION_OCCURED;
                        $jsonResponse = [
                            'status' => 0,
                            'message' => message('Shape', 'exception'),
                            'exception' => show_exception() === true ? $e->getMessage() : ''
                        ];
                    }
                }
            }
        }
        return response($response, ['data' => $jsonResponse, 'status' => $serverStatusCode]);
    }
    
    /**
     * Check Category Relation with Category
     *
     * @param $request  Slim's Argument parameters
     * @param $response Slim's Response object
     * @param $args     Slim's Argument parameters
     *
     * @author satyabratap@riaxe.com
     * @date   20 Jan 2020
     * @return boolean
     */
    public function checkRelCategory($request, $response, $args)
    {
        $serverStatusCode = OPERATION_OKAY;
        $jsonResponse = [
            'status' => 0,
            'message' => 'This Category is associated with an Item. Do you really want to perform the operation'
        ];
        $key = $request->getQueryParam('key');
        if (!empty($args) && $args['id'] > 0) {
            $categoryId = $args['id'];
            $checkCatRelInit = new ShapeCategoryRelation();
            $getCatRel = $checkCatRelInit->where('category_id', $categoryId)->get();
            if ($getCatRel->count() == 0) {
                if ($key == 'disable') {
                    $jsonResponse = $this->disableCat('shapes', $categoryId);
                } elseif ($key == 'delete') {
                    $jsonResponse = $this->deleteCat('shapes', $categoryId, 'Shapes', 'ShapeCategoryRelation');
                }
            }
        }
        return response($response, ['data' => $jsonResponse, 'status' => $serverStatusCode]);
    }

    /**
     * Delete a category from the table
     *
     * @param $request  Slim's Request object
     * @param $response Slim's Response object
     * @param $args     Slim's Argument parameters
     *
     * @author satyabratap@riaxe.com
     * @date   20 Jan 2020
     * @return Delete Json Status
     */
    public function deleteCategory($request, $response, $args)
    {
        $serverStatusCode = OPERATION_OKAY;
        $jsonResponse = [
            'status' => 0,
            'message' => message('Category', 'error')
        ];
        if (!empty($args) && $args['id'] > 0) {
            $categoryId = $args['id'];
            $jsonResponse = $this->deleteCat('shapes', $categoryId, 'Shapes', 'ShapeCategoryRelation');
        }
        return response($response, ['data' => $jsonResponse, 'status' => $serverStatusCode]);
    }

}
