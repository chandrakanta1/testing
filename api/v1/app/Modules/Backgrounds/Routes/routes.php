<?php
/**
 * This Routes holds all the individual route for the Backgrounds
 *
 * PHP version 5.6
 *
 * @category  Backgrounds
 * @package   Assets
 * @author    Satyabrata <satyabratap@riaxe.com>
 * @copyright 2019-2020 Riaxe Systems
 * @license   http://www.php.net/license/3_0.txt  PHP License 3.0
 * @link      http://inkxe-v10.inkxe.io/xetool/admin
 */

use App\Middlewares\ValidateJWTToken as ValidateJWT;
use App\Modules\Backgrounds\Controllers\BackgroundController;

// Instantiate the Container
$container = $app->getContainer();

// Backgrounds Routes List
$app->group(
    '/background-patterns', function () use ($app) {
        $app->get('', BackgroundController::class . ':getBackgrounds');
        $app->get('/{id}', BackgroundController::class . ':getBackgrounds');
        $app->post('', BackgroundController::class . ':saveBackgrounds');
        $app->put('/{id}', BackgroundController::class . ':updateBackground');
        $app->delete('/{id}', BackgroundController::class . ':deleteBackground');
    }
)->add(new ValidateJWT($container));

// Categories Routes List
$app->delete('/categories/background-patterns/{id}',  BackgroundController::class . ':deleteCategory')->add(new ValidateJWT($container));
$app->get('/categories/background-patterns/checkcategory/{id}', BackgroundController::class . ':checkRelCategory')->add(new ValidateJWT($container));
