<?php
/**
 * Manage Background
 *
 * PHP version 5.6
 *
 * @category  Backgrounds
 * @package   Assets
 * @author    Satyabrata <satyabratap@riaxe.com>
 * @copyright 2019-2020 Riaxe Systems
 * @license   http://www.php.net/license/3_0.txt  PHP License 3.0
 * @link      http://inkxe-v10.inkxe.io/xetool/admin
 */

namespace App\Modules\Backgrounds\Controllers;

use App\Components\Controllers\Component as ParentController;
use App\Modules\Backgrounds\Models\Background;
use App\Modules\Backgrounds\Models\BackgroundCategory as Category;
use App\Modules\Backgrounds\Models\BackgroundCategoryRelation;
use App\Modules\Backgrounds\Models\BackgroundTag as Tag;

/**
 * Backgrounds Controller
 *
 * @category Backgrounds
 * @package  Assets
 * @author   Satyabrata <satyabratap@riaxe.com>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://inkxe-v10.inkxe.io/xetool/admin
 */

class BackgroundController extends ParentController
{
    /**
     * POST: Save Background
     *
     * @param $request  Slim's Request object
     * @param $response Slim's Response object
     *
     * @author satyabratap@riaxe.com
     * @date   4th Nov 2019
     * @return json response wheather data is saved or any error occured
     */
    public function saveBackgrounds($request, $response)
    {
        // Get Store Specific Details from helper
        $getStoreDetails = get_store_details($request);
        $serverStatusCode = OPERATION_OKAY;
        $allPostPutVars = $request->getParsedBody();
        $allFileNames = $saveBackgroundList = $recordBackgroundIds = [];
        $jsonResponse = [
            'status' => 0,
            'message' => message('Backgrounds', 'error'),
        ];
        $success = 0;

        if ($allPostPutVars['type'] == 1) {
            $uploadedFiles = $request->getUploadedFiles();
            $uploadingFilesNo = count($uploadedFiles['upload']);
            $allFileNames = mutiple_upload(
                'upload', path('abs', 'background'), true
            );
            if (!empty($allFileNames)) {
                foreach ($allFileNames as $eachFileKey => $eachFile) {
                    $backgroundLastId = 0;
                    if (!empty($eachFile) && $eachFile != "") {
                        $saveBackgroundList[$eachFileKey] = [
                            'store_id' => $getStoreDetails['store_id'],
                            'name' => $allPostPutVars['name'],
                            'price' => $allPostPutVars['price'],
                            'type' => $allPostPutVars['type'],
                            'value' => $eachFile,
                        ];

                        $saveEachBackground = new Background(
                            $saveBackgroundList[$eachFileKey]
                        );
                        if ($saveEachBackground->save()) {
                            $backgroundLastId = $saveEachBackground->xe_id;
                            $recordBackgroundIds[] = $backgroundLastId;
                            /**
                             * Save category and subcategory data
                             * Category id format. [4,78,3]
                             */
                            if (isset($allPostPutVars['categories'])
                                && $allPostPutVars['categories'] != ""
                            ) {
                                $categoryIds = $allPostPutVars['categories'];
                                // Save Background Categories
                                $this->_SaveBackgroundCategories(
                                    $backgroundLastId, $categoryIds
                                );
                            }
                            /**
                             * - Save tags with respect to the Backgrounds
                             * - Tag Names format.: tag1,tag2,tag3
                             */
                            if (isset($allPostPutVars['tags'])
                                && $allPostPutVars['tags'] != ""
                            ) {
                                $tags = $allPostPutVars['tags'];
                                $this->_SaveBackgroundTags(
                                    $backgroundLastId, $tags
                                );
                            }
                            $success++;
                        }
                    }
                }
            }
        } else {
            $allPostPutVars += [
                'value' => $allPostPutVars['upload'],
                'store_id' => $getStoreDetails['store_id'],
            ];
            $saveBackgroundColor = new Background($allPostPutVars);
            if ($saveBackgroundColor->save()) {
                $backgroundLastId = $saveBackgroundColor->xe_id;
                /**
                 * Save category and subcategory data
                 * Category id should come in json array format. e.g.: [4,78,3]
                 */
                if (isset($allPostPutVars['categories'])
                    && $allPostPutVars['categories'] != ""
                ) {
                    $categoryIds = $allPostPutVars['categories'];
                    // Save Background Categories
                    $this->_SaveBackgroundCategories(
                        $backgroundLastId, $categoryIds
                    );
                }
                /**
                 * - Save tags with respect to the Backgrounds
                 * - Tag Names format.: tag1,tag2,tag3
                 */
                if (isset($allPostPutVars['tags'])
                    && $allPostPutVars['tags'] != ""
                ) {
                    $tags = $allPostPutVars['tags'];
                    $this->_SaveBackgroundTags($backgroundLastId, $tags);
                }
                $success++;
                $uploadingFilesNo = 1;
            }
        }
        if (!empty($success) && $success > 0) {
            $jsonResponse = [
                'status' => 1,
                'message' => $success . ' out of ' . $uploadingFilesNo . ' Background(s) uploaded successfully',
            ];
        }
        return response(
            $response, ['data' => $jsonResponse, 'status' => $serverStatusCode]
        );
    }

    /**
     * Save Categories/Sub-categories w.r.t Background
     *
     * @param $backgroundId Background ID
     * @param $categoryIds  (in  an array with comma separated)
     *
     * @author satyabratap@riaxe.com
     * @date   4th Nov 2019
     * @return boolean
     */
    private function _SaveBackgroundCategories($backgroundId, $categoryIds)
    {
        $getAllCategoryArr = json_clean_decode($categoryIds, true);
        //SYNC Categories to the Background_Category Relationship Table
        $backgroundInit = new Background();
        $findBackground = $backgroundInit->find($backgroundId);
        if ($findBackground->categories()->sync($getAllCategoryArr)) {
            return true;
        }
        return false;
    }

    /**
     * Save tags w.r.t Background
     *
     * @param $backgroundId Background ID
     * @param $multipletags Multiple Tags
     *
     * @author satyabratap@riaxe.com
     * @date   4th Nov 2019
     * @return boolean
     */
    private function _SaveBackgroundTags($backgroundId, $multipletags)
    {
        if (isset($multipletags) && $multipletags != "") {
            $tags = $multipletags;
            $updatedTagIds = [];
            $tagsStringToArray = explode(',', $tags);
            foreach ($tagsStringToArray as $tag) {
                // Save each individual Tag to table
                $tagInit = new Tag();
                if ($tagInit->where(['name' => trim($tag)])->count() == 0) {
                    $saveTag = new Tag(['name' => trim($tag)]);
                    $saveTag->save();
                    $tagLastInsertId = $saveTag->xe_id;
                } else {
                    $getTagDetails = $tagInit->where(['name' => trim($tag)])
                        ->select('xe_id')
                        ->first();
                    $tagLastInsertId = $getTagDetails['xe_id'];
                }
                if (isset($tagLastInsertId) && $tagLastInsertId > 0) {
                    $updatedTagIds[] = $tagLastInsertId;
                }
            }
            //SYNC Tags into Background_Tag Relationship Table
            $backgroundInit = new Background();
            $findBackground = $backgroundInit->find($backgroundId);
            if ($findBackground->tags()->sync($updatedTagIds)) {
                return true;
            }
            return false;
        }
    }

    /**
     * GET: List of Backgrounds and single Background
     *
     * @param $request  Slim's Request object
     * @param $response Slim's Response object
     * @param $args     Slim's Argument parameters
     *
     * @author satyabratap@riaxe.com
     * @date   4th Nov 2019
     * @return All/Single Background List
     */
    public function getBackgrounds($request, $response, $args)
    {
        $serverStatusCode = OPERATION_OKAY;
        $offset = 0;
        $backgroundData = [];
        $getStoreDetails = get_store_details($request);
        $jsonResponse = [
            'status' => 0,
            'data' => [],
            'message' => message('Background', 'not_found'),
        ];

        $backgroundInit = new Background();
        $getBackgrounds = $backgroundInit->where('xe_id', '>', 0);
        $getBackgrounds->where('store_id', '=', $getStoreDetails['store_id']);
        // total records irrespectable of filters
        $totalCounts = $getBackgrounds->count();
        if ($totalCounts > 0) {
            if (isset($args['id']) && $args['id'] != '') {
                //For single Background data
                $getBackgrounds->where('xe_id', '=', $args['id'])
                    ->select('xe_id', 'name', 'value', 'price', 'type');
                $backgroundData = $getBackgrounds->orderBy('xe_id', 'DESC')
                    ->first();

                // Get Category Ids
                if (!empty($backgroundData)) {
                    $getCategories = $this->getCategoriesById(
                        'Backgrounds', 'BackgroundCategoryRelation',
                        'background_pattern_id', $args['id']
                    );
                    $getTags = $this->getTagsById(
                        'Backgrounds', 'BackgroundTagRelation',
                        'background_pattern_id', $args['id']
                    );
                    $backgroundData['categories'] = !empty($getCategories) ? $getCategories : [];
                    $backgroundData['tags'] = !empty($getTags) ? $getTags : [];

                }
                // Unset category_name Key in case of single record fetch
                $backgroundData = json_clean_decode($backgroundData, true);
                unset($backgroundData['category_names']);
                $jsonResponse = [
                    'status' => 1,
                    'records' => 1,
                    'data' => [
                        $backgroundData,
                    ],
                ];

            } else {
                // Collect all Filter columns from url
                $page = $request->getQueryParam('page');
                $perpage = $request->getQueryParam('perpage');
                $categoryId = $request->getQueryParam('category');
                $sortBy = $request->getQueryParam('sortby');
                $order = $request->getQueryParam('order');
                $name = $request->getQueryParam('name');

                // For multiple Background data
                $getBackgrounds->select('xe_id', 'name', 'value', 'price', 'type');

                // Multiple Table search for name attribute
                if (isset($name) && $name != "") {
                    $getBackgrounds->where('name', 'LIKE', '%' . $name . '%')
                        ->orWhereHas(
                            'backgroundTags.tag', function ($q) use ($name) {
                                return $q->where('name', 'LIKE', '%' . $name . '%');
                            }
                        )
                        ->orWhereHas(
                            'backgroundCategory.category', function ($q) use ($name) {
                                return $q->where('name', 'LIKE', '%' . $name . '%');
                            }
                        );
                }
                // Filter by Category ID
                if (isset($categoryId) && $categoryId != "") {
                    $searchCategories = json_clean_decode($categoryId, true);
                    $getBackgrounds->whereHas(
                        'backgroundCategory', function ($q) use ($searchCategories) {
                            return $q->whereIn('category_id', $searchCategories);
                        }
                    );
                }

                // Get pagination data
                if (isset($page) && $page != "") {
                    $totalItem = empty($perpage) ? PAGINATION_MAX_ROW : $perpage;
                    $offset = $totalItem * ($page - 1);
                    $getBackgrounds->skip($offset)->take($totalItem);
                }
                // Sorting by column name and sord order parameter
                if (isset($sortBy) && $sortBy != ""
                    && isset($order) && $order != ""
                ) {
                    $getBackgrounds->orderBy($sortBy, $order);
                }
                $backgroundData = $getBackgrounds->get();

                $jsonResponse = [
                    'status' => 1,
                    'records' => count($backgroundData),
                    'total_records' => $totalCounts,
                    'data' => $backgroundData,
                ];
            }
        }
        return response(
            $response, ['data' => $jsonResponse, 'status' => $serverStatusCode]
        );
    }

    /**
     * PUT: Update a single background
     *
     * @param $request  Slim's Request object
     * @param $response Slim's Response object
     * @param $args     Slim's Argument parameters
     *
     * @author satyabratap@riaxe.com
     * @date   4th Nov 2019
     * @return json response wheather data is updated or not
     */
    public function updateBackground($request, $response, $args)
    {
        $serverStatusCode = OPERATION_OKAY;
        $jsonResponse = [
            'status' => 0,
            'message' => message('Background', 'error'),
        ];
        $getStoreDetails = get_store_details($request);
        $allPostPutVars = $updateData = $this->parsePut();

        if (isset($args['id']) && $args['id'] != "") {
            $backgroundInit = new Background();
            $getOldbackground = $backgroundInit->where('xe_id', '=', $args['id']);
            if ($getOldbackground->count() > 0) {
                unset(
                    $updateData['id'], $updateData['tags'],
                    $updateData['categories'], $updateData['upload'],
                    $updateData['value'], $updateData['backgroundId']
                );
                // Update Background Pattern file
                // delete old file
                $this->deleteOldFile(
                    "background_patterns", "value", [
                        'xe_id' => $args['id'],
                    ], path('abs', 'background')
                );
                $getUploadedFileName = save_file(
                    'upload', path('abs', 'background')
                );
                if ($getUploadedFileName != '') {
                    $updateData += ['value' => $getUploadedFileName];
                } else if ($allPostPutVars['upload'] != "") {
                    $updateData += ['value' => $allPostPutVars['upload']];
                }

                $updateData += ['store_id' => $getStoreDetails['store_id']];
                // Update record
                try {
                    $backgroundInit->where('xe_id', '=', $args['id'])
                        ->update($updateData);
                    $jsonResponse = [
                        'status' => 1,
                        'message' => message('Background', 'updated'),
                    ];
                    /**
                     * Save category
                     * Parameter: categories
                     */
                    if (isset($allPostPutVars['categories']) 
                        && $allPostPutVars['categories'] != ""
                    ) {
                        $categoryIds = $allPostPutVars['categories'];
                        $this->_SaveBackgroundCategories($args['id'], $categoryIds);
                    }
                    /**
                     * Save tags
                     * Parameter: tags
                     */
                    if (isset($allPostPutVars['tags'])
                        && $allPostPutVars['tags'] != ""
                    ) {
                        $tags = $allPostPutVars['tags'];
                        $this->_SaveBackgroundTags($args['id'], $tags);
                    }
                } catch (\Exception $e) {
                    $serverStatusCode = EXCEPTION_OCCURED;
                    $jsonResponse = [
                        'status' => 0,
                        'message' => message('Background', 'exeception'),
                        'exception' => show_exception() === true ? $e->getMessage() : '',
                    ];
                }
            }
        }
        return response(
            $response, ['data' => $jsonResponse, 'status' => $serverStatusCode]
        );
    }

    /**
     * DELETE: Delete single/multiple background
     *
     * @param $request  Slim's Argument parameters
     * @param $response Slim's Response object
     * @param $args     Slim's Argument parameters
     *
     * @author satyabratap@riaxe.com
     * @date   4th Nov 2019
     * @return json response wheather data is deleted or not
     */
    public function deleteBackground($request, $response, $args)
    {
        $serverStatusCode = OPERATION_OKAY;
        $jsonResponse = [
            'status' => 0,
            'message' => message('Background', 'error'),
        ];
        if (isset($args) && $args['id'] != '') {
            $getDeleteIdsToArray = json_clean_decode($args['id'], true);
            if (!empty($getDeleteIdsToArray)) {
                $backgroundInit = new Background();
                if ($backgroundInit->whereIn('xe_id', $getDeleteIdsToArray)->count() > 0) {
                    // Fetch Background Pattern details
                    $getBackgroundDetails = $backgroundInit->whereIn(
                        'xe_id', $getDeleteIdsToArray
                    )
                        ->select('xe_id')
                        ->get();
                    try {
                        if (!empty($getBackgroundDetails)) {
                            foreach ($getBackgroundDetails as $backgroundFile) {
                                if (isset($backgroundFile['xe_id'])
                                    && $backgroundFile['xe_id'] != ""
                                ) {
                                    $this->deleteOldFile(
                                        "background_patterns", "value", [
                                            'xe_id' => $backgroundFile['xe_id'],
                                        ], path('abs', 'background')
                                    );
                                }
                            }
                        }
                        $backgroundDelInit = new Background();
                        $backgroundDelInit->whereIn(
                            'xe_id', $getDeleteIdsToArray
                        )->delete();
                        $jsonResponse = [
                            'status' => 1,
                            'message' => message('Background', 'deleted'),
                        ];
                    } catch (\Exception $e) {
                        $serverStatusCode = EXCEPTION_OCCURED;
                        $jsonResponse = [
                            'status' => 0,
                            'message' => message('Background', 'exception'),
                            'exception' => show_exception() === true ? $e->getMessage() : '',
                        ];
                    }
                }
            }
        }
        return response(
            $response, ['data' => $jsonResponse, 'status' => $serverStatusCode]
        );
    }

    /**
     * Check Category Relation with Category
     *
     * @param $request  Slim's Argument parameters
     * @param $response Slim's Response object
     * @param $args     Slim's Argument parameters
     *
     * @author satyabratap@riaxe.com
     * @date   20 Jan 2020
     * @return boolean
     */
    public function checkRelCategory($request, $response, $args)
    {
        $serverStatusCode = OPERATION_OKAY;
        $jsonResponse = [
            'status' => 0,
            'message' => 'This Category is associated with an Item',
        ];
        $key = $request->getQueryParam('key');
        if (!empty($args) && $args['id'] > 0) {
            $categoryId = $args['id'];
            $checkCatRelInit = new BackgroundCategoryRelation();
            $getCatRel = $checkCatRelInit->where('category_id', $categoryId)->get();
            if ($getCatRel->count() == 0) {
                if ($key == 'disable') {
                    $jsonResponse = $this->disableCat('background-patterns', $categoryId);
                } elseif ($key == 'delete') {
                    $jsonResponse = $this->deleteCat(
                        'background-patterns', $categoryId, 'Backgrounds', 'BackgroundCategoryRelation'
                    );
                }
            }
        }
        return response(
            $response, ['data' => $jsonResponse, 'status' => $serverStatusCode]
        );
    }

    /**
     * Delete a category from the table
     *
     * @param $request  Slim's Request object
     * @param $response Slim's Response object
     * @param $args     Slim's Argument parameters
     *
     * @author satyabratap@riaxe.com
     * @date   20 Jan 2020
     * @return Delete Json Status
     */
    public function deleteCategory($request, $response, $args)
    {
        $serverStatusCode = OPERATION_OKAY;
        $jsonResponse = [
            'status' => 0,
            'message' => message('Category', 'error'),
        ];
        if (!empty($args) && $args['id'] > 0) {
            $categoryId = $args['id'];
            $jsonResponse = $this->deleteCat(
                'background-patterns', $categoryId, 'Backgrounds', 'BackgroundCategoryRelation'
            );
        }
        return response(
            $response, ['data' => $jsonResponse, 'status' => $serverStatusCode]
        );
    }
}
