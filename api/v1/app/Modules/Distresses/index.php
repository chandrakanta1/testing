<?php
/**
 * Index File for Distresses
 *
 * PHP version 5.6
 *
 * @category  Distresses
 * @package   Assets
 * @author    Satyabrata <satyabratap@riaxe.com>
 * @copyright 2019-2020 Riaxe Systems
 * @license   http://www.php.net/license/3_0.txt  PHP License 3.0
 * @link      http://inkxe-v10.inkxe.io/xetool/admin
 */
require __DIR__ . '/Routes/routes.php';
