<?php
/**
 * This Routes holds all the individual route for the Cart
 *
 * PHP version 5.6
 *
 * @category  Carts
 * @package   Store
 * @author    Debashri Bhakat <debashrib@riaxe.com>
 * @copyright 2019-2020 Riaxe Systems
 * @license   http://www.php.net/license/3_0.txt  PHP License 3.0
 * @link      http://inkxe-v10.inkxe.io/xetool/admin
 */

use Slim\Http\Request;
use Slim\Http\Response;
use App\Modules\Carts\Controllers\CartsController;
use App\Middlewares\ValidateJWTToken as ValidateJWT;

// Instantiate the Container
$container = $app->getContainer();

//Routs for Cart getTotalCartItem
$app->group(
    '/carts', function () use ($app) {
        $app->post('', CartsController::class . ':addToCart');
    }
)->add(new ValidateJWT($container));

