<?php
/**
 * Manage Stiore carts
 *
 * PHP version 5.6
 *
 * @category  Carts
 * @package   Store
 * @author    Debashri Bhakat <debashrib@riaxe.com>
 * @copyright 2019-2020 Riaxe Systems
 * @license   http://www.php.net/license/3_0.txt  PHP License 3.0
 * @link      http://inkxe-v10.inkxe.io/xetool/admin
 */
namespace CartStoreSpace\Controllers;

use ComponentStoreSpace\Controllers\StoreComponent;

/**
 * Store Carts Controller
 *
 * @category Carts
 * @package  Store
 * @author   Debashri Bhakat <debashrib@riaxe.com>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://inkxe-v10.inkxe.io/xetool/admin
 */

class StoreCartsController extends StoreComponent
{
    /**
     * POST: Save Cart data
     *
     * @param $request  Slim's Request object
     * @param $response Slim's Response object
     *
     * @author debashrib@riaxe.com
     * @date   07 Jan 2020
     * @return json response wheather data is saved or any error occured
     */
    public function addToStoreCart($request, $response) 
    {
        $jsonResponse = [];
        $serverStatusCode = OPERATION_OKAY;
        $getStoreDetails = get_store_details($request);
        $allPostPutVars = $request->getParsedBody();
        $action = (isset($allPostPutVars['action']) && $allPostPutVars['action'] !='')?$allPostPutVars['action']:'add';
        $cartItemId = (isset($allPostPutVars['cart_item_id']) && $allPostPutVars['cart_item_id'] !='')?$allPostPutVars['cart_item_id']:0;
        $cartInfo = [];
        
        // Get quote Id from cookie
        $quoteId = 0;
        if (isset($allPostPutVars['quoteId'])) {
            $quoteId = intval($allPostPutVars['quoteId']);
        } else if (isset($_COOKIE['quoteId'])) {
            $quoteId = intval(base64_decode($_COOKIE['quoteId']));
        }
        
        $customerId = 0;
        if (isset($allPostPutVars['customer_id'])) {
            $customerId = intval($allPostPutVars['customer_id']);
            if ($customerId == 0 || $customerId <= 0) {
                $customerId = 0;
            }
        }

        $filters = array(
            'quoteId' => $quoteId,
            'store' => $getStoreDetails['store_id'],
            'customerId' => $customerId,
            'cartItemId' => $cartItemId,
            'productsData' => $allPostPutVars['product_data'],
            'action' => $action
        );
        
        try {
            $result = $this->apiCall('Cart', 'addToCart', $filters);
            $result = $result->result;
            $cartInfo = json_decode($result);
            $url = $cartInfo->checkoutURL . '?quoteId=' . $cartInfo->quoteId;
            if ($cartInfo->is_Fault == 0) {
                // setting quote id cookie
                $expire = time() + 60 * 60 * 24 * 30; //30 days
                setcookie("quoteId", base64_encode($cartInfo->quoteId), $expire, "/");
                $jsonResponse = [
                    'status' => 1,
                    'message' => message('Cart', 'saved'),
                    'url' => $url,
                ];
            }else if($cartInfo->is_Fault == 1){
                $jsonResponse = [
                    'status' => 0,
                    'message' => $cartInfo,
                ];
            }
        } catch (Exception $e) {
            $serverStatusCode = EXCEPTION_OCCURED;
            $jsonResponse = [
                'status' => 0,
                'message' => message('Cart', 'exception'),
                'exception' => show_exception() === true ? $e->getMessage() : '',
            ];
        }
        return [
            'data' => $jsonResponse,
            'httpStatusCode' => $serverStatusCode
        ];
    }
}
