<?php
/**
 * Manage Stiore carts
 *
 * PHP version 5.6
 *
 * @category  Carts
 * @package   Store
 * @author    Mukesh Pradhan <mukeshp@riaxe.com>
 * @copyright 2019-2020 Riaxe Systems
 * @license   http://www.php.net/license/3_0.txt  PHP License 3.0
 * @link      http://inkxe-v10.inkxe.io/xetool/admin
 */
namespace CartStoreSpace\Controllers;

use CommonStoreSpace\Controllers\StoreController;

/**
 * Store Carts Controller
 *
 * @category Carts
 * @package  Store
 * @author   Mukesh Pradhan <mukeshp@riaxe.com>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://inkxe-v10.inkxe.io/xetool/admin
 */

class StoreCartsController extends StoreController
{
    /**
     * POST: Save Cart data
     *
     * @param $request  Slim's Request object
     * @param $response Slim's Response object
     *
     * @author mukeshp@riaxe.com
     * @date   09 Jan 2020
     * @return json response wheather data is saved or any error occured
     */
    public function addToStoreCart($request, $response) 
    {
        $jsonResponse = [];
        $serverStatusCode = OPERATION_OKAY;
        $getStoreDetails = get_store_details($request);
        $allPostPutVars = $request->getParsedBody();
        $action = (isset($allPostPutVars['action']) && $allPostPutVars['action'] !='')?$allPostPutVars['action']:'add';
        $cartItemId = (isset($allPostPutVars['cart_item_id']) && $allPostPutVars['cart_item_id'] !='')?$allPostPutVars['cart_item_id']:0;
        $cartInfo = [];
        
        // Get quote Id from cookie
        $quoteId = 0;
        if (isset($allPostPutVars['quoteId'])) {
            $quoteId = intval($allPostPutVars['quoteId']);
        } else if (isset($_COOKIE['quoteId'])) {
            $quoteId = intval(base64_decode($_COOKIE['quoteId']));
        }
        
        $customerId = 0;
        if (isset($allPostPutVars['customer_id'])) {
            $customerId = intval($allPostPutVars['customer_id']);
            if ($customerId == 0 || $customerId <= 0) {
                $customerId = 0;
            }
        }
        $filters = [
            'quoteId' => $quoteId,
            'store' => $getStoreDetails['store_id'],
            'productsData' => $allPostPutVars['product_data'],
            'action' => $action,
            'customer_id' => $customerId,
            'cart_item_id' => $cartItemId
       ];
        
        try {
            $result = $this->plugin->post('addtocart', $filters);
            $result = $result->result;
            $cartInfo = $result;
            if ($cartInfo->is_Fault == 0) {
                $jsonResponse = [
                    'status' => 1,
                    'message' => message('Cart', 'saved'),
                    'url' => $cartInfo->checkout_url,
                ];
            }else if($cartInfo->is_Fault == 1){
                $jsonResponse = [
                    'status' => 0,
                    'message' => $cartInfo,
                ];
            }
        } catch (Exception $e) {
            $serverStatusCode = EXCEPTION_OCCURED;
            $jsonResponse = [
                'status' => 0,
                'message' => message('Cart', 'exception'),
                'exception' => show_exception() === true ? $e->getMessage() : '',
            ];
        }
        return [
            'data' => $jsonResponse,
            'httpStatusCode' => $serverStatusCode
        ];
    }
}
