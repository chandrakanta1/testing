<?php 
/**
 * Manage Carts
 *
 * PHP version 5.6
 *
 * @category  Carts
 * @package   Store
 * @author    Debashri Bhakat <debashrib@riaxe.com>
 * @copyright 2019-2020 Riaxe Systems
 * @license   http://www.php.net/license/3_0.txt  PHP License 3.0
 * @link      http://inkxe-v10.inkxe.io/xetool/admin
 */
namespace App\Modules\Carts\Controllers;

use CartStoreSpace\Controllers\StoreCartsController;
use App\Components\Controllers\Component;

/**
 * Carts Controller
 *
 * @category Carts
 * @package  Store
 * @author   Debashri Bhakat <debashrib@riaxe.com>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://inkxe-v10.inkxe.io/xetool/admin
 */
    
class CartsController extends StoreCartsController
{
    /**
     * POST: add To Cart
     *
     * @param $request  Slim's Request object
     * @param $response Slim's Response object
     *
     * @author debashrib@riaxe.com
     * @date   06 Jan 2020
     * @return json response wheather data is saved or any error occured
     */
    public function addToCart($request, $response) {
        $serverStatusCode = OPERATION_OKAY;
        $jsonResponse['data'] = [];
        // Get Store Specific Details from helper
        $getStoreDetails = get_store_details($request);
        $allPostPutVars = $request->getParsedBody();
        
        if(isset($allPostPutVars['design_data']) && $allPostPutVars['design_data'] !=''){

            $designData = json_clean_decode($allPostPutVars['design_data'],true);

            $productId = isset($designData['product_info']['product_id']) && $designData['product_info']['product_id'] !='' ? $designData['product_info']['product_id']:null;
            // Prepare array for saving design data
            $designDetails = [
                'store_id' => $getStoreDetails['store_id'],
                'product_setting_id' => (isset($designData['product_settings_id']) && $designData['product_settings_id'] > 0) ? $designData['product_settings_id'] : null,
                'product_variant_id' => (isset($designData['product_variant_id']) && $designData['product_variant_id'] > 0) ? $designData['product_variant_id'] : null,
                'product_id' => $productId,
                'type' => (isset($designData['template_type']) && $designData['template_type'] != "") ? $designData['template_type'] : "cart",
                'custom_price' => (isset($designData['custome_price']) && $designData['custome_price'] > 0) ? $designData['custome_price'] : 0.00,
            ];
            if(isset($designDetails) && !empty($designDetails !='')){
                // save design data and get customDesignId
                $customDesignId = $this->saveDesignData($designDetails,$allPostPutVars['design_data']);
                if($customDesignId > 0){
                    $jsonResponse = $this->addToStoreCart($request, $response);
                    $jsonResponse['data']['customDesignId'] = $customDesignId;
                }
            }
        }
        return response($response, ['data' => $jsonResponse['data'], 'status' => $serverStatusCode]);
    }
    
}
