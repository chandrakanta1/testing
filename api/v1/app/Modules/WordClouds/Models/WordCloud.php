<?php
/**
 * Word Cloud Model
 *
 * PHP version 5.6
 *
 * @category  Word_Cloud
 * @package   Assets
 * @author    Satyabrata <satyabratap@riaxe.com>
 * @copyright 2019-2020 Riaxe Systems
 * @license   http://www.php.net/license/3_0.txt  PHP License 3.0
 * @link      http://inkxe-v10.inkxe.io/xetool/admin
 */

namespace App\Modules\WordClouds\Models;

use App\Components\Controllers\Component as ParentController;

/**
 * Word Cloud
 *
 * @category Word_Cloud
 * @package  Assets
 * @author   Satyabrata <satyabratap@riaxe.com>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://inkxe-v10.inkxe.io/xetool/admin
 */
class WordCloud extends \Illuminate\Database\Eloquent\Model
{
    protected $primaryKey = 'xe_id';
    protected $fillable = ['name', 'file_name', 'store_id'];
    protected $appends = ['thumbnail', 'category_names'];

    /**
     * Create relationship between Word Cloud and
     * WordCloud-Category-Relationship
     *
     * @author satyabratap@riaxe.com
     * @date   4th Nov 2019
     * @return relationship object of category
     */
    public function wordCloudCategory()
    {
        return $this->hasMany('App\Modules\WordClouds\Models\WordCloudCategoryRelation', 'word_cloud_id');
    }

    /**
     * Create relationship between Disress and Category
     *
     * @author satyabratap@riaxe.com
     * @date   4th Nov 2019
     * @return relationship object of category
     */
    public function categories()
    {
        return $this->belongsToMany('App\Modules\WordClouds\Models\WordCloudCategoryRelation', 'word_cloud_category_rel', 'word_cloud_id', 'category_id');
    }

    /**
     * Create relationship between Word Cloud and
     * Word Cloud-Tag-Relationship
     *
     * @author satyabratap@riaxe.com
     * @date   4th Nov 2019
     * @return relationship object of tags
     */
    public function wordCloudTags()
    {
        return $this->hasMany('App\Modules\WordClouds\Models\WordCloudTagRelation', 'word_cloud_id');
    }
    /**
     * Create relationship between Disress and Tag
     *
     * @author satyabratap@riaxe.com
     * @date   4th Nov 2019
     * @return relationship object of tag
     */
    public function tags()
    {
        return $this->belongsToMany('App\Modules\WordClouds\Models\WordCloudTagRelation', 'word_cloud_tag_rel', 'word_cloud_id', 'tag_id');
    }

    /**
     * This is a method from Eloquent. The basic functionality of this method is
     * to modify the file_name before sending the response
     *
     * @author satyabratap@riaxe.com
     * @date   4th Nov 2019
     * @return file path url
     */
    public function getFileNameAttribute()
    {
        if (isset($this->attributes['file_name']) && $this->attributes['file_name'] != "") {
            return path('read', 'wordcloud') . $this->attributes['file_name'];
        } else {
            return "";
        }
    }

    /**
     * This is a method from Eloquent. The basic functionality of this method is
     * to modify the thumbnail of the file before sending the response
     *
     * @author satyabratap@riaxe.com
     * @date   4th Nov 2019
     * @return file path url
     */
    public function getThumbnailAttribute()
    {
        if (isset($this->attributes['file_name']) && $this->attributes['file_name'] != "") {
            return path('read', 'wordcloud') . 'thumb_' . $this->attributes['file_name'];
        } else {
            return "";
        }
    }

    /**
     * Get Category lists in comma separated format
     *
     * @author tanmayap@riaxe.com
     * @date   14 Jan 2020
     * @return relationship object of category
     */
    public function getCategoryNamesAttribute()
    {
        $categoryList = "";
        $parentInit = new ParentController();
        $getData = $parentInit->getCategoriesById(
            'WordClouds', 'WordCloudCategoryRelation', 
            'word_cloud_id', $this->attributes['xe_id'], 
            'name'
        );
        if (!empty($getData) && count($getData) > 0) {
            $categoryList = implode(', ', $getData);
        }
        return $categoryList;
    }
}
