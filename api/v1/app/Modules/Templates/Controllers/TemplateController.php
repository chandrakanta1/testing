<?php
/**
 * Manage Templates and Designs
 *
 * PHP version 5.6
 *
 * @category  Template
 * @package   Template_Design
 * @author    Tanmaya Patra <tanmayap@riaxe.com>
 * @copyright 2019-2020 Riaxe Systems
 * @license   http://www.php.net/license/3_0.txt  PHP License 3.0
 * @link      http://inkxe-v10.inkxe.io/xetool/admin
 */
namespace App\Modules\Templates\Controllers;

use App\Components\Controllers\Component as ParentController;
use App\Modules\PrintProfiles\Models\PrintProfile;
use App\Modules\Templates\Models as TemplateModel;
use App\Modules\Templates\Models\TemplateCategoryRel;

/**
 * Template Controller
 *
 * @category Class
 * @package  Template_Design
 * @author   Tanmaya Patra <tanmayap@riaxe.com>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://inkxe-v10.inkxe.io/xetool/admin
 */
class TemplateController extends ParentController
{
    /**
     * Post: Save Design State along with Templates
     *
     * @param $request  Slim's Request object
     * @param $response Slim's Response object
     *
     * @author tanmayap@riaxe.com
     * @date   5 Oct 2019
     * @return Save status in json format
     */
    public function saveDesigns($request, $response)
    {
        $getStoreDetails = get_store_details($request);
        $serverStatusCode = OPERATION_OKAY;
        $allPostPutVars = $request->getParsedBody();
        // Initilize json Response
        $jsonResponse = [
            'status' => 0,
            'message' => message('Design Template', 'error'),
        ];
        $designState = [
            'store_id' => $getStoreDetails['store_id'],
            'product_setting_id' => (isset($allPostPutVars['product_settings_id']) && $allPostPutVars['product_settings_id'] > 0) ? $allPostPutVars['product_settings_id'] : null,
            'product_variant_id' => (isset($allPostPutVars['product_variant_id']) && $allPostPutVars['product_variant_id'] > 0) ? $allPostPutVars['product_variant_id'] : null,
            // product_id is added temporary purpose. For inkxe8 Tool
            'product_id' => (isset($allPostPutVars['product_id']) && $allPostPutVars['product_id'] > 0) ? $allPostPutVars['product_id'] : null,
            'type' => (isset($allPostPutVars['template_type']) && $allPostPutVars['template_type'] != "") ? $allPostPutVars['template_type'] : "template",
            'custom_price' => (isset($allPostPutVars['custome_price']) && $allPostPutVars['custome_price'] > 0) ? $allPostPutVars['custome_price'] : 0.00,
        ];

        $designData = (isset($allPostPutVars['design_data']) && $allPostPutVars['design_data'] != "") ? $allPostPutVars['design_data'] : '';
        // Save design data and json formate of design data
        $reffId = $this->saveDesignData($designState, $designData);

        if ($reffId > 0) {
            // Save Template Data with its dedicated function
            $templateSaveResponse = $this->saveTemplates($request, $response, $reffId, 'save');

            if (!empty($templateSaveResponse) && $templateSaveResponse > 0) {
                // Save Print Profile Relations
                $saveTempPrintProfile = $this->savePrintProfileRelations($request, $response, $reffId, $templateSaveResponse, 'save');
                // Save Template Tag Relations
                $templateTagRels = [
                    'reff_id' => $reffId, 
                    'template_id' => $templateSaveResponse, 
                    'store_id' => $getStoreDetails['store_id']
                ];
                $printProfileTempTags = $this->saveTemplateTags($request, $response, $templateTagRels, 'save');
                // Save Template Categories
                $saveTemplateCats = $this->saveTemplateCategories($request, $response, $templateTagRels, 'save');
                /**
                 * - Process capture Images Files will be copied from temporary
                 * folder to template folder and these copy process done over a
                 * unique file name with some required prefixes. prefixes will
                 * be for thumb, main image, with or without product images.
                 */
                // First json decode then check if the decoded array has any data
                $capturedImages = isset($allPostPutVars['captured_images']) ? json_clean_decode($allPostPutVars['captured_images'], true) : array();
                $capturedImagesRes = $this->moveCapturedImages($capturedImages, $reffId);
                /**
                 * Process Design Data SVG File codes captured_images
                 */
                // Prepare Capture folder as per the Reference ID
                if (isset($designData) && !empty($designData)) {
                    $designDataFiles = json_clean_decode($designData, true);
                    $designSvgFiles = $this->saveDesignFile($designDataFiles, $reffId);
                }
            }

            $jsonResponse = [
                'status' => 1,
                'ref_id' => $reffId,
                'template_id' => $templateSaveResponse,
                'message' => message('Design Template', 'saved'),
            ];

        }
        return response(
            $response, ['data' => $jsonResponse, 'status' => $serverStatusCode]
        );
    }
    /**
     * Put: Update Design State along with Templates
     *
     * @param $request  Slim's Request object
     * @param $response Slim's Response object
     * @param $args     Slim's Argument parameters
     *
     * @author tanmayap@riaxe.com
     * @date   5 Oct 2019
     * @return Save status in json format
     */
    public function updateDesigns($request, $response, $args)
    {
        $templateSave = [];
        $getStoreDetails = get_store_details($request);
        $serverStatusCode = OPERATION_OKAY;
        $allPutVars = $this->parsePut();
        $jsonResponse = [
            'status' => 0,
            'message' => message('Design Template', 'error'),
        ];

        $refId = (isset($args['id']) && $args['id'] > 0) ? $args['id'] : null;
        if (isset($args['id']) && $args['id'] > 0) {
            $designState = [
                'product_setting_id' => $allPutVars['product_settings_id'],
                'product_variant_id' => $allPutVars['product_variant_id'],
                'type' => $allPutVars['template_type'],
            ];
            $templateDesignInit = new TemplateModel\TemplateDesignStates();
            $designInit = $templateDesignInit->where('xe_id', $refId);
            if ($designInit->count() > 0) {
                try {
                    $designInit->update($designState);
                    // Save Template Data with its dedicated function
                    $templateSaveResponse = $this->saveTemplates($request, $response, $refId, 'update');
                    if (!empty($templateSaveResponse) && $templateSaveResponse > 0) {
                        // Save Print Profile Relations
                        $saveTempPrintProfile = $this->savePrintProfileRelations($request, $response, $refId, $templateSaveResponse, 'update');
                        // Save Template Tag Relations
                        $templateTagRels = [
                            'reff_id' => $refId,
                            'template_id' => $templateSaveResponse,
                            'store_id' => $getStoreDetails['store_id'],
                        ];
                        $printProfileTempTags = $this->saveTemplateTags($request, $response, $templateTagRels, 'update');
                        // Save Template Categories
                        $saveTemplateCats = $this->saveTemplateCategories($request, $response, $templateTagRels, 'update');
                    }

                    $jsonResponse = [
                        'status' => 1,
                        'message' => message('Design Template', 'updated'),
                    ];
                } catch (\Exception $e) {
                    $jsonResponse = [
                        'status' => 0,
                        'message' => message('Design Template', 'error'),
                        'exception' => show_exception() === true ? $e->getMessage() : message('Design Template', 'exception'),
                    ];
                }
            }
        }
        return response(
            $response, ['data' => $jsonResponse, 'status' => $serverStatusCode]
        );
    }

    /**
     * GET: Get single or all templates
     *
     * @param $request  Slim's Request object
     * @param $response Slim's Response object
     * @param $args     Slim's Argument parameters
     *
     * @author tanmayap@riaxe.com
     * @date   5 Oct 2019
     * @return Json Response
     */
    public function getTemplates($request, $response, $args)
    {
        $serverStatusCode = OPERATION_OKAY;
        $getStoreDetails = get_store_details($request);
        $offset = 0;
        $templateInit = new TemplateModel\Template();
        $getTemplates = $templateInit->where('xe_id', '>', 0);

        if (isset($args) && count($args) > 0 && $args['id'] != '') {
            //For single Shape data
            $getTemplates->where('xe_id', '=', $args['id']);
        } else {
            // Collect all Filter columns from url
            $page = $request->getQueryParam('page');
            $colorUsed = $request->getQueryParam('color_used'); // number of colors
            $perpage = $request->getQueryParam('perpage');
            $sortBy = $request->getQueryParam('sortby');
            $catagoryId = $request->getQueryParam('catagory');
            $tagId = $request->getQueryParam('tag');
            $printProfileId = $request->getQueryParam('print_profile');
            $order = $request->getQueryParam('order');
            $name = $request->getQueryParam('name');

            // For multiple Shape data
            $getTemplates->select('xe_id as id', 'xe_id', 'name', 'ref_id', 'store_id', 'no_of_colors', 'color_hash_codes');
        }
        $totalCounts = $getTemplates->count();
        if ($totalCounts > 0) {
            if (isset($getStoreDetails) && $getStoreDetails > 0) {
                $getTemplates->where($getStoreDetails);
            }
            // Multiple Table search for name attribute
            if (isset($name) && $name != "") {
                $getTemplates->where('name', 'LIKE', '%' . $name . '%')
                    ->orWhereHas(
                        'templateTags.tag', function ($q) use ($name) {
                            return $q->where('name', 'LIKE', '%' . $name . '%');
                        }
                    )
                    ->orWhereHas(
                        'templateCategory.category', function ($q) use ($name) {
                            return $q->where('name', 'LIKE', '%' . $name . '%');
                        }
                    );
            }

            // Color used Filter
            if (isset($colorUsed) && $colorUsed > 0) {
                $getTemplates->where('no_of_colors', $colorUsed);
            }
            // Filter by Category IDs
            if (isset($catagoryId) && $catagoryId != "") {
                $searchCategories = json_clean_decode($catagoryId, true);
                if (isset($searchCategories) && count($searchCategories) > 0) {
                    $getTemplates->whereHas(
                        'templateCategory', function ($q) use ($searchCategories) {
                            return $q->whereIn('category_id', $searchCategories);
                        }
                    );
                }
            }
            // Filter by Tag IDs
            if (isset($tagId) && $tagId != "") {
                $searchTags = json_clean_decode($tagId, true);
                if (isset($searchTags) && count($searchTags) > 0) {
                    $getTemplates->whereHas(
                        'templateTags', function ($q) use ($searchTags) {
                            return $q->whereIn('tag_id', $searchTags);
                        }
                    );
                }
            }
            // Filter by Print Profile
            if (isset($printProfileId) && $printProfileId != "") {
                $searchPrintProfiles = json_clean_decode($printProfileId, true);
                if (isset($searchPrintProfiles) && count($searchPrintProfiles) > 0) {
                    $getTemplates->whereHas(
                        'templatePrintProfiles', function ($q) use ($searchPrintProfiles) {
                            return $q->whereIn('print_profile_id', $searchPrintProfiles);
                        }
                    );
                }
            }
            // Get total records
            $getTotalPerFilters = $getTemplates->count();

            // Get pagination data
            if (isset($page) && $page != "") {
                $totalItem = empty($perpage) ? PAGINATION_MAX_ROW : $perpage;
                $offset = $totalItem * ($page - 1);
                $getTemplates->skip($offset)->take($totalItem);
            }
            // Sorting by column name and sord order parameter
            if (isset($sortBy) && $sortBy != "" && isset($order) && $order != "") {
                $getTemplates->orderBy($sortBy, $order);
            }
            $templateData = $templateList = $getTemplates->orderBy('xe_id', 'DESC')->get();

            foreach ($templateList as $templateKey => $template) {
                $templateId = $template->xe_id;
                // Replace xe_id with id for fetch api by cutting off the xe_id
                // from main array
                unset($templateData[$templateKey]['xe_id']);

                // - Condition for : If all requested, then send less data ; other
                // wise if single requested send more data
                // - Append Category Ids and tags
                if ($templateId != '' && $templateId > 0) {
                    $categoryIdList = $tagNameList = [];
                    $referenceId = $template->ref_id;

                    // Get Associated Tags and Categories
                    $getTagCategories = $this->getAssocCategoryTag($templateId);

                    if (!empty($getTagCategories['categories']) && count($getTagCategories['categories']) > 0) {
                        $templateData[$templateKey]['categories'] = $getTagCategories['categories'];
                    }
                    if (!empty($getTagCategories['tags']) && count($getTagCategories['tags']) > 0) {
                        $templateData[$templateKey]['tags'] = $getTagCategories['tags'];
                    }

                    // Get Associated Print profiles
                    $templateData[$templateKey]['print_profiles'] = $this->getTempPrintProfiles($templateId);

                    // Get Associated product ID
                    $getAssocProductId = $this->getAssocProductId($referenceId);
                    $templateData[$templateKey]['product_id'] = $getAssocProductId;

                    // Get template Images
                    $getAssocCaptures = $this->captureImages($referenceId);
                    $templateData[$templateKey]['capture_images'] = $getAssocCaptures;
                    // Get svg design data
                    $getAssociatedSvgData = $this->getSVGFile($referenceId);
                    $templateData[$templateKey]['design_data'] = $getAssociatedSvgData;
                }
            }

            $jsonResponse = [
                'status' => 1,
                'total_records' => $totalCounts,  //$getTotalPerFilters,
                'records' => count($templateData),
                'data' => $templateData,
            ];
        } else {
            // Set Blank Response for no data
            $jsonResponse = [
                'status' => 1,
                'total_records' => 0,
                'records' => 0,
                'data' => [],
                'message' => message('Template', 'not_found'),
            ];
        }
        return response(
            $response, ['data' => $jsonResponse, 'status' => $serverStatusCode]
        );
    }

    /**
     * Get SVG Files json data associated to the Template's Ref ID
     *
     * @param $refId Slim's Request object
     *
     * @author tanmayap@riaxe.com
     * @date   14 aug 2019
     * @return Array
     */
    private function getSVGFile($refId)
    {
        $svgDataArray = [];
        // Fetch SVG Files and Attach
        $svgJsonPath = path('abs', 'template') . 'REF_ID_' . $refId . '/json_REF_ID_' . $refId . '.json';
        $svgData = read_file($svgJsonPath);
        if ($svgData != "") {
            $svgDataArray = json_clean_decode($svgData, true);
        }
        return $svgDataArray;
    }

    /**
     * Get Associated Product Primary Key from the Template's RefId
     *
     * @param $refId Design Ref Id
     *
     * @author tanmayap@riaxe.com
     * @date   14 aug 2019
     * @return integer
     */
    private function getAssocProductId($refId)
    {
        // Get Product Id : Conditional Case applied for Inkxe 8
        $prodSettsProdId = 0;
        $getDesignStateDetails = TemplateModel\TemplateDesignStates::with('productSetting')->find($refId);
        if (isset($getDesignStateDetails) && count($getDesignStateDetails->toArray()) > 0) {
            $designState = $getDesignStateDetails->toArray();
            if (isset($designState['product_id']) && $designState['product_id'] > 0) {
                $prodSettsProdId = $designState['product_id'];
            } else if (isset($designState['product_setting']['product_id']) && $designState['product_setting']['product_id'] > 0) {
                $prodSettsProdId = (int) $designState['product_setting']['product_id'];
            }
        }

        return $prodSettsProdId;
    }
    /**
     * Get Associated print Profile Lists from the Template ID
     *
     * @param $templateId Slim's Request object
     *
     * @author tanmayap@riaxe.com
     * @date   14 aug 2019
     * @return json response
     */
    private function getTempPrintProfiles($templateId)
    {
        $getPrintProfiles = TemplateModel\TemplatePrintProfileRel::where('template_id', $templateId)->select('print_profile_id')->get();
        $getPrintProfileInit = new PrintProfile();
        $getMasterPrintprofiles = $getPrintProfileInit->get();
        $printProfileIdList = [];
        foreach ($getPrintProfiles as $key => $printProfile) {
            $printProfileIdList[] = $printProfile['print_profile_id'];
        }
        // Selected Print profile. Set as Blank array if no data exist
        $printProfileSelected = [];
        // Loop through all print profile and add is_selected key
        // where key matches
        foreach ($getMasterPrintprofiles as $printprofileMasterKey => $printprofileMaster) {
            if (in_array($printprofileMaster->xe_id, $printProfileIdList)) {
                $printProfileSelected[] = [
                    'id' => $printprofileMaster->xe_id,
                    'name' => $printprofileMaster->name,
                    'is_selected' => 1,
                ];
            }
        }
        return $printProfileSelected;
    }
    /**
     * Get Associated Categories and tags of Template by ID
     *
     * @param $templateId Slim's Request object
     *
     * @author tanmayap@riaxe.com
     * @date   14 aug 2019
     * @return Array
     */
    private function getAssocCategoryTag($templateId)
    {
        // Get Category Ids
        $getCategory = TemplateModel\TemplateCategoryRel::where('template_id', $templateId)->select('category_id')->get();
        $categoryIdList = [];
        foreach ($getCategory as $key => $category) {
            $categoryIdList[] = $category['category_id'];
        }
        // Get Tag names
        $getTags = TemplateModel\TemplateTagRel::where('template_id', $templateId)->select('tag_id')->with('tag')->get();
        $tagNameList = [];
        foreach ($getTags as $key => $tag) {
            $tagNameList[] = $tag['tag']['name'];
        }

        return [
            'categories' => $categoryIdList,
            'tags' => $tagNameList,
        ];
    }
    /**
     * Get Captured Images associated to the Template's Ref ID
     *
     * @param $refId Slim's Request object
     *
     * @author tanmayap@riaxe.com
     * @date   14 aug 2019
     * @return Array
     */
    private function captureImages($refId)
    {
        $getCaptures = [];
        $templateCaptureInit = new TemplateModel\TemplateCaptureImages();
        $getTempCaptures = $templateCaptureInit->where('ref_id', $refId);
        if ($getTempCaptures->count() > 0) {
            $getCaptureImages = $getTempCaptures->first();
            $captureFileName = $getCaptureImages->file_name;
            $captureSideIndex = $getCaptureImages->side_index;

        }
        if (isset($captureFileName) && $captureFileName != '') {

            $templateCaptureImageList = [];
            $absoluteDirectoryName = path('abs', 'capture') . $captureFileName;
            $relativeDirectoryName = path('read', 'capture') . $captureFileName . '/';
            $finalSortedFileList = [];
            if (file_exists($absoluteDirectoryName . '/history.json')) {
                $getImageFileHistory = read_file($absoluteDirectoryName . '/history.json');
                $finalSortedFileList = json_clean_decode($getImageFileHistory, true);
            }
            $getCaptures = $finalSortedFileList;
        }

        return $getCaptures;
    }
    /**
     * Delete: Delete a clipart along with all the tags and categories
     * - Input must be in a valid JSON format like [1,2,3,...] where 1,2,3.. are
     *   clipart IDs
     *
     * @param $request  Slim's Request object
     * @param $response Slim's Response object
     * @param $args     Slim's Argument parameters
     *
     * @author tanmayap@riaxe.com
     * @date   14 aug 2019
     * @return json response
     */
    public function deleteTemplate($request, $response, $args)
    {
        $serverStatusCode = OPERATION_OKAY;
        $jsonResponse = [
            'status' => 0,
            'message' => message('Template', 'error'),
        ];
        if (isset($args) && count($args) > 0 && $args['id'] != '') {
            $getTemplateIdFromJson = json_clean_decode($args['id'], true);

            $templateInit = new TemplateModel\Template();
            $getTemplates = $templateInit->whereIn('xe_id', $getTemplateIdFromJson);
            if ($getTemplates->count() > 0) {
                $templateAssocRefIds = $getTemplates->whereIn('xe_id', $getTemplateIdFromJson)->select('ref_id')->get();
                $refIdList = [];
                foreach ($templateAssocRefIds as $key => $refId) {
                    $refIdList[] = $refId->ref_id;
                }
            }
            if (!empty($refIdList) && count($refIdList) > 0) {
                // Delete Record
                $templateDesignStInit = new TemplateModel\TemplateDesignStates();
                if ($templateDesignStInit->whereIn('xe_id', $refIdList)->delete()) {
                    // Delete Template Data
                    $templateInit = new TemplateModel\Template();
                    $templateInit->whereIn('xe_id', $getTemplateIdFromJson)->delete();
                    // Delete Template_Tag_Rel Data
                    $templateTagInit = new TemplateModel\TemplateTagRel();
                    $templateTagInit->whereIn('template_id', $getTemplateIdFromJson)->delete();
                    // Delete Template_PP_Rel Data
                    $templateProfileInit = new TemplateModel\TemplatePrintProfileRel();
                    $templateProfileInit->whereIn('template_id', $getTemplateIdFromJson)->delete();
                    // Delete Capture_Img Data
                    $templateImgInit = new TemplateModel\TemplateCaptureImages();
                    $templateImgInit->whereIn('ref_id', $refIdList)->delete();
                    // Delete Design Data Files
                    foreach ($refIdList as $refIdForFileDelete) {
                        $deleteTemplateSvgDirectory = path('abs', 'template') . 'REF_ID_' . $refIdForFileDelete;
                        $deleteCaptureDirectory = path('abs', 'capture') . 'REF_ID_' . $refIdForFileDelete;
                        if (is_dir($deleteTemplateSvgDirectory)) {
                            permit_directory($deleteTemplateSvgDirectory);
                            // shell_exec('chmod -R 777 ' . $deleteTemplateSvgDirectory);
                            delete_directory($deleteTemplateSvgDirectory);
                            if (is_dir($deleteCaptureDirectory)) {
                                permit_directory($deleteCaptureDirectory);
                                // shell_exec('chmod -R 777 ' . $deleteCaptureDirectory);
                                delete_directory($deleteCaptureDirectory);
                            }
                        }
                    }
                    $jsonResponse = [
                        'status' => 1,
                        'message' => message('Template', 'deleted'),
                    ];
                }
            }
        }
        return response(
            $response, ['data' => $jsonResponse, 'status' => $serverStatusCode]
        );
    }

    /**
     * Save Template Tags into Tags table and Template-tag Relational table
     *
     * @param $request    Slim's Request object
     * @param $response   Slim's Response object
     * @param $parameters Slim's Argument parameters
     * @param $saveType   Flag for Save or Update
     *
     * @author tanmayap@riaxe.com
     * @date   5 Oct 2019
     * @return Boolean
     */
    private function saveTemplateTags($request, $response, $parameters, $saveType = 'save')
    {
        $getPostData = (isset($saveType) && $saveType == 'update') ? $this->parsePut() : $request->getParsedBody();
        $templateId = $parameters['template_id'];
        $getStoreDetails = $parameters['store_id'];

        // Clean-up old records before updating the table
        if (!empty($saveType) && $saveType == 'update') {
            TemplateModel\TemplateTagRel::where('template_id', $templateId)->delete();
        }

        $getTags = json_clean_decode($getPostData['tags'], true);
        // Save Clipart and tags relation
        $updatedTagIds = [];
        if (isset($getTags) && count($getTags) > 0) {
            foreach ($getTags as $key => $tag) {
                // Save each individual Tag to table
                if (TemplateModel\TemplateTag::where(['store_id' => $getStoreDetails, 'name' => trim($tag)])->count() == 0) {
                    $saveTag = new TemplateModel\TemplateTag(['store_id' => $getStoreDetails, 'name' => trim($tag)]);
                    $saveTag->save();
                    $tagLastInsertId = $saveTag->xe_id;
                } else {
                    $getTagDetails = TemplateModel\TemplateTag::where(['store_id' => $getStoreDetails, 'name' => trim($tag)])->select('xe_id')->first();
                    $tagLastInsertId = $getTagDetails['xe_id'];
                }
                if (isset($tagLastInsertId) && $tagLastInsertId > 0) {
                    $updatedTagIds[] = $tagLastInsertId;
                }
            }
            // Start SYNC Tags into Clipart_Tag Relationship Table
            $templateInit = new TemplateModel\Template();
            $findTemplate = $templateInit->find($templateId);
            if ($findTemplate->tags()->sync($updatedTagIds)) {
                return true;
            } else {
                return false;
            }
        } else {
            // If user requests blank/no tags
            $templateTagRelInit = new TemplateModel\TemplateTagRel();
            $templateTags = $templateTagRelInit->where('template_id', $templateId);
            if ($templateTags->delete()) {
                return true;
            } else {
                return false;
            }
        }
    }

    /**
     * Save Template Categories into Categories table and Template-category
     * Relational table
     *
     * @param $request    Slim's Request object
     * @param $response   Slim's Response object
     * @param $parameters Slim's Argument parameters
     * @param $saveType   Flag for Save or Update
     *
     * @author tanmayap@riaxe.com
     * @date   5 Oct 2019
     * @return Boolean
     */
    private function saveTemplateCategories($request, $response, $parameters, $saveType = 'save')
    {
        //$getPostData = $request->getParsedBody();
        $getPostData = (isset($saveType) && $saveType == 'update') ? $this->parsePut() : $request->getParsedBody();
        $templateId = $parameters['template_id'];
        $getStoreDetails = $parameters['store_id'];
        $categories = $getPostData['categories'];

        $categoryListArray = json_clean_decode($categories, true);
        // Clean-up old records before updating the table
        if (!empty($saveType) && $saveType == 'update') {
            TemplateModel\TemplateCategoryRel::where('template_id', $templateId)->delete();
        }
        // SYNC Categories to the Clipart_Category Relationship Table
        $templateInit = new TemplateModel\Template();
        $findTemplate = $templateInit->find($templateId);
        if ($findTemplate->categories()->sync($categoryListArray)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Save method for Template data
     *
     * @param $request  Slim's Request object
     * @param $response Slim's Response object
     * @param $reffId   REF ID from Design State Table
     * @param $saveType Flag for Save or Update
     *
     * @author tanmayap@riaxe.com
     * @date   5 Oct 2019
     * @return Boolean
     */
    private function saveTemplates($request, $response, $reffId, $saveType = 'save')
    {
        $getStoreDetails = get_store_details($request);
        $getPostData = (isset($saveType) && $saveType == 'update') ? $this->parsePut() : $request->getParsedBody();

        $templateData = [
            'ref_id' => $reffId,
            'store_id' => $getStoreDetails['store_id'],
            'name' => $getPostData['name'],
            'description' => isset($getPostData['description']) ? $getPostData['description'] : null,
            'no_of_colors' => $getPostData['no_of_colors'],
            'color_hash_codes' => $getPostData['color_codes'],
        ];

        if ($saveType == 'update') {
            unset($templateData['ref_id']);
            // Create a new object instance for fetch
            $templateInit = new TemplateModel\Template();
            $updateTemplateInit = $templateInit->where('ref_id', $reffId);
            try {
                $updateTemplateInit->update($templateData);
                // Send template id on success updation so that at update method
                // we can check if update done or not
                return $updateTemplateInit->first()->xe_id;
            } catch (\Exception $e) {
                return false;
            }
        } else {
            // Create a new object instance for save
            $saveTemplateInit = new TemplateModel\Template($templateData);
            $saveTemplateInit->save();
            if ($saveTemplateInit->xe_id) {
                return $saveTemplateInit->xe_id;
            }
            return false;
        }
    }

    /**
     * Save method for Print profile Relations
     *
     * @param $request    Slim's Request object
     * @param $response   Slim's Response object
     * @param $reffId     REF ID from Design State Table
     * @param $templateId Insert ID of Template Record
     * @param $saveType   Flag for Save or Update
     *
     * @author tanmayap@riaxe.com
     * @date   5 Oct 2019
     * @return Boolean
     */
    private function savePrintProfileRelations($request, $response, $reffId, $templateId, $saveType = 'save')
    {
        $getPostData = (isset($saveType) && $saveType == 'update') ? $this->parsePut() : $request->getParsedBody();
        $printProfileRels = [];

        $getPrintProfiles = json_clean_decode($getPostData['print_profiles'], true);

        if (!empty($getPrintProfiles) && count($getPrintProfiles) > 0) {
            foreach ($getPrintProfiles as $printKey => $printProfile) {
                $printProfileRels[$printKey] = [
                    'print_profile_id' => $printProfile,
                    'template_id' => $templateId,
                ];
            }
        }
        // Clean-up old records before updating the table
        if (!empty($saveType) && $saveType == 'update') {
            TemplateModel\TemplatePrintProfileRel::where('template_id', $templateId)->delete();
        }
        $tempPrintProfRelInit = new TemplateModel\TemplatePrintProfileRel();
        $savePrintProfileData = $tempPrintProfRelInit->insert($printProfileRels);
        if ($savePrintProfileData) {
            return true;
        }
        return false;
    }
    /**
     * GET: Get list of all tags of Template Module
     *
     * @param $request  Slim's Request object
     * @param $response Slim's Response object
     *
     * @author tanmayap@riaxe.com
     * @date   5 Oct 2019
     * @return All Print Profile List
     */
    public function getTemplateTags($request, $response)
    {
        $serverStatusCode = OPERATION_OKAY;
        $getStoreDetails = get_store_details($request);
        $offset = 0;
        $templateTagInit = new TemplateModel\TemplateTag();
        $getTemplates = $templateTagInit->where('xe_id', '>', 0);
        if (isset($getStoreDetails['store_id']) && $getStoreDetails['store_id'] > 0) {
            $getTemplates->where('store_id', $getStoreDetails['store_id']);
        }
        if ($getTemplates->count() > 0) {
            $tagsList = $getTemplates->select('xe_id as id', 'name')->get();
            $jsonResponse = [
                'status' => 1,
                'data' => $tagsList,
            ];
        } else {
            $jsonResponse = [
                'status' => 1,
                'data' => [],
                'message' => message('Template Tag', 'not_found'),
            ];
        }

        return response(
            $response, ['data' => $jsonResponse, 'status' => $serverStatusCode]
        );
    }

    /**
     * Check Category Relation with Category
     *
     * @param $request  Slim's Argument parameters
     * @param $response Slim's Response object
     * @param $args     Slim's Argument parameters
     *
     * @author satyabratap@riaxe.com
     * @date   20 Jan 2020
     * @return boolean
     */
    public function checkRelCategory($request, $response, $args)
    {
        $serverStatusCode = OPERATION_OKAY;
        $jsonResponse = [
            'status' => 0,
            'message' => 'This Category is associated with an Item. Do you really want to perform the operation'
        ];
        $key = $request->getQueryParam('key');
        if (!empty($args) && $args['id'] > 0) {
            $categoryId = $args['id'];
            $checkCatRelInit = new TemplateCategoryRel();
            $getCatRel = $checkCatRelInit->where('category_id', $categoryId)->get();
            if ($getCatRel->count() == 0) {
                if ($key == 'disable') {
                    $jsonResponse = $this->disableCat('templates', $categoryId);
                } elseif ($key == 'delete') {
                    $jsonResponse = $this->deleteCat('templates', $categoryId, 'Templates', 'TemplateCategoryRel');
                }
            }
        }
        return response(
            $response, ['data' => $jsonResponse, 'status' => $serverStatusCode]
        );
    }

    /**
     * Delete a category from the table
     *
     * @param $request  Slim's Request object
     * @param $response Slim's Response object
     * @param $args     Slim's Argument parameters
     *
     * @author satyabratap@riaxe.com
     * @date   20 Jan 2020
     * @return Delete Json Status
     */
    public function deleteCategory($request, $response, $args)
    {
        $serverStatusCode = OPERATION_OKAY;
        $jsonResponse = [
            'status' => 0,
            'message' => message('Category', 'error')
        ];
        if (!empty($args) && $args['id'] > 0) {
            $categoryId = $args['id'];
            $jsonResponse = $this->deleteCat('templates', $categoryId, 'Templates', 'TemplateCategoryRel');
        }
        return response(
            $response, ['data' => $jsonResponse, 'status' => $serverStatusCode]
        );
    }
}
