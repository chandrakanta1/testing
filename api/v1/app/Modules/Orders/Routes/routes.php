<?php
/**
 * This Routes holds all the individual route for the Clipart
 *
 * PHP version 5.6
 *
 * @category  Orders
 * @package   Orders
 * @author    Tanmaya Patra <satyabratap@riaxe.com>
 * @copyright 2019-2020 Riaxe Systems
 * @license   http://www.php.net/license/3_0.txt  PHP License 3.0
 * @link      http://inkxe-v10.inkxe.io/xetool/admin
 */

use App\Modules\Orders\Controllers\OrderDownloadController as OrderDownload;
use App\Modules\Orders\Controllers\OrdersController;

$container = $app->getContainer();

//Routs for Order Log
$app->post('/order-logs', OrdersController::class . ':saveOrderLogs');
$app->get('/order-logs/{id}', OrdersController::class . ':getOrderLogs');

$app->get('/orders/{id}', OrdersController::class . ':getOrderList');
$app->get('/orders', OrdersController::class . ':getOrderList');

//For download orders
$app->get('/order-download', OrderDownload::class . ':downloadOrder');
