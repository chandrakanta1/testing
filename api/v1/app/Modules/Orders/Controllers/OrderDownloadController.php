<?php
/**
 * Download order details on various endpoints
 *
 * PHP version 5.6
 *
 * @category  Download_Order
 * @package   OrderDownload
 * @author    Radhanatha Mohapatra <radhanatham@riaxe.com>
 * @copyright 2019-2020 Riaxe Systems
 * @license   http://www.php.net/license/3_0.txt  PHP License 3.0
 * @link      http://inkxe-v10.inkxe.io/xetool/admin
 */
namespace App\Modules\Orders\Controllers;

use App\Components\Controllers\Component as ParentController;
use App\Dependencies\HtmlDomParser as HtmlDomParser;
use App\Dependencies\Zipper as Zipper;

/**
 * Order Download Controller
 *
 * @category Class
 * @package  OrderDownload
 * @author   Radhanatha Mohapatra <radhanatham@riaxe.com>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://inkxe-v10.inkxe.io/xetool/admin
 */
class OrderDownloadController extends ParentController
{
    /**
     * Integer 72 dpi is default dpi for illustrator
     */
    public $dpi = 72;

    /**
     * String The path to the current order zip file
     */
    public $orderPath;

    /**
     * String The path to the current order zip file
     */
    public $sidePath;

    /**
     * String The path to the current svg save file
     */
    public $svgSavePath;

    /**
     * Array empty print color array
     */
    public $printColorsArr = array();

    /**
     * Boolean check product with design parameter
     */
    public $isProductWithDesign = false;

    /**
     * String svg image tag
     */
    public $imageTag;

    /**
     * Array check enabled file format by print method
     * Remove latter
     */
    public $fileFormatArr = array("SVG", "PDF", "PNG");

    /**
     * String print unit for current order
     */
    public $printUnit;

    /**
     * Html dom object
     */
    public $domObj;

    /**
     * Define order path
     **/
    public function __construct()
    {
        $this->orderPath = path('abs', 'order');
        $this->domObj = new HtmlDomParser();
    }

    /**
     * GET: Download order file by order id
     *
     * @param $request  Slim's Request object
     * @param $response Slim's Response object
     * @param $args     Slim's Argument parameters
     *
     * @author radhanatham@riaxe.com
     * @date   03 Jan 2020
     * @return boolean
     */
    public function downloadOrder($request, $response, $args)
    {
        $itemId = $request->getQueryParam('item_id') ?
        $request->getQueryParam('item_id') : 0;
        $orderId = $request->getQueryParam('order_id') ?
        $request->getQueryParam('order_id') : 0;
        $isDownload = $request->getQueryParam('is_download') ? 
        $request->getQueryParam('is_download') : false;
        $status = false;
        $msg = 'error';
        $serverStatusCode = OPERATION_OKAY;
        if ((isset($orderId) && $orderId) && ($itemId == 0)) {
            $orderIncIdList = $orderId;
            $orderIncIdArray = explode(',', $orderIncIdList);
            if (count($orderIncIdArray) > 0) {
                $status = $this->downloadOrderByOrderId($orderIncIdArray);
                if ($status) {
                    $returnStatus = $this->createOrderZipFileByOrderId(
                        $orderIncIdArray
                    );
                    if ($returnStatus) {
                        $status = true;
                    }
                }
            }
        } elseif ((isset($orderId) && $orderId) && (isset($itemId) && $itemId)) {
            $status = $this->downloadOrderByItemId($orderId, $itemId);
            if ($status) {
                $returnStatus = $this->createOrderZipFileByItemId($orderId, $itemId);
                if ($returnStatus) {
                    $status = true;
                }
            }
        } elseif ($orderId == 0 && $itemId == 0 && $isDownload) {
            $status = $this->zipDownload($isDownload);
        }
        
        $msg = $status ? 'done' : 'error';
        $jsonResponse = [
            'status' => $status,
            'message' => message('order download', $msg),
        ];
        return response(
            $response, ['data' => $jsonResponse, 'status' => $serverStatusCode]
        );
    }

    /**
     * GET: Download order file
     *
     * @param $orderId The is current request order Id 
     * @param $itemId  The is current request item Id 
     *
     * @author radhanatham@riaxe.com
     * @date   03 Jan 2020
     * @return boolean
     */
    private function downloadOrderByItemId($orderId, $itemId)
    {
        $status = false;
        $orderIdPath = $this->orderPath . $orderId;
        if (file_exists($orderIdPath) && is_dir($orderIdPath . "/" . $itemId)) {
            $designStateAbsDir = $orderIdPath . "/" . $itemId . "/designState.json";
            if (file_exists($designStateAbsDir)) {
                $designStateStr = read_file($designStateAbsDir);
                $designData = json_clean_decode($designStateStr, true);
                $sidePath = $orderIdPath . "/" . $itemId;
                $scanSideDir = scandir($sidePath);
                $sidePath = $orderIdPath . "/" . $itemId;
                if (file_exists($sidePath) && is_dir($sidePath)) {
                    $scanSideDir = scandir($sidePath);
                    if (is_array($scanSideDir)) {
                        foreach ($scanSideDir as $dir) {
                            $absPath = $orderIdPath . "/" . $itemId . "/" . $dir;
                            if ($dir != '.' && $dir != '..' && is_dir($absPath)) {
                                $i = str_replace("side_", "", $dir);
                                $sideDir = $orderIdPath . "/" . $itemId . "/" . $dir;
                                $svgPath = $sideDir . "/preview_0" . $i . ".svg";
                                $file =  $dir . "_" . $itemId . "_" . $orderId;
                                $svgPathChk = $sideDir . "/" .$file. ".svg";
                                if (file_exists($svgPath)   
                                    && !file_exists($svgPathChk)
                                ) {
                                    $status = $this->generateSvgFile(
                                        $svgPath, $orderId, 
                                        $itemId, $designData
                                    );
                                } else {
                                    $status = true;
                                } 
                            }
                        }
                    }
                }
            }
        } 
        return $status;
    }

    /**
     * GET: Create order zip file 
     *
     * @param $orderNo     The is current request order Id 
     * @param $orderItemId The is current request item Id 
     *
     * @author radhanatham@riaxe.com
     * @date   03 Jan 2020
     * @return boolean
     */
    private function createOrderZipFileByItemId($orderNo, $orderItemId)
    {
        $fileExtention = '';
        if (in_array('SVG', $this->fileFormatArr)) {
            $fileExtention = 'svg';
        }
        if (in_array('PDF', $this->fileFormatArr)) {
            $fileExtention .= ',pdf';
        }
        if (in_array('PNG', $this->fileFormatArr)) {
            $fileExtention .= ',png';
        }
        $fileExtention = ltrim($fileExtention, ',');
        if ($fileExtention == '') {
            $fileExtention = 'svg';
        }
        $status = false;
        $zipName = 'orders.zip';
        if (file_exists($this->orderPath . '/' . $zipName)) {
            unlink($this->orderPath . '/' . $zipName);
        }
        $assetFileExt = 'svg,pdf,png,jpeg,jpg,gif,bmp,ai,psd,eps,cdr,dxf,tif';
        $assetFileExt .= ','.strtoupper($assetFileExt);
        $zip = new Zipper();
        $zipStatus = $zip->make($this->orderPath . '/' . $zipName);
        if ($zipStatus) {
            $orderFolderPath = $this->orderPath . $orderNo;
            $orderJsonPath = $orderFolderPath . '/order.json';
            if (file_exists($orderFolderPath)) {
                if (file_exists($orderJsonPath)) {
                    $orderJson = read_file($orderJsonPath);
                    $jsonContent = json_clean_decode($orderJson, true);
                    $itemList = $jsonContent['order_details']['order_items'];
                    $noOfRefIds = count($itemList);
                    if ($noOfRefIds > 0) {
                        $zip->addEmptyDir($orderNo);
                        //$zip->add($orderJsonPath, $orderNo . '/order.json');
                        foreach ($itemList as $itemDetails) {
                            $itemId = (int)$itemDetails['item_id'];
                            $refId = (int)$itemDetails['ref_id'];
                            $itemPath = $orderFolderPath . "/" . $itemId;
                            $ordeItemSide = $orderNo . "/" . $itemId. "/side_";
                            $designPath = $itemPath . "/designState.json";
                            if (($itemId > 0  && $refId > 0) 
                                && $itemDetails['item_id'] == $orderItemId
                            ) {
                                //Fetch the design state json details //
                                $designState = read_file($designPath);
                                $designData = json_clean_decode($designState, true);
                                $sidesCount = count($designData['sides']);
                                for ($flag = 1; $flag <= $sidesCount; $flag++) {
                                    if (is_dir($itemPath . "/side_" . $flag)) {
                                        $zip->addEmptyDir($ordeItemSide.$flag);
                                    }
                                    $sidePath = $ordeItemSide . $flag;

                                    //Add name and number csv file in zip
                                    $nameNumberPath = $itemPath . "/nameNumber.csv";
                                    $addItemPath = $orderNo. "/" . $itemId;
                                    if (file_exists($nameNumberPath)) {
                                        $optionsPath = array(
                                            'add_path' => $addItemPath . "/",
                                            'remove_path' => $itemPath
                                        );
                                        $zip->addGlob(
                                            $itemPath . '/*{csv}',
                                            $optionsPath
                                        );
                                    }

                                    //Add side folder to zip file //
                                    $fromUrlSide = $itemPath . "/side_" . $flag;
                                    $optionsSide = array(
                                        'add_path' => $sidePath . "/", 
                                        'remove_path' => $fromUrlSide
                                    );
                                    $zip->addGlob(
                                        $fromUrlSide . '/*{' . $fileExtention . '}', 
                                        $optionsSide
                                    );

                                    //Add asset folder to zip file//
                                    if (is_dir($fromUrlSide . "/assets")) {
                                        $zip->addEmptyDir(
                                            $sidePath . "/original_image"
                                        );
                                        $urlAsset = $fromUrlSide . "/assets";
                                        $addPath = $sidePath . "/original_image/";
                                        $optionsAsset = array(
                                            'add_path' => $addPath,
                                            'remove_path' => $urlAsset
                                        );
                                        $zip->addGlob(
                                            $urlAsset . '/*{' .$assetFileExt . '}',
                                            $optionsAsset
                                        );
                                    }

                                    //Add preview folder to zip file//
                                    if (is_dir($fromUrlSide . "/preview")) {
                                        $zip->addEmptyDir($sidePath . "/preview");
                                        $fromUrlPreview = $fromUrlSide . "/preview";
                                        $optionsPreview = array(
                                            'add_path' => $sidePath . "/preview/", 
                                            'remove_path' => $fromUrlPreview
                                        );
                                        $zip->addGlob(
                                            $fromUrlPreview . '/*{png,PNG}',
                                            $optionsPreview
                                        );
                                    }

                                    //remove svg preview file from zip file
                                    $zip->removeFile(
                                        $sidePath . "/preview_0" . $flag . ".svg"
                                    );
                                }
                            }
                        }
                    }
                }
                $status = true;
            }
            $zip->close();
        }
        return $status;
    }

    /**
     * GET: Download order file by order id
     *
     * @param $ordrIdArr The is current request order Id list
     *
     * @author radhanatham@riaxe.com
     * @date   03 Jan 2020
     * @return boolean
     */
    private function downloadOrderByOrderId($ordrIdArr = array())
    {
        $status = false;
        $prvw = 'preview_0';
        if (!empty($ordrIdArr)) {
            foreach ($ordrIdArr as $orderId) {
                $orderAssetPath = $this->orderPath . $orderId;
                if (($orderId != "" && $orderId != 0)  
                    && (file_exists($orderAssetPath) && is_dir($orderAssetPath))
                ) {
                    $scanProductDir = scandir($orderAssetPath);
                    if (is_array($scanProductDir)) {
                        foreach ($scanProductDir as $itemId) {
                            if ($itemId != '.' && $itemId != '..'  
                                && is_dir($orderAssetPath . "/" . $itemId)
                            ) {
                                $sidePath = $orderAssetPath . "/" . $itemId;
                                $designAbsDir = $sidePath . "/designState.json";
                                if (file_exists($designAbsDir)) {
                                    $designStr = read_file($designAbsDir);
                                    $designArr = json_clean_decode(
                                        $designStr, true
                                    );
                                    $scanSideDir = scandir($sidePath);
                                    if (file_exists($sidePath) 
                                        && is_dir($sidePath)
                                        && is_array($scanSideDir)
                                    ) {
                                        foreach ($scanSideDir as $side) {
                                            if ($side != '.'  
                                                && $side != '..' 
                                                && is_dir(
                                                    $sidePath . "/" . $side
                                                )
                                            ) {
                                                $i = str_replace(
                                                    "side_", "", $side
                                                );
                                                //Order side path
                                                $sDir = $sidePath . "/" . $side;
                                                //Order item path
                                                $iPath = $sDir . "/" . $side . "_" ;
                                                //Order SVG directory
                                                $svgDir = $iPath.$itemId . "_";
                                                //Order preview path
                                                $prvDir = $sDir . "/". $prvw; 
                                                //Order preview SVG file path
                                                $svgFile = $prvDir . $i . ".svg";
                                                if (file_exists($svgFile)  
                                                    && !file_exists(
                                                        $svgDir. $orderId. ".svg"
                                                    )
                                                ) {
                                                    $status = $this->generateSvgFile(
                                                        $svgFile, 
                                                        $orderId, 
                                                        $itemId,
                                                        $designArr
                                                    );
                                                } else {
                                                    $status = true;
                                                }
                                            }
                                        }
                                    }
                                } 
                            }
                        }
                    }
                }
            }
        } 
        return $status;
    }

    /**
     * GET: Create order zip file by order id
     *
     * @param $orderIdArr The is current request order id list
     *
     * @author radhanatham@riaxe.com
     * @date   03 Jan 2020
     * @return boolean
     */
    private function createOrderZipFileByOrderId($orderIdArr = array())
    {
        $fileExtention = '';
        if (in_array('SVG', $this->fileFormatArr)) {
            $fileExtention = 'svg';
        }
        if (in_array('PDF', $this->fileFormatArr)) {
            $fileExtention .= ',pdf';
        }
        if (in_array('PNG', $this->fileFormatArr)) {
            $fileExtention .= ',png';
        }
        $fileExtention = ltrim($fileExtention, ',');
        if ($fileExtention == '') {
            $fileExtention = 'svg';
        }
        $assetFileExt = 'svg,pdf,png,jpeg,jpg,gif,bmp,ai,psd,eps,cdr,dxf,tif';
        $assetFileExt .= ','.strtoupper($assetFileExt);
        $status = false;
        $zipName = 'orders.zip';
        if (file_exists($this->orderPath . '/' . $zipName)) {
            unlink($this->orderPath . '/' . $zipName);
        }
        $zip = new Zipper();
        $zipStatus = $zip->make($this->orderPath . '/' . $zipName);
        if ($zipStatus) {
            foreach ($orderIdArr as $orderNo) {
                $orderFolderDir = $this->orderPath . $orderNo;
                $orderJsonPath = $orderFolderDir . '/order.json';
                if (file_exists($orderFolderDir) && file_exists($orderJsonPath)) {
                    $orderJson = read_file($orderJsonPath);
                    $jsonContent = json_clean_decode($orderJson, true);
                    $itemList = $jsonContent['order_details']['order_items'];
                    if (count($itemList) > 0) {
                        $zip->addEmptyDir($orderNo);
                        //$zip->add($orderJsonPath, $orderNo . '/order.json');
                        foreach ($itemList as $itemDetails) {
                            $itemId = $itemDetails['item_id'];
                            $refId = $itemDetails['ref_id'];
                            if ($itemId != null && $itemId > 0 
                                && $refId != null && $refId > 0
                            ) {
                                $orderItemDir = $orderFolderDir . "/" . $itemId;
                                //Fetch the design state json details //
                                $designStr = read_file(
                                    $orderItemDir . "/designState.json"
                                );

                                //Add name and number csv file in zip
                                $nameNumberpath = $orderItemDir . "/nameNumber.csv";
                                if (file_exists($nameNumberpath)) {
                                    $optionsSides = array(
                                        'add_path' => $orderNo. "/" . $itemId . "/",
                                        'remove_path' => $orderItemDir
                                    );
                                    $zip->addGlob(
                                        $orderItemDir . '/*{csv}',
                                        $optionsSides
                                    );
                                }
                                $resultDesign = json_clean_decode($designStr, true);
                                $sidesCount = count($resultDesign['sides']);
                                $zipOrderIdDir = $orderNo . "/" . $itemId . "/side_";
                                for ($flag = 1; $flag <= $sidesCount; $flag++) {
                                    if (is_dir($orderItemDir . "/side_" . $flag)) {
                                        $zip->addEmptyDir($zipOrderIdDir . $flag);
                                    }
                                    $sidePath = $zipOrderIdDir . $flag;
                                    //Add side folder to zip file //
                                    $fromUrlSide = $orderItemDir . "/side_" . $flag;
                                    $optionsSide = array(
                                        'add_path' => $sidePath . "/",
                                        'remove_path' => $fromUrlSide
                                    );
                                    $zip->addGlob(
                                        $fromUrlSide . '/*{' . $fileExtention . '}',
                                        $optionsSide
                                    );

                                    //Add asset folder to zip file//
                                    if (is_dir($fromUrlSide . "/assets")) {
                                        $zip->addEmptyDir(
                                            $sidePath . "/original_image"
                                        );
                                        $urlAsset = $fromUrlSide . "/assets";
                                        $addPath = $sidePath . "/original_image/";
                                        $optionsAsset = array(
                                            'add_path' => $addPath,
                                            'remove_path' => $urlAsset
                                        );
                                        $zip->addGlob(
                                            $urlAsset . '/*{' . $assetFileExt . '}',
                                            $optionsAsset
                                        );
                                    }

                                    //Add preview folder to zip file//
                                    if (is_dir($fromUrlSide . "/preview")) {
                                        $zip->addEmptyDir(
                                            $sidePath . "/preview"
                                        );
                                        $fromUrlPreview = $fromUrlSide . "/preview";
                                        $optionsPreview = array(
                                            'add_path' => $sidePath . "/preview/", 
                                            'remove_path' => $fromUrlPreview
                                        );
                                        $zip->addGlob(
                                            $fromUrlPreview . '/*{png,PNG}', 
                                            $optionsPreview
                                        );
                                    }

                                    //remove svg preview file from zip file
                                    $zip->removeFile(
                                        $sidePath . "/preview_0" . $flag . ".svg"
                                    );
                                }
                            }
                        }
                    }
                    $status = true;
                }
            }
            $zip->close();
        }
        return $status;
    }

    /**
     * GET: Create svg file according to print area dimension
     *
     * @param $reqSvgFile   The is current request SVG string
     * @param $orderId      The is current request order ID
     * @param $itemId       The is current request Item ID
     * @param $resultDesign The is current request design data
     *
     * @author radhanatham@riaxe.com
     * @date   03 Jan 2020
     * @return boolean
     */
    private function generateSvgFile($reqSvgFile, $orderId, $itemId, $resultDesign)
    {
        $svgStatus = false;
        $status = false;
        $this->imageTag = '';
        $this->isProductWithDesign = false;
        $this->printColorsArr = array();
        if (isset($resultDesign['is_product_with_design'])
            && $resultDesign['is_product_with_design']
        ) {
            $this->isProductWithDesign = true;
        }
        $this->printUnit = $resultDesign['sides'][0]['printUnit'] 
        ? $resultDesign['sides'][0]['printUnit'] : 'inch';

        if ($reqSvgFile != '') {
            $svgFileStr = read_file($reqSvgFile);
            $oldReplaceStr = array(
                'data: png', 
                'data: jpg', 
                'data: jpeg', 
                'data:image/jpg'
                );
            $newReplaceStr = array(
                'data:image/png',
                'data:image/jpeg',
                'data:image/jpeg',
                'data:image/jpeg'
                );
            $svgFileStr = str_replace($oldReplaceStr, $newReplaceStr, $svgFileStr);
            $svgFileExtention = basename($reqSvgFile);
            $sideNo = str_replace(
                "preview_0", "", 
                str_replace(".svg", "", $svgFileExtention)
            );
            $this->sidePath = "side_" . $sideNo . "_" . $itemId . "_" . $orderId;
            $svgFileName = $this->sidePath . ".svg";
            $pngFileName = $this->sidePath . ".png";
            $rgbPdfFileName = $this->sidePath . "_rgb.pdf";
            $cmykPdfFileName = $this->sidePath . ".pdf";
            $multiPrintFileName = "multi_" . $this->sidePath. ".svg";
            $itemPath = $orderId . '/' . $itemId . '/side_' . $sideNo;
            $this->svgSavePath = $this->orderPath . $itemPath . '/';
            $svgAbsPath = $this->svgSavePath . $svgFileName;
            $pngAbsPath = $this->svgSavePath . $pngFileName;
            $rgbPdfAbsPath = $this->svgSavePath . $rgbPdfFileName;
            $cmykPdfAbsPath = $this->svgSavePath . $cmykPdfFileName;
            $sideIdIndex = $sideNo - 1;

            $colorArr = $resultDesign['sides'][$sideIdIndex]['printColors'];
            if (isset($resultDesign['is_color_separate_print_file']) 
                && $resultDesign['is_color_separate_print_file']
                && !empty($colorArr)
            ) {
                //Check used color for every individual product side
                $this->printColorsArr = $colorArr; 
            }
            $htmlStr = $this->domObj->strGetHtml($svgFileStr, false);
            $svg = $htmlStr->find('image#svg_1', 0);
            //Reactangle print area
            $rectPath = $htmlStr->find('rect#boundRectangle', 0);
            //remove extra rectangle
            $canvasBackground = $htmlStr->find('rect#canvas_background', 0); 
            $canvasBackground->outertext = '';
            $countLayer = substr_count($htmlStr, 'layer_area');
            $multiPrintStatus = false;
            if ($svg) {
                if (!file_exists($this->svgSavePath)) {
                    mkdir($this->svgSavePath, 0777, true);
                    chmod($this->svgSavePath, 0777);
                }
                if ($countLayer > 1) {
                    $htmlStr->save();
                    //For single svg file for multiple boundary
                    $svgStatus = $this->generateSingleSvgFile(
                        $htmlStr, $svgAbsPath
                    );
                    //For multiple svg files for multiple boundary
                    $svgStatus = $this->generateMultipleSvgFile(
                        $htmlStr, $multiPrintFileName
                    ); 
                    $multiPrintStatus = true;
                   
                } else {
                    if ($rectPath) {
                        $htmlStr->save();
                        //For reactangle print svg files
                        $svgStatus = $this->generateReactAnglePrintSvgFile(
                            $htmlStr, $svgAbsPath
                        );
                    } else {
                        $htmlStr->save();
                        //For shape print svg files
                        $svgStatus = $this->generateShapePrintSvgFile(
                            $htmlStr, $svgAbsPath
                        );
                    }
                }
                if ($svgStatus) {
                    if (in_array('PNG', $this->fileFormatArr)) {
                        $this->svgConvertToPng($pngAbsPath, $svgAbsPath);
                    }
                    if (in_array('PDF', $this->fileFormatArr)) {
                        $this->svgConvertToRGBPdf(
                            $rgbPdfAbsPath, $svgAbsPath
                        );
                        $this->rgbPdfConvertToCMYKPdf(
                            $cmykPdfAbsPath, $rgbPdfAbsPath
                        );
                    }
                }
                $status = $svgStatus;
            }
        }
        return $status;
    }

    /**
     * GET: Create svg file for reactngle print area dimension
     *
     * @param $htmlStr    The is current request SVG string
     * @param $svgAbsPath The is current request SVG path
     *
     * @author radhanatham@riaxe.com
     * @date   03 Jan 2020
     * @return boolean
     */
    private function generateReactAnglePrintSvgFile(
        $htmlStr, $svgAbsPath
    ) {
        $svgFileStatus = false;
        $htmlStr = $this->domObj->strGetHtml($htmlStr, false);
        $svg = $htmlStr->find('image#svg_1', 0);
        $rectPath = $htmlStr->find('rect#boundRectangle', 0); //Reactangle print area
        $reactX = $rectPath->x;
        $reactY = $rectPath->y;
        $width = $rectPath->width;
        $height = $rectPath->height;
        $aHeight = $rectPath->aHeight ? $rectPath->aHeight : $rectPath->aheight;
        $aWidth = $rectPath->aWidth ? $rectPath->aWidth : $rectPath->awidth;
        $svgroot = $htmlStr->find('svg#svgroot', 0);
        if ($this->isProductWithDesign) {
            $this->imageTag = $svgroot->first_child(); // Get product image tage
        }

        if ($aHeight == 'null' || $aHeight == 0) {
            $svgroot->width = 500;
            $svgroot->height = 500;
            $transformStr  =  '<g id="rect" transform="scale(1) translate(0,0)">';
            $svg->outertext = $transformStr . $this->imageTag;
        } else {
            $aWidth = $this->unitConvertionToInch($aWidth);
            $aHeight = $this->unitConvertionToInch($aHeight);
            $svgroot->width = $aWidth * $this->dpi;
            $svgroot->height = $aHeight * $this->dpi;
            $acHeight = $aHeight * $this->dpi;
            $acHeight = $acHeight / $height;
            if ($reactX < 0) {
                $reactX = -($reactX);
            }
            if ($reactY < 0) {
                $reactY = -($reactY);
            }
            $acWidth = $aWidth * $this->dpi;
            $acWidth = $acWidth / $width;
            $transformS = '<g id="rect" transform="scale(';
            $translate = ' translate( -' . $reactX . ', -' . $reactY . ')">';
            if ($acHeight != $acWidth) {
                $scaleValue =  $transformS.$acWidth . ',' . $acHeight.')' ;
            } else {
                $scaleValue =   $transformS.$acHeight.')' ;
            }
            $svg->outertext = $scaleValue.$translate . $this->imageTag;
        }
        $htmlStr->save();
        $htmlStr = str_replace('</svg>', '', $htmlStr);
        $htmlStr = $htmlStr . '</g></svg>';
        if (count($this->printColorsArr) > 1) {
            $svgFileStatus = $this->generateSvgFileByColor($htmlStr);
        } else {
            $svgFileStatus = write_file($svgAbsPath, $htmlStr);
        }
        return $svgFileStatus;
    }

    /**
     * GET: To create separate svg file for every print area
     *
     * @param $reqStr             The is current request SVG string
     * @param $multiPrintFileName The is current request SVG file name
     *
     * @author radhanatham@riaxe.com
     * @date   03 Jan 2020
     * @return boolean
     */
    private function generateMultipleSvgFile($reqStr,
        $multiPrintFileName
    ) {
        $fileStr = chop($multiPrintFileName, '.svg');
        $svgStartTag = '<svg xmlns="http://www.w3.org/2000/svg"';
        $svgXlink =' id="svgroot" xmlns:xlink="http://www.w3.org/1999/xlink"';
        $svgEndTag = '</g></svg>';
        $svgTagStr = $svgStartTag.$svgXlink;
        $html = $this->domObj->strGetHtml($reqStr, false);
        $svg = $html->find('image#svg_1', 0);
        $svgFileStatus = false;
        if ($svg) {
            $mainContent = $html->find('g.mainContent', 0);
            $layerBackground = $html->find('g#layer_background', 0);
            $defs = $html->find('defs', 0);
            if ($mainContent) {
                $main = $html->find("g[id^=layer_area_]");
                foreach ($main as $k => $v) {
                    $id = $main[$k]->id;
                    $name = $main[$k]->name;
                    $height = $main[$k]->height;
                    $width = $main[$k]->width;
                    $aHeight = $main[$k]->aHeight ? $main[$k]->aHeight : 0;
                    $aWidth = $main[$k]->aWidth ? $main[$k]->aWidth : 0;
                    $x = $main[$k]->x;
                    $y = $main[$k]->y;
                    $aWidth = $this->unitConvertionToInch($aWidth);
                    $aHeight = $this->unitConvertionToInch($aHeight);
                    $acHeight = $aHeight * $this->dpi;
                    $acHeight = $acHeight / $height;
                    $acWidth = $aWidth * $this->dpi;
                    $acWidth = $acWidth / $width;
                    if ($id == "layer_area_" . $k . "") {
                        if (isset($main[$k]->filter)) {
                            unset($main[$k]->filter);

                        }
                        if (strpos($main[$k], "xe_p") !== false) {
                            //Prepared SVG
                            $svgWidth = ' width="' . $aWidth . 'in"';
                            $svgHeight = ' height="' . $aHeight . 'in"';
                            $svgXY = ' x="0" y="0" overflow="visible">';
                            $scaleStr = '<g transform="scale(';
                            $scale = $scaleStr . $acWidth . ',' . $acHeight . ')';
                            $translate =' translate( -' . $x . ', -' . $y . ')">';
                            $transForm = $scale.$translate;
                            $svgTag = $svgTagStr . $svgWidth . $svgHeight;
                            $svgTagXY = $svgTag . $svgXY . $transForm;
                            $svgMiddleTag = $svgTagXY . $defs . $layerBackground;
                            $finalSvg = $svgMiddleTag . $main[$k].$svgEndTag;
                            $svgPath = $this->svgSavePath . $name . '_' . $k;
                            $svgFilePath = $svgPath . '_' . $multiPrintFileName;

                            //png and pdf file name
                            $pngAbsPath = $svgPath . '_' . $fileStr . '.png';
                            $rgbPdfAbsPath = $svgPath . '_' . $fileStr . '_rgb.pdf';
                            $cmykPdfAbsPath = $svgPath . '_' . $fileStr . '.pdf';
                            $svgFileStatus = write_file($svgFilePath, $finalSvg);
                            if ($svgFileStatus) {
                                if (in_array('PNG', $this->fileFormatArr)) {
                                    $this->svgConvertToPng(
                                        $pngAbsPath, $svgFilePath
                                    );
                                }
                                if (in_array('PDF', $this->fileFormatArr)) {
                                    $this->svgConvertToRGBPdf(
                                        $rgbPdfAbsPath, $svgFilePath
                                    );
                                    $this->rgbPdfConvertToCMYKPdf(
                                        $cmykPdfAbsPath, $rgbPdfAbsPath
                                    );
                                }
                            }
                        }
                    }
                }
            }
        }
        return $svgFileStatus;
    }

    /**
     * GET: To create output files for multiple boundary products
     *
     * @param $reqStr     The is current request SVG string
     * @param $svgAbsPath The is current request SVG path
     *
     * @author radhanatham@riaxe.com
     * @date   03 Jan 2020
     * @return boolean
     */
    private function generateSingleSvgFile($reqStr, $svgAbsPath)
    {
        $svgFileStatus = false;
        $html = $this->domObj->strGetHtml($reqStr, false);
        $svg = $html->find('image#svg_1', 0);
        $svgroot = $html->find('svg#svgroot', 0);
        if ($svg) {
            $mainContent = $html->find('g.mainContent', 0);
            $aHeighArr = array();
            $aWidthArr = array();
            if ($mainContent) {
                $main = $html->find("g[id^=layer_area_]");
                foreach ($main as $k => $g) {
                    $id = $main[$k]->id;
                    $height = $main[$k]->height;
                    $width = $main[$k]->width;
                    $aHeight = $main[$k]->aHeight;
                    $aWidth = $main[$k]->aWidth;
                    $aWidth = $this->unitConvertionToInch($aWidth);
                    $aHeight = $this->unitConvertionToInch($aHeight);
                    $acHeight = $aHeight * $this->dpi;
                    $acHeight = $acHeight / $height;
                    $acWidth = $aWidth * $this->dpi;
                    $acWidth = $acWidth / $width;
                    array_push($aHeighArr, $acHeight);
                    array_push($aWidthArr, $acWidth);
                    if ($id == "layer_area_" . $k . "") {
                        if (isset($main[$k]->filter)) {
                            unset($main[$k]->filter);
                        }
                        $scale = $acWidth . ',' . $acHeight;
                        $main[$k]->transform = 'scale(' . $scale . ')';
                    }
                }
                $maxWidth = max($aWidthArr);
                $maxHeight = max($aHeighArr);
                $svgHeigh = $maxHeight * 500;
                $svgWidth = $maxWidth * 500;
                $svgroot->width = $svgWidth;
                $svgroot->height = $svgHeigh;
                $svg->outertext = '';
                $html->save();
                $html = str_replace('</svg>', '', $html);
                $html = $html . '</svg>';
                $svgFileStatus = write_file($svgAbsPath, $html);
            }
        }
        return $svgFileStatus;
    }

    /**
     * GET: Create separate svg file by print color for reactngle print
     *
     * @param $svgStr The is current request SVG string
     *
     * @author radhanatham@riaxe.com
     * @date   03 Jan 2020
     * @return boolean
     */
    private function generateSvgFileByColor($svgStr = null)
    {
        $itemSidePath = $this->svgSavePath .$this->sidePath;
        $svgFileStatus = false;
        $html = $this->domObj->strGetHtml($svgStr, false);
        $pattern = $html->find("pattern[id^=p]");
        $main = $html->find("g[id^=layer_area_0]", 0);
        $pathStyle = $main->find('path'); //Get all path from svg string
        foreach ($pathStyle as $k => $v) {
            if ($pathStyle[$k]->id != 'boundMask') {
                $pathStyle[$k]->style = "display:none"; // Hide all path
            }
        }

        $polygonStyle = $main->find('polygon'); //Get all polygon from svg string
        if (isset($polygonStyle) && $polygonStyle) {
            foreach ($polygonStyle as $k => $v) {
                $polygonStyle[$k]->style = "display:none"; // Hide all polygon
            }
        }
        $html->save();
        $htmlStr = $this->domObj->strGetHtml($html, false);
        if (!empty($this->printColorsArr)) {
            foreach ($this->printColorsArr as $k => $color) {
                if ($color[0] == "#") {
                    $path = $htmlStr->find('path[fill^=' . $color . ']');
                    $pathId = '';
                    foreach ($path as $key => $value) {
                        $pathId = $path[$key]->id;
                        $pathTxt = $htmlStr->find("path[id^=" . $pathId . "]", 0);
                        $pathTxt->style = "display:block";
                    }
                    $polygon = $htmlStr->find('polygon[fill^=' . $color . ']');
                    $polygonId = '';
                    foreach ($polygon as $key => $value) {
                        $polygonId = $polygon[$key]->id;
                        $polygonTxt = $htmlStr->find(
                            "polygon[id^=" . $polygonId . "]", 0
                        );
                        $polygonTxt->style = "display:block";
                    }

                    //png and pdf file name
                    $pngAbsPath =  $itemSidePath . '_' . $color . '.png';
                    $rgbPdfPath = $itemSidePath . '_' . $color . '_rgb.pdf';
                    $cmykPdfAbsPath = $itemSidePath . '_' . $color . '.pdf';
                    //svg file
                    $svgFileName = $this->sidePath . "_" . $color . '.svg';
                    $svgPath = $this->svgSavePath . $svgFileName;
                    $svgFileStatus = write_file($svgPath, $htmlStr);
                    //get all group path
                    $pathSvg = $htmlStr->find('path');
                    foreach ($pathSvg as $k => $v) {
                        if ($pathSvg[$k]->id != 'boundMask') {
                            $pathSvg[$k]->style = "display:none";
                        }
                    }
                    //get all polygon group path
                    $polygonSvg = $htmlStr->find('polygon');
                    foreach ($polygonSvg as $k => $v) {
                        $polygonSvg[$k]->style = "display:none";
                    }
                } else {
                    if ($color != '' && (filter_var($color, FILTER_VALIDATE_URL))) {
                        $patternId = '';
                        $baseFileName = basename($color);
                        $fileNameArr = explode(".", $baseFileName);
                        $patternId = $fileNameArr[0];
                        if (isset($pattern) && $pattern) {
                            $pathPattern = $htmlStr->find(
                                'path[fill^=url(#' . $patternId . ')]'
                            );
                            $pathIdPattern = '';
                            foreach ($pathPattern as $kk => $ppv) {
                                $pathIdPattern = $pathPattern[$kk]->id;
                                $pathTxtPattern = $htmlStr->find(
                                    "path[id^=" . $pathIdPattern . "]", 0
                                );
                                $pathTxtPattern->style = "display:block";
                            }
                        }
                        //png and pdf file name
                        $pngAbsPath =  $itemSidePath . '_' . $patternId . '.png';
                        $rgbPdfPath = $itemSidePath . '_' . $patternId . '_rgb.pdf';
                        $cmykPdfAbsPath = $itemSidePath . '_' . $patternId . '.pdf';
                        //SVg file
                        $svgFileName = $this->sidePath . "_" . $patternId . '.svg';
                        $svgPath = $this->svgSavePath . $svgFileName;
                        $svgFileStatus = write_file($svgPath, $htmlStr);
                        //Get all pattern path from svg string
                        $pathStylePattern = $htmlStr->find('path');
                        foreach ($pathStylePattern as $kkk => $pspv) {
                            if ($pathStylePattern[$kkk]->id != 'boundMask') {
                                // Hide all path
                                $pathStylePattern[$kkk]->style = "display:none";
                            }
                        }
                    }
                }
                if ($svgFileStatus) {
                    if (in_array('PNG', $this->fileFormatArr)) {
                        $this->svgConvertToPng($pngAbsPath, $svgPath);
                    }
                    if (in_array('PDF', $this->fileFormatArr)) {
                        $this->svgConvertToRGBPdf(
                            $rgbPdfPath, $svgPath
                        );
                        $this->rgbPdfConvertToCMYKPdf(
                            $cmykPdfAbsPath, $rgbPdfPath
                        );
                    }
                }
            }
        }
        return $svgFileStatus;
    }

    /**
     * GET: Create svg file for shape print area dimension
     *
     * @param $htmlStr    The is current request SVG string
     * @param $svgAbsPath The is current request SVG path
     *
     * @author radhanatham@riaxe.com
     * @date   03 Jan 2020
     * @return boolean
     */
    private function generateShapePrintSvgFile($htmlStr, 
        $svgAbsPath
    ) {
        $svgFileStatus = false;
        $htmlShapeArea =  $this->domObj->strGetHtml($htmlStr, false);
        $htmlShapeArea->save();
        //find mask actual height and width
        $path = $htmlShapeArea->find('path#boundMask', 0); 
        $svgroot = $htmlShapeArea->find('svg#svgroot', 0);
        if ($this->isProductWithDesign) {
            $this->imageTag = $svgroot->first_child(); // Get product image tage
        }
        if (isset($path) && !empty($path)) {
            $pathAx = $path->aX ? $path->aX : $path->ax;
            $pathAy = $path->aY ? $path->aY : $path->ay;
            $pathX = $path->x;
            $pathY = $path->y;
            if (!$pathAx || !isset($pathAx) || ($pathAx == null)) {
                $pathAx = $pathX;
                $pathAy = $pathY;
            }
            if (($pathAx < 0) || ($pathAy < 0)) {
                $svgroot->width = 500;
                $svgroot->height = 500;
                $acHeight = 1;
                $pathAx = 0;
                $pathAy = 0;
            } else {
                $width = $path->width;
                $height = $path->height;
                $aHeight = $path->aHeight ? $path->aHeight : $path->aheight;
                $aWidth = $path->aWidth ? $path->aWidth : $path->awidth;
                if (intval($aWidth) != 0 && intval($aHeight) != 0) {
                    $aWidth = $this->unitConvertionToInch($aWidth);
                    $aHeight = $this->unitConvertionToInch($aHeight);
                    $svgroot->width = $aWidth * $this->dpi;
                    $svgroot->height = $aHeight * $this->dpi;
                    $acHeight = $aHeight * $this->dpi;
                    $acHeight = $acHeight / $height;
                    $acWidth = $aWidth * $this->dpi;
                    $acWidth = $acWidth / $width;
                } else {
                    $svgroot = $htmlShapeArea->find('svg#svgroot', 0);
                    $svgroot->width = 500;
                    $svgroot->height = 500;
                    $acHeight = 1;
                    $acWidth = 1;
                    $pathAx = 0;
                    $pathAy = 0;
                }
            }
        }
        $svgStr = $htmlShapeArea->find('image#svg_1', 0);
        $transformS = '<g id="rect" transform="scale(';
        $translate = ' translate( -' . $pathAx . ', -' . $pathAy . ')">';
        if ($acHeight != $acWidth) {
            $scaleValue =  $transformS.$acWidth . ',' . $acHeight.')' ;
        } else {
            $scaleValue =   $transformS.$acHeight.')' ;
        }
        $svgStr->outertext = $scaleValue.$translate . $this->imageTag;
        $htmlShapeArea = str_replace('</svg>', '', $htmlShapeArea);
        $htmlShapeArea = $htmlShapeArea . '</g></svg>';
        if (count($this->printColorsArr) > 1) {
            $svgFileStatus = $this->generateSvgFileByColor($htmlShapeArea);
        } else {
            $svgFileStatus = write_file($svgAbsPath, $htmlShapeArea);
        }
        return $svgFileStatus;
    }

    /**
     * GET: SVG file convert to PNG file through imagick
     *
     * @param $pngAbsPath The is current request PNG path
     * @param $svgAbsPath The is current request SVG path
     *
     * @author radhanatham@riaxe.com
     * @date   03 Jan 2020
     * @return nothing
     */
    private function svgConvertToPng($pngAbsPath, $svgAbsPath)
    {
        if (extension_loaded('imagick') && file_exists($svgAbsPath)) {
            $image = new \imagick($svgAbsPath);
            $noOfPagesInPDF = $image->getNumberImages();
            if ($noOfPagesInPDF) {
                //$image->setImageResolution(72, 72);
                $image->setImageFormat("png");
                $image->writeImage($pngAbsPath);
            }
        }
    }

    /**
     * GET: SVG file convert to RGB pdf file through Inkscape
     *
     * @param $rgbPdfAbsPath The is current request RGB  PDF path
     * @param $svgAbsPath    The is current request SVG path
     *
     * @author radhanatham@riaxe.com
     * @date   03 Jan 2020
     * @return nothing
     */
    private function svgConvertToRGBPdf($rgbPdfAbsPath, $svgAbsPath)
    {
        $returnResult = $this->checkInkscape();
        if ($returnResult['status'] && file_exists($svgAbsPath)) {
            $shellFun = $returnResult['value'];
            $cmdPdf = "inkscape " . escapeshellarg(
                $svgAbsPath
            ) . " --export-pdf=" . escapeshellarg(
                $rgbPdfAbsPath
            );
            $shellFun($cmdPdf);
        }
    }

    /**
     * GET: RGB pdf file convert to CMYK pdf file through Ghostscript
     *
     * @param $cmykPdfAbsPath The is current request CYMK  PDF path
     * @param $rgbPdfAbsPath  The is current request RGB  PDF path
     *
     * @author radhanatham@riaxe.com
     * @date   03 Jan 2020
     * @return nothing
     */
    private function rgbPdfConvertToCMYKPdf($cmykPdfAbsPath, $rgbPdfAbsPath)
    {
        $returnResult = $this->checkGhostScript();
        if ($returnResult['status'] && file_exists($rgbPdfAbsPath)) {
            $cmykPdfAbsPath = escapeshellarg($cmykPdfAbsPath);
            $fromRgbPdfAbsPath = escapeshellarg($rgbPdfAbsPath);
            $shellFun = $returnResult['value'];
            $cmdPdfCmyk = "gs -dSAFER -dBATCH \
                -dNOPAUSE -dNOCACHE -sDEVICE=pdfwrite -dAutoRotatePages=/None \
                -sColorConversionStrategy=CMYK \
                -dProcessColorModel=/DeviceCMYK \
                -sOutputFile=" . $cmykPdfAbsPath . " \
                " . $fromRgbPdfAbsPath;
            $shellFun($cmdPdfCmyk);
            if (file_exists($rgbPdfAbsPath)) {
                unlink($rgbPdfAbsPath); //remove rgb pdf file
            }
        }
    }

    /**
     * GET: Check Inkscape is avialable or not in server
     *
     * @author radhanatham@riaxe.com
     * @date   03 Jan 2020
     * @return array of shell enabled function
     */
    private function checkInkscape()
    {
        $result['status'] = true;
        $result['value'] = 'shell_exec';
        $retrunResult = $this->getShellEnabledFunction();
        if ($retrunResult['status']) {
            $shell_function = $retrunResult['value'];
            system("which inkscape > /dev/null", $retvalInk);
            if ($retvalInk == 0) {
                $result['status'] = true;
                $result['value'] = $shell_function;
            } else {
                if ($shell_function == 'exec' && empty($retvalInk)) {
                    $result['status'] = true;
                    $result['value'] = $shell_function;
                } else {
                    $result['status'] = false;
                    $result['value'] = '';
                }
            }
        }
        return $result;
    }

    /**
     * GET: Check Ghostscript is avialable or not in server
     *
     * @author radhanatham@riaxe.com
     * @date   03 Jan 2020
     * @return array of shell enabled function
     */
    private function checkGhostScript()
    {
        $result['status'] = true;
        $result['value'] = 'shell_exec';
        $retrunResult = $this->getShellEnabledFunction();
        if ($retrunResult['status']) {
            $shell_function = $retrunResult['value'];
            system("which gs > /dev/null", $retvalInk);
            if ($retvalInk == 0) {
                $result['status'] = true;
                $result['value'] = $shell_function;
            } else {
                if ($shell_function == 'exec' && empty($retvalInk)) {
                    $result['status'] = true;
                    $result['value'] = $shell_function;
                } else {
                    $result['status'] = false;
                }
            }
        }
        return $result;
    }

    /**
     * GET: Check enabled/disabled function in server(php.ini file)
     *
     * @author radhanatham@riaxe.com
     * @date   03 Jan 2020
     * @return array of shell enabled function
     */
    private function getShellEnabledFunction()
    {
        $result = array();
        //default function
        $disableFunctions = ini_get("disable_functions");
        if ($disableFunctions != '') {
            $disableFunctionsArr = explode(',', rtrim($disableFunctions, ','));
        } else {
            $result['status'] = true;
            $result['value'] = 'shell_exec';
        }
        //all default function for run shell command
        $deafaultShell = array(
            "passthru",
            "exec", 
            "system",
            "shell_exec"
        );
        if (!empty($disableFunctionsArr)) {
            foreach ($deafaultShell as $value) {
                if (!in_array($value, $disableFunctionsArr)) {
                    $result['value'] = $value;
                    $result['status'] = true;
                } else {
                    $result['value'] = '';
                    $result['status'] = false;
                }
            }
        }
        return $result;
    }

    /**
     * GET: Check enabled/disabled function in server(php.ini file)
     *
     * @param $value unit value in string
     *
     * @author radhanatham@riaxe.com
     * @date   03 Jan 2020
     * @return float or Integer
     */
    private function unitConvertionToInch($value)
    {
        $result = 0;
        switch ($this->printUnit) {
        case 'cm':
            return $result = ($value / 2.54);
                break;
        case 'mm':
            return $result = ($value / 25.4);
                break;
        case 'ft':
            return $result = ($value * 12);
                break;
        default:
            return $value;
                break;
        }
    }

    /**
     * GET: Download order zip
     *
     * @param $isDownload check for boolean value
     *
     * @author radhanatham@riaxe.com
     * @date   03 Jan 2020
     * @return float or Integer
     */
    private function zipDownload($isDownload)
    {
        if ($isDownload) {
            $zipName = 'orders.zip';
            return $this->zipFileDownload($this->orderPath . $zipName);
        } else {
            return false;
        }
    }

    /**
     * GET: Download Zip file
     *
     * @param $dir This is for zip download directory
     *
     * @author radhanatham@riaxe.com
     * @date   03 Jan 2020
     * @return boolean
     */
    private function zipFileDownload($dir)
    {
        if (file_exists($dir)) {
            header('Content-Description: File Transfer');
            header("Content-type: application/x-msdownload", true, 200);
            header('Content-Disposition: attachment; filename=' . basename($dir));
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header("Pragma: no-cache");
            header('Content-Length: ' . filesize($dir));
            readfile($dir);
            if (file_exists($dir)) {
                unlink($dir);
            }
            $status = true;
            exit();
        } else {
            $status = false;
        }
        return $status;
    }
}
