<?php
/**
 * Manage Order Logs from Store end and Admin end
 *
 * PHP version 5.6
 *
 * @category  Store_Order
 * @package   Order
 * @author    Tanmaya Patra <tanmayap@riaxe.com>
 * @copyright 2019-2020 Riaxe Systems
 * @license   http://www.php.net/license/3_0.txt  PHP License 3.0
 * @link      http://inkxe-v10.inkxe.io/xetool/admin
 */
namespace App\Modules\Orders\Controllers;

use App\Modules\Orders\Models\OrderLog;
use App\Modules\Orders\Models\OrderLogFiles;
use App\Modules\Products\Models\ProductSetting;
use OrderStoreSpace\Controllers\StoreOrdersController;

/**
 * Order Log Controller
 *
 * @category Store_Order
 * @package  Order
 * @author   Tanmaya Patra <tanmayap@riaxe.com>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://inkxe-v10.inkxe.io/xetool/admin
 */
class OrdersController extends StoreOrdersController
{
    /**
     * Get: Get Total Orders
     *
     * @param $request  Slim's Request object
     * @param $response Slim's Response object
     * @param $args     Slim's Argument parameters
     *
     * @author satyabratap@riaxe.com
     * @date   5 Oct 2019
     * @return Total Orders in Json format
     */
    public function getOrderList($request, $response, $args)
    {
        $serverStatusCode = OPERATION_OKAY;
        $jsonResponse = $this->getOrders($request, $response, $args);

        return response($response, ['data' => $jsonResponse['data'], 'status' => $serverStatusCode]);
    }
    
    /**
     * Post: Save Order Log data
     *
     * @param $request  Slim's Request object
     * @param $response Slim's Response object
     *
     * @author satyabratap@riaxe.com
     * @date   5 Oct 2019
     * @return Order logs in Json format
     */
    public function saveOrderLogs($request, $response)
    {
        $serverStatusCode = OPERATION_OKAY;
        $allPostPutVars = $request->getParsedBody();
        $jsonResponse = [
            'status' => 0,
            'message' => message('Order Log', 'error'),
        ];
        $orderLogsJson = $allPostPutVars['log_data'];
        $orderLogsArray = json_decode($orderLogsJson, true);
        $order_id = $orderLogsArray['order_id'];

        if (!empty($orderLogsArray['order_id']) && $orderLogsArray['order_id'] > 0) {
            $saveOrderLog = new OrderLog(
                [
                    'order_id' => $orderLogsArray['order_id'],
                    'agent_type' => $orderLogsArray['agent_type'],
                    'agent_id' => $orderLogsArray['agent_id'],
                    'store_id' => $orderLogsArray['store_id'],
                    'message' => $orderLogsArray['message'],
                    'log_type' => $orderLogsArray['log_type'],
                    'status' => $orderLogsArray['status'],
                ]
            );

            if ($saveOrderLog->save()) {
                $OrderLogInsertId = $saveOrderLog->xe_id;
                if (!empty($orderLogsArray['files'])) {
                    foreach ($orderLogsArray['files'] as $fileKey => $fileData) {
                        // Start saving each sides
                        $imageUploadIndex = $fileData['image_upload_data'];
                        // If image resource was given then upload the image
                        // into the specified folder
                        $getFiles = save_file($imageUploadIndex, path('abs', 'order_log'));
                        // Setup data for Saving/updating
                        $orderLogFiles = [
                            'order_log_id' => $OrderLogInsertId,
                        ];

                        // If File was choosen from frontend then only
                        // save/update the image or skip the image saving
                        if (!empty($getFiles)) {
                            $orderLogFiles['file_name'] = $getFiles;
                        }
                        // Insert Order Log Files
                        $saveOrderLogFile = new OrderLogFiles($orderLogFiles);
                        $saveOrderLogFile->save();
                    }
                }
                $jsonResponse = [
                    'status' => 1,
                    'order_log_id' => $OrderLogInsertId,
                    'message' => message('Order Log', 'saved'),
                ];
            }
        }
        return response($response, ['data' => $jsonResponse, 'status' => $serverStatusCode]);
    }
    /**
     * GET: Get all Order Logs of a single order
     *
     * @param $request  Slim's Request object
     * @param $response Slim's Response object
     * @param $args     Slim's Argument parameters
     *
     * @author satyabratap@riaxe.com
     * @date   5 Oct 2019
     * @return Order Logs in Json format
     */
    public function getOrderLogs($request, $response, $args)
    {
        $serverStatusCode = OPERATION_OKAY;
        $offset = 0;
        $totalOrderLogs = [];
        $processInAppLog = [];
        $jsonResponse = [
            'status' => 1,
            'data' => [],
            'message' => message('Order Log', 'not_found'),
        ];
        $orderLogInit = new OrderLog();
        $initOrderLog = $orderLogInit->with('files');
        if (isset($args['id']) && $args['id'] > 0) {
            $initOrderLog->where('order_id', $args['id']);
        }
        if ($initOrderLog->count() > 0) {
            $inAppOrderLogs = $initOrderLog->orderBy('xe_id', 'desc')
                ->get()->toArray();
            foreach ($inAppOrderLogs as $inAppLogkey => $inAppLog) {
                $processInAppLog[$inAppLogkey] = [
                    'id' => $inAppLog['xe_id'],
                    'order_id' => $inAppLog['order_id'],
                    'agent_type' => $inAppLog['agent_type'],
                    'agent_id' => $inAppLog['agent_id'],
                    'store_id' => $inAppLog['store_id'],
                    'message' => $inAppLog['message'],
                    'log_type' => $inAppLog['log_type'],
                    'status' => $inAppLog['status'],
                    'created_at' => $inAppLog['created_at'],
                    'updated_at' => $inAppLog['updated_at'],
                    'files' => $inAppLog['files'],
                ];
            }
        }

        // Get Logs from Store
        $storeLogs = $this->getStoreLogs($request, $response, $args);

        if (!empty($storeLogs) && is_array($storeLogs) && count($storeLogs) > 0) {
            $totalOrderLogs = array_merge($processInAppLog, $storeLogs);
        }
        // Sort the array by Created Date and time
        usort($totalOrderLogs, 'date_compare');

        if (!empty($totalOrderLogs) && is_array($totalOrderLogs) && count($totalOrderLogs) > 0) {
            $jsonResponse = [
                'status' => 1,
                'data' => $totalOrderLogs,
            ];
        }
        return response($response, ['data' => $jsonResponse, 'status' => $serverStatusCode]);
    }

    /**
     * GET: Get Product Setting Details
     *
     * @param $productId Store Product Id
     *
     * @author satyabratap@riaxe.com
     * @date   5 Oct 2019
     * @return Order Logs in Json format
     */
    public function getSettingDetails($productId)
    {
        $getSettingsAssociatedRecords = ProductSetting::with(
            'sides', 'sides.product_decoration_setting',
            'sides.product_decoration_setting.print_profile_decoration_settings.print_profile',
            'sides.product_decoration_setting.print_area'
        )->where('product_id', $productId);

        $productDecorationSettingData = [];
        if ($getSettingsAssociatedRecords->count() > 0) {
            $getFinalArray = $getSettingsAssociatedRecords->orderBy('xe_id', 'desc')->first();
            foreach ($getFinalArray['sides'] as $sideKey => $side) {
                $printProfileData = [];
                $printAreaData = [];
                // Loop through, Print Area Decoration Setting
                if (isset($side['product_decoration_setting']) && count($side['product_decoration_setting']) > 0) {
                    foreach ($side['product_decoration_setting'] as $decorationSettingLoop => $decorationSetting) {
                        $printAreaData[$decorationSettingLoop] = [
                            'decoration_name' => $decorationSetting['name'],
                            'print_area_name' => (!empty($decorationSetting['print_area'][0]['name']) && $decorationSetting['print_area'][0]['name'] != "") ? $decorationSetting['print_area'][0]['name'] : "",
                            'print_area_id' => (!empty($decorationSetting['print_area'][0]['xe_id']) && $decorationSetting['print_area'][0]['xe_id'] != "") ? $decorationSetting['print_area'][0]['xe_id'] : "",
                        ];
                        // Loop through, Print Profile Decoration Setting
                        if (isset($decorationSetting['print_profile_decoration_settings']) && count($decorationSetting['print_profile_decoration_settings']) > 0) {
                            foreach ($decorationSetting['print_profile_decoration_settings'] as $printProfileDecorationSettingKey => $printProfileDecorationSetting) {
                                if (count($printProfileDecorationSetting['print_profile']) > 0) {
                                    $printProfileData[$printProfileDecorationSettingKey] = [
                                        'print_profiles_id' => $printProfileDecorationSetting['print_profile'][0]['xe_id'],
                                        'print_profiles_name' => $printProfileDecorationSetting['print_profile'][0]['name'],
                                    ];
                                }
                            }
                        }
                    }
                }
                $productDecorationSettingData[$sideKey] = [
                    'name' => $side['side_name'],
                    'printAreaData' => $printAreaData,
                    'printProfileData' => $printProfileData,
                ];
            }
        }
        return $productDecorationSettingData;
    }
}
