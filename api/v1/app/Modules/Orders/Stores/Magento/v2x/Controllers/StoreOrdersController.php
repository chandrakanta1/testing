<?php
/**
 *
 * This Controller used fetch  Magento Orders on various endpoints
 *
 * @category   Orders
 * @package    Magento API
 * @author     Original Author <radhanatham@riaxe.com>
 * @author     radhanatham@riaxe.com
 * @copyright  2019-2020 Riaxe Systems
 * @license    http://www.php.net/license/3_0.txt  PHP License 3.0
 * @version    Release: @1.0
 */
namespace OrderStoreSpace\Controllers;

use App\Modules\Products\Models\ProductSetting;
use ComponentStoreSpace\Controllers\StoreComponent;

class StoreOrdersController extends StoreComponent
{

    /**
     * Get list of orders from the Magento API
     *
     * @author     radhanatham@riaxe.com
     * @date       18 Dec 2019
     * @parameter  Slim default params
     * @response   Array of list/one order(s)
     */
    public function getOrders($request, $response, $args)
    {
        $serverStatusCode = OPERATION_OKAY;
        $orders = array();
        $orderId = 0;
        $getStoreDetails = get_store_details($request);
        if (isset($args['id']) && $args['id'] != "" && $args['id'] > 0) {
            $orderId = $args['id'];
        }
        $getTotalOrdersCount = 0;
        $resultArr = array();
        $resultReturn = array();
        try {
            if (isset($orderId) && $orderId) {
                $filters = array('orderId' => $orderId, 'store' => $getStoreDetails['store_id']);
                //Get all order details by order id
                $orderDetailsObj = $this->apiCall('Order', 'getOrderDetails', $filters);
                $orders = json_decode($orderDetailsObj->result, true);
                if (isset($args['id']) && $args['id'] > 0) {
                    foreach ($orders['order_list']['orders'] as $orderDetailsKey => $orderDetails) {
                        $getSettingsAssociatedRecords = ProductSetting::with(
                            'sides', 'sides.product_decoration_setting',
                            'sides.product_decoration_setting.print_profile_decoration_settings.print_profile',
                            'sides.product_decoration_setting.print_area'
                        )->where('product_id', $orderDetails['product_id']);
                        $productDecorationSettingData = [];
                        if ($getSettingsAssociatedRecords->count() > 0) {
                            $getFinalArray = $getSettingsAssociatedRecords->orderBy('xe_id', 'desc')->first();
                            foreach ($getFinalArray['sides'] as $sideKey => $side) {
                                // Loop through, Print Area Decoration Setting
                                if (isset($side['product_decoration_setting']) && count($side['product_decoration_setting']) > 0) {
                                    $printAreaData = [];
                                    foreach ($side['product_decoration_setting'] as $decorationSettingLoop => $decorationSetting) {
                                        $printAreaData[$decorationSettingLoop] = [
                                            'decoration_name' => $decorationSetting['name'],
                                            'print_area_name' => $decorationSetting['print_area'][0]['name'],
                                            'print_area_id' => $decorationSetting['print_area'][0]['xe_id'],
                                        ];
                                    }
                                }
                                if (isset($side['product_decoration_setting']) && count($side['product_decoration_setting']) > 0) {
                                    foreach ($side['product_decoration_setting'] as $decorationSettingLoop => $decorationSetting) {
                                        // Loop through, Print Profile Decoration Setting
                                        if (isset($decorationSetting['print_profile_decoration_settings']) && count($decorationSetting['print_profile_decoration_settings']) > 0) {
                                            foreach ($decorationSetting['print_profile_decoration_settings'] as $printProfileDecorationSettingKey => $printProfileDecorationSetting) {
                                                $printProfileData[$printProfileDecorationSettingKey] = [
                                                    'print_profiles_id' => $printProfileDecorationSetting['print_profile'][0]['xe_id'],
                                                    'print_profiles_name' => $printProfileDecorationSetting['print_profile'][0]['name'],
                                                ];
                                            }
                                        }
                                    }
                                }
                                $productDecorationSettingData[$sideKey] = [
                                    'name' => $side['side_name'],
                                    'printAreaData' => $printAreaData,
                                    'printProfileData' => $printProfileData,
                                ];
                            }
                        }
                        $orders['order_list']['orders'][$orderDetailsKey]['productDecorationSettingData'] = $productDecorationSettingData;
                    }
                }
            } else {
                //Get all order list
                $filters = [
                    'store' => $getStoreDetails['store_id'],
                    'search' => $request->getQueryParam('name') ? $request->getQueryParam('name') : '',
                    'page' => $request->getQueryParam('page') ? $request->getQueryParam('page') : 1,
                    'per_page' => $request->getQueryParam('perpage') ? $request->getQueryParam('perpage') : 25,
                    'after' => $request->getQueryParam('from') ? $request->getQueryParam('from') : date('Y-m-d', strtotime('2015-01-01')),
                    'before' => $request->getQueryParam('to') ? $request->getQueryParam('to') : date('Y-m-d'),
                    'order' => (!empty($request->getQueryParam('order')) && $request->getQueryParam('order') != "") ? $request->getQueryParam('order') : 'ASC',
                    'orderby' => (!empty($request->getQueryParam('orderby')) && $request->getQueryParam('orderby') != "") ? 'created_date' : 'entity_id',
                    'customize' => $request->getQueryParam('customize') ? $request->getQueryParam('customize') : 0,
                ];
                $orderObj = $this->apiCall('Order', 'getOrders', $filters);
                $orders = json_decode($orderObj->result, true);
            }
            if (isset($orders) && is_array($orders) && count($orders) > 0) {
                $response = [
                    'status' => 1,
                    'records' => (isset($orderId) && $orderId) ? 1 : count($orders['order_list']),
                    'total_records' => (isset($orderId) && $orderId) ? 1 : count($orders['order_list']),
                    'data' => $orders['order_list'],
                ];
            } else {
                $serverStatusCode = OPERATION_OKAY;
                $response = [
                    'status' => 0,
                    'records' => count($orders),
                    'total_records' => $getTotalOrdersCount,
                    'message' => 'No Order available',
                    'data' => [],
                ];
            }
        } catch (\Exception $e) {
            $serverStatusCode = EXCEPTION_OCCURED;
            $response = [
                'status' => 0,
                'message' => 'Invalid request',
                'exception' => $e->getMessage(),
            ];
        }
        // Reset Total product Count
        return [
            'data' => $response,
            'httpStatusCode' => $serverStatusCode,
        ];
    }

}
