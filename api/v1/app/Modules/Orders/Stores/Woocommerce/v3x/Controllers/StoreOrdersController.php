<?php
/**
 * Manage Order at Woo-Commerce store end as well as at Admin end
 *
 * PHP version 5.6
 *
 * @category  Store_Order
 * @package   Order
 * @author    Satyabrata <satyabratap@riaxe.com>
 * @copyright 2019-2020 Riaxe Systems
 * @license   http://www.php.net/license/3_0.txt  PHP License 3.0
 * @link      http://inkxe-v10.inkxe.io/xetool/admin
 */
namespace OrderStoreSpace\Controllers;

use CommonStoreSpace\Controllers\StoreController;

/**
 * Store Order Controller
 *
 * @category Store_Order
 * @package  Order
 * @author   Satyabrata <satyabratap@riaxe.com>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://inkxe-v10.inkxe.io/xetool/admin
 */
class StoreOrdersController extends StoreController
{
    /**
     * Initialize Construct
     *
     * @author satyabratap@riaxe.com
     * @date   5 Oct 2019
     */
    public function __construct()
    {
        parent::__construct();
    }
    /**
     * Get list of product or a Single product from the WooCommerce API
     *
     * @param $request  Slim's Request object
     * @param $response Slim's Response object
     * @param $args     Slim's Argument parameters
     *
     * @author satyabratap@riaxe.com
     * @date   5 Oct 2019
     * @return Order List in Json format
     */
    public function getOrders($request, $response, $args)
    {
        $serverStatusCode = OPERATION_OKAY;
        $orders = [];
        $orderOptions = [];
        $orderCount = 0;
        $jsonResponse = [
            'status' => 1,
            'records' => 0,
            'total_records' => 0,
            'message' => message('Orders', 'not_found'),
            'data' => [],
        ];
        try {
            if (isset($args['id']) && $args['id'] > 0) {
                // Fetch Single Order Details
                $singleOrderDetails = object_to_array($this->wc->get('orders/' . $args['id']));
                if (!empty($singleOrderDetails)) {
                    if ($singleOrderDetails['customer_id'] > 0) {
                        $orderOptions['customer'] = $singleOrderDetails['customer_id'];
                        $orderCount = $this->wc->get('orders', $orderOptions);
                        $orderCount = sizeof($orderCount);
                    }
                    if (isset($singleOrderDetails['line_items']) && count($singleOrderDetails['line_items']) > 0) {
                        $lineOrders = $this->getlineItemDetails($singleOrderDetails['line_items']);
                    }
                
                    $orders = [
                        'id' => $singleOrderDetails['id'],
                        'customer_first_name' => $singleOrderDetails['billing']['first_name'],
                        'customer_last_name' => $singleOrderDetails['billing']['last_name'],
                        'customer_email' => $singleOrderDetails['billing']['email'],
                        'customer_id' => $singleOrderDetails['customer_id'],
                        'created_date' => date('Y-m-d h:i:s', strtotime($singleOrderDetails['date_created'])),
                        'total_amount' => $singleOrderDetails['total'],
                        'total_tax' => $singleOrderDetails['total_tax'],
                        'currency' => $singleOrderDetails['currency'],
                        'note' => $singleOrderDetails['customer_note'],
                        'status' => $singleOrderDetails['status'],
                        'total_orders' => $orderCount,
                        'billing' => $singleOrderDetails['billing'],
                        'shipping' => $singleOrderDetails['shipping'],
                        'orders' => isset($lineOrders) ? $lineOrders : [],
                    ];
                    $jsonResponse = [
                        'status' => 1,
                        'data' => $orders
                    ];
                }
            } else {
                // Get all requested Query params
                $filters = [
                    'search' => $request->getQueryParam('name'),
                    'page' => $request->getQueryParam('page'),
                    'sku' => $request->getQueryParam('sku'),
                    'print_type' => $request->getQueryParam('print_type'),
                    'is_customize' => $request->getQueryParam('is_customize'),
                    'order_by' => $request->getQueryParam('orderby'),
                    'order' => $request->getQueryParam('order'),
                    'to' => $request->getQueryParam('to'),
                    'from' => $request->getQueryParam('from'),
                    'per_page' => $request->getQueryParam('per_page'),
                ];
                $options = [];
                foreach ($filters as $filterKey => $filterValue) {
                    if (isset($filterValue) && $filterValue != "") {
                        $options += [$filterKey => $filterValue];
                    }
                }
                // Fetch All Orders
                // Calling to Custom API for getting Order List
                $orders = object_to_array($this->plugin->get('orders', $options));
                if (!empty($orders['data'])) {
                    $totalRecords = $orders['records'];
                    $orders = $orders['data'];
                    $jsonResponse = [
                        'status' => 1,
                        'records' => count($orders),
                        'total_records' => $totalRecords,
                        'data' => $orders
                    ];
                }
            }
        } catch (\Exception $e) {
            $serverStatusCode = EXCEPTION_OCCURED;
            $jsonResponse = [
                'status' => 0,
                'message' => message('Orders', 'exception'),
                'exception' => show_exception() === true ?
                $e->getMessage() : ''
            ];
        }
        // Reset Total product Count
        return [
            'data' => $jsonResponse,
            'httpStatusCode' => $serverStatusCode,
        ];
    }
    /**
     * Get list of Order Logs
     *
     * @param $request  Slim's Request object
     * @param $response Slim's Response object
     * @param $args     Slim's Argument parameters
     *
     * @author tanmayap@riaxe.com
     * @date   5 Oct 2019
     * @return Order List in Json format
     */
    public function getStoreLogs($request, $response, $args)
    {
        $apiEndpoint = 'orders/' . $args['id'];
        $storeResp = $this->wc->get($apiEndpoint);
        $storeOrderLog = [];
        if (!empty($storeResp['id']) && $storeResp['id'] > 0) {
            $storeOrderLog[] = [
                'id' => $storeResp['id'],
                'order_id' => $storeResp['id'],
                'agent_type' => 'admin',
                'agent_id' => null,
                'store_id' => null,
                'message' => $storeResp['status'],
                'log_type' => 'order_status',
                'status' => 'new',
                'created_at' => date('Y-m-d H:i:s', strtotime($storeResp['date_created'])),
                'updated_at' => date('Y-m-d H:i:s', strtotime($storeResp['date_modified']))
            ];
            if (!empty($storeResp['date_paid']) && $storeResp['date_paid'] != "") {
                $storeOrderLog[] = [
                    'id' => $storeResp['id'],
                    'order_id' => $storeResp['id'],
                    'agent_type' => 'admin',
                    'agent_id' => null,
                    'store_id' => null,
                    'message' => (!empty($storeResp['date_paid']) && $storeResp['date_paid'] != "") ? 'Paid' : 'Not-paid',
                    'date_paid' => (!empty($storeResp['date_paid']) && $storeResp['date_paid'] != "") ? $storeResp['date_paid'] : null,
                    'payment_method' => (!empty($storeResp['payment_method']) && $storeResp['payment_method'] != "") ? $storeResp['payment_method'] : null,
                    'payment_method_title' => (!empty($storeResp['payment_method_title']) && $storeResp['payment_method_title'] != "") ? $storeResp['payment_method_title'] : null,
                    'log_type' => 'payment_status',
                    'status' => 'new',
                    'created_at' => date('Y-m-d H:i:s', strtotime($storeResp['date_created'])),
                    'updated_at' => date('Y-m-d H:i:s', strtotime($storeResp['date_modified']))
                ];
            }
        }

        return $storeOrderLog;
    }

    /**
     * Generate thumb images from store product images by using store end image urls
     *
     * @param $imagePath  Product image path
     * @param $resolution Required Size
     * 
     * @author tanmayap@riaxe.com
     * @date   24 sep 2019
     * @return Image path
     */
    public function getVariableImageSizes($imagePath, $resolution)
    {
        // Only available 100, 150, 300, 450 and 768 resolution image sizes
        $imageResolution = 300;
        if (isset($resolution) && ($resolution == 100 || $resolution == 150 || $resolution == 300 || $resolution == 450 || $resolution == 768)) {
            $imageResolution = $resolution;
        }
        $explodeImage = explode('/', $imagePath);
        $getImageFromUrl = end($explodeImage);
        $fileExtension = pathinfo($getImageFromUrl, PATHINFO_EXTENSION);
        $fileName = pathinfo($getImageFromUrl, PATHINFO_FILENAME);
        $updatedImageName = $fileName . '-' . $imageResolution . 'x' . $imageResolution . '.' . $fileExtension;
        $updatedImagePath = str_replace($getImageFromUrl, $updatedImageName, $imagePath);
        return $updatedImagePath;
    }

    /**
     * GET: Get Line Item Decorations of Orders
     *
     * @param $lineItems Line Item Details
     *
     * @author satyabratap@riaxe.com
     * @date   7 jan 2019
     * @return json
     */
    private function getlineItemDetails($lineItems)
    {
        foreach ($lineItems as $orderDetailsKey => $orderDetails) {
            $productDecorationSettingData = $this->getSettingDetails($orderDetails['product_id']);            
            $productEndpoint = 'products' . '/' . $orderDetails['product_id'];
            $getSingleProducts = object_to_array($this->wc->get($productEndpoint));
            $productImages = [];
            foreach ($getSingleProducts['images'] as $prodImgKey => $prodImg) {
                $productImages[] = [
                    'id' => $prodImg['id'],
                    'src' => $prodImg['src'],
                    'thumbnail' => $this->getVariableImageSizes($prodImg['src'], '150'),
                ];
            }
            $lineOrders[$orderDetailsKey] = [
                'id' => $orderDetails['id'],
                'product_id' => $orderDetails['product_id'],
                'name' => $orderDetails['name'],
                'price' => $orderDetails['price'],
                'quantity' => $orderDetails['quantity'],
                'total' => $orderDetails['total'],
                'sku' => $orderDetails['sku'],
                'images' => $productImages,
                'productDecorationSettingData' => $productDecorationSettingData,
            ];
        }
        return $lineOrders;
    }
}
