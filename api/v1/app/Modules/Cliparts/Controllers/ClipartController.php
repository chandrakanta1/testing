<?php
/**
 * Manage Cliparts
 *
 * PHP version 5.6
 *
 * @category  Clipart
 * @package   Eloquent
 * @author    Tanmaya Patra <tanmayap@riaxe.com>
 * @copyright 2019-2020 Riaxe Systems
 * @license   http://www.php.net/license/3_0.txt  PHP License 3.0
 * @link      http://inkxe-v10.inkxe.io/xetool/admin
 */

namespace App\Modules\Cliparts\Controllers;

use App\Components\Controllers\Component as ParentController;
use App\Modules\Cliparts\Models\Clipart;
use App\Modules\Cliparts\Models\ClipartCategory as Category;
use App\Modules\Cliparts\Models\ClipartCategoryRelation;
use App\Modules\Cliparts\Models\ClipartTagRelation;
use App\Modules\Cliparts\Models\ClipartTag as Tag;

/**
 * Clipart Controller
 *
 * @category Class
 * @package  Clipart
 * @author   Tanmaya Patra <tanmayap@riaxe.com>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://inkxe-v10.inkxe.io/xetool/admin
 */

class ClipartController extends ParentController
{
    /**
     * POST: Save Clipart
     *
     * @param $request  Slim's Request object
     * @param $response Slim's Response object
     *
     * @author tanmayap@riaxe.com
     * @date   12 Aug 2019
     * @return json response wheather data is saved or any error occured
     */
    public function saveCliparts($request, $response)
    {
        $serverStatusCode = OPERATION_OKAY;
        $getStoreDetails = get_store_details($request);
        $jsonResponse = [
            'status' => 0,
            'message' => message('Cliparts', 'error')
        ];
        $allPostPutVars = $request->getParsedBody();
        // save file if request contain files
        $allFileNames = [];
        $allFileNames = mutiple_upload('upload', path('abs', 'vector'), false);
        $saveClipartList = [];
        $success = 0;
        foreach ($allFileNames as $eachFileKey => $eachFile) {
            $clipartLastInsertId = 0;
            if (!empty($eachFile) && $eachFile != "") {
                $saveClipartList[$eachFileKey] = [
                    'store_id' => $getStoreDetails['store_id'],
                    'name' => $allPostPutVars['name'],
                    'price' => $allPostPutVars['price'],
                    'width' => $allPostPutVars['width'],
                    'height' => $allPostPutVars['height'],
                    'file_name' => $eachFile,
                    'is_scaling' => $allPostPutVars['is_scaling'],
                ];
                $saveEachClipart = new Clipart($saveClipartList[$eachFileKey]);
                if ($saveEachClipart->save()) {
                    $clipartLastInsertId = $saveEachClipart->xe_id;
                    /**
                     * Save category and subcategory data
                     * Category id format: [4,78,3]
                     */
                    if (isset($allPostPutVars['categories']) 
                        && $allPostPutVars['categories'] != ""
                    ) {
                        $categoryIds = $allPostPutVars['categories'];
                        $this->_SaveClipartCategories(
                            $clipartLastInsertId, $categoryIds
                        );
                    }
                    /**
                     * Save tags with respect to the cliparts
                     * Tag Names format : tag1,tag2,tag3
                     */
                    if (isset($allPostPutVars['tags']) 
                        && $allPostPutVars['tags'] != ""
                    ) {
                        $tags = $allPostPutVars['tags'];
                        $this->_SaveClipartTags($clipartLastInsertId, $tags);
                    }
                    $success++;
                }
            }
        }
        if (!empty($success) && $success > 0) {
            $jsonResponse = [
                'status' => 1,
                'message' => message('Clipart', 'saved')
            ];
        }
        
        return response(
            $response, ['data' => $jsonResponse, 'status' => $serverStatusCode]
        );
    }

    /**
     * GET: List of Clipart
     *
     * @param $request  Slim's Request object
     * @param $response Slim's Response object
     * @param $args     Slim's Argument parameters
     *
     * @author tanmayap@riaxe.com
     * @date   13 Aug 2019
     * @return All/Single Clipart List
     */
    public function getCliparts($request, $response, $args)
    {
        $serverStatusCode = OPERATION_OKAY;
        $jsonResponse = [
            'status' => 1,
            'data' => [],
            'message' => message('Clipart', 'not_found'),
        ];
        // Get Store Specific Details from helper
        $getStoreDetails = get_store_details($request);
        $clipartInit = new Clipart();
        $getCliparts = $clipartInit->where('xe_id', '>', 0);
        if (!empty($args) && $args['id'] > 0) {
            $clipartId = $args['id'];
            $getCliparts->where('xe_id', '=', $clipartId)
                ->select(
                    'xe_id', 'name', 'price', 'width', 'height',
                    'file_name', 'color_used', 'is_scaling',
                    'store_id', 'user_id'
                );

            $getCategories = $this->getCategoriesById(
                'Cliparts', 'ClipartCategoryRelation', 'clipart_id', $clipartId
            );
            $getTags = $this->getTagsById(
                'Cliparts', 'ClipartTagRelation', 'clipart_id', $clipartId
            );

            if ($getCliparts->count() > 0) {
                $getClipart = $getCliparts->first()->toArray();
                $getClipart['categories'] = $getCategories;
                $getClipart['tags'] = $getTags;
                
                // Unset category_name Key in case of single record fetch
                unset($getClipart['category_names']);

                $jsonResponse = [
                    'status' => 1,
                    'records' => 1,
                    'data' => [
                        $getClipart
                    ]
                ];
            }
        } else {
            //All Filter columns from url
            $page = $request->getQueryParam('page');
            $perpage = $request->getQueryParam('perpage');
            $categoryId = $request->getQueryParam('category');
            $sortBy = (
                !empty($request->getQueryParam('sortby')) 
                && $request->getQueryParam('sortby') != null
            ) ? $request->getQueryParam('sortby') : 'xe_id';
            $order = (
                !empty($request->getQueryParam('order')) 
                && $request->getQueryParam('order') != null
            ) ? $request->getQueryParam('order') : 'desc';
            $name = $request->getQueryParam('name');

            $getCliparts->select(
                'xe_id', 'name', 'height', 'width', 'price', 'file_name', 'is_scaling'
            );
            $getCliparts->where(['store_id' => $getStoreDetails['store_id']]);
            // Searching as per clipart name, category name & tag name
            if (isset($name) && $name != "") {
                $getCliparts->where(
                    function ($query) use ($name) {
                        $query->where('name', 'LIKE', '%' . $name . '%')
                            ->orWhereHas(
                                'clipartTags.tag', function ($q) use ($name) {
                                    return $q->where(
                                        'name', 'LIKE', '%' . $name . '%'
                                    );
                                }
                            )->orWhereHas(
                                'clipartCategory.category', function ($q) use ($name) {
                                    return $q->where(
                                        'name', 'LIKE', '%' . $name . '%'
                                    );
                                }
                            );
                    }
                );
            }
            // Filter by Category IDs and It's Subcategory IDs
            if (isset($categoryId) && $categoryId != "") {
                $searchCategories = json_clean_decode($categoryId, true);
                $getCliparts->whereHas(
                    'clipartCategory', function ($q) use ($searchCategories) {
                        return $q->whereIn('category_id', $searchCategories);
                    }
                );
            }
            // Total records including all filters
            $getTotalPerFilters = $getCliparts->count();
            // Pagination Data
            $offset = 0;
            if (isset($page) && $page != "") {
                $totalItem = empty($perpage) ? PAGINATION_MAX_ROW : $perpage;
                $offset = $totalItem * ($page - 1);
                $getCliparts->skip($offset)->take($totalItem);
            }
            // Sorting All records by column name and sord order parameter
            if (isset($sortBy) && $sortBy != "" && isset($order) && $order != "") {
                $getCliparts->orderBy($sortBy, $order);
            }
            if ($getTotalPerFilters > 0) {
                $cliparts = $getCliparts->get();
                $jsonResponse = [
                    'status' => 1,
                    'records' => count($cliparts),
                    'total_records' => $getTotalPerFilters,
                    // Convert object to Array
                    'data' => $cliparts->toArray(),
                ];
            }
        }

        return response(
            $response, ['data' => $jsonResponse, 'status' => $serverStatusCode]
        );
    }
    /**
     * PUT: Update a single clipart
     *
     * @param $request  Slim's Request object
     * @param $response Slim's Response object
     * @param $args     Slim's Argument parameters
     *
     * @author tanmayap@riaxe.com
     * @date   13 Aug 2019
     * @return json response wheather data is updated or not
     */
    public function updateClipart($request, $response, $args)
    {
        $serverStatusCode = OPERATION_OKAY;
        $jsonResponse = [
            'status' => 0,
            'message' => message('Clipart', 'error')
        ];
        // Get Store Specific Details from helper
        $getStoreDetails = get_store_details($request);
        $allPostPutVars = $updateData = $this->parsePut();

        if (!empty($args['id']) && $args['id'] > 0) {
            $clipartId = $args['id'];
            
            $clipartInit = new Clipart();
            $getOldClipart = $clipartInit->where('xe_id', '=', $clipartId);
            if ($getOldClipart->count() > 0) {
                unset(
                    $updateData['id'], $updateData['tags'],
                    $updateData['categories'], $updateData['upload'],
                    $updateData['clipartId']
                );
             
                // delete old file
                $this->deleteOldFile(
                    'cliparts', 'file_name', ['xe_id' => $clipartId], path(
                        'abs', 'vector'
                    )
                );
                
                $getUploadedFileName = save_file('upload', path('abs', 'vector'));
                
                if (!empty($getUploadedFileName) && $getUploadedFileName != null) {
                    $updateData['file_name'] = $getUploadedFileName;
                }

                $updateData['store_id'] = $getStoreDetails['store_id'];
                // Update record into the database
                try {
                    $clipartInit = new Clipart();
                    $clipartInit->where('xe_id', '=', $clipartId)
                        ->update($updateData);
                    $jsonResponse = [
                        'status' => 1,
                        'message' => message('Clipart', 'updated')
                    ];
                    /**
                     * Save category and subcategory data
                     * Category id format: [4,78,3]
                     */
                    if (isset($allPostPutVars['categories']) 
                        && $allPostPutVars['categories'] != ""
                    ) {
                        $categoryIds = $allPostPutVars['categories'];
                        $this->_SaveClipartCategories($clipartId, $categoryIds);
                    }
                    /**
                     * Save tags with respect to the cliparts
                     * Tag Names format : tag1,tag2,tag3
                     */
                    $tags = (isset($allPostPutVars['tags']) &&
                     $allPostPutVars['tags'] != "") ? $allPostPutVars['tags'] : "";
                    $this->_SaveClipartTags($clipartId, $tags);
                } catch (\Exception $e) {
                    $serverStatusCode = EXCEPTION_OCCURED;
                    $jsonResponse['exception'] = show_exception() === true 
                        ? $e->getMessage() : '';
                }
            }
        }
        return response(
            $response, ['data' => $jsonResponse, 'status' => $serverStatusCode]
        );
    }

    /**
     * DELETE: Delete a clipart along with all the tags and categories
     *
     * @param $request  Slim's Argument parameters
     * @param $response Slim's Response object
     * @param $args     Slim's Argument parameters
     *
     * @author tanmayap@riaxe.com
     * @date   13 Aug 2019
     * @return json response wheather data is deleted or not
     */
    public function deleteClipart($request, $response, $args)
    {
        $serverStatusCode = OPERATION_OKAY;
        $jsonResponse = [
            'status' => 0,
            'message' => message('Clipart', 'error')
        ];
        if (!empty($args) && $args != null) {
            // Multiple Ids in json format
            $getDeleteIds = $args['id'];
            $getDeleteIdsToArray = json_clean_decode($getDeleteIds, true);
            $totalCount = count($getDeleteIdsToArray);
            if (!empty($getDeleteIdsToArray) 
                && count($getDeleteIdsToArray) > 0
            ) {
                $clipartInit = new Clipart();
                if ($clipartInit->whereIn('xe_id', $getDeleteIdsToArray)->count() > 0
                ) {
                    try {
                        $success = 0;
                        foreach ($getDeleteIdsToArray as $clipartId) {
                            // Delete from Directory
                            $this->deleteOldFile(
                                'cliparts', 'file_name', [
                                    'xe_id' => $clipartId
                                ], path('abs', 'vector')
                            );
                            $clipartInit->where('xe_id', $clipartId)->delete();
                            $success++;
                        }
                        $jsonResponse = [
                            'status' => 1,
                            'message' => $success . ' out of ' . $totalCount .
                             ' Clipart(s) deleted successfully',
                        ];
                    } catch (\Exception $e) {
                        $serverStatusCode = EXCEPTION_OCCURED;
                        $jsonResponse['exception'] = show_exception() === true 
                            ? $e->getMessage() : '';
                    }
                }
            }
        }

        return response(
            $response, ['data' => $jsonResponse, 'status' => $serverStatusCode]
        );
    }

    /**
     * Save Tags and Clipart-Tag Relations
     *
     * @param $clipartId    Clipart's ID
     * @param $multipletags (in comma separated)
     *
     * @author tanmayap@riaxe.com
     * @date   13 Aug 2019
     * @return boolean
     */
    public function _SaveClipartTags($clipartId, $multipletags)
    {
        // Save Clipart and tags relation
        if (isset($multipletags) && $multipletags != "") {
            $tags = $multipletags;
            $updatedTagIds = [];
            $tagsStringToArray = explode(',', $tags);
            foreach ($tagsStringToArray as $tag) {
                // Save each individual Tag to table
                $tagInit = new Tag();
                if ($tagInit->where(['name' => trim($tag)])->count() == 0) {
                    $saveTag = new Tag(['name' => trim($tag)]);
                    $saveTag->save();
                    $tagLastInsertId = $saveTag->xe_id;
                } else {
                    $getTagDetails = $tagInit->where(
                        ['name' => trim($tag)]
                    )->select('xe_id')->first();
                    $tagLastInsertId = $getTagDetails['xe_id'];
                }
                if (isset($tagLastInsertId) && $tagLastInsertId > 0) {
                    $updatedTagIds[] = $tagLastInsertId;
                }
            }

            // Start SYNC Tags into Clipart_Tag Relationship Table
            $clipartInit = new Clipart();
            $findClipart = $clipartInit->find($clipartId);
            if ($findClipart->tags()->sync($updatedTagIds)) {
                return true;
            }
            return false;
        } else {
            // If user requests blank/no tags
            $tagRelInit = new ClipartTagRelation();
            $clipartTags = $tagRelInit->where('clipart_id', $clipartId);
            if ($clipartTags->delete()) {
                return true;
            }
            return false;
        }
    }

    /**
     * Save Categories/Sub-categories and Clipart-Category Relations
     *
     * @param $clipartId   Clipart's ID
     * @param $categoryIds (in comma separated)
     *
     * @author tanmayap@riaxe.com
     * @date   13 Aug 2019
     * @return boolean
     */
    private function _SaveClipartCategories($clipartId, $categoryIds)
    {
        $getAllCategoryArr = json_clean_decode($categoryIds, true);
        // SYNC Categories to the Clipart_Category Relationship Table
        $clipartInit = new Clipart();
        $findClipart = $clipartInit->find($clipartId);
        if ($findClipart->categories()->sync($getAllCategoryArr)) {
            return true;
        }
        return false;
    }
    
    /**
     * Check Category Relation with Category
     *
     * @param $request  Slim's Argument parameters
     * @param $response Slim's Response object
     * @param $args     Slim's Argument parameters
     *
     * @author satyabratap@riaxe.com
     * @date   20 Jan 2020
     * @return boolean
     */
    public function checkRelCategory($request, $response, $args)
    {
        $serverStatusCode = OPERATION_OKAY;
        $jsonResponse = [
            'status' => 0,
            'message' => 'This Category is associated with an Item.'
        ];
        $key = $request->getQueryParam('key');
        if (!empty($args) && $args['id'] > 0) {
            $categoryId = $args['id'];
            $checkCatRelInit = new ClipartCategoryRelation();
            $getCatRel = $checkCatRelInit->where('category_id', $categoryId)->get();
            if ($getCatRel->count() == 0) {
                if ($key == 'disable') {
                    $jsonResponse = $this->disableCat('cliparts', $categoryId);
                } elseif ($key == 'delete') {
                    $jsonResponse = $this->deleteCat('cliparts', $categoryId, 'Cliparts', 'ClipartCategoryRelation');
                }
            }
        }
        return response(
            $response, ['data' => $jsonResponse, 'status' => $serverStatusCode]
        );
    }

    /**
     * Delete a category from the table
     *
     * @param $request  Slim's Request object
     * @param $response Slim's Response object
     * @param $args     Slim's Argument parameters
     *
     * @author satyabratap@riaxe.com
     * @date   20 Jan 2020
     * @return Delete Json Status
     */
    public function deleteCategory($request, $response, $args)
    {
        $serverStatusCode = OPERATION_OKAY;
        $jsonResponse = [
            'status' => 0,
            'message' => message('Category', 'error')
        ];
        if (!empty($args) && $args['id'] > 0) {
            $categoryId = $args['id'];
            $jsonResponse = $this->deleteCat('cliparts', $categoryId, 'Cliparts', 'ClipartCategoryRelation');
        }
        return response(
            $response, ['data' => $jsonResponse, 'status' => $serverStatusCode]
        );
    }

}
