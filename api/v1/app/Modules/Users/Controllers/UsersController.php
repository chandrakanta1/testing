<?php
/**
 * Manage User
 *
 * PHP version 5.6
 *
 * @category  Users
 * @package   Users
 * @author    Ramasankar <ramasankarm@riaxe.com>
 * @copyright 2019-2020 Riaxe Systems
 * @license   http://www.php.net/license/3_0.txt  PHP License 3.0
 * @link      http://inkxe-v10.inkxe.io/xetool/admin
 */

namespace App\Modules\Users\Controllers;

use App\Components\Controllers\Component as ParentController;
use App\Modules\Users\Models\Privileges;
use App\Modules\Users\Models\User;
use App\Modules\Users\Models\UserPrivilegesRel;
use App\Modules\Users\Models\UserRole;
use App\Modules\Users\Models\UserRolePrivilegesRel;
use App\Modules\Users\Models\UserRoleRel;
use \Firebase\JWT\JWT;
use \Illuminate\Database\Capsule\Manager as DB;

/**
 * User Controller
 *
 * @category Class
 * @package  Users
 * @author   Ramasankar <ramasankarm@riaxe.com>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://inkxe-v10.inkxe.io/xetool/admin
 */
class UsersController extends ParentController
{
    /**
     * POST: Authenticate user for login
     *
     * @param $request  Slim's Request object
     * @param $response Slim's Response object
     * @param $args     Slim's Argument parameters
     *
     * @author tanmayap@riaxe.com
     * @date   5 Oct 2019
     * @return User details
     */
    public function login($request, $response)
    {
        $serverStatusCode = OPERATION_OKAY;
        $jsonResponse = ['status' => 0];
        $allPostPutVars = $request->getParsedBody();
        if (isset($allPostPutVars) && count($allPostPutVars) > 0 && isset($allPostPutVars['email']) && isset($allPostPutVars['password'])) {
            $userData = [
                'email' => $allPostPutVars['email'],
                'password' => $allPostPutVars['password'],
            ];
            $userObj = new User();
            $getUserDetails = $userObj->with('userRoles');
            $getUser = $getUserDetails->where(['email' => $userData['email']])->first();
            if (isset($getUser) && $getUser['email'] != "" && $getUser['xe_id'] != "") {
                // Compare the Databse password and user's password by hashing method
                if (password_verify($userData['password'], $getUser['password'])) {
                    $privileges = [];
                    $roleId = $getUser['userRoles'][0]['role_id'];
                    if($roleId > 1) {
                        $userPrivilegesRel = new UserPrivilegesRel();
                        $userPrivilleges = $userPrivilegesRel->where('user_id', $getUser['xe_id'])->get();
                        if(count($userPrivilleges) > 0) {
                            $privileges = DB::table('user_privileges_rel')
                                        ->join('user_privileges', 'user_privileges_rel.privilege_id', '=', 'user_privileges.xe_id')
                                        ->where('user_privileges_rel.user_id', '=', $getUser['xe_id'])
                                        ->get(['user_privileges.module_name AS name', 'user_privileges_rel.privilege_type AS privilege']);
                        } else {  
                            $privileges = DB::table('user_role_rel')
                                        ->join('user_role_privileges_rel', 'user_role_privileges_rel.role_id', '=', 'user_role_rel.role_id')
                                        ->join('user_privileges', 'user_role_privileges_rel.privilege_id', '=', 'user_privileges.xe_id')
                                        ->where('user_role_rel.user_id', '=', $getUser['xe_id'])
                                        ->get(['user_privileges.module_name AS name', 'user_role_privileges_rel.privilege_type AS privilege']);
                        }
                    }

                    $token = array(
                        "iss" => isset($getUser['email']) ? $getUser['email'] : "INKXE", // Issuer
                        "aud" => "http://inkxe.com", // Audience
                        "iat" => date_time('today', [], 'timestamp'), // Issued-at time
                        "exp" => date_time('add', ['days' => 60], 'timestamp'),
                        "data" => [
                            "user_id" => isset($getUser['xe_id']) ? $getUser['xe_id'] : 0,
                        ]
                    );

                    $jwtObj = new JWT();
                    $jwt = $jwtObj->encode($token, $this->secret);

                    $jsonResponse = [
                        'status' => 1,
                        'message' => "Login Successfull",
                        'jwt' => $jwt,
                        'expired_at' => $token['exp'],
                        'role_id' => $roleId,
                        'privileges' => $privileges
                    ];
                } else {
                    $jsonResponse['message'] = message('Auth', 'invalid_login');
                }
            } else {
                $jsonResponse['message'] = message('Auth', 'invalid_login');
            }
        }
        return response($response, ['data' => $jsonResponse, 'status' => $serverStatusCode]);
    }
    /**
     * GET: List of Privileges
     *
     * @param $request  Slim's Request object
     * @param $response Slim's Response object
     * @param $args     Slim's Argument parameters
     *
     * @author ramasankarm@riaxe.com
     * @date   06 Jan 2020
     * @return All Privileges List
     */
    public function getPrivileges($request, $response, $args)
    {
        $serverStatusCode = OPERATION_OKAY;
        $jsonResponse = ['status' => 0];
        // Get Store Specific Details from helper
        $getStoreDetails = get_store_details($request);
        // Set default Store ID
        if (isset($getStoreDetails['store_id']) && $getStoreDetails['store_id'] != '') {
            $storeId = $getStoreDetails['store_id'];
        }
        $privilegesObj = new Privileges();
        $privilegesObj->where('status', '=', 1);
        $totalCounts = $privilegesObj->count();
        $privilegesObj->select('xe_id', 'store_id', 'module_name');
        // if store_id is on request
        if ($storeId != '') {
            $privilegesObj->where('store_id', '=', $getStoreDetails['store_id']);
        }

        if ($totalCounts > 0) {
            $jsonResponse['data'] = $privilegesObj->orderBy('module_name', 'asc')->get();
            $info = [
                'status' => 1,
                'records' => count($jsonResponse['data']),
            ];
            // Merge 2 arrays
            $jsonResponse = $info + $jsonResponse;
        } else {
            $jsonResponse = [
                'status' => 1,
                'data' => [],
                'message' => message('Privileges', 'not_found'),
            ];
        }
        return response($response, ['data' => $jsonResponse, 'status' => $serverStatusCode]);
    }

    /**
     * GET: List of user roles/ a single user role
     *
     * @param $request  Slim's Request object
     * @param $response Slim's Response object
     * @param $args     Slim's Argument parameters
     *
     * @author ramasankarm@riaxe.com
     * @date   06 Jan 2020
     * @return All User Role List
     */

    public function getUserRoles($request, $response, $args)
    {
        $serverStatusCode = OPERATION_OKAY;
        $jsonResponse = ['status' => 0];
        // Get Store Specific Details from helper
        $getStoreDetails = get_store_details($request);

        $userRoleObj = new UserRole();
        $userRoles = $userRoleObj->with('privileges');
        // Set default Store ID
        if (isset($getStoreDetails['store_id']) && $getStoreDetails['store_id'] != '') {
            $userRoles->where('store_id', '=', $getStoreDetails['store_id']);
            $userRoles->where('xe_id', '<>', 1);
        } else {
            $userRoles->where('xe_id', '<>', 1);
        }
        // Condition for single user role
        if (isset($args['id']) && $args['id'] != "") {
            $userRoles->where('xe_id', '=', $args['id']);
        }
        if ($userRoles->count() > 0) {
            $allUserRoles = $userRoles->orderBy('xe_id', 'desc')->get();
            $userRoles = [];
            foreach ($allUserRoles as $key => $userRole) {
                $userRoles[$key] = [
                    'id' => $userRole['xe_id'],
                    'store_id' => $userRole['store_id'],
                    'role_name' => $userRole['role_name'],
                    'privileges' => $userRole['privileges'],
                ];
            }

            // Condition for single user role
            if (isset($args['id']) && $args['id'] != "") {
                $userRoles = $userRoles[0];
            }
            
            $jsonResponse['data'] = $userRoles;
            $info = [
                'status' => 1,
                'records' => count($jsonResponse['data']),
            ];
            // Merge 2 arrays
            $jsonResponse = $info + $jsonResponse;
        } else {
            $jsonResponse = [
                'status' => 1,
                'data' => [],
                'message' => message('User Role', 'not_found'),
            ];
        }
        return response($response, ['data' => $jsonResponse, 'status' => $serverStatusCode]);
    }

    /**
     * POST: Save user role
     *
     * @param $request  Slim's Request object
     * @param $response Slim's Response object
     * @param $args     Slim's Argument parameters
     *
     * @author ramasankarm@riaxe.com
     * @date   06 Jan 2020
     * @return json response wheather data is saved or any error occured
     */
    public function saveUserRole($request, $response, $args)
    {
        $serverStatusCode = OPERATION_OKAY;
        $jsonResponse = ['status' => 0];
        $allPostPutVars = $request->getParsedBody();
        $roleName = $allPostPutVars['role_name'];
        if ($roleName != "") {
            $userRoleObj = new UserRole();
            $rows = $userRoleObj->where('role_name', '=', $roleName)->get();
            if (count($rows) == 0) {
                $getStoreDetails = get_store_details($request);
                // Set default Store ID
                if (isset($getStoreDetails['store_id']) && $getStoreDetails['store_id'] != '') {
                    $storeId = $getStoreDetails['store_id'];
                }
                $getPostPutData = [
                    'store_id' => $storeId,
                    'role_name' => $roleName,
                ];
                $saveUserRole = new UserRole($getPostPutData);
                try {
                    if ($saveUserRole->save()) {
                        $jsonResponse = [
                            'status' => 1,
                            'message' => 'User Role saved successfully',
                            'user_role_id' => $saveUserRole->xe_id,
                        ];
                    }
                } catch (\Exception $e) {
                    $serverStatusCode = EXCEPTION_OCCURED;                    
                    $jsonResponse['message'] = message('User Role', 'exception');
                    $jsonResponse['exception'] = show_exception() === true ? $e->getMessage() : '';
                }
            } else {
                $jsonResponse['message'] = message('User Role', 'exist');
            }
        }
        return response($response, ['data' => $jsonResponse, 'status' => $serverStatusCode]);
    }

    /**
     * PUT: Update user role
     *
     * @param $request  Slim's Request object
     * @param $response Slim's Response object
     * @param $args     Slim's Argument parameters
     *
     * @author ramasankarm@riaxe.com
     * @date   06 Jan 2020
     * @return json response wheather data is updated or any error occured
     */
    public function updateUserRole($request, $response, $args)
    {
        $serverStatusCode = OPERATION_OKAY;
        $jsonResponse = ['status' => 0];
        $allPostPutVars = $this->parsePut();
        $roleName = $allPostPutVars['role_name'];
        // Id should be greater than 1 because it is the super admin role, so can't be updated.
        if (isset($args['id']) && $args['id'] > 1) {
            $userRoleObj = new UserRole();
            $getUserRole = $userRoleObj->where('role_name', '=', $roleName);
            $getUserRole->where('xe_id', '<>', $args['id']);
            if ($getUserRole->count() == 0) {
                $updateData = ['role_name' => $roleName];
                $userRoleObj = new UserRole();
                $getOldRow = $userRoleObj->where('xe_id', '=', $args['id']);
                if ($getOldRow->count() > 0) {
                    try {
                        $userRoleObj->where('xe_id', '=', $args['id'])->update($updateData);
                        $jsonResponse = [
                            'status' => 1,
                            'message' => message('User Role', 'updated'),
                        ];
                    } catch (\Exception $e) {
                        $serverStatusCode = EXCEPTION_OCCURED;
                        $jsonResponse['message'] = message('User Role', 'exception');
                        $jsonResponse['exception'] = show_exception() === true ? $e->getMessage() : '';
                    }
                }
            } else {  
                $jsonResponse['message'] = message('User Role', 'exist');
            }
        }
        return response($response, ['data' => $jsonResponse, 'status' => $serverStatusCode]);
    }

    /**
     * POST: Save user role privileges
     *
     * @param $request  Slim's Request object
     * @param $response Slim's Response object
     * @param $args     Slim's Argument parameters
     *
     * @author ramasankarm@riaxe.com
     * @date   07 Jan 2020
     * @return json response wheather data is saved or any error occured
     */
    public function saveUserRolePrivileges($request, $response, $args)
    {
        $serverStatusCode = OPERATION_OKAY;
        $jsonResponse = ['status' => 0];
        $allPostPutVars = $request->getParsedBody();
        if (!empty($allPostPutVars['data'])) {
            $privileges = json_decode($allPostPutVars['data'], true);
            $userRoleId = $args['id'];
            try {
                $userRolePrivilegesRelObj = new UserRolePrivilegesRel();
                $userRolePrivilegesRelObj->where('role_id', $userRoleId)->delete();
                foreach ($privileges as $privillege) {
                    if(!empty($privillege)) {
                        $postData = [
                            'role_id' => $userRoleId,
                            'privilege_id' => $privillege['id'],
                            'privilege_type' => json_encode($privillege['privilege_type']),
                        ];
                        $saveUserRolePrivilage = new UserRolePrivilegesRel($postData);
                        $saveUserRolePrivilage->save();
                    }
                }
                $jsonResponse = [
                    'status' => 1,
                    'message' => 'User Role Privilege saved successfully',
                ];
            } catch (\Exception $e) {
                $serverStatusCode = EXCEPTION_OCCURED;
                $jsonResponse['message'] = message('User Role Privilege', 'exception');
                $jsonResponse['exception'] = show_exception() === true ? $e->getMessage() : '';
            }
        }
        return response($response, ['data' => $jsonResponse, 'status' => $serverStatusCode]);
    }
    /**
     * DELETE: Delete user roles along with its relationship with privileges
     *
     * @param $response Slim's Response object
     * @param $args     Slim's Argument parameters
     *
     * @author ramasankarm@riaxe.com
     * @date   07 jan 2020
     * @return json response wheather data is deleted or not
     */
    public function deleteUserRole($request, $response, $args)
    {
        $serverStatusCode = OPERATION_OKAY;
        $jsonResponse = ['status' => 0];
        if (isset($args['id']) && $args['id'] > 0) {
            $getDeleteIds = $args['id'];
            $getDeleteIdsToArray = json_decode($getDeleteIds);
            if (isset($getDeleteIdsToArray) && is_array($getDeleteIdsToArray) && count($getDeleteIdsToArray) > 0 && !in_array(1, $getDeleteIdsToArray)) {
                $userRoleObj = new UserRole();
                if ($userRoleObj->whereIn('xe_id', $getDeleteIdsToArray)->count() > 0) {
                    $userRoleRelObj = new UserRoleRel();
                    if($userRoleRelObj->whereIn('role_id', $getDeleteIdsToArray)->count() == 0) {
                        try {
                            $userRoleObj->whereIn('xe_id', $getDeleteIdsToArray)->delete();
                            $userRolePrivilegesRelObj = new UserRolePrivilegesRel();
                            $userRolePrivilegesRelObj->whereIn('role_id', $getDeleteIdsToArray)->delete();
                            $jsonResponse = [
                                'status' => 1,
                                'message' => message('User Role', 'deleted'),
                            ];
                        } catch (\Exception $e) {
                            $serverStatusCode = EXCEPTION_OCCURED;
                            $jsonResponse['message'] = message('User Role', 'exception');
                            $jsonResponse['exception'] = show_exception() === true ? $e->getMessage() : '';
                        }
                    } else {
                        $jsonResponse['message'] = "This user type is already assigned to  a user";
                    }
                }
            }
        }
        return response($response, ['data' => $jsonResponse, 'status' => $serverStatusCode]);
    }

    /**
     * GET: List of users/ a single user
     *
     * @param $request  Slim's Request object
     * @param $response Slim's Response object
     * @param $args     Slim's Argument parameters
     *
     * @author ramasankarm@riaxe.com
     * @date   076 Jan 2020
     * @return All User List
     */

    public function getUsers($request, $response, $args)
    {
        $serverStatusCode = OPERATION_OKAY;
        $jsonResponse = ['status' => 0];

        $name = $request->getQueryParam('name');
        $page = $request->getQueryParam('page');
        $perpage = $request->getQueryParam('perpage');
        $userType = $request->getQueryParam('userType');
        // Get Store Specific Details from helper
        $getStoreDetails = get_store_details($request);
        $userObj = new User();
        $getUsers = $userObj->with('userRoles', 'hasPrivileges');
        // Set default Store ID
        if (isset($getStoreDetails['store_id']) && $getStoreDetails['store_id'] != '') {
            $getUsers->where('store_id', '=', $getStoreDetails['store_id']);
        }

        if (isset($name) && $name != "") {
            $getUsers->where('name', 'LIKE', '%' . $name . '%');
        }

        // Condition for single user role
        if (isset($args['id']) && $args['id'] != "") {
            $getUsers->where('xe_id', '=', $args['id']);
        }
        // Check whether users are available or not
        if ($getUsers->count() > 0) {

            if (isset($page) && $page != "") {
                $offset = $perpage * ($page - 1);
                $getUsers->skip($offset)->take($perpage);
            }

            $getUsers->orderBy('xe_id', 'desc');
            
            $allUsers = $getUsers->get();
            $users = [];
            foreach ($allUsers as $key => $user) {
                $users[$key] = [
                    'id' => $user['xe_id'],
                    'store_id' => $user['store_id'],
                    'name' => $user['name'],
                    'email' => $user['email'],
                    'role_id' => (!empty($user['userRoles'][0]['role_id']) && $user['userRoles'][0]['role_id'] > 0) ? $user['userRoles'][0]['role_id'] : null,
                ];
                if (count($user['hasPrivileges']) > 0) {
                    foreach ($user['hasPrivileges'] as $k => $privilege) {
                        $users[$key]['privileges'][$k] = [
                            'id' => $privilege['privilege_id'],
                            'privilege_type' => $privilege['privilege_type'],
                        ];
                    }
                } else {
                    $users[$key]['privileges'] = [];
                }
            }
            // Condition for single user role
            if (isset($args['id']) && $args['id'] != "") {
                $users = $users[0];
            }
            $jsonResponse['data'] = $users;
            $info = [
                'status' => 1,
                'records' => count($jsonResponse['data']),
            ];
            // Merge 2 arrays
            $jsonResponse = $info + $jsonResponse;
        } else {
            $jsonResponse = [
                'status' => 1,
                'data' => [],
                'message' => message('User Role', 'not_found'),
            ];
        }
        return response($response, ['data' => $jsonResponse, 'status' => $serverStatusCode]);
    }

    /**
     * POST: Save user
     *
     * @param $request  Slim's Request object
     * @param $response Slim's Response object
     * @param $args     Slim's Argument parameters
     *
     * @author ramasankarm@riaxe.com
     * @date   07 Jan 2020
     * @return json response wheather data is saved or any error occured
     */
    public function saveUser($request, $response, $args)
    {
        $serverStatusCode = OPERATION_OKAY;
        $jsonResponse = ['status' => 0];
        $allPostPutVars = $request->getParsedBody();
        if (!empty($allPostPutVars['data'])) {
            $getAllFormData = json_decode($allPostPutVars['data'], true);
            if ($getAllFormData['name'] != "" && $getAllFormData['email'] != "" && $getAllFormData['password'] != "" && $getAllFormData['role_id'] != "" && $getAllFormData['role_id'] > 1) {
                $user = new User();
                //Check email, whether exist or not.
                $getUserFromDb = $user->where('email', '=', $getAllFormData['email']);
                if ($getUserFromDb->count() == 0) {
                    $getPostPutData = [];
                    $getStoreDetails = get_store_details($request);
                    // Set default Store ID
                    if (isset($getStoreDetails['store_id']) && $getStoreDetails['store_id'] != '') {
                        $storeId = $getStoreDetails['store_id'];
                        $getPostPutData += ['store_id' => $storeId];
                    }
                    $getPostPutData += [
                        'name' => $getAllFormData['name'],
                        'email' => $getAllFormData['email'],
                        'password' => password_hash($getAllFormData['password'], PASSWORD_BCRYPT),
                        'avatar' => "",
                        'created_at' => date('Y-m-d H:i:s'),
                    ];
                    $saveUser = new User($getPostPutData);
                    try {
                        if ($saveUser->save()) {
                            //Set user role
                            $userRoleRelData = [
                                'user_id' => $saveUser->xe_id,
                                'role_id' => $getAllFormData['role_id'],
                            ];
                            $saveUserRoleRel = new UserRoleRel($userRoleRelData);
                            $saveUserRoleRel->save();
                            $jsonResponse = [
                                'status' => 1,
                                'message' => 'User saved successfully',
                                'user_id' => $saveUser->xe_id,
                            ];
                        }
                    } catch (\Exception $e) {
                        $serverStatusCode = EXCEPTION_OCCURED;                        
                        $jsonResponse['message'] = message('User', 'exception');
                        $jsonResponse['exception'] = show_exception() === true ? $e->getMessage() : '';
                    }
                } else {
                    $jsonResponse['message'] = message('User', 'exist');
                }
            }
        }
        return response($response, ['data' => $jsonResponse, 'status' => $serverStatusCode]);
    }

    /**
     * PUT: Update user
     *
     * @param $request  Slim's Request object
     * @param $response Slim's Response object
     * @param $args     Slim's Argument parameters
     *
     * @author ramasankarm@riaxe.com
     * @date   06 Jan 2020
     * @return json response wheather data is updated or any error occured
     */
    public function updateUser($request, $response, $args)
    {
        $serverStatusCode = OPERATION_OKAY;
        $jsonResponse = ['status' => 0];
        $allPostPutVars = $this->parsePut();
        if (isset($args['id']) && $args['id'] > 0) {
            $getAllFormData = json_decode($allPostPutVars['data'], true);
            //if ($getAllFormData['name'] != "" && $getAllFormData['role_id'] != "") {
            if ($getAllFormData['name'] != "" && $getAllFormData['email'] != "" && $getAllFormData['role_id'] != "" && $getAllFormData['role_id'] > 1) {
                $userObj = new User();
                $getUserFromDb = $userObj->where('email', '=', $getAllFormData['email']);
                $getUserFromDb->where('xe_id', '<>', $args['id']);
                if ($getUserFromDb->count() == 0) {
                    try {
                        $updateData = [
                            'name' => $getAllFormData['name'],
                            'email' => $getAllFormData['email']
                        ];
                        $userObj = new User();
                        $userObj->where('xe_id', '=', $args['id'])->update($updateData);
                        // Dete user role relation table and inserting data again
                        $userRoleRelObj = new UserRoleRel();
                        $userRoleRelObj->where('user_id', $args['id'])->delete();
                        $userRoleRelData = [
                            'user_id' => $args['id'],
                            'role_id' => $getAllFormData['role_id'],
                        ];
                        $saveUserRoleRel = new UserRoleRel($userRoleRelData);
                        $saveUserRoleRel->save();
                        
                        if (isset($getAllFormData['password']) && isset($getAllFormData['confirmpassword']) && $getAllFormData['password'] != "" && $getAllFormData['confirmpassword'] != "" && $getAllFormData['password'] === $getAllFormData['confirmpassword']) {
                            $password = password_hash($getAllFormData['password'], PASSWORD_BCRYPT);
                            $updateData = ['password' => $password];
                            $userObj = new User();
                            $userObj->where('xe_id', '=', $args['id'])->update($updateData);
                        }
                        $jsonResponse = [
                            'status' => 1,
                            'message' => message('User', 'updated'),
                        ];
                    } catch (\Exception $e) {
                        $serverStatusCode = EXCEPTION_OCCURED;                        
                        $jsonResponse['message'] = message('User', 'exception');
                        $jsonResponse['exception'] = show_exception() === true ? $e->getMessage() : '';
                    }
                } else {
                    $jsonResponse['message'] = message('User', 'exist');
                }
            }
        }
        return response($response, ['data' => $jsonResponse, 'status' => $serverStatusCode]);
    }

    /**
     * DELETE: Delete user along with its relationship with user role and privileges
     *
     * @param $response Slim's Response object
     * @param $args     Slim's Argument parameters
     *
     * @author ramasankarm@riaxe.com
     * @date   07 jan 2020
     * @return json response wheather data is deleted or not
     */
    public function deleteUser($request, $response, $args)
    {
        $serverStatusCode = OPERATION_OKAY;
        $jsonResponse = ['status' => 0];
        if (isset($args['id']) && $args['id'] > 0) {
            $getDeleteIds = $args['id'];
            $getDeleteIdsToArray = json_decode($getDeleteIds);
            if (isset($getDeleteIdsToArray) && is_array($getDeleteIdsToArray) && count($getDeleteIdsToArray) > 0) {
                // Checking whether user ia a super admin or normal user.
                $userRoleRelObj = new UserRoleRel();
                $admin = $userRoleRelObj->where('role_id', '=', 1)->get();
                if(!in_array($admin[0]['user_id'], $getDeleteIdsToArray)) {
                    $userObj = new User();
                    if ($userObj->whereIn('xe_id', $getDeleteIdsToArray)->count() > 0) {
                        try {
                            $userObj->whereIn('xe_id', $getDeleteIdsToArray)->delete();
                            $userPrivilegesRelObj = new UserPrivilegesRel();
                            $userRoleRelObj = new UserRoleRel();
                            $userPrivilegesRelObj->whereIn('user_id', $getDeleteIdsToArray)->delete();
                            $userRoleRelObj->whereIn('user_id', $getDeleteIdsToArray)->delete();
                            $jsonResponse = [
                                'status' => 1,
                                'message' => message('User', 'deleted'),
                            ];
                        } catch (\Exception $e) {
                            $serverStatusCode = EXCEPTION_OCCURED;
                            $jsonResponse['message'] = message('User', 'exception');
                            $jsonResponse['exception'] = show_exception() === true ? $e->getMessage() : '';
                        }
                    }
                }                 
            }
        }
        return response($response, ['data' => $jsonResponse, 'status' => $serverStatusCode]);
    }

    /**
     * POST: Save user privileges
     *
     * @param $request  Slim's Request object
     * @param $response Slim's Response object
     * @param $args     Slim's Argument parameters
     *
     * @author ramasankarm@riaxe.com
     * @date   07 Jan 2020
     * @return json response wheather data is saved or any error occured
     */
    public function saveUserPrivileges($request, $response, $args)
    {
        $serverStatusCode = OPERATION_OKAY;
        $jsonResponse = ['status' => 0];
        $allPostPutVars = $request->getParsedBody();
        if (!empty($allPostPutVars['data'])) {
            $privileges = json_decode($allPostPutVars['data'], true);
            $userId = $args['id'];
            try {
                $userPrivilegesRel = new UserPrivilegesRel();
                $userPrivilegesRel->where('user_id', $userId)->delete();
                foreach ($privileges as $privillege) {
                    $postData = [
                        'user_id' => $userId,
                        'privilege_id' => $privillege['id'],
                        'privilege_type' => json_encode($privillege['privilege_type']),
                    ];
                    $saveUserPrivilage = new UserPrivilegesRel($postData);
                    $saveUserPrivilage->save();
                }
                $jsonResponse = [
                    'status' => 1,
                    'message' => 'User Privilege saved successfully',
                ];
            } catch (\Exception $e) {
                $serverStatusCode = EXCEPTION_OCCURED;
                $jsonResponse['message'] = message('User Privilege', 'exception');
                $jsonResponse['exception'] = show_exception() === true ? $e->getMessage() : '';
            }
        }
        return response($response, ['data' => $jsonResponse, 'status' => $serverStatusCode]);
    }
}
