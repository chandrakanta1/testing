<?php
/**
 * Login for Users
 * 
 * @category   Routes
 * @package    Eloquent
 * @author     Original Author <tanmayap@riaxe.com>
 * @author     Another Author <>
 * @copyright  2019-2020 Riaxe Systems
 * @license    http://www.php.net/license/3_0.txt  PHP License 3.0
 * @version    Release: @package_version@1.0
 */
use App\Middlewares\ValidateJWTToken as ValidateJWT;
use App\Modules\Users\Controllers\UsersController as Users;

$container = $app->getContainer();

$app->group('/user', function () use ($app) {
    $app->post('/login', Users::class . ':login'); // Token matching with login functionality
})->add(new ValidateJWT($container));


//Privileges
$app->group('/privileges', function () use ($app) {
        $app->get('', Users::class . ':getPrivileges');
    }
)->add(new ValidateJWT($container));

// User Roles

$app->group('/user-roles', function () use ($app) {
    $app->get('', Users::class . ':getUserRoles');
    $app->get('/{id}', Users::class . ':getUserRoles');
    $app->post('', Users::class . ':saveUserRole');
    $app->put('/{id}', Users::class . ':updateUserRole');
    $app->delete('/{id}', Users::class . ':deleteUserRole');
    $app->post('/{id}/privileges', Users::class . ':saveUserRolePrivileges');
}
)->add(new ValidateJWT($container));

// Users
$app->group('/users', function () use ($app) {
    $app->get('', Users::class . ':getUsers');
    $app->get('/{id}', Users::class . ':getUsers');
    $app->post('', Users::class . ':saveUser');
    $app->put('/{id}', Users::class . ':updateUser');
    $app->delete('/{id}', Users::class . ':deleteUser');
    $app->post('/{id}/privileges', Users::class . ':saveUserPrivileges');
}
)->add(new ValidateJWT($container));