<?php
/**
 * Manage Fonts
 *
 * PHP version 5.6
 *
 * @category  Fonts
 * @package   Assets
 * @author    Satyabrata <satyabratap@riaxe.com>
 * @copyright 2019-2020 Riaxe Systems
 * @license   http://www.php.net/license/3_0.txt  PHP License 3.0
 * @link      http://inkxe-v10.inkxe.io/xetool/admin
 */

namespace App\Modules\Fonts\Controllers;

use App\Components\Controllers\Component as ParentController;
use App\Dependencies\FontMeta as FontMeta;
use App\Modules\Fonts\Models\Font;
use App\Modules\Fonts\Models\FontCategory as Category;
use App\Modules\Fonts\Models\FontCategoryRelation;
use App\Modules\Fonts\Models\FontTag as Tag;
use App\Modules\Fonts\Models\FontTagRelation;

/**
 * Fonts Controller
 *
 * @category Fonts
 * @package  Assets
 * @author   Satyabrata <satyabratap@riaxe.com>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://inkxe-v10.inkxe.io/xetool/admin
 */
class FontController extends ParentController
{
    /**
     * POST: Save Font
     *
     * @param $request  Slim's Request object
     * @param $response Slim's Response object
     *
     * @author satyabratap@riaxe.com
     * @date   23 oct 2019
     * @return json response wheather data is saved or any error occured
     */
    public function saveFonts($request, $response)
    {
        $getStoreDetails = get_store_details($request);
        $serverStatusCode = OPERATION_OKAY;
        $jsonResponse = [
            'status' => 0,
            'message' => message('Color', 'error'),
        ];
        $allPostPutVars = $request->getParsedBody();

        $getUploadedFileName = save_file('upload', path('abs', 'font'));
        if ($getUploadedFileName != '') {
            $allPostPutVars += ['file_name' => $getUploadedFileName];
        }
        // Get the font family name from the font file.
        $resource = path('abs', 'font') . $getUploadedFileName;
        $ttfInfo = new FontMeta();
        $ttfInfo->setFontFile($resource);
        $getFontInfo = $ttfInfo->getFontInfo();
        if (!empty($getFontInfo)) {
            $allPostPutVars += ['font_family' => $getFontInfo[1]];
        }
        // Set default Store ID
        $allPostPutVars += ['store_id' => $getStoreDetails['store_id']];
        // Save Font data
        $font = new Font($allPostPutVars);
        try {
            $font->save();
            $fontLastInsertId = $font->xe_id;
        } catch (\Exception $e) {
            $serverStatusCode = EXCEPTION_OCCURED;
            $jsonResponse = [
                'status' => 0,
                'message' => message('Font', 'exception'),
                'exception' => show_exception() === true ? $e->getMessage() : '',
            ];
        }
        if (isset($fontLastInsertId) && $fontLastInsertId > 0) {
            $jsonResponse = [
                'status' => 1,
                'message' => message('Font', 'saved'),
                'font_id' => $fontLastInsertId,
            ];

            /**
             * Save category and subcategory data
             * Category id should come in json array format. e.g.: [4,78,3]
             */
            if (isset($allPostPutVars['categories']) && $allPostPutVars['categories'] != "") {
                $categoryIds = $allPostPutVars['categories'];
                // Send to the Component method to sync the category ids according to the fonts
                $this->_saveFontCategories($fontLastInsertId, $categoryIds);
            }
            /**
             * Save tags with respect to the fonts
             * Tag Names should come in json array format. e.g.: tag1,tag2,tag3
             */
            $tags = (isset($allPostPutVars['tags']) && $allPostPutVars['tags'] != "") ? $allPostPutVars['tags'] : "";
            $this->_saveFontTags($fontLastInsertId, $tags);
        }
        return response(
            $response, ['data' => $jsonResponse, 'status' => $serverStatusCode]
        );
    }

    /**
     * Save categories w.r.t font
     *
     * @param $fontId      Font ID
     * @param $categoryIds (in  an array with comma separated)
     *
     * @author satyabratap@riaxe.com
     * @date   4th Nov 2019
     * @return boolean
     */
    private function _saveFontCategories($fontId, $categoryIds)
    {
        $getAllCategoryArr = json_clean_decode($categoryIds, true);
        // SYNC Categories to the Font_Category Relationship Table
        $fontsInit = new Font();
        $findFont = $fontsInit->find($fontId);
        if ($findFont->categories()->sync($getAllCategoryArr)) {
            return true;
        }
        return false;
    }

    /**
     * Save tags w.r.t font
     *
     * @param $fontId       Font ID
     * @param $multipletags Multiple Tags
     *
     * @author satyabratap@riaxe.com
     * @date   4th Nov 2019
     * @return boolean
     */
    private function _saveFontTags($fontId, $multipletags)
    {
        if (isset($multipletags) && $multipletags != "") {
            $updatedTagIds = [];
            $tagsStringToArray = explode(',', $multipletags);
            foreach ($tagsStringToArray as $key => $tag) {
                // Save each individual Tag to table
                $tagInit = new Tag();
                if ($tagInit->where(['name' => trim($tag)])->count() == 0) {
                    $saveTag = new Tag(['name' => trim($tag)]);
                    $saveTag->save();
                    $tagLastInsertId = $saveTag->xe_id;
                } else {
                    $getTagDetails = $tagInit->where(['name' => trim($tag)])->select('xe_id')->first();
                    $tagLastInsertId = $getTagDetails['xe_id'];
                }
                if (isset($tagLastInsertId) && $tagLastInsertId > 0) {
                    $updatedTagIds[] = $tagLastInsertId;
                }
            }

            // Start SYNC Tags into Font_Tag Relationship Table
            $fontsInit = new Font();
            $findFont = $fontsInit->find($fontId);
            if ($findFont->tags()->sync($updatedTagIds)) {
                return true;
            }
        } else {
            // If user requests blank/no tags
            $tagRelInit = new FontTagRelation();
            $fontTags = $tagRelInit->where('font_id', $fontId);
            if ($fontTags->delete()) {
                return true;
            }
        }
        return false;
    }

    /**
     * GET: List of fonts
     *
     * @param $request  Slim's Request object
     * @param $response Slim's Response object
     * @param $args     Slim's Argument parameters
     *
     * @author satyabratap@riaxe.com
     * @date   23 oct 2019
     * @return All/Single Fonts List
     */
    public function getFonts($request, $response, $args)
    {
        $serverStatusCode = OPERATION_OKAY;
        $offset = 0;
        $jsonResponse = [
            'status' => 0,
            'data' => [],
            'message' => message('Font', 'not_found'),
        ];
        $getStoreDetails = get_store_details($request);
        $fontsInit = new Font();
        $getFonts = $fontsInit->where('xe_id', '>', 0);
        if (isset($getStoreDetails['store_id']) && $getStoreDetails['store_id'] > 0) {
            $getFonts->where('store_id', '=', $getStoreDetails['store_id']);
        }
        // total records irrespectable of filters
        $totalCounts = $getFonts->count();
        if ($totalCounts > 0) {
            if (isset($args['id']) && $args['id'] != '') {
                //For single Font data
                $getFonts->where('xe_id', '=', $args['id'])->select('xe_id', 'name', 'price', 'font_family', 'file_name');
                $fontsData = $getFonts->orderBy('xe_id', 'DESC')->first();

                $categoryIdList = $tagNameList = [];
                // Get Category Ids
                $getCategories = $this->getCategoriesById(
                    'Fonts', 'FontCategoryRelation',
                    'font_id', $args['id']
                );
                $fontsData['categories'] = !empty($getCategories) ? $getCategories : [];
                // Get Tag names
                $getTags = $this->getTagsById(
                    'Fonts', 'FontTagRelation',
                    'font_id', $args['id']
                );
                $fontsData['tags'] = !empty($getTags) ? $getTags : [];

                // Unset category_name Key in case of single record fetch
                $fontsData = json_clean_decode($fontsData, true);
                unset($fontsData['category_names']);

                $jsonResponse = [
                    'status' => 1,
                    'records' => 1,
                    'data' => [
                        $fontsData,
                    ],
                ];
            } else {
                // Collect all Filter columns from url
                $page = $request->getQueryParam('page');
                $perpage = $request->getQueryParam('perpage');
                $categoryId = $request->getQueryParam('category');
                $sortBy = $request->getQueryParam('sortby');
                $order = $request->getQueryParam('order');
                $name = $request->getQueryParam('name');

                // For multiple Font data
                $getFonts->select('xe_id', 'name', 'price', 'font_family', 'file_name');

                // Multiple Table search for name attribute
                if (isset($name) && $name != "") {
                    $getFonts->where('name', 'LIKE', '%' . $name . '%')
                        ->orWhereHas(
                            'fontTags.tag', function ($q) use ($name) {
                                return $q->where('name', 'LIKE', '%' . $name . '%');
                            }
                        )
                        ->orWhereHas(
                            'fontCategory.category', function ($q) use ($name) {
                                return $q->where('name', 'LIKE', '%' . $name . '%');
                            }
                        );
                }
                // Filter by Category ID
                if (isset($categoryId) && $categoryId != "") {
                    $searchCategories = json_clean_decode($categoryId, true);
                    $getFonts->whereHas(
                        'fontCategory', function ($q) use ($searchCategories) {
                            return $q->whereIn('category_id', $searchCategories);
                        }
                    );
                }

                // Get pagination data
                if (isset($page) && $page != "") {
                    $totalItem = empty($perpage) ? PAGINATION_MAX_ROW : $perpage;
                    $offset = $totalItem * ($page - 1);
                    $getFonts->skip($offset)->take($totalItem);
                }
                // Sorting by column name and sord order parameter
                if (isset($sortBy) && $sortBy != "" && isset($order) && $order != "") {
                    $getFonts->orderBy($sortBy, $order);
                }
                $fontsData = $getFonts->orderBy('xe_id', 'DESC')->get();

                $jsonResponse = [
                    'status' => 1,
                    'records' => count($fontsData),
                    'total_records' => $totalCounts,
                    'data' => $fontsData,
                ];
            }
        }
        return response(
            $response, ['data' => $jsonResponse, 'status' => $serverStatusCode]
        );
    }

    /**
     * PUT: Update a single font
     *
     * @param $request  Slim's Request object
     * @param $response Slim's Response object
     * @param $args     Slim's Argument parameters
     *
     * @author satyabratap@riaxe.com
     * @date   23 oct 2019
     * @return json response wheather data is updated or not
     */
    public function updateFont($request, $response, $args)
    {
        $serverStatusCode = OPERATION_OKAY;
        $jsonResponse = [
            'status' => 0,
            'message' => message('Category', 'error'),
        ];
        $getStoreDetails = get_store_details($request);

        $allPostPutVars = $updateData = $this->parsePut();

        if (isset($args['id']) && $args['id'] != "") {
            $fontsInit = new Font();
            $getOldFont = $fontsInit->where('xe_id', '=', $args['id']);
            if ($getOldFont->count() > 0) {
                unset($updateData['id'], $updateData['tags'], $updateData['categories'], $updateData['upload'], $updateData['fontId']);
                // Update record
                $this->deleteOldFile("fonts", "file_name", ['xe_id' => $args['id']], path('abs', 'font'));
                $getUploadedFileName = save_file('upload', path('abs', 'font'));
                if ($getUploadedFileName != '') {
                    $updateData += ['file_name' => $getUploadedFileName];
                }
                // Update record
                try {

                    $fontsInit->where('xe_id', '=', $args['id'])->update($updateData);
                    $jsonResponse = [
                        'status' => 1,
                        'message' => message('Font', 'updated'),
                    ];
                    /**
                     * Save category
                     * Parameter: categories
                     */
                    if (isset($allPostPutVars['categories']) && $allPostPutVars['categories'] != "") {
                        $categoryIds = $allPostPutVars['categories'];
                        // Send to the Component method to sync the category ids according to the fonts
                        $this->_saveFontCategories($args['id'], $categoryIds);
                    }
                    /**
                     * Save tags
                     * Parameter: tags
                     */
                    if (isset($allPostPutVars['tags']) && $allPostPutVars['tags'] != "") {
                        $tags = $allPostPutVars['tags'];
                        $this->_saveFontTags($args['id'], $tags);
                    }
                } catch (\Exception $e) {
                    $serverStatusCode = EXCEPTION_OCCURED;
                    $jsonResponse = [
                        'status' => 0,
                        'message' => message('Font', 'exeception'),
                        'exception' => show_exception() === true ? $e->getMessage() : '',
                    ];
                }
            }
        }
        return response(
            $response, ['data' => $jsonResponse, 'status' => $serverStatusCode]
        );
    }

    /**
     * DELETE: Delete single/multiple font
     *
     * @param $request  Slim's Argument parameters
     * @param $response Slim's Response object
     * @param $args     Slim's Argument parameters
     *
     * @author satyabratap@riaxe.com
     * @date   23 oct 2019
     * @return json response wheather data is deleted or not
     */
    public function deleteFont($request, $response, $args)
    {
        $serverStatusCode = OPERATION_OKAY;
        $jsonResponse = [
            'status' => 0,
            'message' => message('Font', 'error'),
        ];
        if (isset($args) && $args['id'] != '') {
            $getDeleteIdsToArray = json_clean_decode($args['id'], true);
            if (!empty($getDeleteIdsToArray)) {
                $fontsInit = new Font();
                if ($fontsInit->whereIn('xe_id', $getDeleteIdsToArray)->count() > 0) {
                    // Fetch Font details
                    $getFontDetails = $fontsInit->whereIn('xe_id', $getDeleteIdsToArray)->select('xe_id')->get();

                    try {
                        if (!empty($getFontDetails)) {
                            foreach ($getFontDetails as $fontFileKey => $fontFile) {
                                if (isset($fontFile['xe_id']) && $fontFile['xe_id'] != "") {
                                    $this->deleteOldFile("fonts", "file_name", ['xe_id' => $fontFile['xe_id']], path('abs', 'font'));
                                }
                            }
                        }
                        // Delete from Database
                        $fontsInit->whereIn('xe_id', $getDeleteIdsToArray)->delete();
                        $jsonResponse = [
                            'status' => 1,
                            'message' => message('Font', 'deleted'),
                        ];
                    } catch (\Exception $e) {
                        $serverStatusCode = EXCEPTION_OCCURED;
                        $jsonResponse = [
                            'status' => 0,
                            'message' => message('Font', 'exception'),
                            'exception' => show_exception() === true ? $e->getMessage() : '',
                        ];
                    }
                }
            }
        }
        return response(
            $response, ['data' => $jsonResponse, 'status' => $serverStatusCode]
        );
    }

    /**
     * Check Category Relation with Category
     *
     * @param $request  Slim's Argument parameters
     * @param $response Slim's Response object
     * @param $args     Slim's Argument parameters
     *
     * @author satyabratap@riaxe.com
     * @date   20 Jan 2020
     * @return boolean
     */
    public function checkRelCategory($request, $response, $args)
    {
        $serverStatusCode = OPERATION_OKAY;
        $jsonResponse = [
            'status' => 0,
            'message' => 'This Category is associated with an Item. Do you really want to perform the operation'
        ];
        $key = $request->getQueryParam('key');
        if (!empty($args) && $args['id'] > 0) {
            $categoryId = $args['id'];
            $checkCatRelInit = new FontCategoryRelation();
            $getCatRel = $checkCatRelInit->where('category_id', $categoryId)->get();
            if ($getCatRel->count() == 0) {
                if ($key == 'disable') {
                    $jsonResponse = $this->disableCat('fonts', $categoryId);
                } elseif ($key == 'delete') {
                    $jsonResponse = $this->deleteCat('fonts', $categoryId, 'Fonts', 'FontCategoryRelation');
                }
            }
        }
        return response(
            $response, ['data' => $jsonResponse, 'status' => $serverStatusCode]
        );
    }

    /**
     * Delete a category from the table
     *
     * @param $request  Slim's Request object
     * @param $response Slim's Response object
     * @param $args     Slim's Argument parameters
     *
     * @author satyabratap@riaxe.com
     * @date   20 Jan 2020
     * @return Delete Json Status
     */
    public function deleteCategory($request, $response, $args)
    {
        $serverStatusCode = OPERATION_OKAY;
        $jsonResponse = [
            'status' => 0,
            'message' => message('Category', 'error')
        ];
        if (!empty($args) && $args['id'] > 0) {
            $categoryId = $args['id'];
            $jsonResponse = $this->deleteCat('fonts', $categoryId, 'Fonts', 'FontCategoryRelation');
        }
        return response(
            $response, ['data' => $jsonResponse, 'status' => $serverStatusCode]
        );
    }
}
