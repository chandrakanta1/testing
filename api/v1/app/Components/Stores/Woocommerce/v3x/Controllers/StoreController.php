<?php
/**
 * Woocommerce Store's Common functionalities will be written here
 *
 * PHP version 5.6
 *
 * @category  Woo-Commerce
 * @package   Store
 * @author    Tanmaya Patra <tanmayap@riaxe.com>
 * @copyright 2019-2020 Riaxe Systems
 * @license   http://www.php.net/license/3_0.txt  PHP License 3.0
 * @link      http://inkxe-v10.inkxe.io/xetool/admin
 */
namespace CommonStoreSpace\Controllers;

use App\Components\Controllers\Component as ParentController;
use Automattic\WooCommerce\Client;

/**
 * Asset Type Class
 *
 * @category Woo-Commerce
 * @package  Store
 * @author   Tanmaya Patra <tanmayap@riaxe.com>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://inkxe-v10.inkxe.io/xetool/admin
 */
class StoreController extends ParentController
{
    /**
     * Initialize Constructor
     */
    public function __construct()
    {
        $this->wc = new Client(
            WC_API_URL,
            WC_API_CK,
            WC_API_CS,
            [
                'wp_api' => true,
                //'timeout' => 36000,
                'version' => WC_API_VER,
                'verify_ssl' => WC_API_SECURE,
            ]
        );

        $this->plugin = new Client(
            WC_API_URL,
            WC_API_CK,
            WC_API_CS,
            [
                'wp_api' => true,
                //'timeout' => 36000,
                'version' => 'InkXEProductDesigner',
                'verify_ssl' => WC_API_SECURE,
            ]
        );

        set_time_limit(0);
    }
}
