<?php
/**
 * This is a common Component which will be used accross all Module-Controllers
 *
 * PHP version 5.6
 *
 * @category  Component
 * @package   Slim
 * @author    Tanmaya Patra <tanmayap@riaxe.com>
 * @copyright 2019-2020 Riaxe Systems
 * @license   http://www.php.net/license/3_0.txt  PHP License 3.0
 * @link      http://inkxe-v10.inkxe.io/xetool/admin
 */
namespace App\Components\Controllers;

use App\Components\Models\AssetType as AssetType;
use App\Components\Models\Category as Category;
use App\Modules\Templates\Models as TemplateModel;
use Illuminate\Database\Capsule\Manager as DB;

/**
 * Component Class
 *
 * @category Component
 * @package  Slim
 * @author   Tanmaya Patra <tanmayap@riaxe.com>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://inkxe-v10.inkxe.io/xetool/admin
 */
class Component
{
    /**
     * Initialize Constructor
     */
    public function __construct()
    {
        // Set a secure hash salt key for Encryption methods
        $this->secret = "SgUkXp2s5v8y/B?E(H+MbQeThWmYq3t6w9z^C&F)J@NcRfUjXn2r4u7x!A%D*G-K";
    }
    /**
     * Check if the prodcut associated with a Print Profile then send print
     * Profile data
     *
     * @param $productId Slim's Request object
     *
     * @author tanmayap@riaxe.com
     * @date   5 Oct 2019
     * @return Array list of print profiles
     */
    public function getAssocPrintProfiles($productId = null)
    {
        $printProfiles = [];
        $isDecorationExists = 0;
        if (isset($productId) && $productId > 0) {
            $productSettings = new \App\Modules\Products\Models\ProductSetting();
            $getSettings = $productSettings->where('product_id', $productId);
            if ($getSettings->count() > 0) {
                $isDecorationExists = 1;
                $productSetting = $getSettings->with(
                    'print_profiles', 'print_profiles.profile'
                )->first();
                foreach ($productSetting['print_profiles'] as $profileKey => $profile) {
                    if (!empty($profile->profile['xe_id']) && $profile->profile['xe_id']) {
                        $printProfiles[] = [
                            'id' => $profile->profile->xe_id,
                            'name' => $profile->profile->name,
                        ];
                    }
                }
            }
        }

        return [
            'print_profiles' => $printProfiles,
            'is_decoration_exists' => $isDecorationExists,
        ];
    }

    /**
     * Get the color swatches from inkxe database
     *
     * @param $colorDetail Color Details
     *
     * @author satyabratap@riaxe.com
     * @date   23 Jan 2019
     * @return Array 
     */
    public function getColorSwatchData($colorDetail)
    {
        $variantData = [];
        foreach ($colorDetail as $termKey => $termValue) {
            $colorSwatchInit = new \App\Modules\Settings\Models\ColorSwatch();
            $getTermLocalData = $colorSwatchInit->where('attribute_id', $termValue['id'])->first();
            $variantData[$termKey] = [
                'id' => (!empty($getTermLocalData['xe_id']) && $getTermLocalData['xe_id'] > 0) ? $getTermLocalData['xe_id'] : "",
                'attribute_id' => $termValue['id'],
                'name' => $termValue['name'],
                'hex_code' => (!empty($getTermLocalData['hex_code']) && $getTermLocalData['hex_code'] != "") ? $getTermLocalData['hex_code'] : "",
                'file_name' => (!empty($getTermLocalData['file_name']) && $getTermLocalData['file_name'] != "") ? $getTermLocalData['file_name'] : "",
                'color_type' => (!empty($getTermLocalData['color_type']) && $getTermLocalData['color_type'] != "") ? $getTermLocalData['color_type'] : "",
            ];
        }
        return $variantData;
    }

    /**
     * As with predefirened PUT parser we were unable to get data and files in
     * form data. So this custom Parser is used for adding a intermediate Parser
     * so that we can get the form data
     *
     * @author tanmayap@riaxe.com
     * @date   17 sept 2019
     * @return All Formdata from Put method
     */
    public function parsePut()
    {
        $matches = $type = "";
        // PUT data comes in on the stdin stream
        $putdata = fopen("php://input", "r");
        // Open a file for writing
        // $fp = fopen("myputfile.ext", "w");
        $rawData = '';
        // Read the data 1 KB at a time and write to the file
        while ($chunk = fread($putdata, 1024)) {
            $rawData .= $chunk;
        }
        // Close the streams
        fclose($putdata);
        // Fetch content and determine boundary
        $boundary = substr($rawData, 0, strpos($rawData, "\r\n"));
        if (empty($boundary)) {
            parse_str($rawData, $data);
            return $data;
        }
        // Fetch each part
        $parts = array_slice(explode($boundary, $rawData), 1);
        $data = array();
        foreach ($parts as $part) {
            // If this is the last part, break
            if ($part == "--\r\n") {
                break;
            }

            // Separate content from headers
            $part = ltrim($part, "\r\n");
            list($rawHeaders, $body) = explode("\r\n\r\n", $part, 2);
            // Parse the headers list
            $rawHeaders = explode("\r\n", $rawHeaders);
            $headers = array();
            foreach ($rawHeaders as $header) {
                list($name, $value) = explode(':', $header);
                $headers[strtolower($name)] = ltrim($value, ' ');
            }
            // Parse the Content-Disposition to get the field name, etc.
            if (isset($headers['content-disposition'])) {
                $filename = null;
                $tmpName = null;
                preg_match(
                    '/^(.+); *name="([^"]+)"(; *filename="([^"]+)")?/', $headers['content-disposition'], $matches
                );
                list(, $type, $name) = $matches;
                //Parse File
                if (isset($matches[4])) {
                    //if labeled the same as previous, skip
                    if (isset($_FILES[$matches[2]])) {
                        continue;
                    }
                    //get filename
                    $filename = $matches[4];
                    //get tmp name
                    $filenameParts = pathinfo($filename);
                    $tmpName = tempnam(ini_get('upload_tmp_dir'), $filenameParts['filename']);
                    // populate $_FILES with information, size may be off in
                    // multibyte situation
                    $_FILES[$matches[2]] = array(
                        'error' => 0,
                        'name' => $filename,
                        'tmp_name' => $tmpName,
                        'size' => strlen($body),
                        'type' => $value,
                    );
                    //place in temporary directory
                    file_put_contents($tmpName, $body);
                }
                //Parse Field
                else {
                    $data[$name] = substr($body, 0, strlen($body) - 2);
                }
            }
        }
        return $data;
    }

    /**
     * Get Assets Type ID from module slug name
     *
     * @param $slug Slug value of the individual helper file
     *
     * @author tanmayap@riaxe.com
     * @date   3rd Dec 2019
     * @return asset_type_id
     */
    public function assetsTypeId($slug)
    {
        if (isset($slug) && $slug != "") {
            if (AssetType::where('slug', '=', $slug)->count() > 0) {
                $statement = AssetType::where('slug', $slug)->first();
                return [
                    'status' => 1,
                    'asset_type_id' => $statement->xe_id,
                ];
            }
        }
        return [
            'status' => 0,
            'message' => message('Slug', 'not_found'),
        ];
    }
    /**
     * Get Category and Subcategory List of Assets Modules
     * Used for Print Profile Module
     *
     * @param $slug Slug value of the individual helper file
     *
     * @author tanmayap@riaxe.com
     * @date   3rd Dec 2019
     * @return Array of categories
     */
    public function getAssetCategories($slug)
    {
        $categories = [];
        if ($slug != '' && $slug != 'products') {
            $assetType = DB::table('asset_types')->where('slug', $slug)->first();
            if (!empty($assetType)) {
                $assetTypeId = $assetType->xe_id;
                if (Category::where('asset_type_id', $assetTypeId)->count() > 0) {
                    $categories = Category::where(
                        'asset_type_id', $assetTypeId
                    )->select(
                        'xe_id as id', 'parent_id', 'name', 'is_disable'
                    )->get();
                    $categories = $categories->toArray();
                } else {
                    $categories = [];
                }
            }
        } else if ($slug == 'products') {
            $guzzle = new \GuzzleHttp\Client();
            $products = $guzzle->request(
                'GET', BASE_URL . 'products/list-of-categories'
            );
            $productsCategoryJson = $products->getBody();
            $categories = json_clean_decode($productsCategoryJson, true);
            $categories = $categories['data'];
        }
        return $categories;
    }
    /**
     * Get: Get related Categories by Module
     *
     * @param $moduleDir The Directory name where the Model is exists
     * @param $modelName The model name that we use to get relational records
     * @param $colName   module's foreign key column name
     * @param $colValue  category's foreign key column name
     * @param $type      it is a alternate between name and id
     *
     * @author tanmayap@riaxe.com
     * @date   13 Jan 2020
     * @return Array
     */
    public function getCategoriesById($moduleDir, $modelName, $colName, $colValue, $type = 'id')
    {
        $categoryIdList = [];
        $modelPath = 'App\\Modules\\' . $moduleDir . '\\' . 'Models' . '\\';
        $modelPath .= $modelName;
        $categoryInit = new $modelPath();
        $getCategory = $categoryInit->where($colName, $colValue)
            ->select('category_id')
            ->with('category')
            ->get();
        if (!empty($getCategory)) {
            // if name given, parent_id == 0 will be added
            foreach ($getCategory as $category) {
                if ($category['category']['is_disable'] == 0) {
                    if ($type == 'id') {
                        $categoryIdList[] = $category['category_id'];
                    }
                    if ($type == 'name' && $category['category']['parent_id'] == 0) {
                        $categoryIdList[] = $category['category']['name'];
                    }
                }
            }
        }

        return $categoryIdList;
    }
    /**
     * Get: Get related Tags by Module
     *
     * @param $moduleDir The Directory name where the Model is exists
     * @param $modelName The model name that we use to get relational records
     * @param $colName   module's foreign key column name
     * @param $colValue  tag's foreign key column name
     *
     * @author tanmayap@riaxe.com
     * @date   13 Jan 2020
     * @return Array
     */
    public function getTagsById($moduleDir, $modelName, $colName, $colValue)
    {
        $tagNames = [];
        // Get Tag names
        $modelPath = 'App\\Modules\\' . $moduleDir . '\\' . 'Models' . '\\';
        $modelPath .= $modelName;
        $tagRelInit = new $modelPath();

        $getTags = $tagRelInit->where($colName, $colValue)
            ->select('tag_id')
            ->with('tag')
            ->get();
        if (!empty($getTags)) {
            foreach ($getTags as $tag) {
                $tagNames[] = $tag['tag']['name'];
            }
        }
        return $tagNames;
    }

    /**
     * Delete/Trash old file and thumbs(is exists) of a specific Model from its
     * corsp. Folder
     *
     * @param $table     Database Table Name
     * @param $column    Column in which we can get file name
     * @param $condition Condition to get the specific file
     * @param $folder    Folder from where we will delete the file
     *
     * @author tanmayap@riaxe.com
     * @date   5 Oct 2019
     * @return All Print Profile List
     */
    public function deleteOldFile($table, $column, $condition, $folder, $newFileCheck = true)
    {
        $deleteCount = 0;
        // Delete the file if and only if any file uploaded from api
        // Or, you can disable this by sending false key to $newFileCheck
        $fileCheck = 1;
        if ($newFileCheck === true) {
            $fileCheck = 0;
            if (isset($_FILES) && count($_FILES) > 0) {
                $fileCheck = 1;
            }
        }

        if (!empty($table) && !empty($column)
            && $fileCheck === 1
        ) {
            $initTable = new DB();
            $getSelectedRecord = $initTable->table($table);
            if (isset($condition) && count($condition) > 0) {
                $getSelectedRecord->where($condition);
            }
            if ($getSelectedRecord->select($column)->count() > 0) {
                $getAllFileNames = $getSelectedRecord->select($column)->get();
                foreach ($getAllFileNames as $getFile) {
                    if (isset($getFile->file_name) && $getFile->file_name != "") {
                        $rawFileLocation = $folder . $getFile->file_name;
                        if (delete_file($rawFileLocation)) {
                            $deleteCount++;
                            $rawThumbFileLocation = $folder . 'thumb_' .
                            $getFile->file_name;
                            delete_file($rawThumbFileLocation);
                        }
                    }
                }
            }
        }

        return $deleteCount > 1 ? true : false;
    }
    /**
     * GET: Get active store's details from database
     *
     * @author tanmayap@riaxe.com
     * @date   5 Oct 2019
     * @return Array
     */
    public function getActiveStoreDetails()
    {
        $storeDetails = [];
        $databaseStoreInfo = DB::table('stores')->where('status', 3);
        if ($databaseStoreInfo->count() > 0) {
            $storeData = $databaseStoreInfo->first();
            if (!empty($storeData->xe_id) && $storeData->xe_id > 0) {
                $storeDetails = [
                    'store_id' => $storeData->xe_id,
                ];
            }
        }
        return $storeDetails;
    }
    /*
     *---------------------------------------------------------------
     * Common Category Operations
     *---------------------------------------------------------------
     *
     * All Category CRUD Operations are handled by these common functions.
     * We can access to these common methods from any individual routes .
     */
    /**
     * Get all Categories in Recursion format from the Database
     *
     * @param $request  Slim's Request object
     * @param $response Slim's Response object
     * @param $args     Slim's Argument parameters
     *
     * @author tanmayap@riaxe.com
     * @date   5 Oct 2019
     * @return List of category and subcategories
     */
    public function getCategories($request, $response, $args)
    {
        $serverStatusCode = OPERATION_OKAY;
        $startTime = microtime(true);
        $jsonResponse = [
            'status' => 0,
            'message' => message('Category', 'error'),
        ];
        $categoryId = (isset($args['id']) && $args['id'] > 0) ? $args['id'] : 0;

        if (isset($args['slug']) && $args['slug'] != "") {
            $moduleSlugName = $args['slug'];
            // Getting Assets module id
            $assetTypeArr = $this->assetsTypeId($moduleSlugName);

            if (!empty($assetTypeArr) && $assetTypeArr['status'] == 1) {
                $assetTypeId = $assetTypeArr['asset_type_id'];
                if (isset($categoryId) && $categoryId > 0) {
                    $categoryInit = new Category();
                    $getCategories = $categoryInit->select(
                        'xe_id as id', 'parent_id', 'name',
                        'sort_order', 'is_disable', 'is_default'
                    )->where(
                        [
                            'parent_id' => 0,
                            'asset_type_id' => $assetTypeId,
                            'xe_id' => $categoryId,
                        ]
                    )->orderBy('sort_order', 'asc')->get();
                } else {
                    $categoryInit = new Category();
                    $getCategories = $categoryInit->select(
                        'xe_id as id', 'parent_id', 'name',
                        'sort_order', 'is_disable', 'is_default'
                    )->where(
                        [
                            'parent_id' => 0,
                            'asset_type_id' => $assetTypeId,
                        ]
                    )->orderBy('sort_order', 'asc')->get();
                }
                if ($getCategories->count() > 0) {
                    $categoryDetails = [];
                    foreach ($getCategories->toArray() as $value) {
                        $categoryDetails[] = [
                            'id' => $value['id'],
                            'name' => $value['name'],
                            'order' => $value['sort_order'],
                            'is_disable' => $value['is_disable'],
                            'is_default' => $value['is_default'],
                            'subs' => $this->getSubCatetgories($value['id']),
                        ];
                    }
                    $jsonResponse = [
                        'status' => 1,
                        'data' => $categoryDetails,
                    ];
                }
            }
        }

        return response(
            $response, ['data' => $jsonResponse, 'status' => $serverStatusCode]
        );
    }
    /**
     * Getting subcategory recurssively
     *
     * @param $parentCategoryId Parent Category ID
     *
     * @author tanmayap@riaxe.com
     * @date   5 Oct 2019
     * @return All Print Profile List
     */
    private function getSubCatetgories($parentCategoryId)
    {
        $subcategory = [];
        $categoryInit = new Category();
        $getCategories = $categoryInit->select(
            'xe_id as id', 'parent_id', 'name',
            'sort_order', 'is_disable', 'is_default'
        )->where(
            [
                'parent_id' => $parentCategoryId,
            ]
        )->orderBy('sort_order', 'asc')->get();

        foreach ($getCategories->toArray() as $value) {
            $categoryInit = new Category();
            $fetchCount = $categoryInit->select(
                'xe_id as id', 'parent_id', 'name', 'is_disable', 'is_default'
            )->where(
                [
                    'parent_id' => $value['id'],
                ]
            )->count();

            $subCatList = [];
            if ($fetchCount > 0) {
                $subCatList = $this->getSubCatetgories($value['id']);
            }

            $subcategory[] = [
                'id' => $value['id'],
                'name' => $value['name'],
                'order' => $value['sort_order'],
                'parent_id' => $value['parent_id'],
                'is_disable' => $value['is_disable'],
                'is_default' => $value['is_default'],
                'subs' => $subCatList,
            ];
        }

        return $subcategory;
    }
    /**
     * Post: Save category according to slug
     *
     * @param $request  Slim's Request object
     * @param $response Slim's Response object
     * @param $args     Slim's Argument parameters
     *
     * @author tanmayap@riaxe.com
     * @date   5 Oct 2019
     * @return Save Json response
     */
    public function saveCategory($request, $response, $args)
    {
        $serverStatusCode = OPERATION_OKAY;
        $jsonResponse = [
            'status' => 1,
            'message' => message('Category', 'saved'),
        ];
        $allPostPutVars = $request->getParsedBody();
        $storeDetails = get_store_details($request);

        if (isset($args['slug']) && $args['slug'] != '') {
            // Getting assets module id
            $assetTypeArr = $this->assetsTypeId($args['slug']);
            $getCategoryInit = new Category();
            $getCategoryDetails = $getCategoryInit->where(
                [
                    'name' => $allPostPutVars['name'],
                    'asset_type_id' => $assetTypeArr['asset_type_id'],
                    'parent_id' => $allPostPutVars['parent_id'],
                ]
            );
            if ($getCategoryDetails->count() > 0) {
                $jsonResponse = [
                    'status' => 0,
                    'message' => 'The Name is already taken',
                ];
            } else {
                if (!empty($assetTypeArr) && $assetTypeArr['status'] == 1) {
                    $allPostPutVars['asset_type_id'] = $assetTypeArr['asset_type_id'];
                    if (!isset($allPostPutVars['sort_order'])) {
                        $allPostPutVars['sort_order'] = Category::max('sort_order') + 1;
                    }
                    if (empty($allPostPutVars['store_id'])) {
                        $allPostPutVars['store_id'] = $storeDetails['store_id'];
                    }
                    if (!empty($allPostPutVars['name'])) {
                        $saveCategory = new Category($allPostPutVars);
                        try {
                            $saveCategory->save();
                            $jsonResponse = [
                                'status' => 1,
                                'message' => message('Category', 'saved'),
                            ];
                        } catch (\Exception $e) {
                            $serverStatusCode = EXCEPTION_OCCURED;
                            $jsonResponse = [
                                'status' => 0,
                                'message' => message('Category', 'exception'),
                                'exception' => showException() === true ? $e->getMessage() : '',
                            ];
                        }
                    }
                }
            }
        }

        return response(
            $response, ['data' => $jsonResponse, 'status' => $serverStatusCode]
        );
    }
    /**
     * Update Category according to the slug
     *
     * @param $request  Slim's Request object
     * @param $response Slim's Response object
     * @param $args     Slim's Argument parameters
     *
     * @author tanmayap@riaxe.com
     * @date   5 Oct 2019
     * @return Update Json response
     */
    public function updateCategory($request, $response, $args)
    {
        $serverStatusCode = OPERATION_OKAY;
        $jsonResponse = [
            'status' => 0,
            'message' => message('Category', 'error'),
        ];
        $allPostPutVars = $this->parsePut();
        $assetTypeArr = $this->assetsTypeId($args['slug']);

        if (isset($args['slug']) && $args['slug'] != ''
            && !empty($assetTypeArr) && $assetTypeArr['status'] == 1
        ) {
            $updateCategory = [
                'name' => $allPostPutVars['name'],
            ];
            $categoryId = isset($args['id']) ? $args['id'] : '';

            try {
                Category::where('xe_id', $categoryId)->update($updateCategory);
                $jsonResponse = [
                    'status' => 1,
                    'message' => message('Category', 'updated'),
                ];
            } catch (\Exception $e) {
                $serverStatusCode = EXCEPTION_OCCURED;
                $jsonResponse = [
                    'status' => 0,
                    'message' => message('Category', 'exception'),
                    'exception' => showException() === true ? $e->getMessage() : '',
                ];
            }
        }

        return response(
            $response, ['data' => $jsonResponse, 'status' => $serverStatusCode]
        );
    }

    /**
     * Disable/Enable a category according to the current status of the category
     * in the table
     *
     * @param $slug       asset slug
     * @param $categoryId category Id
     *
     * @author tanmayap@riaxe.com
     * @date   5 Oct 2019
     * @return Disable/Enable Json response
     */
    public function disableCat($slug, $categoryId)
    {
        $serverStatusCode = OPERATION_OKAY;
        $jsonResponse = [
            'status' => 0,
            'message' => message('Category', 'error'),
        ];
        $assetTypeArr = $this->assetsTypeId($slug);
        if (isset($slug) && $slug != '' && !empty($assetTypeArr) && $assetTypeArr['status'] == 1) {
            $getCategoryInit = new Category();
            $category = $getCategoryInit->find($categoryId);
            if ($category->parent_id == 0) {
                $getSubCat = Category::where(
                    [
                        'parent_id' => $category->xe_id,
                        'asset_type_id' => $assetTypeArr['asset_type_id'],
                    ]
                )->get();
                foreach ($getSubCat as $subCatValue) {
                    $subCatValue->is_disable = !$category->is_disable;
                    $subCatValue->save();
                }
            }
            if ($category->is_default === 1) {
                $category->is_default = !$category->is_default;
            }
            $category->is_disable = !$category->is_disable;

            try {
                $category->save();
                $jsonResponse = [
                    'status' => 1,
                    'message' => message('Category', 'done'),
                ];
            } catch (\Exception $e) {
                $serverStatusCode = EXCEPTION_OCCURED;
                $jsonResponse = [
                    'status' => 0,
                    'message' => message('Category', 'exception'),
                    'exception' => showException() === true ? $e->getMessage() : '',
                ];
            }
        }

        return $jsonResponse;
    }

    /**
     * Disable/Enable a category according to the current status of the category
     * in the table
     *
     * @param $request  Slim's Request object
     * @param $response Slim's Response object
     * @param $args     Slim's Argument parameters
     *
     * @author tanmayap@riaxe.com
     * @date   5 Oct 2019
     * @return Disable/Enable Json response
     */
    public function disableCategory($request, $response, $args)
    {
        $serverStatusCode = OPERATION_OKAY;
        $jsonResponse = [];
        if (!empty($args) && $args['id'] > 0) {
            $categoryId = $args['id'];
            $jsonResponse = $this->disableCat($args['slug'], $categoryId);
        }
        return response(
            $response, ['data' => $jsonResponse, 'status' => $serverStatusCode]
        );
    }

    /**
     * Delete a category according to the current status of the category
     * in the table
     *
     * @param $slug       asset slug
     * @param $categoryId category Id
     *
     * @author tanmayap@riaxe.com
     * @date   5 Oct 2019
     * @return Delete Json response
     */
    public function deleteCat($slug, $categoryId, $moduleDir, $modelName)
    {
        $modelPath = 'App\\Modules\\' . $moduleDir . '\\' . 'Models' . '\\';
        $modelPath .= $modelName;
        $categoryRelInit = new $modelPath();
        $assetTypeArr = $this->assetsTypeId($slug);
        if (isset($slug) && $slug != '' && !empty($assetTypeArr) && $assetTypeArr['status'] == 1) {
            $deleteId = $categoryId;
            $category = Category::find($deleteId);
            if (isset($category->xe_id) && $category->xe_id != "" && $category->xe_id > 0) {
                try {
                    $category->delete();
                    $categoryRelInit->where(['category_id' => $deleteId])->delete();
                    $jsonResponse = [
                        'status' => 1,
                        'message' => message('Category', 'deleted'),
                    ];
                } catch (\Exception $e) {
                    $serverStatusCode = EXCEPTION_OCCURED;
                    $jsonResponse = [
                        'status' => 0,
                        'message' => message('Category', 'exception'),
                        'exception' => showException() === true ? $e->getMessage() : '',
                    ];
                }
            }
        }
        return $jsonResponse;
    }

    /**
     * Sort category
     *
     * @param $request  Slim's Request object
     * @param $response Slim's Response object
     * @param $args     Slim's Argument parameters
     *
     * @author tanmayap@riaxe.com
     * @date   5 Oct 2019
     * @return Sort Json Status
     */
    public function sortCategory($request, $response, $args)
    {
        $serverStatusCode = OPERATION_OKAY;
        $jsonResponse = [
            'status' => 0,
            'message' => message('Category', 'error'),
        ];
        $allPostPutVars = $request->getParsedBody();
        $sortData = $allPostPutVars['sort_data'];
        $sortDataArray = json_decode($sortData, true);

        $assetTypeArr = $this->assetsTypeId($args['slug']);

        if (isset($args['slug']) && $args['slug'] != '' && !empty($assetTypeArr) && $assetTypeArr['status'] == 1) {
            if (!empty($sortDataArray)) {
                foreach ($sortDataArray as $category) {
                    $sortedData[] = [
                        'parent' => 0,
                        'child' => $category['id'],
                    ];
                    if (isset($category['children']) && is_array($category['children']) && count($category['children']) > 0) {
                        foreach ($category['children'] as $child) {
                            $sortedData[] = [
                                'parent' => $category['id'],
                                'child' => $child['id'],
                            ];
                        }
                    }
                }
            }
            // Final procesing: Set a update array and Update the each record
            $updateSortedData = [];
            $updateStatus = 0;
            if (!empty($sortedData)) {
                foreach ($sortedData as $sortKey => $data) {
                    $updateSortedData[] = [
                        'parent_id' => $data['parent'],
                        'sort_order' => $sortKey + 1,
                    ];
                    if (Category::where(['xe_id' => $data['child']])->update(['sort_order' => $sortKey + 1])) {
                        $updateStatus++;
                    }
                }
            }

            // Setup Response
            if (isset($updateStatus) && $updateStatus > 0) {
                $jsonResponse = [
                    'status' => 1,
                    'message' => message('Category', 'done'),
                ];
            }
        }

        return response($response, ['data' => $jsonResponse, 'status' => $serverStatusCode]);
    }

    /**
     * Set a Category & Subcategory as Default
     *
     * @param $request  Slim's Request object
     * @param $response Slim's Response object
     * @param $args     Slim's Argument parameters
     *
     * @author tanmayap@riaxe.com
     * @date   5 Oct 2019
     * @return Json Status
     */
    public function setDefault($request, $response, $args)
    {
        $serverStatusCode = OPERATION_OKAY;
        $jsonResponse = [
            'status' => 0,
            'message' => message('Category', 'error'),
        ];
        $allPostPutVars = $request->getParsedBody();
        $defaultIds = $allPostPutVars['default'];
        $assetTypeArr = $this->assetsTypeId($args['slug']);

        if (isset($args['slug']) && $args['slug'] != '' && !empty($assetTypeArr) && $assetTypeArr['status'] == 1) {
            /**
             * For easy processing and less coding :- If invalid/blank data come
             * from frontend then convert to a blank JSON object or assign the
             * valid JSON to a variable for durther processing
             */
            if (isset($defaultIds) && $defaultIds != "") {
                $processingIds = $defaultIds;
            } else {
                $processingIds = '[]';
            }

            $getDefaultIds = json_decode($processingIds, true);
            $getCategoryInit = new Category();
            $categories = $getCategoryInit->whereIn('xe_id', $getDefaultIds);
            if ($categories->count() > 0) {
                try {
                    // Set default by asset type id
                    $getCategoryInit = new Category();
                    $getCategoryInit->where(
                        [
                            'is_default' => 1,
                            'asset_type_id' => $assetTypeArr['asset_type_id'],
                        ]
                    )->update(['is_default' => 0]);

                    $getCategoryInit = new Category();
                    $getCategoryInit->whereIn(
                        'xe_id', $getDefaultIds
                    )->update(['is_default' => 1]);

                    $jsonResponse = [
                        'status' => 1,
                        'message' => message('Category', 'done'),
                    ];
                } catch (\Exception $e) {
                    $serverStatusCode = EXCEPTION_OCCURED;
                    $jsonResponse = [
                        'status' => 0,
                        'message' => message('Category', 'exception'),
                        'exception' => showException() === true ? $e->getMessage() : '',
                    ];
                }
            }
        }

        return response(
            $response, ['data' => $jsonResponse, 'status' => $serverStatusCode]
        );
    }

    /**
     * Save design data and json format
     *
     * @param $designDetails  Design data array
     * @param $jsonDesignData json data
     *
     * @author debashrib@riaxe.com
     * @date   06 Jan 2020
     * @return $refId  auto incremented Id
     */
    public function saveDesignData($designDetails, $jsonDesignData)
    {
        $refId = 0;
        if (isset($designDetails) && !empty($designDetails)) {
            $saveDesignStateInit = new TemplateModel\TemplateDesignStates($designDetails);
            $saveDesignStateInit->save();
            if ($saveDesignStateInit->xe_id) {
                $refId = $saveDesignStateInit->xe_id;
                // save json data into json file
                if (isset($jsonDesignData) && !empty($jsonDesignData)) {
                    $dirAccToRefId = "REF_ID_" . $refId;
                    $captureCSVLocation = path('abs', 'template') . $dirAccToRefId;
                    create_directory($captureCSVLocation);
                    $saveLocation = $captureCSVLocation . '/' . 'json_' . $dirAccToRefId . '.json';
                    write_file($saveLocation, $jsonDesignData);
                }
            }
        }
        return $refId;
    }

    /**
     * Save design svg files
     *
     * @param $designFiles Design files array
     * @param $refId       Ref ID
     *
     * @author tanmayap@riaxe.com
     * @author debashrib@riaxe.com
     * @date   07 Jan 2020
     * @return boolean
     */
    public function saveDesignFile($designFiles, $refId)
    {
        if (count($designFiles) > 0) {
            $dirAccToRefId = "REF_ID_" . $refId;
            $captureCSVLocation = path('abs', 'template') . $dirAccToRefId;

            create_directory($captureCSVLocation);
            // Loop through the sides and save each SVG file
            foreach ($designFiles as $svgFileKey => $svgFile) {
                $svgFileSideIndex = "side_" . ($svgFileKey + 1) . '_';
                $svgCodeContent = urldecode($svgFile['svg']);
                $svgSaveLocation = $captureCSVLocation . '/' . $svgFileSideIndex . $dirAccToRefId . '.svg';
                write_file($svgSaveLocation, $svgCodeContent);
            }
        }
    }

    /**
     * Move Captured Images from temp folder to Capture folder
     *
     * @param $captureImages Captured Images array
     * @param $refId         Ref ID
     *
     * @author tanmayap@riaxe.com
     * @author debashrib@riaxe.com
     * @date   07 Jan 2020
     * @return boolean
     */
    public function moveCapturedImages($captureImages, $refId)
    {
        $captureImgFilename = 'REF_ID_' . $refId;
        if (isset($captureImages) && count($captureImages) > 0) {
            $captureImgDir = path('abs', 'capture') . $captureImgFilename . '/';

            // Create a Directory
            create_directory($captureImgDir);

            // Template without Product
            $allFileList = [];
            $wopFileIndex = 0;
            foreach ($captureImages['template_file_url'] as $templateFileUrl) {
                if (!empty($templateFileUrl['file_name']) && $templateFileUrl['file_name'] != "") {
                    $captureWithoutProductFile = $templateFileUrl['file_name'];
                    $captureWithoutProductFileExtn = pathinfo($captureWithoutProductFile, PATHINFO_EXTENSION);
                    // Main Image
                    $captureWithoutProductFileSource = path('abs', 'temp') . $captureWithoutProductFile;
                    $captureWithoutProductFileDestination = $captureImgDir . $wopFileIndex . '_wop_' . $captureWithoutProductFile;
                    if (file_exists($captureWithoutProductFileSource)) {
                        $allFileList['template_with_out_product_image'][$wopFileIndex] = [
                            'main' => path('read', 'capture') . $captureImgFilename . '/' . $wopFileIndex . '_wop_' . $captureWithoutProductFile,
                            'thumb' => path('read', 'capture') . $captureImgFilename . '/' . 'thumb_' . $wopFileIndex . '_wop_' . $captureWithoutProductFile,
                        ];
                        rename($captureWithoutProductFileSource, $captureWithoutProductFileDestination);
                    }
                    // Thumbs Image
                    $captureWithoutProductFileSourceThumb = path('abs', 'temp') . 'thumb_' . $captureWithoutProductFile;
                    $captureWithoutProductFileDestinationThumb = $captureImgDir . 'thumb_' . $wopFileIndex . '_wop_' . $captureWithoutProductFile;
                    if (file_exists($captureWithoutProductFileSourceThumb)) {
                        rename($captureWithoutProductFileSourceThumb, $captureWithoutProductFileDestinationThumb);
                    }
                    $wopFileIndex++;
                }
            }

            // Template with Product
            $wpFileIndex = 0;
            foreach ($captureImages['template_with_product_file'] as $templateWithProduct) {
                if (!empty($templateWithProduct['file_name']) && $templateWithProduct['file_name'] != "") {
                    $captureWithProductFile = $templateWithProduct['file_name'];
                    $captureWithProductFileExtn = pathinfo($captureWithProductFile, PATHINFO_EXTENSION);
                    // Main Image
                    $captureWithProductFileSource = path('abs', 'temp') . $captureWithProductFile;
                    $captureWithProductFileDestination = $captureImgDir . $wpFileIndex . '_wp_' . $captureWithProductFile;
                    if (file_exists($captureWithProductFileSource)) {
                        $allFileList['template_with_product_image'][$wpFileIndex] = [
                            'main' => path('read', 'capture') . $captureImgFilename . '/' . $wpFileIndex . '_wp_' . $captureWithProductFile,
                            'thumb' => path('read', 'capture') . $captureImgFilename . '/' . 'thumb_' . $wpFileIndex . '_wp_' . $captureWithProductFile,
                        ];
                        rename($captureWithProductFileSource, $captureWithProductFileDestination);
                    }
                    // Thumbs Image
                    $captureWithProductFileSourceThumb = path('abs', 'temp') . 'thumb_' . $captureWithProductFile;
                    $captureWithProductFileDestinationThumb = $captureImgDir . 'thumb_' . $wpFileIndex . '_wp_' . $captureWithProductFile;
                    if (file_exists($captureWithProductFileSourceThumb)) {
                        rename($captureWithProductFileSourceThumb, $captureWithProductFileDestinationThumb);
                    }
                    $wpFileIndex++;
                }
            }

            $processedImagesHistory = json_encode($allFileList);
            // captureImgDir
            write_file($captureImgDir . '/history.json', $processedImagesHistory);
            // Save Capture Image Data
            $captureImageData = [
                'ref_id' => $refId,
                'side_index' => 1,
                'file_name' => $captureImgFilename,
            ];
            $captureImageSave = new TemplateModel\TemplateCaptureImages($captureImageData);
            if ($captureImageSave->save()) {
                return true;
            }
        }
        return false;
    }

    /**
     * Save Captured Images
     *
     * @param $request  Slim's Request object
     * @param $response Slim's Response object
     *
     * @author tanmayap@riaxe.com
     * @author debashrib@riaxe.com
     * @date   07 Jan 2020
     * @return json
     */
    public function saveCaptureImage($request, $response)
    {
        $serverStatusCode = OPERATION_OKAY;
        $allPostPutVars = $request->getParsedBody();
        $getCapturedImages = [];
        $successCheck = 0;

        $woProdTemplatePaths = [];

        // Get And save Template with out products
        if (isset($allPostPutVars['template']) && $allPostPutVars['template'] != "") {
            $templateWithoutProduct = $allPostPutVars['template'];
            foreach ($templateWithoutProduct as $templatewoFile) {
                $templateWithoutProductFileName = save_file($templatewoFile, path('abs', 'temp'));
                $woProdTemplatePaths[] = $templateWithoutProductFileName;
            }

            if (isset($woProdTemplatePaths) && count($woProdTemplatePaths) > 0) {
                foreach ($woProdTemplatePaths as $withoutProductPathKey => $withoutProductPath) {
                    $getCapturedImages['template_file_url'][$withoutProductPathKey] = [
                        'file_name' => $withoutProductPath,
                        'url' => path('read', 'temp') . $withoutProductPath,
                        'thumbnail' => path('read', 'temp') . 'thumb_' .
                        $withoutProductPath,
                    ];
                }
                $successCheck++;
            }
        }
        if (isset($successCheck) && $successCheck > 0) {
            $jsonResponse = [
                'status' => 1,
                'data' => $getCapturedImages,
            ];
        } else {
            $jsonResponse = [
                'status' => 0,
                'message' => message('Print Profile', 'error'),
            ];
        }
        return response(
            $response, ['data' => $jsonResponse, 'status' => $serverStatusCode]
        );
    }
}
