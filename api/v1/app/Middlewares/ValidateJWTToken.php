<?php
/**
 * While request the api the api must have a Token which will be validated each
 * and every time in this method. if the token will be validated then the server
 * responses with a valid response or else, an error message will be thrown
 *
 * __invoke(): The __invoke() method gets called when the object is called as a
 * function. When you declare it, you say which arguments it should expect
 *
 * PHP version 5.6
 *
 * @category  Middleware
 * @package   Middleware
 * @author    Tanmaya Patra <tanmayap@riaxe.com>
 * @copyright 2019-2020 Riaxe Systems
 * @license   http://www.php.net/license/3_0.txt  PHP License 3.0
 * @link      http://inkxe-v10.inkxe.io/xetool/admin
 */
namespace App\Middlewares;

use App\Components\Controllers\Component as ParentController;
use \Firebase\JWT\JWT;

/**
 * Validate JWT Token Class
 *
 * @category Middleware
 * @package  Middleware
 * @author   Tanmaya Patra <tanmayap@riaxe.com>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://inkxe-v10.inkxe.io/xetool/admin
 */
class ValidateJWTToken extends ParentController
{
    /**
     * Check JWT Token
     *
     * @param $request  Slim's Request object
     * @param $response Slim's Response object
     * @param $next     IF all goes good, then the code moved to teh next step
     *
     * @author tanmayap@riaxe.com
     * @date   07 Aug 2019
     * @return Json
     */
    public function __invoke($request, $response, $next)
    {
        $doAllowJWT = get_app_settings('do_load_jwt');
        $headerResponse = [];
        $serverStatusCode = OPERATION_OKAY;
        $jsonResponse = [
            'status' => 0,
            'message' => message('Authentication Token', 'token_mismatch'),
        ];
        $headers = server_request_headers();
        // Easily turn on/off JWT from config
        if ($doAllowJWT === false) {
            return $next($request, $response);
        }
        if (isset($headers['TOKEN']) && $headers['TOKEN'] != '' && $doAllowJWT === true) {
            // Handle JWT Exception with try catch
            try
            {
                // Get the token value from Bearer string
                $tokenWithBearer = explode(" ", $headers['TOKEN']);
                $token = (isset($tokenWithBearer[1]) && $tokenWithBearer[1] != "") ? $tokenWithBearer[1] : null;

                $secret = $this->secret;
                $decoded = (array) JWT::decode(
                    $token, $secret, array(
                        'HS256',
                    )
                );
                return $next($request, $response);
            } catch (\Exception $e) {
                if (show_exception() === true) {
                    $jsonResponse += [
                        'exception' => $e->getMessage(),
                    ];
                }
                create_log('activity', 'warning', ['message' => 'JWT Token does not match', 'extra' => []]);
            }
        }
        return response(
            $response, ['data' => $jsonResponse, 'status' => $serverStatusCode]
        );
    }
}
