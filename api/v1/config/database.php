<?php
/**
 * Database Configuration for the Application
 *
 * PHP version 5.6
 *
 * @category  Database
 * @package   Configuration
 * @author    Tanmaya Patra <tanmayap@riaxe.com>
 * @copyright 2019-2020 Riaxe Systems
 * @license   http://www.php.net/license/3_0.txt  PHP License 3.0
 * @link      http://inkxe-v10.inkxe.io/xetool/admin
 */ 
return [
    'driver'    => 'mysql',
    'host'      => 'localhost',
    'database'  => 'beta_inkxe_v10',
    'username'  => 'beta_inkxev10',
    'password'  => 'dLu2epwNl8',
    'charset'   => 'utf8',
    'collation' => 'utf8_unicode_ci',
    'prefix'    => ''
];
