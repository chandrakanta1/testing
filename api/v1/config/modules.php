<?php
/**
 * This file helps to Disable or Enable Module(s)
 *
 * PHP version 5.6
 *
 * @category  Modules
 * @package   Configuration
 * @author    Tanmaya Patra <tanmayap@riaxe.com>
 * @copyright 2019-2020 Riaxe Systems
 * @license   http://www.php.net/license/3_0.txt  PHP License 3.0
 * @link      http://inkxe-v10.inkxe.io/xetool/admin
 */
return [
    'Backgrounds' => ['CUSTOM' => false],
    'Cliparts' => ['CUSTOM' => false],
    'ColorPalettes' => ['CUSTOM' => false],
    'Customers' => ['CUSTOM' => false],
    'DecorationAreas' => ['CUSTOM' => false],
    'Distresses' => ['CUSTOM' => false],
    'Fonts' => ['CUSTOM' => false],
    'Masks' => ['CUSTOM' => false],
    'Orders' => ['CUSTOM' => false],
    'PrintProfiles' => ['CUSTOM' => false],
    'Products' => ['CUSTOM' => false],
    'Settings' => ['CUSTOM' => false],
    'Shapes' => ['CUSTOM' => false],
    'Templates' => ['CUSTOM' => false],
    'WordClouds' => ['CUSTOM' => false],
    'Users' => ['CUSTOM' => false],
    'Carts' => ['CUSTOM' => false],
];
