<?php
/**
 * Email Configuration for the Application
 *
 * PHP version 5.6
 *
 * @category  Email
 * @package   Configuration
 * @author    Tanmaya Patra <tanmayap@riaxe.com>
 * @copyright 2019-2020 Riaxe Systems
 * @license   http://www.php.net/license/3_0.txt  PHP License 3.0
 * @link      http://inkxe-v10.inkxe.io/xetool/admin
 */ 
return [
    'enabled' => true,
    'host_name' => 'smtp.gmail.com',
    'smtp_auth' => true,
    'username' => 'shradha.riaxe.02@gmail.com',
    'password' => 'riaxe#1234',
    'port' => 587,
    'user_agent' => 'php_mailer',
    'do_debug' => false,
];
