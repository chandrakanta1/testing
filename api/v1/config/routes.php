<?php
/**
 * Manage Common routes
 *
 * PHP version 5.6
 *
 * @category  Common_Routes
 * @package   Configuration
 * @author    Tanmaya Patra <tanmayap@riaxe.com>
 * @copyright 2019-2020 Riaxe Systems
 * @license   http://www.php.net/license/3_0.txt  PHP License 3.0
 * @link      http://inkxe-v10.inkxe.io/xetool/admin
 */
use App\Components\Controllers\Component as ParentController;
use App\Middlewares\ValidateJWTToken as ValidateJWT;

/**
 * Common Categories Routes List
 */
$app->group(
    '/categories/{slug}', function () use ($app) {
        $app->get('', ParentController::class . ':getCategories');
        $app->get('/{id}', ParentController::class . ':getCategories');
        $app->post('', ParentController::class . ':saveCategory');
        $app->put('/{id}', ParentController::class . ':updateCategory');
        $app->get('/disable/{id}', ParentController::class . ':disableCategory');
        $app->post('/sort', ParentController::class . ':sortCategory');
        $app->post('/default', ParentController::class . ':setDefault');
    }
)->add(new ValidateJWT($container));

// Capture Image Routes
$app->group(
    '/capture-images', function () use ($app) {
        $app->post('', ParentController::class . ':saveCaptureImage');
    }
)->add(new ValidateJWT($container));
