<?php
/**
 * Global Constants for the Application
 *
 * PHP version 5.6
 *
 * @category  Constants
 * @package   Configuration
 * @author    Tanmaya Patra <tanmayap@riaxe.com>
 * @copyright 2019-2020 Riaxe Systems
 * @license   http://www.php.net/license/3_0.txt  PHP License 3.0
 * @link      http://inkxe-v10.inkxe.io/xetool/admin
 */ 
/*
|--------------------------------------------------------------------------
| Switch between : production or development
|--------------------------------------------------------------------------
|
 */
defined('ENVIRONMENT') or define('ENVIRONMENT', 'development');

/*
|--------------------------------------------------------------------------
| Need to change after Installation process
|--------------------------------------------------------------------------
|
 */
defined('BASE_DIR') or define('BASE_DIR', 'xetool');
defined('WORKING_DIR') or define('WORKING_DIR', '/' . BASE_DIR . '/api/v1/');

/*
|--------------------------------------------------------------------------
| Base Site URL. No need to change
|--------------------------------------------------------------------------
 */
$domainUrl = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://" . $_SERVER['HTTP_HOST'];
defined('RELATIVE_PATH') or define('RELATIVE_PATH', $_SERVER['DOCUMENT_ROOT'] . WORKING_DIR);
defined('BASE_URL') or define('BASE_URL', $domainUrl . WORKING_DIR); // http://localhost/api/v2/

/*
|--------------------------------------------------------------------------
| Assets Read and Write Path Configuration
|--------------------------------------------------------------------------
|
| You can set the Assets path before using the applciation
|
| IMPORTANT: Assets folder contains all the module's corresponding fodlers and sub-folders.
|            Make sure that all the module's folders are setup correctly
 */
defined('ASSETS_PATH_W') or define('ASSETS_PATH_W', $_SERVER['DOCUMENT_ROOT'] . '/dev' . '/' . BASE_DIR . '/' . 'assets/');
defined('ASSETS_PATH_R') or define('ASSETS_PATH_R', $domainUrl . '/dev' . '/' . BASE_DIR . '/' . 'assets/');

// Modules Folders
defined('EXTRACTED_FOLDER') or define('EXTRACTED_FOLDER', 'extracted/');
defined('VECTOR_FOLDER') or define('VECTOR_FOLDER', 'vectors/'); // clipart folder
defined('PRODUCT_FOLDER') or define('PRODUCT_FOLDER', 'products/');
defined('SWATCH_FOLDER') or define('SWATCH_FOLDER', 'swatches/');
defined('PRINT_PROFILE_FOLDER') or define('PRINT_PROFILE_FOLDER', 'print_profile/');
defined('PRINT_AREA_FOLDER') or define('PRINT_AREA_FOLDER', 'print_areas/');
defined('PRINT_AREA_TYPE_FOLDER') or define('PRINT_AREA_TYPE_FOLDER', 'print_area_types/');
defined('FONT_FOLDER') or define('FONT_FOLDER', 'fonts/');

defined('SHAPE_FOLDER') or define('SHAPE_FOLDER', 'shapes/');
defined('DISTRESS_FOLDER') or define('DISTRESS_FOLDER', 'distresses/');
defined('COLOR_FOLDER') or define('COLOR_FOLDER', 'colors/');
defined('BACKGROUND_FOLDER') or define('BACKGROUND_FOLDER', 'patterns/');
defined('MASK_FOLDER') or define('MASK_FOLDER', 'masks/');
defined('GRAPHICFONT_FOLDER') or define('GRAPHICFONT_FOLDER', 'graphics/');
defined('WORDCLOUD_FOLDER') or define('WORDCLOUD_FOLDER', 'wordclouds/');
defined('ORDER_FOLDER') or define('ORDER_FOLDER', 'orders/');
defined('LANGUAGE_FOLDER') or define('LANGUAGE_FOLDER', 'languages/');
defined('COLORPALETTE_FOLDER') or define('COLORPALETTE_FOLDER', 'color_palettes/');
defined('PRODUCT_FOLDER') or define('PRODUCT_FOLDER', 'products/');
defined('SETTING_FOLDER') or define('SETTING_FOLDER', 'settings/');
defined('3D_OBJECT_FOLDER') or define('3D_OBJECT_FOLDER', '3d_objects/');
// For template module
defined('TEMP_FOLDER') or define('TEMP_FOLDER', 'temp/');
defined('TEMPLATE_FOLDER') or define('TEMPLATE_FOLDER', 'template/');
defined('CAPTURE_FOLDER') or define('CAPTURE_FOLDER', 'template/captures/');
defined('PREDECORATOR_FOLDER') or define('PREDECORATOR_FOLDER', 'template/predecorators/');
// For template module
defined('TEMP_FOLDER') or define('TEMP_FOLDER', 'temp/');
defined('TEMPLATE_FOLDER') or define('TEMPLATE_FOLDER', 'template/');
defined('CAPTURE_FOLDER') or define('CAPTURE_FOLDER', 'template/captures/');
defined('ORDER_LOG_FOLDER') or define('ORDER_LOG_FOLDER', 'orders_log/');

/*
|--------------------------------------------------------------------------
| Store's Installation Global Constants
|--------------------------------------------------------------------------
|
| IMPORTANT: Make sure that your Store's API keys are setup correctly incl. the URL
 */
defined('STORE_NAME') or define('STORE_NAME', 'Woocommerce');
defined('STORE_VERSION') or define('STORE_VERSION', 'v3x');

defined('WC_API_URL') or define('WC_API_URL', 'http://inkxe-v10.inkxe.io/');
defined('WC_API_CK') or define('WC_API_CK', 'ck_87f13f02c90e8cc08bd23445774af78c380db0de');
defined('WC_API_CS') or define('WC_API_CS', 'cs_c3b60ee020778207f396116411de9acd1bbfbda7');

defined('WC_API_VER') or define('WC_API_VER', 'wc/v3');
defined('WC_API_SECURE') or define('WC_API_SECURE', false);

/*
|--------------------------------------------------------------------------
| Directory Separation for different OS
|--------------------------------------------------------------------------
 */
if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {
    // For Windows System configs
    defined('SEPARATOR') or define('SEPARATOR', '\\');
} else {
    // For Linux System configs
    defined('SEPARATOR') or define('SEPARATOR', '/');
}

/*
|--------------------------------------------------------------------------
| HTTP ERROR CODE Mapping Constants
|--------------------------------------------------------------------------
|
| IMPORTANT: You should use correct HTTP codes to the constants or else it will
|            show irrelevant output codes at frontend
 */
defined('AUTH_ERROR') or define('AUTH_ERROR', 401);
defined('OPERATION_OKAY') or define('OPERATION_OKAY', 200);
defined('NO_DATA_FOUND') or define('NO_DATA_FOUND', 200);
defined('MISSING_PARAMETER') or define('MISSING_PARAMETER', 400);
defined('EXCEPTION_OCCURED') or define('EXCEPTION_OCCURED', 500);
defined('DATA_NOT_PROCESSED') or define('DATA_NOT_PROCESSED', 500);
defined('INVALID_FORMAT_REQUESTED') or define('INVALID_FORMAT_REQUESTED', 406);
