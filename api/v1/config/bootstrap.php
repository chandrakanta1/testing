<?php
/**
 * Autoload File
 *
 * PHP version 5.6
 *
 * @category  Bootstrap
 * @package   Configuration
 * @author    Tanmaya Patra <tanmayap@riaxe.com>
 * @copyright 2019-2020 Riaxe Systems
 * @license   http://www.php.net/license/3_0.txt  PHP License 3.0
 * @link      http://inkxe-v10.inkxe.io/xetool/admin
 */

ini_set('display_errors', 0);
error_reporting(E_ALL & ~E_NOTICE & ~E_DEPRECATED & ~E_STRICT & ~E_USER_NOTICE & ~E_USER_DEPRECATED);

require __DIR__ . '/constants.php';

/*
 *---------------------------------------------------------------
 * ERROR REPORTING
 *---------------------------------------------------------------
 *
 * Different environments will require different levels of error reporting.
 * By default development will show errors but testing and live will hide them.
 */
switch (ENVIRONMENT) {
case 'development':
    error_reporting(-1);
    ini_set('display_errors', 1);
    break;

case 'testing':
case 'production':
    // Surpress all error and warnings
    ini_set('display_errors', 0);
    error_reporting(E_ALL & ~E_NOTICE & ~E_DEPRECATED & ~E_STRICT & ~E_USER_NOTICE & ~E_USER_DEPRECATED);
    break;
default:
    header('HTTP/1.1 503 Service Unavailable.', true, 503);
    echo 'The application environment is not set correctly.';
    exit(1); // EXIT_ERROR
}

// Application blocks if the server has PHP version lower than 7.1
if (!version_compare(PHP_VERSION, '5.6', '>=')) {
    header('HTTP/1.1 503 Service Unavailable.', true, 503);
    echo 'Warning: Application requires PHP version greater than 7.0. PHP version 7.2.x will be suits best';
    exit(1); // EXIT_ERROR
}

// Autoload Composer
$vendor = include RELATIVE_PATH . '/vendor/autoload.php';

// Instantiate the app
$settings = include RELATIVE_PATH . '/config/settings.php';
$app = new \Slim\App($settings);
$container = $app->getContainer();

// Autoload components of applciation
require RELATIVE_PATH . '/config/autoload.php';

// Autoload Common Routes
require RELATIVE_PATH . '/config/routes.php';

// Set default timezone
// ini_set('date.timezone', 'Asia/Kolkata');

// Configure Eloquent Capsule and run the applciation
$dbSettings = $container->get('settings')['db'];
$capsule = new Illuminate\Database\Capsule\Manager;
$capsule->addConnection($dbSettings);
$capsule->bootEloquent();
$capsule->setAsGlobal();
