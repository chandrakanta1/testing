<?php
/**
 * Autoload File
 *
 * PHP version 5.6
 *
 * @category  Autoload
 * @package   Configuration
 * @author    Tanmaya Patra <tanmayap@riaxe.com>
 * @copyright 2019-2020 Riaxe Systems
 * @license   http://www.php.net/license/3_0.txt  PHP License 3.0
 * @link      http://inkxe-v10.inkxe.io/xetool/admin
 */
require RELATIVE_PATH . '/app/Helpers/helper.php';

/**
 * Registering all other child routes
 */
$modules = include RELATIVE_PATH . '/config/modules.php';
$customLoaderDir = $container->get('settings')['custom_loader_directory'];
foreach ($modules as $module => $status) {
    $routeFilePath = "";
    if (isset($status) && count($status) > 0) {
        // Include core files
        $routeFilePath = RELATIVE_PATH . '/app/Modules/' . $module . '/index.php';
        if (isset($status) && $status['CUSTOM'] === true) {
            // Include custom files
            $routeFilePath = RELATIVE_PATH . '/app/' . $customLoaderDir . '/' . $module . '/index.php';
        }
        if (!empty($routeFilePath) && file_exists($routeFilePath)) {
            include $routeFilePath;
        }
    }
}
//End of registration of routes

// Autoload StoreSpaces
$vendor->setPsr4("CommonStoreSpace\\", "app/Components/Stores/" . STORE_NAME . "/" . STORE_VERSION . "/");
