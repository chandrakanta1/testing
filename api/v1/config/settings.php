<?php
/**
 * Global Configuration for the Application
 *
 * PHP version 5.6
 *
 * @category  GLobal_Configurations
 * @package   Configuration
 * @author    Tanmaya Patra <tanmayap@riaxe.com>
 * @copyright 2019-2020 Riaxe Systems
 * @license   http://www.php.net/license/3_0.txt  PHP License 3.0
 * @link      http://inkxe-v10.inkxe.io/xetool/admin
 */

$databaseSettings = include RELATIVE_PATH . 'config/database.php';

return [
    'settings' => [
        // set to false in production
        'displayErrorDetails' => true,
        // Allow the web server to send the content-length header
        'addContentLengthHeader' => false, 
        'db' => $databaseSettings,
        'pagination' => [
            // Default value for per page item showing
            'per_page' => 40,
        ],
        // Enable or Disable JWT Authentication
        'do_load_jwt' => false,
        'show_exception' => true,
        'custom_loader_directory' => 'Custom',
    ],
];
